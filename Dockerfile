FROM java:8
VOLUME /tmp
EXPOSE 8080
ADD target/thecase-0.0.1-SNAPSHOT.jar app.jar
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java", "-Dspring.data.mongodb.uri=mongodb://mongodb/january", "-Djava.security.egd=file:/dev/./urandom", "-Dserver.ssl.key-store=certpath/keystore.p12", "-Dserver.ssl.key-store-password=password", "-Dserver.ssl.keyStoreType=PKCS12", "-Dserver.ssl.keyAlias=tomcat", "-jar","/app.jar"]