package ua.softgroup.thecase.serviceImpl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.repository.*;
import ua.softgroup.thecase.service.EmailService;
import ua.softgroup.thecase.service.ItemService;

import java.util.*;

/**
 * Created by java-1-03 on 08.08.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ItemServiceImplTest {

    @Autowired
    private CaseRepository caseRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private Model3DRepository model3DRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private MongoTemplate mongoTemplate;


    @Autowired
    private ItemService itemService;

    @Test
    public void getModels3D() throws Exception {

        Assert.assertNotEquals(productRepository.count(), 0);
        Assert.assertNotEquals(model3DRepository.count(), 0);

        Assert.assertNotEquals(itemService.getItem(Product.class).size(), 0);
        Assert.assertNotEquals(itemService.getItem(Model3D.class).size(), 0);

        System.out.println("model count = " + itemService.getItem(Model3D.class).size());
        System.out.println("product count = " + itemService.getItem(Product.class).size());

    }

    @Test
    public void removeTag() throws Exception {

       /* Item item = itemService.findItemById("5993fcad7feff0c344092bc8");
        User user = userRepository.findOne("5993f7d27feff06a881427e2");*/


       Product product = productRepository.findOne("59ba27b6bbaf96356c73aacb");

//        product.setBrand(brandRepository.findOne("5993fca67feff0c3440921fd"));

        productRepository.save(product);



        /*ItemHolder itemHolder = new ItemHolder(item, user);
        System.out.println("+++=== == = " + itemHolder);*/


       /* Case aCase = caseRepository.findOne("599af412bbaf961ff4c26372");


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfGenerator.pdfFromCase(aCase, outputStream);
        byte[] bytes = outputStream.toByteArray();



        emailService.sendCaseToEmail("s.o.lavrynenko@gmail.com", bytes);*/


    }

    List<String> tagList = Arrays.asList("Ceramic", "Fabric", "Felt", "Forging", "Glass", "Leather", "MDF",
            "Metal", "Mirror", "Plastic", "Plywood", "Rottang", "Silver", "Stone", "Wooden", "Wool","Bedroom", "Dining", "Kids", "Kitchen", "Living", "Office",
            "Outdoor", "HoReCa");

    @Test
    public void test3() {
        System.out.println("------------- START CLEAN TAGS IN PRODUCTS -------------------");
        removeKvaziDuplicate(productRepository);
        System.out.println("------------- START CLEAN TAGS IN MODEL3Ds -------------------");
        removeKvaziDuplicate(model3DRepository);
        System.out.println("------------- ----------- DONE ----------- -------------------");
    }


    private <T extends Item> void removeKvaziDuplicate(MongoRepository<T, String> repository) {
        final int pageLimit = 1000;
        int pageNumber = 0;
        Page<T> page = repository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {
            System.out.println("TagShorter: executing " + ((page.getNumber() * 100) / page.getTotalPages()) + "%");
            removeKvaziDuplicate(page.getContent());
            page = repository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        removeKvaziDuplicate(page.getContent());
    }

    private void removeKvaziDuplicate(List<? extends Item> items) {
        items.forEach(item -> {
            Set<String> list = item.getTagsList();
            if (list == null) list = new HashSet<>();

            for (String tag: tagList) {

                if(list.contains(tag)){

                    List<String> toRemove = new ArrayList<>();

                    for (String listTag: list) {
                        if(listTag.contains(tag)){
                            toRemove.add(listTag);
                        }
                    }
                    toRemove.remove(tag);
                    list.removeAll(toRemove);
                }

            }

            System.out.println("Size shorted to " + ((list!=null)?list.size():"") + " / " + ((item.getTagsList()!=null)?item.getTagsList().size():"") + " | in item: " + item.getName());

            item.setTagsList(list);
            itemService.save(item);
        });
    }

}