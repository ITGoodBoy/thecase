package ua.softgroup.thecase.serviceImpl;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.TagRepository;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

/**
 * Created by java-1-07 on 17.08.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
public class GroupServiceImplTest {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;



    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();


    }

//    @Test
//    public void addGroupTest() throws Exception {
//
//        mockMvc.perform(post("/api/admin/addGroup").param("groupName", "Eggs").param("groupIcon", "pregnant_woman"))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//        Assert.assertEquals(groupRepository.findByName("Eggs").getName(), "Eggs");
//
//        groupRepository.delete(groupRepository.findByName("Eggs"));
//    }

//    @Test
//    public void addTagTest() throws Exception {
//
//        Tag tag = new Tag("Oil", 13);
//
//        mockMvc.perform(put("/api/admin/addTag")
//                .param("groupName", "Material").contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(tag)))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//        Group group = groupRepository.findByName("Material");
//        group.getCurrentCombination().clear();
//        groupRepository.save(group);
//    }
//
//    @Test
//    public void addTagsTest() throws Exception {
//
//        Tag tag = new Tag("Oil", 13);
//        Tag tag1 = new Tag("Nafta", 11);
//
//        Set<Tag> set = new HashSet<>();
//        set.add(tag);
//        set.add(tag1);
//
//        mockMvc.perform(put("/api/admin/addTags")
//                .param("groupName", "Material").contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(set)))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//        Group group = groupRepository.findByName("Material");
//        group.getCurrentCombination().clear();
//        groupRepository.save(group);
//    }
//
//    @Test
//    public void removeGroupTest() throws Exception {
//
//        mockMvc.perform(delete("/api/admin/removeGroup").param("groupName", "Material"))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//        groupRepository.save(new Group("Material", new HashSet<>()));
//    }
//
//    @Test
//    public void removeTagTest() throws Exception {
//
//        Group group = groupRepository.findByName("Material");
//        Set<Tag> currentCombination = new HashSet<>();
//        Tag tag = new Tag("Hello", 1);
//        currentCombination.add(tag);
//        group.setCurrentCombination(currentCombination);
//        tagRepository.save(currentCombination);
//        groupRepository.save(group);
//
//
//        mockMvc.perform(put("/api/admin/removeTag")
//                .param("groupName", "Material")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(new Gson().toJson(tag)))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//    }
//
//    @Test
//    public void removeTagsTest() throws Exception {
//
//        Group group = groupRepository.findByName("Material");
//        Set<Tag> currentCombination = new HashSet<>();
//        currentCombination.add(new Tag("Hello", 1));
//        currentCombination.add(new Tag("GoodBye", 4));
//        group.setCurrentCombination(currentCombination);
//        tagRepository.save(currentCombination);
//        groupRepository.save(group);
//
//
//        mockMvc.perform(put("/api/admin/removeTags")
//                .param("groupName", "Material")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(new Gson().toJson(currentCombination)))
//                .andExpect(status().isOk())
//                .andDo(print());
//    }
//
//    @Test
//    public void renameGroupTest() throws Exception {
//
//        mockMvc.perform(put("/api/admin/renameGroup").param("groupOldName", "Material").param("groupNewName", "Eggs"))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//        mockMvc.perform(put("/api/admin/renameGroup").param("groupOldName", "Eggs").param("groupNewName", "Material"))
//                .andExpect(status().isOk())
//                .andDo(print());
//    }
//
//    @Test
//    public void renameTagTest() throws Exception {
//
//        Tag tag = new Tag("eggs", 3);
//        tag = tagRepository.save(tag);
//        Group group = groupRepository.findByName("Material");
//        Set<Tag> currentCombination = new HashSet<>();
//        currentCombination.add(tag);
//        group.setCurrentCombination(currentCombination);
//        groupRepository.save(group);
//
//        mockMvc.perform(put("/api/admin/renameTag")
//                .param("groupName", "Material")
//                .param("oldTagName", tag.getName())
//                .contentType(MediaType.APPLICATION_JSON_VALUE)
//                .content(new Gson().toJson(new Tag("This is new tag", 23))))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//    }






}
