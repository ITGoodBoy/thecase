package ua.softgroup.thecase.model;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.ProductRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by java-1-03 on 19.07.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
@SuppressWarnings("Duplicates")
public class CalcFrequency {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private Model3DRepository model3dRepository;

    @Autowired
    private TagRepository tagRepository;

    private ExecutorService executorService;

    private final int pageLimit = 1000;

    @Test
    public void checkItemsType() throws Exception {

        executorService = Executors.newFixedThreadPool(4);

        System.out.println("TAG REPO #" + tagRepository.count());
        System.out.println("TAG REPO #" + tagRepository.count());

        int pageNumber = 0;
        Page<Tag> page = tagRepository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {

            Page<Tag> finalPage = page;
            executorService.execute(() -> calcFrequency(finalPage));

            page = tagRepository.findAll(new PageRequest(++pageNumber, pageLimit));
        }

        Page<Tag> finalPage = page;
        executorService.execute(() -> calcFrequency(finalPage));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);

        System.out.println("DONE");
    }

    private void calcFrequency(Page<Tag> page) {
        System.out.println(tagRepository.getClass().getSimpleName() + " checkItemsType executing " + ((float)(page.getNumber() * 100) / page.getTotalPages()) + "%");
        List<Tag> tagList = page.getContent();
        tagList.forEach(tag -> {
            int freq = productRepository.countByTagsListIn(tag.getName()) + model3dRepository.countByTagsListIn(tag.getName());
//            System.out.println("TAG: " +tag.getName()+ " | frequency: " + freq);
            tag.setFrequency(freq);
            tagRepository.save(tag);
            System.out.println("Tag #" + ((page.getNumber()*pageLimit)+tagList.indexOf(tag)));

        });
    }
}