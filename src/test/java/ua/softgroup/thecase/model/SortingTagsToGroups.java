package ua.softgroup.thecase.model;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.ProductRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by java-1-03 on 19.07.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class SortingTagsToGroups {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private Model3DRepository model3dRepository;

    private ExecutorService executorService;

    @Test
    public void createSortedTagGroups() throws Exception{


        List<Tag> tagList = tagRepository.findAll();

        for (Tag tag: tagList) {
            tag.setGroup(null);
        }

        tagRepository.save(tagList);

        executorService = Executors.newFixedThreadPool(16);

//        final List<String> TAGS = Arrays.asList("Bedroom", "Dining", "Kids", "Kitchen", "Living", "Office", "Outdoor", "HoReCa");
        Group group = groupRepository.findByName("Material");
        createSortedTagGroups(group, Arrays.asList("Ceramic", "Fabric", "Felt", "Forging", "Glass", "Leather", "MDF",
                "Metal", "Mirror", "Plastic", "Plywood", "Rottang", "Silver", "Stone", "Wooden", "Wool"));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);

        executorService = Executors.newFixedThreadPool(16);

        Group group2 = groupRepository.findByName("Room");
        createSortedTagGroups(group2, Arrays.asList("Bedroom", "Dining", "Kids", "Kitchen", "Living", "Office",
                "Outdoor", "HoReCa"));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);
    }

    private void createSortedTagGroups(Group group, List<String> TAGS) throws Exception{
        final int pageLimit = 1000;
        int pageNumber = 0;
        Page<Tag> page = tagRepository.findAll(new PageRequest(pageNumber, pageLimit));

        while (page.hasNext()) {
            System.out.println("Tag Packing proceed GROUP "+ group.getName()+ " | "+((page.getNumber()*100)/page.getTotalPages()) + "%");

            Page<Tag> finalPage1 = page;
            executorService.execute(() ->{
                Page<Tag> finalPage = finalPage1;
                System.out.println("Tag Packing proceed GROUP "+ group.getName()+ " | "+((finalPage.getNumber()*100)/finalPage.getTotalPages()) + "%");
                proceed(TAGS, finalPage.getContent(), group);
            });


            page = tagRepository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        System.out.println("Last page "+ page.isLast() + " | size = " + page.getContent().size());
        proceed(TAGS, page.getContent(), group);

        System.out.println("Tag packing done");

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);
    }

    private void proceed(List<String> tagList, List<Tag> content, Group group) {

        content.forEach(contentTag -> {
//            contentTag.setGroup(null);
//            tagRepository.save(contentTag);
            tagList.forEach(tag ->{
                if(contentTag.getName().toLowerCase().trim().contains(tag.toLowerCase().trim())){

                    Tag existTag = tagRepository.findByName(tag);

                    if(existTag==null){
                        existTag = new Tag();
                        existTag.setName(tag);
                    }
                    existTag.setGroup(group);
                    tagRepository.save(existTag);

                    System.out.println("existing tag = " + existTag);

                    addPermanentTag(contentTag.getName(), existTag);

                    System.out.println("------------------------ TAG FOUND: " + tag);
                }
            });
        });
    }

    private void addPermanentTag(String tag, Tag permTag) {

        List<Product> products = productRepository.findByTagsListIn(tag);

        products.forEach(product -> {
            Set<String> list = product.getTagsList();
            list.add(permTag.getName());
            product.setTagsList(product.getTagsList());
        });

        productRepository.save(products);
        System.out.println("p");

        List<Model3D> model3Ds = model3dRepository.findByTagsListIn(tag);

        model3Ds.forEach(model3D -> {
            Set<String> list = model3D.getTagsList();
            list.add(permTag.getName());
            model3D.setTagsList(model3D.getTagsList());
        });

        model3dRepository.save(model3Ds);
        System.out.println("m");

    }


}