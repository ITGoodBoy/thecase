package ua.softgroup.thecase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.ProductRepository;
import ua.softgroup.thecase.service.ItemService;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

/**
 * Created by java-1-03 on 19.07.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class CleanDbFromDuplicateTags {

    @Autowired
    private ItemService itemService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private Model3DRepository model3DRepository;

     ExecutorService executorService;


    //CLEAN TAGS DUPLICATES
    @Test
    public void test1() {
        System.out.println("------------- START CLEAN TAGS IN PRODUCTS -------------------");
        iter(productRepository);
        System.out.println("------------- START CLEAN TAGS IN MODEL3Ds -------------------");
        iter(model3DRepository);
        System.out.println("------------- ----------- DONE ----------- -------------------");
    }

    @Before
    public void setUp() throws Exception {
        executorService = Executors.newCachedThreadPool();
    }

    @Data
    @AllArgsConstructor
    class TypeKeyWords {
        private String type;
        private List<String> keys;
    }

    //SET UP TYPE & CATEGORY TO ITEMS
    @Test
    public void test2() throws Exception {

//        categoryRepository.deleteAll();

        List<TypeKeyWords> typeKeyWords = new ArrayList<>();
        typeKeyWords.add(new TypeKeyWords("SEATING", Arrays.asList("Chair", "Armchair", "Pouf", "Bar chair", "Bench")));
        typeKeyWords.add(new TypeKeyWords("SOFAS", Arrays.asList("Sofa", "Couch", "Ottoman")));
        typeKeyWords.add(new TypeKeyWords("TABLES", Arrays.asList("Coffee table", "Work-table", "Dining table", "Dressing table", "Table")));
        typeKeyWords.add(new TypeKeyWords("STORAGE", Arrays.asList("Commode", "Wardrobe", "Showcase", "TV stand", "Console", "Stand", "Shelf", "Bar", "Sideboard", "Storage")));
        typeKeyWords.add(new TypeKeyWords("SEATING", Arrays.asList("Bed")));

        sortingAndPublishItemByTypeAndKeywords(typeKeyWords);

        executorService.shutdown();
        executorService.awaitTermination(5, TimeUnit.DAYS);

    }

    private void sortingAndPublishItemByTypeAndKeywords(List<TypeKeyWords> typeKeyWords) {
        executorService.execute(() -> {
            System.out.println("-- product --");
            generalizatorForSortingAndPublishing(typeKeyWords, productRepository);
        });
        executorService.execute(() -> {
            System.out.println("-- model3d --");
            generalizatorForSortingAndPublishing(typeKeyWords, model3DRepository);
        });
    }

    private <T extends Item> void generalizatorForSortingAndPublishing(List<TypeKeyWords> typeKeyWords,
                                                                       MongoRepository<T, String> repository) {
        final int pageLimit = 1000;
        int pageNumber = 0;
        Page<T> page = repository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {
            System.out.println(repository.getClass().getSimpleName() + " Category packer: executing " + ((page.getNumber() * 100) / page.getTotalPages()) + "%");

            proceedItems(typeKeyWords, repository, page);
            page = repository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        proceedItems(typeKeyWords, repository, page);
        getSize();
    }

    private <T extends Item> void proceedItems(List<TypeKeyWords> typeKeyWords, MongoRepository<T, String> repository, Page<T> page) {
        AtomicInteger i = new AtomicInteger(0);
        List<T> items = page.getContent();
        items.forEach(item -> typeKeyWords.forEach(tk -> {
            item.setPublished(false);
            String key = isInCategory(item, tk.getKeys());
            if (!key.isEmpty()) {
                //System.out.println("KEY FOUND: "+key + " in " +item.getName() + " " + item.getTagsList().toString());
                //Add category
                item.setCategory(key);
                //ADD TYPE
                item.setType(tk.getType());
                //ADD publish
                item.setPublished(true);
                repository.save(item);
                i.getAndIncrement();
            }
        }));
        System.out.println("ADDED " + i.get());

    }

    private String isInCategory(Item item, List<String> keywords) {
        if (item.getTagsList() != null)
            item.getTagsList().removeAll(
                    item.getTagsList().stream()
                            .filter(tag -> containsIgnoreCase(tag, "and") || containsIgnoreCase(tag, "&"))
                            .collect(Collectors.toList())
            );
        for (String key : keywords) {
            if (containsIgnoreCase(item.getName(), key)) return key;
            if (item.getCategory() != null && item.getCategory() != null
                    && containsIgnoreCase(item.getCategory(), key))
                return key;
            if (item.getTagsList() != null && item.getTagsList().stream().anyMatch(tag -> containsIgnoreCase(tag, key)))
                return key;
        }
        return "";
    }

    @Test
    public void getSize() {
//        long start = System.currentTimeMillis();
        System.out.println("Published Product size: " + productRepository.findByIsPublishedTrueAndIsPublishedBySourceTrue().size());
        System.out.println("Published Model3D size: " + model3DRepository.findByIsPublishedTrueAndIsPublishedBySourceTrue().size());
//        System.out.println(System.currentTimeMillis()-start);
    }

    private <T extends Item> void iter(MongoRepository<T, String> repository) {
        final int pageLimit = 1000;
        int pageNumber = 0;
        Page<T> page = repository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {
            System.out.println("TagShorter: executing " + ((page.getNumber() * 100) / page.getTotalPages()) + "%");
            clearTag(page.getContent());
            page = repository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        clearTag(page.getContent());
    }

    private void clearTag(List<? extends Item> items) {
        items.forEach(item -> {
            Set<String> list = item.getTagsList();
            if (list == null) list = new HashSet<>();
            Set<String> tagSet = new HashSet<>();
            tagSet.addAll(list);
            if (list.size() != tagSet.size())
                System.out.println("Size shorted to " + tagSet.size() + " / " + list.size() + " | in item: " + item.getName());
            item.setTagsList(new HashSet<>(tagSet));
            itemService.save(item);
        });
    }

    List<String> tagList = Arrays.asList("Ceramic", "Fabric", "Felt", "Forging", "Glass", "Leather", "MDF",
            "Metal", "Mirror", "Plastic", "Plywood", "Rottang", "Silver", "Stone", "Wooden", "Wool","Bedroom", "Dining", "Kids", "Kitchen", "Living", "Office",
            "Outdoor", "HoReCa");


    @Test
    public void test3() {
        System.out.println("------------- START CLEAN TAGS IN PRODUCTS -------------------");
        removeKvaziDuplicate(productRepository);
        System.out.println("------------- START CLEAN TAGS IN MODEL3Ds -------------------");
        removeKvaziDuplicate(model3DRepository);
        System.out.println("------------- ----------- DONE ----------- -------------------");
    }


    private <T extends Item> void removeKvaziDuplicate(MongoRepository<T, String> repository) {
        final int pageLimit = 1000;
        int pageNumber = 0;
        Page<T> page = repository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {
            System.out.println("TagShorter: executing " + ((page.getNumber() * 100) / page.getTotalPages()) + "%");
            removeKvaziDuplicate(page.getContent());
            page = repository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        removeKvaziDuplicate(page.getContent());
    }

    private void removeKvaziDuplicate(List<? extends Item> items) {
        items.forEach(item -> {
            Set<String> list = item.getTagsList();
            if (list == null) list = new HashSet<>();

            for (String tag: tagList) {

                if(list.contains(tag)){

                    List<String> toRemove = new ArrayList<>();

                    for (String listTag: list) {
                        if(listTag.contains(tag)){
                            toRemove.add(listTag);
                        }
                    }
                    toRemove.remove(tag);
                    list.removeAll(toRemove);
                }

            }

            System.out.println("Size shorted to " + list.size() + " / " + item.getTagsList().size() + " | in item: " + item.getName());

            item.setTagsList(list);
            itemService.save(item);
        });
    }
}