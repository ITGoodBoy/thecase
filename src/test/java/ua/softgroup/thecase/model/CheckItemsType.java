package ua.softgroup.thecase.model;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.ProductRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by java-1-03 on 19.07.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class CheckItemsType {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private Model3DRepository model3dRepository;

    private ExecutorService executorService;

    @Test
    public void checkItemsType() throws Exception{
        ExecutorService executorService = Executors.newCachedThreadPool();

        Set<String> typeSet = new HashSet<>();//ConcurrentHashMap.newKeySet();

        executorService.execute(() -> proceedItems(typeSet, productRepository));
        executorService.execute(() -> proceedItems(typeSet, model3dRepository));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);

        typeSet.forEach(System.out::println);
    }

    private <T extends Item> void proceedItems(Set<String> typeSet, MongoRepository<T, String> repository) {
        final int pageLimit = 1000;
        int pageNumber = 0;
        Page<T> page = repository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {
            System.out.println(repository.getClass().getSimpleName() + " checkItemsType executing " + ((page.getNumber() * 100) / page.getTotalPages()) + "%");

            page.getContent().forEach(item -> typeSet.add(item.getType()));

            page = repository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        page.getContent().forEach(item -> typeSet.add(item.getType()));
    }


}