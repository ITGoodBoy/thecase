package ua.softgroup.thecase.model;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.repository.*;
import ua.softgroup.thecase.service.ItemService;
import ua.softgroup.thecase.service.TagService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class TestTags {


    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private Model3DRepository model3DRepository;

    @Autowired
    private ItemService itemService;

    @Autowired
    private TagService tagService;

    @Autowired
    private TagRepository tagRepository;

    @Test
    public void createSortedTagGroups() throws Exception{
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(() -> checkAll(productRepository, Product.class));
        executorService.execute(() -> checkAll(model3DRepository, Model3D.class));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);

    }

    private <T extends Item, E extends ItemRepository<T>> void checkAll(E repository, Class<T> clazz) {
        List<Item> items = repository.findByIsPublishedTrueAndIsPublishedBySourceTrue().stream()
                .filter(item -> item.getTagsList().containsAll(tagRepository.findByGroup(groupRepository.findByName("Material"))))
                .collect(Collectors.toList());
        System.out.println(clazz.getSimpleName()+" size: " + items.size());
    }


}