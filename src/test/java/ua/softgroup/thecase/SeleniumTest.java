package ua.softgroup.thecase;

import io.github.bonigarcia.wdm.PhantomJsDriverManager;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.nio.file.Paths;

/**
 * Created by java-1-03 on 19.07.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class SeleniumTest {

//    @Autowired
//    private ProductRepository productRepository;
//    @Autowired
//    private Model3DRepository model3dRepository;
//
//    @Autowired
//    private TagRepository tagRepository;
//
//    private ExecutorService executorService;
//
//    private final int pageLimit = 1000;

    @Test
    public void checkItemsType() throws Exception {
        PhantomJsDriverManager.getInstance().setup();

        PhantomJSDriverService service = new PhantomJSDriverService.Builder()
                .usingAnyFreePort()
                .usingPhantomJSExecutable(
                        Paths.get(System.getProperty("phantomjs.binary.path"))
                                .toFile())
                .usingCommandLineArguments(new String[] {
                        "--ignore-ssl-errors=true", "--ssl-protocol=tlsv1",
                        "--web-security=false", "--webdriver-loglevel=INFO" })
                .build();
        DesiredCapabilities desireCaps = new DesiredCapabilities();
        WebDriver driver = new PhantomJSDriver(service, desireCaps);

        driver.get("https://ukr.net");

        System.out.println(driver.getPageSource());

        if (driver != null) {
            driver.quit();
        }

    }


}