package ua.softgroup.thecase.tests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.repository.UserRepository;

/**
 * Created by java-1-03 on 28.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
public class Test {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MongoTemplate mongoTemplate;



    @Before
    public void setUp() throws Exception {


    }

    @org.junit.Test
    public void test() throws Exception {
    }

}
