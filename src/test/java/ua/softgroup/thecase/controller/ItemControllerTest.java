package ua.softgroup.thecase.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.model.Filter;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.service.ItemService;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by java-1-03 on 28.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
public class ItemControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private ItemService itemService;

    private User user;
    private String token;

    private MockMvc mockMvc;


    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();

        setUpInfo();

    }

    @Test
    public void getProducts() throws Exception {

        mockMvc.perform(
                get("/api/items/product"))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                get("/api/items/model3d"))
                .andExpect(status().isOk())
                .andDo(print());

//        List<Item> items = itemService.findAll();
//        itemService.deleteAll();

        mockMvc.perform(
                get("/api/items/product"))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(
                get("/api/items/model3d"))
                .andExpect(status().isNoContent())
                .andDo(print());

//        itemService.save(items);

    }

    @Test
    public void getProd() throws Exception {

        mockMvc.perform(
                get("/api/funnels/activateByGroup")
                        .param("groupId", "59a41d21bbaf9628a44073a2")
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                get("/api/items/product")
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());


    }

    @Test
    public void getDistinctTest() throws Exception {

//        System.out.println(itemService.findDistinctItemFilterField(Item.Field.type).size());
//        itemService.findDistinctItemFilterField(Filter.Field.type).forEach(s -> System.out.println(s));
//
//        System.out.println(itemService.findDistinctItemFilterField(Filter.Field.category).size());
//        itemService.findDistinctItemFilterField(Filter.Field.category).forEach(s -> System.out.println(s));



    }


    private void setUpInfo() throws Exception {

        user = userRepository.findAll().get(0);
        token = InfoInit.getToken(user, mockMvc);
    }

}
