package ua.softgroup.thecase.controller;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.UserToken;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.repository.UserTokenRepository;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class UserControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTokenRepository userTokenRepository;
    private MockMvc mockMvc;

    private User user;
    private String EMAIL;
    private final String PASSWORD = "password";


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();

        user = userRepository.findAll().get(0);
        EMAIL = user.getEmail();
    }

    @Test
    public void login() throws Exception {
        mockMvc
                .perform(
                        post("/api/users/login")
                                .param("email", EMAIL)
                                .param("password", PASSWORD))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc
                .perform(
                        post("/api/users/login")
                                .param("email", "1")
                                .param("password", PASSWORD))
                .andExpect(status().isConflict())
                .andDo(print());
    }


    @Test
    public void registration() throws Exception {

        mockMvc
                .perform(post("/api/users/registration")
                        .param("userName", "cyborg")
                        .param("email", "sg.serhii.lavrinenko@gmail.com")
                        .param("password", "13"))
                .andExpect(status().isOk())
                .andDo(print());


        /*mockMvc
                .perform(post("/api/users/registration")
                        .param("userName", "gogogogoog")
                        .param("email", EMAIL)
                        .param("password", PASSWORD))
                .andExpect(status().isConflict())
                .andDo(print());*/

    }


    @Test
    public void registrationConfirm() throws Exception {
        User userNew = userRepository.findByEmail("sg.serhii.lavrinenko@gmail.com");
        userNew.setEnabled(true);
        userNew.setAccountNonLocked(true);
        userRepository.save(userNew);
//        UserToken userToken = userTokenRepository.findByUser(userNew);
//        String token = userToken.getToken();
//
//        mockMvc
//                .perform(get("/api/users/registration/" + token))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//        userRepository.delete(userNew.getId());
//
//        mockMvc
//                .perform(get("/api/users/registration/" + "1"))
//                .andExpect(status().isConflict())
//                .andDo(print());
    }

    @Test
    public void resetForgottenPassword() throws Exception {
        mockMvc
                .perform(put("/api/users/resetForgottenPassword")
                        .param("email", EMAIL))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc
                .perform(put("/api/users/resetForgottenPassword")
                        .param("email", "1"))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void resetForgottenPasswordConfirm() throws Exception {

        UserToken userToken = userTokenRepository.findByUser(user);
        String token = userToken.getToken();

        mockMvc
                .perform(put("/api/users/resetForgottenPasswordConfirm/" + "1"))
                .andExpect(status().isConflict())
                .andDo(print());

        mockMvc
                .perform(put("/api/users/resetForgottenPasswordConfirm/" + token))
                .andExpect(status().isOk())
                .andDo(print());

        user.setPassword(new BCryptPasswordEncoder().encode("password"));
        userRepository.save(user);

//        userTokenRepository.delete(user.getId());

//        mockMvc
//                .perform(
//                        post("/api/users/login")
//                                .param("email", EMAIL)
//                                .param("password", PASSWORD))
//                .andExpect(status().isNotFound())
//                .andDo(print());
//
//        user.setPassword(new BCryptPasswordEncoder().encode("password"));
//
//        userTokenRepository.save(userToken);
//
//        userRepository.save(user);
    }

    @Test
    public void replacePassword() throws Exception {
        mockMvc
                .perform(put("/api/users/replacePassword")
                        .param("email", EMAIL)
                        .param("oldPassword", PASSWORD)
                        .param("newPassword", "ploxoiPassword"))
                .andExpect(status().isOk())
                .andDo(print());

        user.setPassword(new BCryptPasswordEncoder().encode(PASSWORD));
        userRepository.save(user);

        mockMvc
                .perform(put("/api/users/replacePassword")
                        .param("email", "1")
                        .param("oldPassword", PASSWORD)
                        .param("newPassword", "ploxoiPassword"))
                .andExpect(status().isNotFound())
                .andDo(print());

        mockMvc
                .perform(put("/api/users/replacePassword")
                        .param("email", EMAIL)
                        .param("oldPassword", "pass")
                        .param("newPassword", "ploxoiPassword"))
                .andExpect(status().isConflict())
                .andDo(print());


    }
}
