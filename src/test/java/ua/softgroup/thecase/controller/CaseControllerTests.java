package ua.softgroup.thecase.controller;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.repository.CaseRepository;
import ua.softgroup.thecase.repository.RolePermissionRepository;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.service.ItemService;

import java.util.List;
import java.util.TreeSet;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by java-1-07 on 14.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
public class CaseControllerTests {

    @Autowired
    private ItemService itemService;
    @Autowired
    private RolePermissionRepository rolePermissionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CaseRepository caseRepository;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private MongoTemplate mongoTemplate;

    private MockMvc mockMvc;

    private User user;
    private String token;
    private RolePermission rolePermission;
    private List<Case> cases;
    private int permissionMaxCase;
    private List<Case> casesAnotherUser;
    private String aliensItemId;

    private MultiValueMap<String, String> createCaseParams = new LinkedMultiValueMap<>();
    private MultiValueMap<String, String> renameCaseParams = new LinkedMultiValueMap<>();

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();

//        setUpInfo();
    }

   /* @Test
    public void getActiveCase() throws Exception {

        mockMvc.perform(
                get("/api/cases/active"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                get("/api/cases/active")
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        List<Case> casesForTime = caseRepository.findByOwnerOrderByCreatedAtDesc(user);
        casesForTime.forEach(c -> {
            c.setActive(false);
        });
        caseRepository.save(casesForTime);

        mockMvc.perform(
                get("/api/cases/active")
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print()); // OK because if no any active case method return first case and make this case active

    }*/

    /*@Test
    public void getCasesTest() throws Exception {

        mockMvc.perform(
                get("/api/cases"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                get("/api/cases")
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        caseRepository.delete(cases);
        mockMvc.perform(
                get("/api/cases")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        caseRepository.save(cases);
    }

    @Test
    public void createTest() throws Exception {

        createCaseParams.add("name", "createTest");
        createCaseParams.add("description", "this is dress case package for dinner");

        rolePermission.setCasesMaxCount(cases.size());
        rolePermissionRepository.save(rolePermission);

        mockMvc.perform(
                post("/api/cases").params(createCaseParams)
                        .header("X-Auth-Token", token))
                .andExpect(status().isLocked())
                .andDo(print());

        rolePermission.setCasesMaxCount(cases.size() + 2);
        rolePermissionRepository.save(rolePermission);

        mockMvc.perform(
                post("/api/cases").params(createCaseParams)
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                post("/api/cases").params(createCaseParams)
                        .header("X-Auth-Token", token))
                .andExpect(status().isNotAcceptable())
                .andDo(print());

        List<Case> list = caseRepository.findByOwnerOrderByCreatedAtDesc(user);
        list.forEach(aCase -> {
            if (aCase.getName().toLowerCase().equals(createCaseParams.get("name").get(0).toLowerCase())) {
                caseRepository.delete(aCase);
            }
        });

        rolePermission.setCasesMaxCount(permissionMaxCase);
        rolePermissionRepository.save(rolePermission);

        mockMvc.perform(
                post("/api/cases").params(createCaseParams))
                .andExpect(status().isForbidden())
                .andDo(print());

    }

    @Test
    public void rename() throws Exception {

        renameCaseParams.add("name", "test rename");
        String oldName = cases.get(0).getName();

        mockMvc.perform(
                put("/api/cases/" + cases.get(0).getId())
                        .params(renameCaseParams)
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        Case mCase = cases.get(0);
        mCase.setName(oldName);
        caseRepository.save(mCase);

        if (cases.size() > 1) {
            String secondName = cases.get(1).getName();
            mockMvc.perform(
                    put("/api/cases/" + cases.get(0).getId())
                            .param("name", secondName)
                            .header("X-Auth-Token", token))
                    .andExpect(status().isNotAcceptable())
                    .andDo(print());
        }

        mockMvc.perform(
                put("/api/cases/" + "1")
                        .params(renameCaseParams)
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(
                put("/api/cases/" + cases.get(0).getId())
                        .params(renameCaseParams))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                put("/api/cases/" + casesAnotherUser.get(0).getId())
                        .params(renameCaseParams)
                        .header("X-Auth-Token", token))
                .andExpect(status().isConflict())
                .andDo(print());

    }

    @Test
    public void remove() throws Exception {

        mockMvc.perform(
                delete("/api/cases/" + cases.get(0).getId()))
                .andExpect(status().isForbidden())
                .andDo(print());

        if (cases.size() > 1) {
            Case mCase = cases.get(0);
            System.out.println("----------------- test removing case");
            mockMvc.perform(
                    delete("/api/cases/" + cases.get(0).getId())
                            .header("X-Auth-Token", token))
                    .andExpect(status().isOk())
                    .andDo(print());
            caseRepository.save(mCase);
        }

        mockMvc.perform(
                delete("/api/cases/" + "1")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        *//*mockMvc.perform(
                delete("/api/cases/" + casesAnotherUser.get(0).getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isConflict())
                .andDo(print());*//*
    }

    @Test
    public void getCase() throws Exception {

        mockMvc.perform(
                get("/api/cases/" + cases.get(0).getId()))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                get("/api/cases/" + cases.get(0).getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                get("/api/cases/" + "1")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(
                get("/api/cases/" + casesAnotherUser.get(0).getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isConflict())
                .andDo(print());
    }

    @Test
    public void addItem() throws Exception {

        Product product = mongoTemplate.find(new Query()
                .restrict(Product.class, Product.class)
                .limit(1), Product.class).get(0);

        mockMvc.perform(
                post("/api/cases/items")
                        .param("itemId", product.getId()))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                post("/api/cases/items")
                        .param("itemId", product.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                delete("/api/cases/items/" + product.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                post("/api/cases/items")
                        .param("itemId", "1")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    *//*@Test
    public void removeAllItems() throws Exception {

        List<Case> casesForTime = caseRepository.findByOwnerOrderByCreatedAtDesc(user);
        Case activeCase = casesForTime.stream().filter(Case::isActive).findFirst().orElse(casesForTime.stream().findFirst().orElse(null));
        if (activeCase!=null){
            mockMvc.perform(
                    delete("/api/cases/items")
                            .header("X-Auth-Token", token))
                    .andExpect(status().isOk())
                    .andDo(print());

            caseRepository.save(activeCase);
        }

        mockMvc.perform(
                delete("/api/cases/items"))
                .andExpect(status().isForbidden())
                .andDo(print());
    }*//*

    @Test
    public void removeItem() throws Exception {

        Product product = mongoTemplate.find(new Query()
                .restrict(Product.class, Product.class)
                .limit(1), Product.class).get(0);

        mockMvc.perform(
                post("/api/cases/items")
                        .param("itemId", product.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                delete("/api/cases/items/" + product.getId()))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                delete("/api/cases/items/" + product.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(
                delete("/api/cases/items/" + product.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    *//*@Test
    public void removeAllProducts() throws Exception {

        List<Case> casesForTime = caseRepository.findByOwnerOrderByCreatedAtDesc(user);
        Case activeCase = casesForTime.stream().filter(Case::isActive).findFirst().orElse(casesForTime.stream().findFirst().orElse(null));

        mockMvc.perform(
                delete("/api/cases/items/products"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                delete("/api/cases/items/products")
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        caseRepository.save(activeCase);
    }*//*

   *//* @Test
    public void removeAllModels3D() throws Exception {

        List<Case> casesForTime = caseRepository.findByOwnerOrderByCreatedAtDesc(user);
        Case activeCase = casesForTime.stream().filter(Case::isActive).findFirst().orElse(casesForTime.stream().findFirst().orElse(null));

        mockMvc.perform(
                delete("/api/cases/items/models3d"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                delete("/api/cases/items/models3d")
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        caseRepository.save(activeCase);
    }*//*

   *//* private void setUpInfo() throws Exception {

        user = userRepository.findAll().get(0);
        token = InfoInit.getToken(user, mockMvc);
        cases = caseRepository.findByOwnerOrderByCreatedAtDesc(user);

        rolePermission = rolePermissionRepository.findByRole(user.getSecondaryRoles().stream().filter(o -> !o.equals(Role.ADMIN)).findFirst().orElse(Role.ADMIN));

        User anotherUser = userRepository.findDistinctByEmailNot(user.getEmail());
        casesAnotherUser = caseRepository.findByOwnerOrderByCreatedAtDesc(anotherUser);

        if (casesAnotherUser.size() == 0) {
            caseRepository.save(Case.builder().name("name").description("desc").isActive(true).products(new TreeSet<>()).models3D(new TreeSet<>()).owner(anotherUser).build());
            casesAnotherUser = caseRepository.findByOwnerOrderByCreatedAtDesc(anotherUser);
        }
    }*/
}
