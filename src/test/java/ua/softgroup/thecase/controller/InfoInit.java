package ua.softgroup.thecase.controller;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ua.softgroup.thecase.model.User;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by java-1-03 on 07.08.2017.
 */
public class InfoInit {

    public static String getToken(User user, MockMvc mockMvc) throws Exception {

        MultiValueMap<String, String> loginParams = new LinkedMultiValueMap<>();

        loginParams.add("email", user.getEmail());
        loginParams.add("password", "password");

        System.out.println("login params = " + loginParams.toString());

        MvcResult mvcResult = mockMvc
                .perform(post("/api/users/login").params(loginParams))
                .andExpect(status().isOk())
                .andReturn();

        return mvcResult.getResponse().getContentAsString();
    }

}
