package ua.softgroup.thecase.controller;

import org.apache.http.entity.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.repository.UserRepository;

import java.io.FileInputStream;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Created by java-1-07 on 19.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
public class ImageControllerTest {

    private String token;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;


    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();

        User user = userRepository.findAll().get(0);

        token = InfoInit.getToken(user, mockMvc);

    }

    @Test
    public void getImageTest() throws Exception{
        mockMvc.perform(get("/api/image/img1.jpg")
                    .header("X-Auth-Token", token))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/image/img1.jpg"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/image/img-.jpg")
                .header("X-Auth-Token", token))
                .andExpect(status().isNoContent());
    }

    @Test
    public void uploadImageTest() throws Exception{

        MockMultipartFile multipartFile = new MockMultipartFile("image", "a1.jpg", ContentType.MULTIPART_FORM_DATA.toString(), new FileInputStream("src/test/a1.jpg"));

        MvcResult mvcResult =
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/api/image/upload")
                .file(multipartFile)
                .header("X-Auth-Token", token))
                .andExpect(status().isCreated()).andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());

    }

}
