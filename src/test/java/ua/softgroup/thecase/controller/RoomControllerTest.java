package ua.softgroup.thecase.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.repository.UserRepository;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by java-1-03 on 28.07.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
public class RoomControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired

    private MockMvc mockMvc;

    private String token;
    @Autowired
    private UserRepository userRepository;



    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();

        User user = userRepository.findAll().get(0);

        token = InfoInit.getToken(user, mockMvc);

    }

    @Test
    public void getRooms() throws Exception {

        mockMvc.perform(
                get("/api/rooms"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                get("/api/rooms")
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());


        mockMvc.perform(
                get("/api/rooms")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

}
