package ua.softgroup.thecase.controller;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.TheCaseApplication;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.ItemRequest;
import ua.softgroup.thecase.repository.*;
import ua.softgroup.thecase.service.ImageService;
import ua.softgroup.thecase.service.ItemService;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by java-1-03 on 09.08.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TheCaseApplication.class})
@WebAppConfiguration
public class AdminControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ItemService itemService;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ImageService imageService;

    private MockMvc mockMvc;
    private String token;
    private List<User> users;
    private User admin;
    private User user;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();

        users = userRepository.findAll();
        admin = users.stream().filter(user -> user.getSecondaryRoles().stream().anyMatch(role -> role.name().equals("ADMIN"))).findFirst().orElse(null);
        token = InfoInit.getToken(admin, mockMvc);

        user = userRepository.findDistinctByEmailNot(admin.getEmail());
    }

    @Test
    public void removeUser() throws Exception {

        mockMvc.perform(
                delete("/api/admin/users/" + "-")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(
                delete("/api/admin/users/" + user.getId()))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                delete("/api/admin/users/" + user.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        userRepository.save(user);

    }

    @Test
    public void banUser() throws Exception {

        mockMvc.perform(
                put("/api/admin/users/ban")
                        .param("id", "-")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(
                put("/api/admin/users/ban")
                        .param("id", user.getId()))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                put("/api/admin/users/ban")
                        .param("id", user.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        userRepository.save(user);
    }

    @Test
    public void unbanUser() throws Exception {

        mockMvc.perform(
                put("/api/admin/users/unban")
                        .param("id", "-")
                        .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(
                put("/api/admin/users/unban")
                        .param("id", user.getId()))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(
                put("/api/admin/users/unban")
                        .param("id", user.getId())
                        .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        userRepository.save(user);
    }

    @Test
    public void addGroupTest() throws Exception {

        Group group = mongoTemplate.find(new Query()
                .restrict(Group.class, Group.class)
                .limit(1), Group.class).get(0);

        String testCreateGroup = "testCreateGroup";

        mockMvc.perform(post("/api/admin/addGroup")
                .param("groupName", "Eggs").param("groupName", "Eggs").param("groupIcon", "pregnant_woman"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(post("/api/admin/addGroup")
                .param("groupName", group.getName()).param("groupIcon", "pregnant_woman")
                .header("X-Auth-Token", token))
                .andExpect(status().isConflict())
                .andDo(print());

        mockMvc.perform(post("/api/admin/addGroup")
                .param("groupName", testCreateGroup).param("groupIcon", "pregnant_woman")
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        Assert.assertNotNull(groupRepository.findByName(testCreateGroup));

        groupRepository.delete(groupRepository.findByName(testCreateGroup));
    }

    @Test
    public void addTagTest() throws Exception {

        long countBefore = tagRepository.count();

        String tagName = "TestTag";
        String groupName = "Material";

        Tag tagOrigin = tagRepository.findByName(tagName);

        mockMvc.perform(put("/api/admin/addTag")
                .param("groupName", groupName)
                .param("tagName", tagName)
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        Tag tagNew = tagRepository.findByName(tagName);

        Assert.assertEquals(tagNew.getGroup().getName(), groupName);

        reSaveOrDeleteTag(tagOrigin, tagNew);

        long countAfter = tagRepository.count();
        Assert.assertEquals(countBefore, countAfter);


        mockMvc.perform(put("/api/admin/addTag")
                .param("groupName", "-")
                .param("tagName", tagName)
                .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(put("/api/admin/addTag")
                .param("groupName", "-")
                .param("tagName", tagName))
                .andExpect(status().isForbidden())
                .andDo(print());

    }

    @Test
    public void addTagsTest() throws Exception {

        long countBefore = tagRepository.count();

        String tagName1 = "serhio1";
        String tagName2 = "serhio2";

        Tag tag1 = tagRepository.findByName(tagName1);
        Tag tag2 = tagRepository.findByName(tagName2);

        List<String> tagsName = new ArrayList<>();
        tagsName.add(tagName1);
        tagsName.add(tagName2);

        mockMvc.perform(put("/api/admin/addTags")
                .param("groupName", "Material").contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(tagsName))
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        Tag tagNew1 = tagRepository.findByName(tagName1);
        Tag tagNew2 = tagRepository.findByName(tagName2);

        Assert.assertEquals(tagNew1.getGroup().getName(), "Material");
        Assert.assertEquals(tagNew2.getGroup().getName(), "Material");

        reSaveOrDeleteTag(tag1, tagNew1);
        reSaveOrDeleteTag(tag2, tagNew2);

        long countAfter = tagRepository.count();
        Assert.assertEquals(countBefore, countAfter);


        mockMvc.perform(put("/api/admin/addTags")
                .param("groupName", "-").contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(tagsName))
                .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(put("/api/admin/addTags")
                .param("groupName", "-").contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(tagsName)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }

    @Test
    public void addTagsToTypeGroupTest() throws Exception {

        String tagName1 = "serhio1";
        String tagName2 = "serhio2";

        List<String> tagsName = new ArrayList<>();
        tagsName.add(tagName1);
        tagsName.add(tagName2);

        mockMvc.perform(put("/api/admin/addTags")
                .param("groupName", "Type").contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(tagsName))
                .header("X-Auth-Token", token))
                .andExpect(status().isConflict())
                .andDo(print());

    }

    private void reSaveOrDeleteTag(Tag old, Tag young){
        if (old != null){
            tagRepository.save(old);
        } else {
            tagRepository.delete(young);
        }
    }

    @Test
    public void getGroupTest() throws Exception {

        Group group = mongoTemplate.find(new Query()
                .restrict(Group.class, Group.class)
                .limit(2), Group.class).get(0);

        System.out.println(" -------------- group name = " + group.getName());


        mockMvc.perform(get("/api/admin/getGroup")
                .param("groupName", "Eggs")
                .param("countTagsOnPage", "10")
                .param("page", "0"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(get("/api/admin/getGroup")
                .param("groupName", group.getName())
                .param("countTagsOnPage", "10")
                .param("page", "0")
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void removeGroup() throws Exception {

        Group group = mongoTemplate.find(new Query()
                .restrict(Group.class, Group.class)
                .limit(1), Group.class).get(0);

        System.out.println(" -------------- group name = " + group.getName());


        mockMvc.perform(delete("/api/admin/removeGroup")
                .param("groupName", group.getName()))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(delete("/api/admin/removeGroup")
                .param("groupName", "-")
                .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(delete("/api/admin/removeGroup")
                .param("groupName", group.getName())
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        Assert.assertNull(groupRepository.findByName(group.getName()));

        groupRepository.save(group);
    }


    @Test
    public void findByNameTest() throws Exception {

        mockMvc.perform(get("/api/admin/items/product/byNameStartingWith/" + "executive")
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(get("/api/admin/items/model3d/byNameStartingWith/" + "executive")
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

    }

   /* @Test
    public void removeTagTest() throws Exception {

        Tag testTag = new Tag("testTagForRemove");
        tagRepository.save(testTag);



        Group group = groupRepository.findByName("Material");
        Set<Tag> tags = new HashSet<>();
        Tag tag = new Tag("Hello", 1);
        tags.add(tag);
        group.setTagsSet(tags);
        tagRepository.save(tags);
        groupRepository.save(group);


        mockMvc.perform(put("/api/admin/removeTag")
                .param("groupName", "Material")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(tag)))
                .andExpect(status().isOk())
                .andDo(print());

    }*/

    /*@Test
    public void removeTagsTest() throws Exception {

       *//* Group group = groupRepository.findByName("Material");
        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag("Hello", 1));
        tags.add(new Tag("GoodBye", 4));
        group.setTagsSet(tags);
        tagRepository.save(tags);
        groupRepository.save(group);


        mockMvc.perform(put("/api/admin/removeTags")
                .param("groupName", "Material")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(tags)))
                .andExpect(status().isOk())
                .andDo(print());*//*
    }*/

    @Test
    public void renameGroupTest() throws Exception {

        Group group = mongoTemplate.find(new Query()
                .restrict(Group.class, Group.class)
                .limit(1), Group.class).get(0);

        mockMvc.perform(put("/api/admin/renameGroup")
                .param("groupOldName", "-")
                .param("groupNewName", "Eggs"))
                .andExpect(status().isForbidden())
                .andDo(print());

        mockMvc.perform(put("/api/admin/renameGroup")
                .param("groupOldName", "-")
                .param("groupNewName", "Eggs")
                .header("X-Auth-Token", token))
                .andExpect(status().isNoContent())
                .andDo(print());

        mockMvc.perform(put("/api/admin/renameGroup")
                .param("groupOldName", group.getName())
                .param("groupNewName", "Eggs")
                .header("X-Auth-Token", token))
                .andExpect(status().isOk())
                .andDo(print());

        Assert.assertNull(groupRepository.findByName(group.getName()));
        Assert.assertNotNull(groupRepository.findByName("Eggs"));

        groupRepository.save(group);
    }

    /*@Test
    public void renameTagTest() throws Exception {

      *//*  Tag tag = new Tag("eggs", 3);
        tag = tagRepository.save(tag);
        Group group = groupRepository.findByName("Material");
        Set<Tag> tags = new HashSet<>();
        tags.add(tag);
        group.setTagsSet(tags);
        groupRepository.save(group);

        mockMvc.perform(put("/api/admin/renameTag")
                .param("groupName", "Material")
                .param("oldTagName", tag.getName())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new Gson().toJson(new Tag("This is new tag", 23))))
                .andExpect(status().isOk())
                .andDo(print());*//*

    }*/

    @Test
    public void removeBrandTest() throws Exception {

//        Brand brand = mongoTemplate.find(new Query()
//                .restrict(Brand.class, Brand.class)
//                .limit(1), Brand.class).get(0);
//
//        mockMvc.perform(delete("/api/admin/brands/" + brand.getId()))
//                .andExpect(status().isForbidden())
//                .andDo(print());
//
//        mockMvc.perform(delete("/api/admin/brands/" + "-")
//                .header("X-Auth-Token", token))
//                .andExpect(status().isNoContent())
//                .andDo(print());

//        mockMvc.perform(delete("/api/admin/brands/" + brand.getId())
//                .header("X-Auth-Token", token))
//                .andExpect(status().isOk())
//                .andDo(print());
//
//        brandRepository.save(brand);

    }

    @Test
    public void editItem() throws Exception {

        Item item = productRepository.findOne("59c13eb7bbaf9618888bc2e3");

        ItemRequest itemRequest = new ItemRequest();
        itemRequest.setName("ACAB");
        itemRequest.setPublished(true);
        itemRequest.setImgs(new ArrayList<>());

        List<String> upload = new ArrayList<>();
//        upload.add("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAkGBwgHBgkICAgKCgkLDhcPDg0NDhwUFREXIh4jIyEeICAlKjUtJScyKCAgLj8vMjc5PDw8JC1CRkE6RjU7PDn/2wBDAQoKCg4MDhsPDxs5JiAmOTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTn/wAARCAEZAfQDASIAAhEBAxEB/8QAHAAAAQUBAQEAAAAAAAAAAAAAAgABAwQFBgcI/8QATRAAAQMCAwUFBQUDCAkDBQEAAQACAwQRBRIhEzFBUWEGInGBkQcUMlKhQmKxwdEVI/AzQ0RTcpKT4RYXJCVjgoOi8TRUVQhFsrPC4v/EABoBAAIDAQEAAAAAAAAAAAAAAAABAgMEBQb/xAAzEQACAgEDAgQEBQQCAwAAAAAAAQIRAwQSIQUxEyJBURRhgaEjMlJxkRUzQvA04SSx0f/aAAwDAQACEQMRAD8A8lLEysFqEtstVGHcQpIy0IC0pDEkm1SuiwHTJJJgJJJMUhjpb0ySBjprpJIASSX1TuFrH7J3FICamqZKZ4fE4tcOS0JcZkqwG1TBKLW3LGLmptoFOOSUezISxRk7a5NJ1HSVOsMmzd8pVGpo5KY98At4EKITOG5HLI8xtEjybi4b0TlOEl25JxjOL78EKSSe2lzoOHVUlgydN9E4TEOjadECe6YmiQFEEDbncCjLLHfomiLHBT3Q90dUYdysE0RoNu5ECow66IFOwLAeOacPCro2p2MsBwU2UB4bmGqqNKMFKyUZRXdWWQzM4i4uE17qJpRApqxScWuEHdECgBRBMgSgowVCCjaU0xNEwKK6iBRgqREkBRgqMFFdMVEoKK6iBRA6JiJW3J0U7YXHVxACrNdY3CPauPG6nFxXcrmpviLosgRt+8U+1O5oA8FXD7og4KXiP04K/BXeXJKXE8UyAFPdRssoJMldMSnYqHSTXTJWOh7prpkkrAdNdMShugYV0kOiSAOWy2QkKxluhc1U7SSkVi1NZTliEt8lHaTUiCyYtClLUBCi0STIrWTEqSyEhInYNwU6EtQd4HelY6JEtOJso7vPNMWO3kG3VFj2hkga7x0Q7QcimALTcGxRFoe0uaLEfEPzCVkqQzy4AHe07iEURzscxx0+Lw4H+OiuYbhlZiDXMpqaSVo1JaNBz1Ol10+Eez6oqGslqq6KNr2/DE0vJBHM2CqyZoY+ZMshjcvyo4cgtJB3g2KS9hovZpgrjmqJKydx3naBgPoFsQezPsuR3qGV3U1D/wBVil1TBF9y5aXIeExx7SWNg0z2SfeSZ2UXudPBe6QezDs7NLVBsFXAYpS2JzZjo3KDuOh1JVGt9jNE5j/cMVqYXOFrTxiQeosVKPU9O3TZB4Jo8ZYwF1i64AuSOSFzsxv/AAF3GNey/tLhMUr4qdlfFp36U5nAdWGx9LriTBIJXROY5sjTZzXCxb433LZDJDIri7KnFruAkCpMrBo0F553sE/dZvAJ5BWpEXwC1jncLIhkZv1KBz3O42TDVFipkpkJ3aBNe6Eb1I1jinbYmkhJwpGxczdSNjaN6aiyDkiIBGApbMHFLu8FLaR3A5SjDTuTiyMFFINzBAKcBEE4sih7geKMFEA1FlCe0W9AgowU2XkUrEIodph3RAoE4KAJQUYKiBsjBTTFRKCjBUIKMFSIkt0QKjBTg6IESg3RAqIIgbqQiQFECowU4KdiJA5EHKMFOmKiTMnzBRXSQIlumuok90BQZchuhT2QArpap7AJ0ACkiskgLOduldACnzKsKHsEsqWZLMEByRuYo3MVgm6EhJokpFYsQlqsEIcqg4k1IrFqHKQrBCAtS2k1IisfHxRMbbd3T03FSZVZoaKWtqGwQi7jqSdzRzKTjXJJSbdFeKlfUyCOKMukPAD8eXjuXX4N2Up4C2WutPL8g0Y39VcoMNgooBHBpJv2xHeJ/TotWnccrcwDXW1ANwFxtbrnBVjN+HT+si/SRtja1rGta1u5oFgPJamG0zYYmRtuWt0F/X81lxStYLk7ldjxeip2XkfJI7hHCBfzcdB4alcTHh1WsbUF/wDDZLJjwrzM6SmYAAr8QXFf6WWP7vD2Nb96Z7nfkPotHD+1NDN3Z5KmlfzLBIweW9al0HUJW2vuUPX43xydfGNyssAVCkldNAJoHw1UX9ZAb+o3j6qdlS22h0WHLpMmB1NDtZFcS5YLne1nY3Cu09JI2qp421RFmVLRaRvmN46G62HVcbBdzg0cybLNxDtNh9AcstQ3af1bRmf5gbvMhadJHPKX4SZXOorzHhXbnsDinZVzpgDV4aXWbUxt1byDx9k9dx6blx7oHM1kc1p5XufQL6VPaigro3xyysZFICxwlYHhwO8Fo4ea4iv9lGG4pNLPgeNQNLiXNp3g5W9Ab3tyXp8Xi7fxFyY5ShflPH8rT8LiT/ZUkcBOp0XS9oux2Mdmn/7wpC2ImzZo+9G49CNx6FYZutEYlUp+iI9m1nJOkfVEGHjop0Qv3HaEYAKEZR1RWugix8o52ThrBxSDQiDRdSSFYbQ3qpMjeSlpaZ00gYwXJ5LrouxNY6hFQ5hFxoLHVSpJckJTSdHF2aE1mniuig7KV1TK5vcha02Jkvf0GqKh7JS1lZMwCpkpYzkEzQ2Fr3cbOdfQbtASeiz5NTih3kjTjwZJ9kczdrTa9zyCcSfcd5r0yi9neG5RtY53cbNrP/8AACvt9m2CTHI2fEIJODXSAnyuLHyWT+pYL/MXvSTS7HlDXXHFFdeiYj7Kp2d6hxESt4tmi7w/u7/RcxinY/HcLjM0tE6anH89THaNt1tqPMLRj1WLJ+WSKZYJR7owwLpFpHVIWIuD5o720K0cMobaBuiBTFoOoUFZK6CmfI0AuaNEnwTXmLYKIFbXa/sXiHZGjoKypxSGsjq5tlliiLQ3ukg3J13clhgpRmpdiUoOPDJQUQKiBRgqwrJQU4KAIgUCYYRKF8scTc0j2sbzcbBRyYhTRBhMrXF3whneLvCyk2vUSTfYuAJwbqm+vije1lQ2amc/4dvE5l/UK23XUITT7BJOPdBJFSYRhuMdoauSmwKg942Ryy1EjskMZ5F3E9Bqupj9lHap7M0mLYSyT5GMkc31IB+ihLNCLonHBOSs5FIKzjOE4t2cxGOgxqniY6cEwTwvzRy23gX1B6FaPZDsrH2lgrMXxWvdQdn6IuYXRvyumLdXEuO5o+qHlio7gjhm5bTGzNBy5hfldFZdHFUexwvFN+zanYuOX3t4nDL882a49FW7b9k2dj6ihq8PqZKjBa9+yYHuzmGQi7bO4tIvbwUI503TRKenaVp2Y1kgESdaaMtg2SRXSRtFuORunzqLMlmWezRtJsyfMocycFO2LaTXSJUV0sye4VBpiUIuTYJ5Y3xGzwQlYA9UN0xchukTSJQCSA25J0A5rtsJoG4fShhA2ztZXdeXgFz3Zil29cZnC7IBmH9o6D9V1d1g1uXatqNemhb3Mka5SbW1gq5dYKnLPtKj3dp71++OIHJcmGmlmkkjdLKoKzRdMXm9/wB2N3Xqq1ZUPg/dQxGWci+XgwdevRXqWMCOaoygtp2BwFtC8kNYPU38ireH0bY22aM0ztXyHWx/VdptaeKxYlz/ALyznxi8zc5vg5Oemxt4zvqHRNO4NcG/gqLqnFaR1xWvd92Zudp/Neq0mExv7zm5id5KkrOzdPUREbMA25KyKyesiTUF6HCdmO2M1FWss73SpO4XvDN0/wDK9bp61mPYYa+gGWsj0mp76k9Py/VeR492LrInO2FMXsOoJIH4rQ7IYviOBVcZqWOE8XcmYXXEjOBvxRlwLNDbNEIZPClcTRxjGqqrl2ZvHHE64B0dmGlzyssaunbQkMkjkkqXC4gZ8QvuLjw/FadEWPkrMRkaHNpmmUNI0dI51mD1N/JS0FCwXe4GWokOZznam53k9VU5LTpYsS5JRi8zc5vg5Gerxl3eZTxRM5Bmb6kqvFj+KUczXPjjksf5v928eFtF6dFgbJxeUZvwWbjHYyGWJzoow13MBWxeX1YShj9EaHY/tzR45SHDsVDKinkGzftW6tvweOXVcP7TOxR7MVzaikzOwupJ2ZOpjdvLCfqDy8FnVGA4thlY2qgpZAWODSTYBwOljc6g7l61gTou2fYyqwerJ2rY7MLx3mH7J8WuFlN+XzfyVVfl/g+fz3Roo3OPNWKuCSnnkglaWyRuLHN5EGxCqnRTbK4hNKmYVWB1UzChMJIsNdZGyQnWw5DTeoWNdK9rGAlzjYADUrrsHwllEWzSgOqANOTPDr19EsmVY42x48Tm6QeAYfUQSNqKi0dtWxgDN58l2DsRqKhrWSOzNG4HVY7CrUMgB3hed1vUMsuI8I62DR44c1yaUAa34WtaCb6Cy0oNbXuSsuCRvzBatK9ht3h6rhZJ5JStnQiopcGrTACy0Ymte3K5oLeRVGmjzWsVpQsIsoRckyvK1RZiZpYkm3E70QhyybRmhce+Pm6+KUanG5ascnZikzku1HYLCceL5mRikrHD+XhFrnhmbucOfHqvGu0WAYj2drvdMQiyk3McjdWSjm0/iN4X0o1ZuPYLRY/hslDXRl8TtWuHxRu4OaeBH/ld7Ra2UfLPlGXLiUla7nzSCQoK8Z6OYfcJ9FtdpMEquzuLzYdWC7md6OQCwlYdzh+FuBusiZuaGRvNpC7d7o8GNKpcnrXtTd7/AOyfAK65OWSjqLnjmjI//peWAr0/Eh+0f/p2heNXRUsRaeWzlAP0BXlzXZmh3MXVWH1L8/oSgqxhlBX41icWF4VEH1cgzOc7RkTOL3HkqYXpnsQZFSYV2mxtzM88cmz3a5I2ZrDxv9Ap5JOK4IY4KUuStV9iOxHZ10cPantVUSVrgHOiZJs2i/3GguA6kqLEvZxDNhH7Y7F4y/FIACTTSyCTaW3hjwBZ33SFwFNLJXukxGsdtqureZZZHakkn8Oi7L2SYk/Ce3MFBG8tpMUjex8V+4JGNzNdbnYEearcZRW6y1TjJ7KMrsFHQYr26wSKtpYamlqBOx0M8Yc0ODC4Ag8bhd52jqMF9mdXJJg2FUs+O4rI6SJmUMZTxDTQD4W79Ba5vyXL4rSx4F7c6VsIDIZsRglY0aWMzbO9XXUvtY2g9pEm13HD4tl0bmdf/uukvPNX6jf4cHXobnZ/2gHtDWswDthhVA6DEDs4ZYWuMRedzXNcSQTwIO/1HGds8Bm7IY3V4VAZJKWWA1FA5xu4N1BZfiWn6W5rGxeR8NEaiMgSwPZIw8nBwsvTPbuY2N7OYiWFr2vmBBFnZSwEg+FlNrw8iSIRl4sG2YdR26w3CfZ3h+Adm6qamxHuMqpjG6Mx3BMrw4jeXaDjY9FyFKamrqc+CSY1W4i03ZLTOke5ruBJXoHZjs/gOA9kI+2nayn99nnY2SGBzc7WNcf3bWMOhcRY3O6/QlQYh7VMdlgMeD4VQYPStb3XTHaPaOYaLNHgQVBeqSsm/Rt0bftTjrKj2aYTiGJw7PEqWamlnGndeRlfu4Encs72b1eD4r2Sr+w+LTinmc5+zBeGGWN7swcwnQuB4dPFXMQnr8e9gU1Xicr5a2SIzvkNrnLPmB0GndA3cFwmBdkcT7b4ZX1lHLh7WUbnMbC8OdNI8NzAcgHbgb8+SiqcXZJ2mqOwrvY7jDIXR0OP01TGW5RHWUxZpyLmk38bLjsa7K492YdBN2hgknpYyI4aplQ6aGI7mix1aNwFwszDsZqMNi/2XHsSw2SPR8AqXANcNCCw77L1jAMUr8Z9j+NVfaM7VhhqGwzSMyGaMM7jiOea4B6BSuUabdkUlJNJUedhwPUIrqhhxd7hT5t+zbf0VguW9O1ZzHGnRMSElBmSUrI7TjxIiDwmy+CYtWI3cMkDwlmUWVC9rgEWG1FjOlmVcFwASD3ck7YbCyHkG4OqKaofKRndeyq5yltB1RuYtnNkhcnBUOcKamjdUTxxR2zvcGi6ExtUjr+zUQhwoSHQzPLr9BoPzU9TilPBcZs7hwasOrrPdqeOk2hc2JuWw0v4rKMlRP8AA0hvTd6rPk06nK5Mthme3yo6SDE5K2pdTtYGskjeLbydLqv2bzOnqJHEuc55uSddFn4RTuNcxpkdnc1w7rrH4TxWl2VlijqZWSWBL8nhr/4WnFCMUklRnyzly7s7mlaBgNa8fH7zTj6P/NaOGsDWtsqOERmf3zDNNpUx3h6ysOZo8xmHmrGFzhwHDoeCjNVNmjDLdDg6mjFrLWhYCAsejcLDVbETxlTQpMzu1NdRUtIx1Q8Nkk0a0C5Nui89xh0TqiGaOx2jXNuOPEKt2+xual7W4iJKYVMTaX3eEOdZsLyAQ8dQb6Lnez1bX18xNVLnihju0BoADienRThNdiqWJpbjs6KP/cFS7nWQAjn3XkfVbOFxAAczxWXgQ95o8RoftPjbUR/2o3XP/aStLCpb2DtCNCORVMklkbNGN/hnTUjRoFq08bQ5pIBWVRuGinxavOHYRWVjQC6CJz2j73D62U/Qrlb4M3tbBTOLoy5hc5pzMuL28Fg+zqrdF2nkhJ1lY5rurgN//bfzXnsvaiQ00UxbJLijqpxne8HIWaZbdb5r9LLsvZA2oxDHZK+bX43iwsALZdB4lNzi4UQWJwaZx3tNpG0fbfFY2Ns18u0A/tAH8SuReF1/tNrGVnbbFZYyHNbKIwR90AH8FyhA4oS4RC/MyFoJUrWkDekSdzQreFUhrK2OJ3wXzPP3Rqf0806od2bfZygEEQrZR+8kH7oH7Lefifw8VvNeGhVXSa2A6BoG7kAo5ZJCRFTm8jvikbw6N/M+i52SEtTPauxvjKOCFvuXJq1kJyucS8fzbBdw8eA80MdbJd2SFoDgP5R5da3ICwBUJpqfD6cSVkzYmncOJ8lRf2ko4TanonSW3Okda/ktmLR4MK5XPz5MU9VlzfkTr+Eb0OIljhtaeJ7eTRlW7S4lhEwa1xNM/wD4rCW/3majzC4Idq2k/vMPiLfuuIKuUuL4XXENzupZTuEm4+am8OnnxS/gj4meHMr/AJs741ZoTG+OoIZI4NYQ7aRuJ0HeG7zW/h+NNdJsapuzluRfgSvNIX1FHJmb8J3je13iOK6qBtPW0QqaMBjGWbPATcwuO7Xi08Dw4rn6rpWGSuC2v7GrBrZS4lyj0GF7XNuDdThcZgOLvim90ncb/ZJ3nouuilDgNV52WJ4puMu5skrVx7FgJ0IKdaYOipnGe1Ls4Mc7PPqoI711ADLHYavb9tnmBcdQF4MHWtxC+rLi2u5fOnbrCYsJx6qp4ABEJCWDkDqB5Xsu7oMm+Lj7GTURppna9jW/tD2C4nSWuaeCsYBa+oLpAPUryakdnpoTzYPwXq3sYrsO/wBDsaw3Eq2CmjfWSj97K1hyPjaNM3n6queyXsnoGtFT2jFSALWOINf/APrCujLY2WThvSPOHFrRq9o8SvSvYTWQSOx/A5iDtstS1vzscMr/AE7vqo/efYrQi0UEdS/5RHUyk+btF53h9XUYVixxXBJHU8sNRI6mLm2Doi42a5vIjgpOTycJEFFY+Ww5qOXAsQqsFrRs6ijkczvaZ23u1w6EWK6X2XUMuLdv8PmpwXQYa2SaokHwtu0ta2/Mk7uQPJbVV7QuyHaKnjHazsvMauMWD4YxIP8AleCHAdFBV+0qlocNfhnYjAf2bG+96idjWZSftBgJzHkSeWiW6TjtoeyKlvsx/aRiDJPalPWU7g4UFTSsLuGdpBPoTbyXovtY7G1mPe6Ytg8YlxGkaY3QlwaZ4ib2BOlwbkeJ6Lw+vhyYXUNL3Pe79497jdz3XuSV7B7W8UxGDCOzGNYZXTUcz5cueI79pGHWI3Ed3cUSi4tV3HGSmnfY5PAOwXaDHsSp4cRwubD8NjkDqmSoADntab5Gt43tv3fgZva5jdP2k7Sw4fRyiWkwyKRkkjT3TM/Qgc7ADXndZeI9q+1uK05pa7tBKKdwyvbTwshLx1c0X8tyyYIYoIxHGwNYOAVsccpS3TKZZIxjtgejdmMWwLth2Di7I43XMw/EKSNkTc8gYTs/gewnR2gFx4+KzKjsP2Zwlm37Q9uoqqjbr7vCxkb5LcO65zneQXFz08FQAJomPtuuNyCKjpYXZo6eNrudtUeA12fAePF8tcnpLfaZ2axHAsVwSup5sLo3w+7UUbYXPc6IsLQbAWBBG6/EdVwPZDG8X7Mzx4jhuQvewR1FNNcMqGt0aeYdbcf87hdK6lHAl3Iy1DdUjup/adhlU7b1XYSOauNrl74n6/2y2/0XO9qu1mNdr7U9ayKhwpjg4UcLsxeRuzu425Cw6cVj5krojginYpZ5tUHmslmQXSJV9meg8ySjuklYUZb8ExVm+gn8m3Vd+G17PjpJx/yFexiMcktk3kr/AIKPucddcku8EeLiKVju/DILc2EfkrNfPBLGxscBjc0WJtZevmBp4AoHUML/AIomHxaEvgvaRL+tRbTcO3z/AOjxO3gmXs0mCUMnxUkB8WBV5OzOGP30UPkwBReil7ly65i9Ys8gt0Qm4K9Zf2Qwp/8AQ4/K4UD+xGFO/oxHg8hR+Dn7ouj1rT+qf+/U8r8grWGSGKvp3taS4PAA6nT816K/sFhjtzZW+Dymi7B0UMscsUs7XscHN7wIuFH4TImWf1fTNVz/AAcdWsp6N7mm0kgOrnbr9AoBDW1OrYiG8C42HougiwCSjraiSuAdNnIZxGXmPFW9kBwXOz5pQbSVHVwwhOKldmDhtHVUdXHUulaSy/dDd9wQquzlp6yaZ3da52bffQ7z5aLpnRhUa2nzN7trjcqMepldSZc8Ue6RuYTiDqqKNzZC2qhN2uB1JHEdQupa1mLF1bR5I60DNU0+4OPFzeh+nHSxXktNVS0U3duA0/CTu8/wK7DDsYjnLJGvdFUMsQ9ujh1I3+YXQbWVd6Zl2ywu0rR29FWZHbOUFkg3tcLELYZXMDPiC49uNVU0YbNHTVgG5xFiPMKGWqklactIWDkJXEIjGa7oHkg/Ur9vJ6Osqm7IOkqXgB8TGg57biTw6qPs1hkbby1eVtPD353DQE8Gj8FC4RseSS1gOrgwXJ8f8yud7T9o31FJ+zaA5IRcPc06ddeJPE+ilKUcauQluy+WPY6yixaCeuNfhpZmilLgwG7SL6jwtoR1XQtijqAK3DyNmSA5hOrD8juRHB25w6rw3B8RnwurD23t9poNr9R1XouEY4yTLV0dQYJdxezcb8HD8jcKt1mSadNEkpYW+Lid9Q1gByOu143tIsR5K3iMsNTQT08rgGSsLSuZZjr54h71h8E5G6SFxYfpdZ9fPJUtIZFUMYeBlP42CnGM+zQnlg+UzmMZpoZsVMEQhllb/VAhkd97ndei9Aw2pp+wvZCeukIbVzx5aeM792l/W58lg4eKLD3Mfs2VE97x08Y7oPNx4lY3tCdWTxUtTWSlz3vcCwfC0ACwH1UW4QrH6v0HKc5vf7HFyySTyvmkcXPe4ucTxJ1KjdYJzmdu0HNMQBu1KsKUyMuPBb/ZmPJBU1Lt5Ijaeg1P5LEawuOo18V0dINhg0IG95c71P8Akoz4i2WY6ckizA11VUhrSbi5NjuG7/L1V+vqocDo9pYOqHaMb/HBN2ajaKSark+28geA0XJ49WvxDE32PdacrbcgnBeDiXuyuT+IzOP+MfuNI6or5zPUSOe93Hl4KxHRC25VsBa41DojcggkeS6NsFhuVaV8mtuuEY7qRo4KrLTAcLLdki6KnNFpuQ0CYsFxqWge2Cqc6SlOmupZ4dF3WF1bsOrGVkFpInNyyx8JYzvC83lhJvp6ro+yWIO2TqOU6xHuX5FXYpbvJIyZ8e174dzvcepvdP31ITIwtE1O6+r2kXHn9n0U3ZbtvDXYBPidWzZNglbE6OMF7hm3H6EIKeRtX2aez7VDJ3bf1b/0cuCw33qnqO0uFUsD5W1EBma1v2crg8O8BruWPPo8eWXnXJfj1MlFNdj1tvbCjDQ6OKqluLi0YH4lUq3tzNGDsaGKMD7VRN+Q/VeXUmFdo62jjMdVKyIDKA1gZu6uI/BRzdlJRd2IYk0W/rJi78LKiPT8MfdlrzNnU4x7Ra0BzTikEXDLTxi/qblcTXYo7EYKqR7pHuc5ry99yXG/M70UtJgtF8NQZXcmNt9VHLIyGll/2aSKKdjmMc5h7537zy0W3Djhj/KkijLJyRiughkdnfG0u5kKWOCFvwwxjwaEgCVKFYoorch2gbtB4KUaaKOyJpuLcVNFbdjPFk7HWT3DtCo9xSfBKLsOoZtqaWIb3sIF+dl0PaDthP2g7J4NgDsJfA/D3QOdVvmBDnRsLSQ0Djc8VzrXWUgKjKKk02TjNxTSJsyWdRZk+ZWWVUSZglmQZk10WFEmZNmQXS8kDokuldBcpXKAoPMnuo7/AMWT3QKg7pIEkBR34qpgeCIVkvIKBOumkePcI+xZbWv+UeqkFc75FTCMCydEPDiXBW/8MoxWtO+NyohGEg8KJdFYz5SEQqo+voqQCJAeCi8KmI80Qnh+ZUWqQIYvC+Zk4+1rqoyN1BaNVjFb2Mj920rBK811FVlZ7npTvTw/YAhRPbdTlCRdc26OnRj1tE2TXUEbiOCzZaeqY9rmvJLNGkGxAXTOjzKM0mbgrY55RDajFhxPEo7BwbJbi9uvqFcjxTEJBZsMTfHMfzWjHhzSdQr8GHsH2U5a6a4TDwod2jnqhlbVMtPK4s+QDK30CpOoLfZXcigaR8KikwoG/dVDzubtssSS7HBzUGbQhV2QVFNJngkc1w4g2K7qTB/uqD9iOJ3CynDPKPYHFPuYFJi2KsNixkvVzbH1C6TCY8UxN7dqIoIuNgXO+psreHYG0PBLbldVQ0TYALAKyWtyJcMr8GF9itS4TS0z2ztj/fNaWh5cSRff0WhhjGTVUrJGNcAy9nAHilUHK2yDByffHlpschH1Cr0MpZNVGTZl6kq0k0vY1/cKV2+mh/w2oThNC7fRU5/6Tf0UwfJ8w9EQfJzHovV8nht8/YqPwXDLFzqCksNSTE1eW9tJqaHFJo6bIIGuAZs7ZRpwsvXJM0rHMcRlcC0+BXiXavC5qKtkZnDmMfod1wCqM6bjR1elTrK3I0qeXY4FFG02OzufNcQx5c5795IXVUTjPFFEftDIPwWPBhzYSzaXIcN3msuafCZ6HBj2uS92F2ZOatPINJXUl7baaqlRQxRMAjja3wFldACyvWV2RcsHuyJ+Z25qrSwyO5K+QgfxVL1c2WLDFGLUwOA1cVBhpdBibSCe80g/itOqGio0jM1ZfkCfyV2HJKUlyQyQSizuOzuIf+tpHvLRVUskTTyda7fqFjNxM4d29w+ue0ZZo/3zWN0LSS11hys46IKDM2qZIL9w3KvR4ZTYjisMlRE2QRRgC/C5utWozLHHezNiw3LaZtBhnaCofNTRyOjpxIQ17nfEBoCLcwAV0eGezqOciTEKuaY7y0HKP1XWYdTRsa0BoFui3KdoAC4ObX5Jvh0dGGCMV2MvCOyGDYdZ0NBDnH2nNzH1KDtV2cocaFNFVMfkhJczI7LYkW/JdIwWUGINOyEjRezreqs0GRvNG+TJ1G1p5OPFHBf6vMFG5lQP+qUv9XuDj7NR/ildfmf8n1SzP+T6r0/0PF/FZfd/c5Aez7B/lqP8VOPZ7g975aj/ABSuvzP+RLM/5Pqj6B8Vl939zk/9XuDfLUf4pSPs9wQ72VH+KV1mZ/yfVLM/5Pqj6D+Ky/qf3OT/ANXuCcGVH+KUv9X2Cj7E/wDildbmf8o9Urv+UeqVL2D4vL+p/c5MdgMF/qpz/wBUox2CwT+pl/xSupu/5R6pXf8AKPVH0D4rL+p/c5j/AECwMf0eT/FcnHYPAv8A20h/6rl013/KPVK7/lHqmL4nL+p/c5sdhcCH9Fd/iuT/AOg+A/8Asz/iOXR3f8oS7/IIF8Tl/U/uc4OxOAj+gj++5EOxeBD+gN/vuXQ9/kEu/wAggPiMv6mYI7G4F/8AHx+p/VEOyGBj/wC2w/Vbn7zolZ/RFi8fL7sxP9E8E/8AjYPQpLayv6JIsXi5PdnnidLu/MiAb830W9FDE1GmAb8wRhoP2ggQwRhINHzBOG9QgkOESbL4eqLKenqlQDtRhCIz09VIIygaRnY1/JNXProcaFoW+a58715vqf8AdZ7LpH/HiCnATomhcls6tCazVWI4gbIYwrUYVMpE0goolciiCjjarcI1WdyJUSxRA8Fcjo8w3JqdgJC2aWIEBR8Qko8GScPB4JhQN5LpRSAjconUuXWy0RnaItGNBRtYdysmMNVox2O5RVAsEpSsVGTWmwQ4G+9e5vNhT125VsEktizRza4fRbOmcZ4mPqSvTz/Y6hE1BdOCvWs8OSLz/wBpWEQlrKqNpa6S5dY6X5rvw4Ln+3EO2wbNbVj/AMQq8iuLNWjntzRPK8McTC5ubvsNweN//KsYsP3TZ2DKxztoOhPxN8j9CFnxSmlqs4FxfUc1uZY56Uttmgl103g8x14Fc6D8SLiesktrUirRTBzRqr7XXXPuZNh0mSQ5oz8Mg3EK9DWAgarm5MTizTGSaNS6jkfZVfeRbeq89UAN6hGDHYqyUao8IgzRumcD+8Pd/sjd6m6q09PJXv3EQ37zvm6D9V0MMIjYNAAOW4f5LpabHt8zM+WV8BxtEcLvmeQNVtYFHdzpSN508FjCN0tRkyuGXTUWOu8+fBdPh0eyY1trLB1PUKljRdpsf+R0FGbNC1IH7ljUzty04DdcTubaNSM3UkwzUsnQXVeEq2Bmje3m0rdop7csWYdXDdilH3TMxL0SCdesPCCSKSSAGskkkmIb+NySdJIBimTlMmRYQTpgkgkh0kydAxJJvRJAh0kydADJJJIoDzIFEq+bqjDyt5S4kye6iDrorppkdrJWuRg9VACnzosKZYBRhyqhyIPRY0iyHFGHFVhIjD0WPaQYuc0Lel1hFbeJG8HmsRec6p/dPYdI/wCOv99RwjaEAUjVx5HWRNGFZjVdisx6rPkZNFlilbJkSp2AnVR4tSS9008gbp3rtuVn4l2ZOKL0Fa0EWOq38Lqc9tNOpXAxQzhwDqp7eZawLSgpHuAti8zB/ZCqeF3bkWJnpkLri9rjoboJsu/d4ri6GiawjNjVYT/w7D8lu0mHSOs/9oVUrB9mZrdfMWV8eEQceS1I3W6p1I7q03RhrbclRqmaKd2hUYFd8JWfg77YzD1JH0K0a9uhWRRu2eLwHhnH6Ld051niZNdG8E18mdoD0CLN0CrCREJF7FxZ4VIsA9AqGPxibB6puUaNzeish6Ge0lPLGftMI+ii48FmO4yTPDq1lp3t6qSgrXUxLHXdGd4/NHi7clY8dVUDbrzuScsWVtHtsaU4Kzfa5kjNA2Rj+BFwVWmwmnJvCZITfcDcLOhlmp3XicAOLSNCr8WKtt++hcDzbqFrjnx5F5ip45R7EYwyW9tu7+6rEWEMBu/NIfvHT0RftaltrtLj7iRxZrg3YwueTuzEC3jxUrwx5Cpvg0I2NjHDQeQCie51ZeGEnZnRzxxHIfqoIYZ6twdM6zPkboP810FDSNYBYLDqdfxtx/yW48PrIlw+lygFxLjYauNzyWvEMtlHDHlASmqYINHyNDjubvJ8lxpuU5e7ZsikkaUM7WEXK06aoa62UhcBjmM1FJSulhpXHgHSGwHlvWt2brZJiM1wSAVHLhyYac1Vk4yjO6Z3kDr2WhAb2WTSEloutOnOoU8Ets0yjMvKZp7rnDKdCQmzfcPqjq+5Uyt+9f1UefqvZxdpM8HkxbZuPsFf7p9U2b7pTZ+oTOfuU6K/DYWb7pSv90qPOlnRQthJf7pSzfdKizp86NobCTMBwTZxyUTnoc6e0W0sZuie/wB1QtfoizpUNQZLfomv91BtE20sig2Mkv0Sv0QbTqmzooNjJb9E1+ij2ifOih+Gws33UkG0SRQth5ddOHKAPRZ1ssi4snD0QeoM6fMixbSyHJ7qtnTh/VFj2lm6e6gD0Qeix7CYORhyr5kWdFhsMjHsXdSzCHI1zQATrrqqlLiEFTo11n8Wneq3aqCR1Q6TKcpGh4LnQ58bgd1t3RcvV6aGdvnk9LoJyxYor0O2BUrVzFFir4zlkOYDnvW5TVkco0cL8ddy4Wo0uTF3XB1seWM+xpRq3CqULgVcicFzMqNCNCDRW7ZhYqlC5XI3LLTRYhmUTHu3K0zDY+SKAi6uscLKffuOySho447d0aLajIDQBZZkLrK216lETJpHaKpPqCpnv0VOaZjQczgPEqxKXYjdGVXt0K4/FK4UVUxzXDaBwcOi3O0GMxU4cyJzJJeNjcN6n9FwVY588+dziS43JP5rtdM6fknJZZcRX3MOr1EVFwXdnqFHUiqpIZxukYHaKcP8Vj4Jmgwumjc4E5bjoDrZXxKvVLlHkJQptFxsiqYri0OF0u2mDnAnKA06pxKOq5H2hzv90hY1pLSHG45/xZRm9sWyeLFvmonG4hXxVVa9zToTomj1WB3g/pdaVDVbmvPmvParC296PV4ZpLayXEXTxNMkFjl1c0i9wmpa0TDUDyK0A0SNVJ+EjabSGQxuO8AXB8lmg4dpF7b9Cwx8R+JwHjor1M6nBB2kdvFZjcKqHaGYWPRXqbAZHWG2ytHANuiax/q+wKT9jbgxGhhAzTs05aqxF2jp3d2lhkmPAnutVWm7KbRv/rZG35MCsQ9hJWOzU+JZP+Qj81TGOmvzSZJufojQpJa7EHtbLK2GMn4Y9P8AuVjB34Y2SZ8dRC60jhmLxc2JHFBR9jJ7HbVYmv8AM91kUXs9oBKX1T9owuzbJgytPQnetUdVpcC8hW8WXI+Q8QbFjcsdNSlskDXgyyNN26fZB581sYNh+xq3OA0V6gooadjYoImRxsFmtaLABaMMbWHRcfVaqWpnufb0NeLGsUaRehblaFZY8N1JAss6proKSB0s0gYxu8lcxiGLzYqDEy8VM8AtAOrweLjw/s+vJX6LRZdROor6+xRqM8McbkaXvxrMeqaiOoc+jcxrI2j4S4fE4dNwHPVaGZYdAC2W571rmwFr9Vo7cL2McPhxUE7o8nqXvm5VVly6F7lW24QunCnsZnaRZzJZlV24S24T2EaLWZLMqm36JzOjYFFh7kOdVzNdNtE1Ei4l1rtEWZUhMQltz0S2Eki7mSzKltym2x5pbALuZLOFS2yW1T2AXc45ptoOapbVLa+CNgqLu0CSpbVJGwKPNg4ogSq4J5pw7qVMHFlm5R5iqwPUp79SlYtpZBKcHoq9zzKIO6lOw2MnB9U4JUGY8ynDjzKLDayzcpxe/iqwKkiOpd8ouoTmoRcn6E8eKU5qK9QK/LIchAIAsucr8KbcvhHi3mt6U3uVXDs3qvLPVT8RzTPXxwxjBQrhHIPgdHe98wOo5JMqnUrtpcgtXTVdFHO3UWdzXP4lSujtGSA4HM11tPNdXBq1lW19zLkwuDtdixSYg6rlhayc0zm5mgu0Y3iMw18EUWK1kceVk0rnP+2XXt4KrR09Q1r6iRoftbxh1u7cDcOtio4Y45AQJDG8fZK1RwwkrlFFTyNPhmyMarY2tjbNKcp1fcEnf/ktD9uVYaGsmfe2ribm+n+fquci7hyyXdyIVljm6jcpLSYH/gv4RU8+RepvQY/XPMr9tJlAytFwNbnX0sp4e0Fc6520uUDKBm1vpr+PqsKDugDUX4K2whtrX9VNaTB+hfwiD1GT9TNWHHcRLmuM8rgN42hAO+x+o9FKcZxMubaeR2urTIQCP45LJDmjUkC/G6niBLhru3HgVZHS4V/iv4Qnnye5pmsqXOe6SqLGH55DYLKrcQg/2l0shm2IYRsySHg3430tx53VmofRxlm1jfUPJ0Y0A3WZUMZJORUNbSwVMTmZY94LSHDxKsnjhFeVUEMkm/MyWmmfVtkY+F0Do98Z3AHUFWqag2jmvIOzbbhvKtYDhMcmcGXagEF7idSeGnBatc1sDAALDcAuH1LqMoLwYd/VnQ02mTfiP6FnD5P9na35NPJWQ8rJpZC0mx0KtbU810NDnWXCn9Dka7B4eZ+z5L2crmO2FYJAylFjk1d4lbElRs43SE6NF1xlbK6ed73G5JuVHX5/Dx0vUu6dp9+Te/Q56spNS9vmFRF2m/Albr3tMpYPNQVFBtAXM0J4LkafVbXtn2Ozlw3zEqU9dJG22YW6hXI8WLSc0IOuljvCyqymkYLFpuDfKeIQ0zXMjk7pYHaAA/RbPBxZHwu5n3yiuWdNDjFPmDS11yNSNwWjSYvSOdYOdpxsuLYHEXABHFSxkjcbX4JPp2KXuL4mSPRaTtBQR5bzEXIHwnp+q2Ie0+FssHzkHNk+E715Nc93XcrDXF7w6/G9lH+k4X6sPjJr0PXh2wwmEOu+YluhAjO/X9Cnq+1tBG5zcsji3eW2tx/T8F5OzVwO9Wo3aaN08FJdE075d/yRfUMq7Ho/+lkTXxCKNjmSNDszX5rX4acd3qqlX2kxCUtp4pBBPNJs4u5YE9C7d1v5LmKDaOa0NLWZePAKvjDowyJ8dbLWVLHh+yOrBbgR1WyHS9NjVqC+vP8A7KHrMs3yzdixf3nES2Wt29SBoDcBltDkG7z10vqtimjcLMa03eDlaOB8FzGF4a+SeldG6N8DnOmhym5IIIAPK1yL34Beo4NhLaOIPkOeUjUnh4KOp1sdFi2xjTfYlj07zztvgtdnKEU37yps+aQWdy3WICyq2F9JVy05v3HWHUcPork2Jtp64Q31B1CbtVYspK1n84DG7xGo+l1zOla2U88oZHe4t6jpUsKlFdjNL3cim2juRVLbn+CmM5/gr0to4GwvZzyKWc8lR25/gpbY/wAFKxbC/nPVNtD1VHbFLblFhtL20PIptp/F1S2xTbXqUWGwv7U8ktoeRVDbFPtyiw2F7aOtuSzu5FUtueaW3PMotBtLu0dyKWd3Iqjtz1S255osNhe2juRS2h6qjtzzKW3PMp2GwvbQ9UlR255lJFhtOFuiDlHdPdVWTcSQFEDZRAorp2LYShxRByhDkQcgW0mDk4d1UQKQKLHtJrqeP+RJ5n8FUzK40dxjeQ1XP6ll2YX8zf07Fuy37FOsfs4yVTonF4c48XIsakyMtz0QUWkYXmo+56KXBctdYuNMLpCQCQ1lzbktsLHxJmeqdll2cjYzlBtZ3MLdoOcyRmz/ANtmLG9zWhoc7KDmAvx3XRWDnZjoeaFrcptYiykbv42Xo0jltkm/M7NqBp4qcTA0FPG5jTI25LsupBOlz0Vdwsw2ve+nVSyu2UTHl1wyK/4p+pEsS1cNPYyBrfM3PkjZiFJ3GysMZc0FpeHAFZUMJY11bWgl29rPwurgfHiFOWzQyxu3xvDS4eRt9ElJsk4JGwcohLhHGTbTS4/FXYWNMbe85wLRcWy2P6LGwVkwo5YZtDE4hnhblw1W1A2Vgexz8zA6wGlxpy4LRB3yUyVOiaIRM+CMZnbyUM8Wdwc4Alt7ZW626Ig213X8EzxHkL5HkN6K5FZt9l42OppnMgfEC61nixOm9B2h7kbTyeFe7NBvuL3NaQC87+Kqdpm3pXnlYrwfUHepn+56PTcYolOj7zEeeyhwp+ZgTVDwyZ7ddDyXU6Nl/ND6nO6tj4jMhxaoLaXLfVx+i5uodljcVp4tMHSsYL6Nv6rNLNrPDEdznC/lr+SNdJ5M2xF2hiseHd9TLpSXuLjvJWpGLhZlPo93iVpRG4C5k1UmjenaKmKR5msI33ssdrnBpjucua9uu5buJgGEX1GYaBYrgA85TmF+O9dnp/OL6nP1XEwWixupCO64g620TAaoy27TYdV0EZWSnZnYlrS0lgzWO88SjD4Yz35MovvNlE52SNrzYgR3PldV4Y7D3uqGt7tba6e5oe2+5t07IJBmbM94G8NtorcEEXvlPG3N33a53DL6LCeI52+9U0gjmb8TScpK3KF7qqKmqQ1ofxa7cD+mithKymcaNCOnFg1z8o4hu714q22CAx5I4gGjjex13+arx53xM2hZfKPgtb6KePuAC+9XrkpfBPg0FHRV9JE6F19q3ZhjCQDfQkr1xvwjwXlWHStirqdhmmdd4OzaSBv0Oi9TYe43wXmevKskK9jrdNdwdnBdp5DTdonHN/KMa78vyXQVUnvXZOV28wOZIPWx/Fcn25lLe0cA4GEfif1XSYG73js9XRc6d/qBf8lx9M/B1MJfNG/UR8TTtfJnPGWyW26qkHuIByu16JZ3fK70XubPIl3a9Utr1VLO75XeiWd3yu9EWBd23VPteqo53fK70Szu+V3oiwovbXqlteqo7R3yu9E+d3yu9EWFF3apbXqqWZ3yu9Es5+V3oiwou7XqlteqpbQ/K70S2h+V3oiwLu1S2qpbQ8neiW0PJ3oiwL216ptt1VLadHeiba9HeiLCi9teqSo7To70KSLCjCEX3j9EQiHzORjKOIRAt/gKrcaNgAjHM+qLZN5n1Ul2dfRPnaOZHgiw2Eeybzd6otk3+CjD28j6IwQUWw2Ij2TeR9U4iZyUgsPFOD90p2w2IAQt+UKzBrcqK532VimH7tcPrE3UYnU6bBLczmu0Djt2NHB1ypqR3cCzu0dQ5mI7NtrZbuJ4C6gp8RkAIytsLa33rHi0uScFKKN2TLFOmdGHaLFxYbSWXo0aW4oW4s4m2Rvqq0r5Kh0ksb8rw4NDPmB4hbtFpsmPLukjLnyRlCkyBmm9E0a+KbaPjuJI9eOiMSNcOC7SZgaJLfu7ixHHVEYn1MOQ73w2HTU/5IQQWmMnuneUTHmKHLICWNN2ubvaNL+OgTEVMOlbNGaKqDncG3Nt32f0VqWaHDIXRUrXbeS2pcTlH8bk9RTUFRM57nvjI1BDCCeXTqpaSnoRO97jNNMbuEuW5BtvUEn2+5NtPn7FvCqY0lGDILyPcJZMwuSOPoFsQNeyRzHwuY69xJmDhI3gfGyzY3OqKJsUhLMgGt7uvxPgVbgEzI2tj1yi2q0wVKkZ5O3yaTWDKASAmdJSxXEkzGeOqqBtS8hpNm8wrD6SCnpjLKNpM82Y073H9OKuXsQ+Z02AywuoM8Lg6MuNnWtfms/tPK0UM5JA7qzqWWU0ZdZocRdrWuc1ovu05cVWrIc7Q/ad9jLHS+bdqfyXnMvRMubJLI5Km7OrDXQhFRrsS4E/M1aEsV5Hd4DVY/ZeTMXNBBty3XW/LcSu7t9yx9Pg8OpeN+llmurJhv8AY5DGrtxh7bg2Yz8FRfVCjqY53NDgwONi6wPdPFXsfP8AvyTS37tmnkufx8nY6Kc3/wCQ38yeNfgpfIhhqozI4h2hOhIWlBUxm3fHqucjsSB9k71Pn4aK2WhU3aYviNvFG5XzMMO+4uFl5QXktcCCUELiZYwLBwcCPEKRrI3nTuPvustmlw+FFxM2ae92PkdyUjBcHpqgySM45gnBIA01WsoHDdvAGn4SCy6gh/eg0dSXB40a7mrUd2kubbXeDuKJzIpWR7enDiG7w4jVDRJMZ87aKDYxvfLO7cXAHKPD8ls4W2Snip2yyhrgTnkk7wa47rjlc2WXCyBu0LICJHg5ZHPuQf8APVae0bVRkStsRYNDTYgbt/HzVkEVzaNSBrxmZJFHG5uhEb7sPUHkrgbH9pwtZZkMUpYBG8BvJTw0k0j8skjrfd4q+PBQzUoqrDWVMQkqIs2bRpZck8F6QycbIa8F5hUtpqPYxUsMb6jM3O94zZbm1h14+S22VD3hhJGQXzAvcc3IeC5nUunZNXOLi6o16XUxwRaaMftvUxzdoYWska57LtcAdRo0i/JdZ2UdbD5gd2yf/wDiVxvaGOKF1GXzNeTmLHZAHF2/U8Rwt4arsuzQDcInfyicfouBrtK8GeEW/RHVwZlkwNr5nPCMBo37uScMb19FeGSw0+qY5dLZV6hPg85tKeQHn6JZPulXBk4lvqnyt+Zn94J2PaUsh+Upizm0hXg1tz32D/mCRDDpnb/eRYtpQDP7SfJpxV392NMzfUIe7weweYRY9pTy+KWUcyrZDbfGPKyHK0/ab5lFhtK1h1v4J8v8WVkRtJ3t9U+za3e5vqiw2orBh5FEIjyPorTcn9Y31Un7sfzg9UWLaUNl90+ibZHkfRXrxn+cHqiDI7fyjfVFhtRn7I/KfRJX8sX9YPVJOxUcaBbmiFr8Uw/5T5ogPvNHmq7NVDgeKMC/NCPL1RtKLFQTWnkVIB4pN3cB5p2kW4hOyNC48UQcObkgdE2t99kWPaO4tyutm3KxT/yJVKWRkbDnkYwEfacBf1VinqIdiQJoif7Y/VcHq3M4/sdTQKos4bH3l2Lyt+yQLHqqhNmC3VafaCAGP3xrmkidzXAEEgaW0WVmGRuouujoltxRXyKtRzNscHjxVoNyRiM6OvmDhxCgiYCQ52jB9VMHBxIOhG4rYnbMzXBKydw7s3eB4p3CEm4KHRzcrt/ilsxuDgel9ysINe4sotcKcHu2c5ttxUTGXFjony2Hw3CaYmiy7LZveAvuRMOU2vuULGAuFmlpB0udL26qWVxdK/MONvIbtykpckXEkZUsj4ZvBXIa57dwAVBjW20AU7Q1pubaclKyNGqK5zWtLWjNv1RNkdI0zyOLngENvzKzWTi9yG66BSvqwXNH2WjcFZGVEXFmgHup6aEahoaGeB5KOSoBim/sqM1+YFrm5mutdt9fJZWKVsdFA97C83abtcBYHh1upvIoR5HHG5S4NvsqLvkdzcV0cj4I3vMoJI1NgTpZed9ne08dHGA6le8jec4C6LC+0H7YxZ9OykexoZnLg+5AFh+a8npoZFqpZJLvf3OxqNrxbUzAxnGMPrMYkmp5gIsrWjNpe3HwWXiFXSPbKHyg3icGZNe9pa/TesWvhNLiNRAd8Urma9DZXMDhZUY1QxSPjYwzNLnPcA0AanUq/wAO8m/1sSe2G0r0+rgQpuui2O1WDPwjEy9jf9kqTnjIFgL6lvlw6LKYGizgLrbF1wUPnkkibZve3u3dFKGZ9HfEFDe6kDuHFXRZXJEjXyAAHXkU7SXHUa9EwkNiCETHNJ0tdSTRFodpAG4KXP3W3bx15IAWk2ItqpGhrSCHX8VKxbbJCCx2ltw3ImzSN1aLc7qJ7xtHcL9UTZBuN7KSkVuJdiqZm7ibnhwV51ZMRZpI52Cy2ytbqNFJHUEG5de+9TUkhODNr7EbbF0hkbI7nor8lTJDKWuvZwzNJ+0OfqudjrnCXaX1G5X2VzXR5JWtdE03uTYt6gq+GTmyp4+KJsbqmzsw9lhcyX9AV3GCH/cNUL6mFwXjPaHFKmLEYo4dpE2FpczPZxIdqDu5WWpRdrMXFC+mM7jFI2zssWtt+8aheb6hhlqNUskeyr/s7GmkseDZ68ndin+8PIBF7v4+gRU4Pu0JluZSwF/jbVHZvULp8HOpgCnB4n0CXu7fvegUjcvVHpYG5T4FTIfdmnifol7sz73qFNv4n1Stzci0FMi92Zzd6hL3ZtuP95S+NkhlvrZHAUyIU0fX+8n93YBuPqpwGniEj4XT4FTIBTt5OHmnEDeTvVTgNNxb6ohl5FAuSD3dnJ3qn93Z19VOWhx7qcNLeSOBUyv7vGOB9U4p2cvqrIb91OGuG4D1TFyVthF8p9Ulayu+UJICmcBu4I+miDMfl+qcFxOoHqo2i/aww63BGHWG4BRgut8P1SueQ9UWFEwf0CLaE8lBv4fVELng31RYbSXN/F0vX1QhxH2R6p3OsLuAHmix7TmO20QtSS5Razmk7zwK52nAv3Wj0XQ9s6kP91p9btu8jx0H5rn4Y87d5Cy5OZGnH2NrstRw1kmIOqGgsMYi3ai+tx6KnVU0uHVToJQSN7TweOYVOGvrcNcRTVD42k3Ld4J8Fq0mPMxQsosVhYWvdZk7O6WO4H/NEZpcDlFvkq57jfdExxurtdgFbRkuiaamIcWDvDxb+iz2m7rfaG8HeFoUipxJw6+hOqK7gN6ivoibIpbkR2krJeDrgKRkuXc7TqoM7SNCLoSHHd+Ke4Npe22Zw679UDn98m/EqvGcvCyd0jAd5J6I3g4Ewk1vcgqQTOI36KkZW8j5ohUN3ZgpLIR8MvbUpw/qqkTzK8MjDpHnc1ouVYxGkrKDDnV00TWtzhgY53eJPTgn4yXIeHzRKJnAhrPidoNbfVQ4tXYRHg9RRMldV18uUl8Q/dxkG9gTv43I3rlqmqmqXXe7TgBuCaBtiCVky6lz8q7F8MKjyWqDuA5mE9CSF2vs7jb7zXztbYgRxjwJJOvkFyLZQG23ldr7O43so6ycjuyTtt5DX8VXj7ksn5Tz2ukdNiFRK74nyOcfEkoXNDgrOLUUlNidXA5pDo5XC1uF7g+ir7m310Vb7smuxbgxqtp6GXD3PbNSSD+SmGYMPAt4tI6KXD4KirppZ4oXSMhIEpYL5b7iQNbdVmRtNRK2JjHF7jZoGpJXofYLDpsHFVU1l4pJw1jYuIAN7m27wU8bbZGdJWciBokvR8Rw3B8Qu6aBrJD/ADkXcd9NCvO8cp3YZiMtK1+0Y2xa4i2Zp1CvtoqVMcOuLGyIsA11Wc2qeDq26mZWAb2OHgnvHtLWcjcpGEmxJupcOpKrE2PfTRBzWEAl7g0X81ZfguJRtc/3VpDRc5JGk2G/S6NwUVDmveya5HiqnvsXzO8gmNcwbs7vJPxBbC806Iw7qVl++uJs2M+ZXX4D2eirKCCsqqp/70ZhFGA0AX5701kE4IyA8NFyQAN5PBVJcdigl7kTajLuDz3L9eYT9uaIUWKsihY5lK6JrmC5IJ3HU7zdc4GgfFoqp6iXZFkcUe7L+IV9XjFb71VSB8tg0d0NAA3AAcFq4RtjUQx7Z4aXtaRmNrXCxIbXFty6XspA6pximuCWtdnPgNVTHlk3wj0wy94jKUs90xlbxvbxCDaC+/6LTZm2kgd91Pe5+HRBtAeKIPaOKLCgh4FIeaQIO6580QPiEWKhiRdOB1T5td9kWYc7ppiBDbcEQbfc26JvmfJGDfjr4JgCGkfZRgeKIajmiA6hBEYWG9Fw0Fj1RZfAhPpu/JFi4BAF94B807Lj4SCEWm4XKbQHUFOxcCu/kEk9x1SRZGkedbRo4fRPtQODioQXckrO6qFmnaTiVvI+qcy9FAXFozOcABxNlA6uhad5I6DRVzzQh+Zl+LS5Mv5I2X9r936pCbhos0Ygy18jR/zIWVs8sgawtF9LAaqh62Hpya49KzPl0jXEhsd2guSdABzULpoQ7V4e43sRoLrOqqwRANLvh1N7G7ra8P4ssuoqS5pJsL6WKonrZN1FGvF0qNXJlLtHO2fFHuZq0NaAqdPIGmxQ1TtrVPdfTRADldobq6M3JWzn5cahJxXoHVlrjuVTZu3gb93NWpbENLjZt9SOA5qw2J0Lr3s9hBBB05g+ChOVFmDD4lnoVPOW00O0/lRG3Nr9q2qjqWUlX/6injl6utf13rl4e0UzTllyude17W81fp8dgkIz3aemoWuOaD9TJLT5Y+hU7QYaylhFTR5hEDaRhObLfcQeXBc/tJj9pdxtoaqFzBIyaN7bObbguLr6eTD5hHOQCRdut7i+9Sdd12IK+z7gCWcfaCKKaV88UZI77w3dzNlB7xH830Wt2foPe6ltUXWjgeDYjVx3jyUbT4TJcrub9bglIylqDA+bbNYSwufcXG7Sy4wVMzv5wjyXfOkO429VxWLUIw6os1xexwzDSxClPjkjB+hXtI7e8lTRU7iL95QR1UItmLx4BauFS01VM2JryH6ENfpm6BEXB+oNSXodTgzWYZQMjAAfIA6Q21JP6J8UZHiVBJSySWa4gggatI3eKrumBPeFvNMX5uIt0Um1VENvNnDYjSuoKySmLw8st3gLX4qKHMT0XR43hMlXUbeAtLyAHNcbXtxCDDMG2UrX1mUsbrkab3PVZnje7g0KarkzYWFzg34nE2DRvK9JwmIYfhsNMPia277He46lZNGyhpnZqemiY7ffLY+quOqwB8LlZCO0hJ7hYjS0de4OqqYOeBYPByutyuFzGOYCWzCXD4i6EgB0YN3NPPXeCt99W53gojMpuKkRUnE57AMEqTiUNRNE6KGF2cl2hcRuAHiu22wPj4rJ29uKf3nTeiEVFcBJ7jWMoXI9tI2+8w1GdgzMDS3MM2hOtuVuK0qqtfDTSysGZzWkgcyuMLHVc5lqqiznG7nHUpZJ0qQ4Rt2CyeMbzbyRte2aVsbN7iBuQVNNCz+SmLx4IICWSMLQdCDcBVKbLNqPRsKg/Z1BFSlzXOZfM5osCbq6J7EW3grFbX52hwNwRcaJ/fSdxPotCZTRzPaOlFJiUuRxcH/vNwFr3NllNmbxuPJaePSvqK+V7QbaDdyCynNeB3mfRZ5SafBbFWuSdlRG0g94+AXoHZzHMONBTUjahpljaG5XDKSfPevNWhpOpstHCfdIqpjqmJ88fFo3ojkfqDgj0/E44MQw6endEx73McGZ2g5XW0I5aryExPbIWPBa5ps4HeD1XotFWRGMiFsrYAf3bZdXNFt3hyUskdLUSbSWkikf8xjBJ81OUN9NEYy28HKdncCmxNznNIjhZve4G1+Q6rvMGwqnwsEhxklcLF2WwA5AIIah8bQyOPK0bmhtgrbJnO1IIKcYKIOTZbzt5HyRB0duJ81WDjvubJwSSOakQottycHqQBm8OVPW29G25tuTE0XGsbvzDyCkaxtr3PXRU2uN7ZR6qVjnC/dcUWRaLOQfwEYJAsN3gooy6+n4qVpJB3qREe/O2vikAN9wnAd9kkeaPLKPtApiE0t36I7sA4aoWl1wNyMaj/JBFiDmW4Jxs/u+qWVw04cyE1v4KBBgt4EeqW/iPIoLEW3FIm28IAKw6eqSj2jeqSYUeW1FZFTODXuJdvsOCrSYsDpE0X5uP5K9ncDvHmAfxR7Z175IBb/gM/RYciyN8OkdrBPBBJyjbMpkk9VfLG+V3DK0uKCShqnfyphgA4SSgH0FytWbPPpLI9zflzZR6DRVxR07d0MYH9kKlaZd2zVLqMu0I0ig2jFxGK2mc53BhcT+CsQsZSC7nCSXWw4A7v46q5TYYZmF0klLTX4Fzb/Q3R1VGwsymridbdsoy79B9VU8Mn+VG3FqYKP4kl+xhTyXGYm5PVV5Hwx394f3+EQ/M8PDf4LYdRgAiK7XHfI6xcPAbh46qu3A6YauDnHjclTx6druZtX1BS8uPsYk0sc8gc17QbBuXLYW4WUGW0p6i3mugkwWmcO60t8CoTgMd+7K8eS0KFKjkuduzID2llircMgkpBf44O74sO70OnmFb/Ylz3pj/dCmiwdkbswkkvYg6jUFKUNyoniybJKRky5C8kNcSdTY2CdgYN4k/vj9FrtwWHS5kcf7SlbgsI4vHmiOJJcksmduTcXwUaSpNOc8bScuuriSVlT7auqnzSus55ub8Oi6V2FMaxwa94JGhVEYAWuuagEciz/NWOPFIobbdvuZNXQRwMBZUtkdxAG5a+AVWzY+M2BytIAU/wCx2EWzAD7rQpaXCIYn5mufe3EogtrsjLlUWTV8Vg4xUmSscG2NmWIPOy6Q0EeXNdxPDVUpMGp3OOZjiTv7ysk7RCMTj2R5t7gFsYXTYa6J3vNS+OUatLRxWmez9Kd21b4P/VSwYJDF8Mkh8SNFSolrdjwTudBGZO84jVx49UZeD8Nx5rRo6KJseUsBsd5N1Y90iG6Ng8lenwVVyYueXgMyIGW99k9vlotbIAe6AEQH8XRYUZse2d9kEeNldhjeBfXwUuyY7e0J2QAHukjwKLDaiKQEA5mjxtZQ+85Tazh5gq/lkG59+hCjMWb4qeN3hooskkVm1TT8R9QpGuhf/NtPhZEaSE74pY/A3/FL3Frj3JgOjhZHIUiGqp6aWJ0ToTZwsdDuWezA8PabsY7wzlavuVU0dwh4+666B0c7P5SMjxak1Y1wU/2RRkawX8XEohhVEP6O0DzVpjubfrZGJAPtOH1RQDU9DS5QzZjTqVK+gpmtvsx6lM2ax+IHxCPbB1tAfAqRFoonC6U67Fuqf9l0v9Sz0V4Pad4PonDmc7eISoZR/ZVHfWlhPiwJNwuiDr+6Qg/2FotLDxCMNFkUFkFPDHBoyNoHIBT5iEg0A8k+vNMQbXnTcpmncq4tdSAjddNMRYbfgN3JGM3XwUUZG66lGvFMTD14KRl+tyo22G91lK3KLXI1QRbDbfS6nj5h1lC0t37+l1I0t0s4i3BSRFk7S6/xA80YLt+h8SoQ4C3eR3abd66kQJQb77dUYt0UAcPm0RNe3iUCJd40aCExz8BYdFGH33bkg82sB6lAB63u5rreKV2nh6lR5zv1HmmLwPtalAE3C+/zS4bvqog5x3a9QlnsNeCAokAPIJKHO1JAjgSWckwb/Fkh8SIb1QbVwNYDn6JWvuvdO/4R4pM+14JUSTBy23JAEndfxTu+yhZ8CRIkDHEfD9EDmtB1cPVTfzY8FWd8SYPsMbX0slpusCUw3ohuHimVjHTkkGX6Iz8QTy7kAMG24AqW7edlFw8kmfCf7KBMJ5zeCDKP4KQRBAxso5ImgfLZLgiCAESSUspJTjefFFxQIHIiyHoiO7yTjcgLFD3Xc1K93IWUTd6J/wCSYA3J8E4uhCNIY4uAjDvEIRuSCBEl+qcO5oE5QBKHeKK1/s/RRfqj4JgPs2ngR5o2te0aSvb5oRuRDeECYVpD8QikH3moDFG746Rv/I4hSN3BPwQBVdSU5+zPH53QGhjP8nUgdHNIWgPyUFRuQBV9wmt3ZIn+D7JjSVjbfu3kD5dfwR8PJT0nxJBZSO0Zo9hHiE7ZPu+hW8/4FkVO9yYwRKBuc4eaITD5vUKufiKJm/zSHRYEl+R9QjD/ALt/DVR8D4phvKLFRaZM2+rXDyU7JY+LreKosVtnwqSZFotRyRH7Y9VK0sO5wKzftFENxUiDRrAN4I2hp5eaoQK6d4UkQaJW5TyRZLnhZRN3BSDgmiA5bxsLpW3d1Mzcj4BOgsjPhZPmI37kXPxTS8EUA2bW9im04qRvwlQ8UgHJA5JzIDpqgO9FwCVDGv1akgO9JMR//9k=");
        itemRequest.setUploadImgs(upload);

        itemRequest.setType(item.getType());

        List<String> tags = new ArrayList<>();
        tags.add("serhio1");
        tags.add("serhio2");
        tags.add("serhio3");
        tags.add("serhio4");

        itemRequest.setTags(tags);


        mockMvc.perform(put("/api/admin/items/product/" + item.getId())
                .header("X-Auth-Token", token)
                .contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(itemRequest)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void removeImg() throws Exception {
        imageService.removeImage("acab.jpg");
    }

}
