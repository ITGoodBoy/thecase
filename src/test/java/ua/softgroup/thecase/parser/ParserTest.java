package ua.softgroup.thecase.parser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ParserTest {

    @Autowired
    private DesignConnectedParser parser;

    @Test
    public void loadProducts() throws Exception {
        System.out.println("test");
        parser.loadModels();
    }

}