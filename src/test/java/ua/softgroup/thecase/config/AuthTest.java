package ua.softgroup.thecase.config;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.softgroup.thecase.service.UserService;

import java.time.LocalDateTime;
import java.util.Optional;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AuthTest {

    @Autowired
    private UserService userService;

    @Test
    public void generateToken(){
        TokenHandler tokenHandler = new TokenHandler();
        String token = tokenHandler.generateAccessToken(userService.findById(new ObjectId("5993f7d27feff06a881427e3")).orElse(null), LocalDateTime.now().plusDays(14));
        System.out.println(token);

        Optional<ObjectId> id = tokenHandler.extractUserId(token);
        System.out.println(id.get().toString());
    }

}