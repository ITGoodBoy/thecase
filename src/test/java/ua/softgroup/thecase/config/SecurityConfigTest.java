package ua.softgroup.thecase.config;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.service.ItemService;

import java.util.UUID;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SecurityConfigTest {

    public static final String AUTH_HEADED = "X-Auth-Token";
    public static final String LOGIN_URL = "/api/users/login";
    public static final String ADD_SIDEBAR_URL = "/api/sidebar/item/add";
    public static final String API_ITEMS_MODEL3_D = "/api/items/model3d";
    public static final String API_ITEMS_PRODUCT = "/api/items/product";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ItemService itemService;

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    private Gson gson = new Gson();

    private User user;

    @Before
    public void setUp() {

        user = userRepository.findAll().get(0);

        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void test1() throws Exception{

        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();

        multiValueMap.add("email", user.getEmail());

        //WRONG PASS
        multiValueMap.add("password", "password1");
        getPost(LOGIN_URL, multiValueMap, status().isUnauthorized());
        //WRONG EMAIL
        multiValueMap.set("email", user.getEmail()+"1");
        multiValueMap.set("password", "password");
        getPost(LOGIN_URL, multiValueMap, status().isUnauthorized());
        //OK
        multiValueMap.set("email", user.getEmail());
        multiValueMap.set("password", "password");
        String token = getPost(LOGIN_URL, multiValueMap, status().isOk());

        System.out.println("Token: " + token);
    }

    @Test //get items
    public void test2() throws Exception{
        mockMvc.perform(
                get(API_ITEMS_MODEL3_D))
                .andExpect(status().isOk())
                .andReturn();
        mockMvc.perform(
                get(API_ITEMS_PRODUCT))
                .andExpect(status().isOk())
                .andReturn();
    }

//    @Test //get sidebar
//    public void test3() throws Exception{
//        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
//        multiValueMap.set("email", user.getEmail());
//        multiValueMap.set("password", "password");
//        String token = getPost(LOGIN_URL, multiValueMap, status().isOk());
//
//        //GET SIDEBAR
//
//        mockMvc.perform(
//                get("/api/sidebar"))
//                .andExpect(status().isForbidden())
//                .andReturn();
//
//        mockMvc.perform(
//                get("/api/sidebar").header(AUTH_HEADED, UUID.randomUUID()))
//                .andExpect(status().isForbidden())
//                .andReturn();
//
//        String sidebarStr = mockMvc.perform(
//                get("/api/sidebar").header(AUTH_HEADED, token))
//                .andExpect(status().isOk())
//                .andReturn().getResponse().getContentAsString();
//
//        System.out.println(sidebarStr);
//
////        Sidebar sidebar = gson.fromJson(sidebarStr, Sidebar.class);
////        assertEquals(sidebar.getId(), sidebarRepository.findByOwner(user).getId());
////        assertEquals(sidebar.getItemHolders().size(), sidebarRepository.findByOwner(user).getItemHolders().size());
//
//        //ADD TO SIDEBAR
//        mockMvc.perform(
//                post(ADD_SIDEBAR_URL)
//                        .params(multiValueMap).header(AUTH_HEADED, token))
//                .andExpect(status().isBadRequest())
//                .andReturn()
//                .getResponse().getContentAsString();
//
//        multiValueMap = new LinkedMultiValueMap<>();
//        multiValueMap.set("itemId", itemService.findAll(0).get(0).getId());
//
//        mockMvc.perform(
//                post(ADD_SIDEBAR_URL)
//                        .params(multiValueMap).header(AUTH_HEADED, token+"1"))
//                .andExpect(status().isForbidden())
//                .andReturn()
//                .getResponse().getContentAsString();
//
//        sidebarStr = mockMvc.perform(
//                post(ADD_SIDEBAR_URL)
//                        .params(multiValueMap).header(AUTH_HEADED, token))
//                .andExpect(status().isOk())
//                .andReturn()
//                .getResponse().getContentAsString();
//
//        System.out.println(sidebarStr);
//
////        sidebar = gson.fromJson(sidebarStr, Sidebar.class);
////
////        assertEquals(sidebar.getId(), sidebarRepository.findByOwner(user).getId());
////        assertEquals(sidebar.getItemHolders().size(), sidebarRepository.findByOwner(user).getItemHolders().size());
//    }

    private String getPost(String url, MultiValueMap<String, String> multiValueMap, ResultMatcher resultMatcher) throws Exception {
        return mockMvc.perform(
                post(url)
                        .params(multiValueMap))
                        .andExpect(resultMatcher)
                        .andReturn()
                .getResponse().getContentAsString();
    }


}