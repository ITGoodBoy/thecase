package ua.softgroup.thecase.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by java-1-07 on 16.08.2017.
 */
@Data
@Document(collection = "tag")
public class Tag {

    @Id
    private String id;
    @Indexed
    private String name;
    private int frequency;

    private boolean combinable = false;

    @DBRef
    private Group group;

    public Tag() {
    }

    public Tag(String name, int frequency) {
        this.name = name;
        this.frequency = frequency;
    }

    public Tag(String name) {
        this.name = name;
        this.frequency = 0;
    }
}
