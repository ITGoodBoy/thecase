package ua.softgroup.thecase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User  implements UserDetails {

    @Id
    private String id;
    private String username;
    @Indexed(unique = true)
    private String email;

    @JsonIgnore
    private String password;
    private List<Role> authorities;
    private String imgUrl;
    @DBRef
    private UserToken userToken;
    private Date createdAt;
    @JsonIgnore
    private String referrer;

    private String activeCaseId = "";

    private boolean enabled;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    @Transient
    private boolean isOnline;

    public User(String username, String email, String password, Role... role) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = Arrays.asList(role);
    }

    public User(String username, String email, String password, String referrer, Role... role) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.referrer = referrer;
        this.authorities = Arrays.asList(role);
    }

    public List<Role> getSecondaryRoles(){
        return authorities.stream().filter(role -> !role.equals(Role.USER)).collect(Collectors.toList());
    }

    public boolean isAdmin(){
        return authorities.stream().anyMatch(role -> role.equals(Role.ADMIN));
    }

    public void addToRoles(Role role){
        authorities.add(role);
    }

    public boolean removeRole(Role role){
        return authorities.remove(role);
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (Role role: authorities) {
            String name = role.name().toUpperCase();
            if(!name.startsWith("ROLE_"))
                name = "ROLE_"+name;
            grantedAuthorities.add(new SimpleGrantedAuthority(name));
        }
        return grantedAuthorities;
    }

}
