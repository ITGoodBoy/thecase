package ua.softgroup.thecase.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Created by ITGoodBoy on 18.07.2017.
 */

@Data
public class Subscription {

    @Id
    @Indexed
    private String id;
    private Role role;
    private Price perMonth;
    private Price perThreeMonths;
    private Price perYear;

    public Subscription(Role role, Price perMonth, Price perThreeMonths, Price perYear) {
        this.role = role;
        this.perMonth = perMonth;
        this.perThreeMonths = perThreeMonths;
        this.perYear = perYear;
    }
}
