package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Created by java-1-03 on 11.09.2017.
 */
@Data
@NoArgsConstructor
public class InviteCase {

    @Id
    private String id;
    @DBRef
    private User owner;
    @DBRef
    private User invitedUser;
    @DBRef
    private Case aCase;
    private boolean isConfirmed;

    private int permissionNumber;

    public InviteCase(User owner, User invitedUser, Case aCase, int permissionNumber) {
        this.owner = owner;
        this.invitedUser = invitedUser;
        this.aCase = aCase;

        this.permissionNumber = permissionNumber;
    }



    public void changeParmissions(int permissionNumber){
        this.permissionNumber = permissionNumber;
    }

}
