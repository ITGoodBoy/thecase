package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Created by java-1-03 on 08.09.2017.
 */
@Data
@NoArgsConstructor
public class Relationship {

    @Id
    private String id;
    @DBRef
    private User currentUser;
    @DBRef
    private User targetUser;

    public Relationship(User currentUser, User targetUser) {
        this.currentUser = currentUser;
        this.targetUser = targetUser;
    }
}
