package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * Created by java-1-07 on 11.08.2017.
 */

@Data
@NoArgsConstructor
@Document(collection = "group")
public class Group {

    @Id
    private String id;
    @Indexed(unique = true)
    private String name;
    private boolean published = true;
    private String icon;

    public Group(String name, String icon) {
        this.name = name;
        this.icon = icon;
    }

}
