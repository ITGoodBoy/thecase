package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Created by java-1-03 on 18.09.2017.
 */
@Data
@NoArgsConstructor
public class ItemStatistic {

    @Id
    private String id;
    @DBRef
    private Item item;
    private long countOfViews = 0;
    private long countOfCases = 0;
    private long countOfOpening = 0;
    private long countOfLikes = 0;

    public ItemStatistic(Item item) {
        this.item = item;
    }

    public void addView(){
        countOfViews++;
    }

    public void addCase(){
        countOfCases++;
    }

    public void removeCase(){
        if (countOfCases > 0)
            countOfCases--;
    }

    public void addOpening(){
        countOfOpening++;
    }

    public void addLike(){
        countOfLikes++;
    }

    public void removeLike(){
        if (countOfLikes > 0)
            countOfLikes--;
    }
}
