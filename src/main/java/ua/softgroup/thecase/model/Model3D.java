package ua.softgroup.thecase.model;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Set;

/**
 * Created by java-1-03 on 24.07.2017.
 */
@Data
@Document/*(collection = "items")*/
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Model3D extends Item {

    private String designer;

    private String file;

//    @Builder
//    private Model3D(String name, List<String> imageUrls, List<Material> materials, Volume volume, Brand brand, Category category,
//                    SubCategory subCategory, List<Room> rooms, String urlToShopOrDraw, List<String> tagsList, Price price, String designer){
//
//        super(name, imageUrls, materials, volume, brand, category, subCategory, rooms, urlToShopOrDraw, tagsList, price);
//        this.designer = designer;
//    }

    @Builder
    private Model3D(String name, List<String> imageUrls, String brand, String category,
                    String link, Set<String> tagsList, Price price, String designer,
                    boolean isPublished, String type, String source){

        super(name, imageUrls, brand, category, link, tagsList, price, source);

        this.designer = designer;
        this.isPublished = isPublished;
        this.type = type;
    }

    public boolean model3DEqual(Model3D newModel3D) {
        return getName().equals(newModel3D.getName())
                && getImageUrls().equals(newModel3D.getImageUrls())
                && getLink().equals(newModel3D.getLink())
                && getSource().equals(newModel3D.getSource())
                && getBrand().equals(newModel3D.getBrand())
                && getTagsList().equals(newModel3D.getTagsList())
                && getCategory().equals(newModel3D.getCategory())
                && getType().equals(newModel3D.getType())
                && getDesigner().equals(newModel3D.getDesigner())
                && getNativePrice().getAmount().equals(newModel3D.getNativePrice().getAmount())
                && getNativePrice().getCurrency().equals(newModel3D.getNativePrice().getCurrency());

    }

    @Override
    public String toString() {
        return "Model3D{" + super.toString() + "}";
    }
}
