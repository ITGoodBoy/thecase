package ua.softgroup.thecase.model;

import lombok.Getter;
import org.springframework.data.annotation.Transient;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by java-1-03 on 31.08.2017.
 */
public class Combinator {

    private List<String> tagsByGroup;

    private Set<String> currentTags; // can be null if it is first click on funnel ever

//    @Transient
    private List<List<String>> combinations;

//    private int oneCombinationSize;

    private String groupName;

    private int defaultCombinationSize = 1;

    private int currentCombinationNumber;
    @Getter
    private int progress;

    private Random random = new Random();

    private static boolean USE_RANDOM_IN_COMBINATOR = false;

    public Combinator() {
    }

    public Combinator(List<String> tagsByGroup, List<String> currentTags, Group group) {
        this.tagsByGroup = tagsByGroup;
        this.currentTags = currentTags.stream().collect(Collectors.toSet());
        this.groupName = group.getName();

        if (group.getName().equals("Material"))
            generateMaterialCombinations();
        else
            this.combinations = generateCombinations(tagsByGroup, this.currentTags.size());

        this.currentCombinationNumber = combinations.indexOf(currentTags);
        this.progress = generateProgress();
    }

    public Combinator(List<String> tagsByGroup, Group group) {
        this.tagsByGroup = tagsByGroup;
        this.groupName = group.getName();

        if (group.getName().equals("Material"))
            generateMaterialCombinations();
        else
            this.combinations = generateCombinations(tagsByGroup, defaultCombinationSize);

        this.currentCombinationNumber = 0;
        this.progress = generateProgress();
    }

    private void generateMaterialCombinations(){
        this.combinations = new ArrayList<>();
        for (int i = 1; i < 4; i++) {
            this.combinations.addAll(generateCombinations(tagsByGroup, i));
        }
    }



    private List<List<String>> generateCombinations(List<String> tagsByGroup, int oneCombSize) {

        int allCount = tagsByGroup.size();

        System.out.println(" combinations tagsByGroup.size = " + allCount);

        List<List<String>> combinations = new ArrayList<>();

        List<String> comb = new ArrayList<>();
        for (int i = 0; i < oneCombSize; i++) {
            comb.add("");
        }
        combinationUtil(combinations, tagsByGroup, comb, 0, allCount - 1, 0, oneCombSize);
        return combinations;
    }

    private void combinationUtil(List<List<String>> lists, List<String> tagsByGroup, List<String> oneComb, int start,
                                 int end, int index, int oneCombSize) {

        if (index == oneCombSize) {
            List<String> com = new ArrayList<>();
            oneComb.forEach(com::add);
            lists.add(com);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= oneCombSize - index; i++) {
            oneComb.set(index, tagsByGroup.get(i));
            combinationUtil(lists, tagsByGroup, oneComb, i + 1, end, index + 1, oneCombSize);
        }
    }

    public List<String> changeTags() {

        if (combinations == null){
            if (groupName.equals("Material"))
                generateMaterialCombinations();
            else
                combinations = generateCombinations(tagsByGroup, defaultCombinationSize);
        }

        List<String> tagsName;
        if (USE_RANDOM_IN_COMBINATOR) {
            currentCombinationNumber = randomizer();
        } else {
            if (++currentCombinationNumber >= combinations.size())
                currentCombinationNumber = 0;
        }
        tagsName = combinations.get(currentCombinationNumber);

        System.out.println(" current number of comb =  " + currentCombinationNumber + " / all size = " + combinations.size());

        progress = generateProgress();

        return tagsName;
    }

    private int randomizer() {
        int r = random.nextInt(combinations.size() - 1);
        if (r == currentCombinationNumber)
            randomizer();
        currentCombinationNumber = r;
        return r;
    }

    private int generateProgress() {
        return ((currentCombinationNumber + 1)*100)/combinations.size();
    }


}
