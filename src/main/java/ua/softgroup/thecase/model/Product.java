package ua.softgroup.thecase.model;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Set;

/**
 * Created by java-1-03 on 24.07.2017.
 */
@Data
@Document/*(collection = "items")*/
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Product extends Item {

    @Builder
    private Product(String name, List<String> imageUrls, String brand, String category,
                    String link, Set<String> tagsList, Price price,
                    boolean isPublished, String type, String source){

        super(name, imageUrls, brand, category, link, tagsList, price, source);

        this.isPublished=isPublished;
        this.type=type;

    }

//    public boolean productEqual(Product newProduct) {
//        return getName().equals(newProduct.getName())
//                && getImageUrls().equals(newProduct.getImageUrls())
//                && getLink().equals(newProduct.getLink())
//                && getSource().equals(newProduct.getSource())
//                && getBrand().equals(newProduct.getBrand())
//                && getTagsList().equals(newProduct.getTagsList())
//                && getCategory().equals(newProduct.getCategory())
//                && getType().equals(newProduct.getType());
//    }


    @Override
    public String toString() {
        return "Product{" + super.toString() +  '}';
    }
}
