package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by java-1-03 on 16.09.2017.
 */
@Data
@NoArgsConstructor
public class CaseChange {

    private String ownerEmail;
    private String whichChange;
    private String whoChangeEmail;
    private long changedAt;

    public static final String ADD_ITEM = "to case added item - ";
    public static final String REMOVE_ITEM = "from case removed item - ";
    public static final String RENAME_CASE = "case renamed to -";
    public static final String CHANGE_COUNT = "changed count of item in case";
    public static final String CHANGE_COUNT_AND_PRICE = "changed count and/or price of item in case";
    public static final String INVITE_USER_TO_CASE = "to case invited user - ";

    public CaseChange(String ownerEmail, String whichChange, String whoChangeEmail) {
        this.ownerEmail = ownerEmail;
        this.whichChange = whichChange;
        this.whoChangeEmail = whoChangeEmail;
        this.changedAt = System.currentTimeMillis();
    }
}
