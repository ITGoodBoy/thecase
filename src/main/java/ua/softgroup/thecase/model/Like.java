package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Created by java-1-03 on 14.09.2017.
 */
@Data
@NoArgsConstructor
public class Like {

    private String id;
    @DBRef
    private Item item;
    @DBRef
    private User liker;

    public Like(Item item, User liker) {
        this.item = item;
        this.liker = liker;
    }


}
