package ua.softgroup.thecase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by java-1-03 on 29.08.2017.
 */
@ToString
@NoArgsConstructor
public class Funnel {
    @Id
    @Setter
    @Getter
    @JsonIgnore
    private String id;
    @JsonIgnore
    @DBRef
    @Getter
    @Setter
    private User user;
    @JsonIgnore
    @DBRef
    @Setter
    private Group group;
    @JsonIgnore
    @Getter
    @Setter
    private List<String> tagsByGroup;
    @Setter
    @Getter
    private List<String> currentCombination;

    @JsonIgnore
    private Combinator combinator;
    @JsonIgnore
    @Getter
    @Setter
    private boolean createdByGroup = false;

    public static Funnel createFunnelByItem(User user, Group group, List<String> tagsByGroup, List<String> currentCombination){
        return new Funnel(user, group, tagsByGroup, currentCombination);
    }

    public static Funnel createFunnelByGroup(User user, Group group, List<String> tagsByGroup){
        return new Funnel(user, group, tagsByGroup);
    }

    private Funnel(User user, Group group, List<String> tagsByGroup, List<String> currentCombination) {
        this.user = user;
        this.group = group;
        this.tagsByGroup = tagsByGroup;
        this.currentCombination = currentCombination;
        this.combinator = new Combinator(tagsByGroup, currentCombination, group);
    }

    private Funnel(User user, Group group, List<String> tagsByGroup) {
        this.createdByGroup = true;
        this.user = user;
        this.group = group;
        this.tagsByGroup = tagsByGroup;
        this.combinator = new Combinator(tagsByGroup, group);
        this.currentCombination = combinator.changeTags();
    }

    public String getName(){
        return group.getName();
    }

    public int getProgress(){
        return combinator.getProgress();
    }

    public void changeTags(){
        this.currentCombination = combinator.changeTags();
    }
}
