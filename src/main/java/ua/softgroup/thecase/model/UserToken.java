package ua.softgroup.thecase.model;

import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * @Author ITGoodBoy
 **/
@Getter
public class UserToken {

    @Indexed
    @Id
    private String id;
    @DBRef
    private User user;
    private String token;
    private long expireAt;

    public UserToken(User user, String token, long expireAt) {
        this.user = user;
        this.token = token;
        this.expireAt = expireAt + System.currentTimeMillis();
    }



}


