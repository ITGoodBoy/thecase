package ua.softgroup.thecase.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.service.EmailService;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by java-1-03 on 16.09.2017.
 */
@Component
class Notificator {

    @Autowired
    private EmailService emailService;

    private final AtomicInteger atomicInteger = new AtomicInteger(0);
    private Thread thread = null;
    private Set<CaseChange> caseChanges = new HashSet<>();


    @EventListener
    public void caseHistoryEvent(CaseChange caseChange){

        int init = atomicInteger.incrementAndGet();
        caseChanges.add(caseChange);

        if (thread == null){
            thread = new Thread(() -> {
                try {
                    TimeUnit.MINUTES.sleep(5);
                    if (atomicInteger.get() == init) {
                        System.out.println(" ============= sending changes size = " + caseChanges.size());
                        emailService.sendChangesOnCase(caseChanges);
                        atomicInteger.set(0);
                        caseChanges = new HashSet<>();
                        thread = null;
                    } else {
                        System.out.println(" ============= NOT send");
                        thread = null;
                        caseHistoryEvent(caseChange);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
    }
}
