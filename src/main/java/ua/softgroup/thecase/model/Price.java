package ua.softgroup.thecase.model;

import lombok.Data;

/**
 * Created by java-1-07 on 06.07.2017.
 */
@Data
public class Price {

    private Long amount;
    private String currency;

    public static final String USD = "USD";
    public static final String EUR = "EUR";
    public static final String RUB = "RUB";

    public Price() {
    }

    public Price(Long amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }
}
