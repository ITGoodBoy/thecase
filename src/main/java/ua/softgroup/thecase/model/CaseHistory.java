package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by java-1-03 on 16.09.2017.
 */
@Data
@NoArgsConstructor
@Component
public class CaseHistory {

    @Id
    private String id;
    @DBRef
    private Case aCase;
    private List<CaseChange> caseChangeList = new ArrayList<>();

    public CaseHistory(Case aCase, CaseChange caseChange) {
        this.aCase = aCase;
        this.caseChangeList.add(caseChange);
    }

    public void addCaseChange (CaseChange caseChange){
        caseChangeList.add(caseChange);
    }

}
