package ua.softgroup.thecase.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Created by TLJD Andrew Kobin on 14.07.2017.
 */
public enum Role implements GrantedAuthority {

    USER,
    FREE,
    BASIC,
    PRO,
    PREMIUM,
    ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }

    public static SimpleGrantedAuthority getRole(Role role) {
        return new SimpleGrantedAuthority("ROLE_" + role);
    }

}
