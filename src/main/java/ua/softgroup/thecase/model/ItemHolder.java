package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import ua.softgroup.thecase.utils.Comparators;

@Data
@NoArgsConstructor
public class ItemHolder implements Comparable<ItemHolder> {

    @DBRef
    private Item item;

    private int count;
    private Price price;
    private long addedAt;
    private String editorName;
    private boolean isLiked;

    public ItemHolder(Item item, String editorName) {
        this.item = item;
        this.count = 1;
        if (item.getNativePrice() != null) {
            this.price = new Price(item.getNativePrice().getAmount(), item.getNativePrice().getCurrency());
        }
        this.addedAt = System.currentTimeMillis();
        this.editorName = editorName;
    }

    public ItemHolder(Item item, String editorName, boolean isLiked) {
        this(item, editorName);
        this.isLiked = isLiked;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != this.getClass()) return false;
        try {
            if (item.equals(((ItemHolder) o).getItem())) return true;
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return item != null ? item.hashCode() : 0;
    }


    @Override
    public int compareTo(ItemHolder o) {
        return new Comparators.TimeDescComparator().compare(this, o);
    }

    /*For Frontend*/
    public Object getPrice() {
        return (price == null) ? false : price;
    }

    public void setCount(int count) {
        if (count > 0) {
            this.count = count;
            if (item.getNativePrice() != null) {
                this.price = new Price((count * item.getNativePrice().getAmount()), item.getNativePrice().getCurrency());
            }
        }
    }

}
