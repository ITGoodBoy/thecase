package ua.softgroup.thecase.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * Created by TLJD Andrew Kobin on 04.10.2017.
 */
@Data
@NoArgsConstructor
public class BlackWord {

    @Id
    private String id;
    private String word;

    public BlackWord(String word) {
        this.word = word;
    }
}
