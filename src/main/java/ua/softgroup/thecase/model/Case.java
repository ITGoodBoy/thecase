package ua.softgroup.thecase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import ua.softgroup.thecase.utils.Comparators;
import ua.softgroup.thecase.validate.CaseLive;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by java-1-07 on 13.07.2017.
 */
@Builder
public class Case {

    @Indexed
    @Id
    @CaseLive
    @Getter
    @Setter
    private String id;

    @JsonIgnore
    @DBRef
    @Getter
    @Setter
    private User owner;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    @Builder.Default
    private String description = "";

    @Getter
    @Setter
    @Builder.Default
    private long createdAt = System.currentTimeMillis();

    @Getter
    @Setter
    private TreeSet<ItemHolder> products = new TreeSet<>(new Comparators.TimeDescComparator());
    @Getter
    @Setter
    private TreeSet<ItemHolder> models3D = new TreeSet<>(new Comparators.TimeDescComparator());

    @Getter
    @Setter
    transient private boolean mine;

    @Getter
    @Setter
    transient private int permission;


    public <T extends Item> SortedSet<ItemHolder> getItemHolders(Class<T> tClass) {
        if (tClass.equals(Product.class)) {
            return products;
        } else {
            return models3D;
        }
    }

    @JsonIgnore
    public <T extends Item> List<? extends Item> getItems(Class<T> tClass) {
        List<T> items = new ArrayList<>();
        if (tClass.equals(Product.class)){
            products.forEach(itemHolder -> {
                items.add((T) itemHolder.getItem());
            });
        } else {
            models3D.forEach(itemHolder -> {
                items.add((T) itemHolder.getItem());
            });
        }
        return items;
    }

    @JsonIgnore
    public List<Item> getAllItems(){
        List<Item> items = new ArrayList<>();
        products.forEach(itemHolder -> items.add(itemHolder.getItem()));
        models3D.forEach(itemHolder -> items.add(itemHolder.getItem()));
        return items;
    }

    public void clearProducts() {
        products.clear();
    }

    public void clearModels() {
        models3D.clear();
    }

    public void clearBoth() {
        clearModels();
        clearProducts();
    }

    public int getCountProduct() {
        return products.size();
    }

    public int getCountModel3D() {
        return models3D.size();
    }

    @JsonIgnore
    public List<String> getThumbnails() {
        List<String> list = new ArrayList<>();
        generateThumbnails(list, products);
        generateThumbnails(list, models3D);
        return list;
    }

    private void generateThumbnails(List<String> list, SortedSet<ItemHolder> itemHolders) {
        for (ItemHolder holder : itemHolders) {
            if (list.size() < 3) {
                if(holder.getItem().getImageUrls().size()>0)
                    list.add(holder.getItem().getImageUrls().get(0));
            } else break;
        }
    }



}
