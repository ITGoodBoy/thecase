package ua.softgroup.thecase.model.message;

import lombok.Data;

/**
 * Created by TLJD Andrew Kobin on 25.09.2017.
 */
@Data
public class PermissionResponse {

    private boolean can_create_case;

}
