package ua.softgroup.thecase.model.message;

import lombok.Data;
import ua.softgroup.thecase.model.Item;

/**
 * Created by java-1-03 on 14.09.2017.
 */
@Data
public class ItemResponse {

    private Item item;
    private boolean isLiked;
    private int likesCount = 0;

    public ItemResponse(Item item, boolean isLiked, int likesCount) {
        this.item = item;
        this.isLiked = isLiked;
        this.likesCount = likesCount;
    }
}
