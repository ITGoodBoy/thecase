package ua.softgroup.thecase.model.message;

import lombok.Data;
import ua.softgroup.thecase.model.User;

/**
 * Created by java-1-03 on 12.09.2017.
 */
@Data
public class UserResponse {

    private User user;
    private boolean isFriend;

    public UserResponse(User user, boolean isFriend) {
        this.user = user;
        this.isFriend = isFriend;
    }
}
