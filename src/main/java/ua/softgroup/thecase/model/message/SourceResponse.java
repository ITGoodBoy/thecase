package ua.softgroup.thecase.model.message;

import lombok.Data;

/**
 * Created by java-1-03 on 19.01.2018.
 */
@Data
public class SourceResponse {

    private String source;
    private boolean published;

    public SourceResponse(String source, boolean published) {
        this.source = source;
        this.published = published;
    }
}
