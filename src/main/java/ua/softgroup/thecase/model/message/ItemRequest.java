package ua.softgroup.thecase.model.message;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ua.softgroup.thecase.model.Price;

import java.util.List;

/**
 * Created by TLJD Andrew Kobin on 31.08.2017.
 */
@Data
@NoArgsConstructor
@ToString
public class ItemRequest {
    private String name;
    private boolean published;
    private boolean isPublishedBySource;
    private String category;
    private String type;
    private String url;
    private String brand;
    private List<String> tags;
    private List<String> uploadImgs;
    private List<String> imgs;
    private Price price;
    private String file;
    private String fileType;
}
