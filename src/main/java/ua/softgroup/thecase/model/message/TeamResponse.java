package ua.softgroup.thecase.model.message;

import lombok.Data;
import ua.softgroup.thecase.model.User;

import java.util.List;

/**
 * Created by java-1-03 on 21.09.2017.
 */
@Data
public class TeamResponse {

    private String caseId;
    private List<TeamUser> users;

    public TeamResponse(String caseId, List<TeamUser> users) {
        this.caseId = caseId;
        this.users = users;
    }
}
