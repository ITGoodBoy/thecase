package ua.softgroup.thecase.model.message;

import lombok.Data;
import ua.softgroup.thecase.model.Case;

import java.util.List;

/**
 * Created by java-1-03 on 07.08.2017.
 */

@Data
public class ShortCase {

    private String id;
    private String name;
    private List<String> imageUrls;
    private int productsCount;
    private int models3dCount;
    private boolean isActive;
    private long createdAt;
    private String ownerName;

    private boolean isMine;
    private boolean isConfirmed;
    private int permissionNumber;

    // TODO: 11.09.2017 create permissions in this class on booleans

    public ShortCase(Case aCase, boolean isActive) {
        this.id = aCase.getId();
        this.name = aCase.getName();
        this.imageUrls = aCase.getThumbnails();
        this.productsCount = aCase.getCountProduct();
        this.models3dCount = aCase.getCountModel3D();
        this.isActive = isActive;
        this.createdAt = aCase.getCreatedAt();
        this.ownerName = aCase.getOwner().getUsername();
        this.isMine = true;
        this.isConfirmed = true;
    }

    public ShortCase(Case aCase, boolean isActive, boolean isConfirmed, int permissionNumber) {
        this.id = aCase.getId();
        this.name = aCase.getName();
        this.imageUrls = aCase.getThumbnails();
        this.productsCount = aCase.getCountProduct();
        this.models3dCount = aCase.getCountModel3D();
        this.isActive = isActive;
        this.createdAt = aCase.getCreatedAt();
        this.ownerName = aCase.getOwner().getUsername();
        this.isMine = false;
        this.isConfirmed = isConfirmed;
        this.permissionNumber = permissionNumber;
    }
}
