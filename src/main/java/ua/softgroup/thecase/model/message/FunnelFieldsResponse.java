package ua.softgroup.thecase.model.message;

import lombok.Data;
import ua.softgroup.thecase.model.Group;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by java-1-03 on 17.01.2018.
 */
@Data
public class FunnelFieldsResponse {

    private String id;
    private String name;
    private boolean activated;

    private Map<String, Boolean> tags = new HashMap<>();

    public FunnelFieldsResponse(Group group, List<String> tagsByGroup, List<String> activeTags) {
        this.id = group.getId();
        this.name = group.getName();

        if (activeTags.isEmpty()){
            this.activated = false;

            this.tags = tagsByGroup.stream().collect(Collectors.toMap(s -> s, s -> false));

        } else {
            this.activated = true;
            tagsByGroup.forEach(s -> {
                if (activeTags.contains(s))
                    this.tags.put(s, true);
                else
                    this.tags.put(s, false);
            });
        }
    }
}
