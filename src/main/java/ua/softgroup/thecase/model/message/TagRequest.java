package ua.softgroup.thecase.model.message;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by TLJD Andrew Kobin on 31.08.2017.
 */

@Data
@NoArgsConstructor
@ToString
public class TagRequest {

    private int id;
    private String text;

}
