package ua.softgroup.thecase.model.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import ua.softgroup.thecase.model.Filter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by java-1-03 on 28.09.2017.
 */
@Data
public class FilterFieldsResponse {

    @Data
    private class OneElementResponse{
        private boolean used;
        private Map<String, Boolean> content;

        OneElementResponse(Map<String, Boolean> content) {
            this.used = isUsed(content);
            this.content = content;
        }

        private boolean isUsed(Map<String, Boolean> stringBooleanMap) {
            return stringBooleanMap.entrySet().stream().anyMatch(Map.Entry::getValue);
        }
    }

    private static final String USED = "used";

    @JsonIgnore
    private Map<String, Boolean> categoryMap = new HashMap<>();
    @JsonIgnore
    private Map<String, Boolean> typeMap = new HashMap<>();
    @JsonIgnore
    private Map<String, Boolean> brandMap = new LinkedHashMap<>();
    @JsonIgnore
    private Map<String, Boolean> sourceMap = new HashMap<>();

    private OneElementResponse category;
    private OneElementResponse type;
    private OneElementResponse brand;
    private OneElementResponse source;

    public FilterFieldsResponse(List<String> type, List<String> category, List<String> source, List<String> brand, Filter filter) {

        type.forEach(s -> this.typeMap.put(s, filter.getType().contains(s)));

        category.forEach(s -> this.categoryMap.put(s, filter.getCategory().contains(s)));

        System.out.println(" sources count = " + source.size());
        filter.getSource().forEach(s -> this.sourceMap.put(s, true));
        source.forEach(s -> {
            if (!filter.getSource().contains(s))
                this.sourceMap.put(s, filter.getSource().contains(s));
        });

        System.out.println(" brands count = " + brand.size());
        filter.getBrand().forEach(s -> this.brandMap.put(s, true));
        brand.forEach(s -> {
            if (!filter.getBrand().contains(s))
                this.brandMap.put(s, false);
        });

        generateResponse();
    }

    public FilterFieldsResponse(List<String> type, List<String> category, List<String> source, List<String> brand) {
        type.forEach(s -> this.typeMap.put(s, false));
        category.forEach(s -> this.categoryMap.put(s, false));
        source.forEach(s -> this.sourceMap.put(s, false));
        brand.forEach(s -> this.brandMap.put(s, false));
        generateResponse();
    }

    private void generateResponse() {
        this.category = new OneElementResponse(categoryMap);
        this.type = new OneElementResponse(typeMap);
        this.brand = new OneElementResponse(brandMap);
        this.source = new OneElementResponse(sourceMap);
    }
}
