package ua.softgroup.thecase.model.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.softgroup.thecase.model.Tag;

import java.util.List;

/**
 * Created by TLJD Andrew Kobin on 06.09.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagByGroupResponce {
    private String name;
    private boolean published;
    private List<Tag> tags;
}
