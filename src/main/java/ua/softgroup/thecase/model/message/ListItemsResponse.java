package ua.softgroup.thecase.model.message;

import lombok.Data;
import ua.softgroup.thecase.model.Item;

import java.util.List;

/**
 * Created by java-1-03 on 06.09.2017.
 */
@Data
public class ListItemsResponse {

    private long count;
    private List<ItemResponse> items;

    public ListItemsResponse(long count, List<ItemResponse> items) {
        this.count = count;
        this.items = items;

    }


}
