package ua.softgroup.thecase.model.message;

import lombok.Data;
import ua.softgroup.thecase.model.User;

/**
 * Created by java-1-03 on 18.10.2017.
 */
@Data
public class TeamUser {

    private User user;
    private int permissionNumber;

    public TeamUser(User user, int permissionNumber) {
        this.user = user;
        this.permissionNumber = permissionNumber;
    }
}
