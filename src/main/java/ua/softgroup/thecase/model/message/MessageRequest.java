package ua.softgroup.thecase.model.message;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by java-1-03 on 13.10.2017.
 */
@Data
@NoArgsConstructor
@ToString
public class MessageRequest {

    private String file;
    private String fileName;
    private String message;
    private String sender;

}
