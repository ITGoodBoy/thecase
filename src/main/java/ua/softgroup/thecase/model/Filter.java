package ua.softgroup.thecase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.*;

@Data
@NoArgsConstructor
public class Filter {

    @Id
    @JsonIgnore
    private String id;

    @DBRef
    @JsonIgnore
    private User user;
    private Set<String> category = new HashSet<>();
    private Set<String> type = new HashSet<>();
    private Set<String> brand = new HashSet<>();
    private Set<String> source = new HashSet<>();

    public Filter(User user) {
        this.user = user;
    }

    public HashMap<String, List<String>> generateMap(){
        HashMap<String, List<String>> map = new HashMap<>();
        if (category.size() != 0)
            map.put("category", new ArrayList<>(category));
        if (type.size() != 0)
            map.put("type", new ArrayList<>(type));
        if (brand.size() != 0)
            map.put("brand", new ArrayList<>(brand));
        if (source.size() != 0)
            map.put("source", new ArrayList<>(source));
        if (map.size()<1)
            return null;
        return map;
    }
}
