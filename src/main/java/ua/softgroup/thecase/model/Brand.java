//package ua.softgroup.thecase.model;
//
//import lombok.Data;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.index.Indexed;
//import org.springframework.data.mongodb.core.mapping.Document;
//
///**
// * Created by java-1-07 on 06.07.2017.
// */
//@Data
//@Document(collection = "brands")
//public class Brand {
//
//    @Id
//    @Indexed
//    private String id;
//    private String name;
//    private String description;
//
//    public Brand() {
//    }
//
//    public Brand(String name, String description) {
//        this.name = name;
//        this.description = description;
//    }
//}
