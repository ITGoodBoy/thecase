package ua.softgroup.thecase.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

import java.util.*;

/**
 * Created by TLJD Andrew Kobin on 06.07.2017.
 */

@Getter
@Document(collection = "items")
@ToString
public abstract class Item {

    public enum Field{
        type,
        category,
        source,
        brand
    }

    public enum Type{
        model3D,
        product
    }


    @Id
    @Indexed
    @Setter
    private String id;
    @Setter
    private String name;
    @Setter
    private boolean isChangedByAdmin;

    @Setter
    private List<String> imageUrls;

    @TextScore Float score;
    @Setter
    @TextIndexed (weight = 3)
    private Set<String> tagsList;

    @Setter
    @JsonIgnore
    private String brand;

    @JsonIgnore
    @Setter
    private String category;

    @Setter
    private String link;

    @Getter @Setter
    private String source;

    @Setter
    protected String type;

    @JsonIgnore
    @Setter
    protected boolean isPublished;

    @JsonIgnore
    @Setter
    private long createAt = System.currentTimeMillis();
    @JsonIgnore
    @Setter
    private long updateAt;

    @Setter
    private Price price;

    @JsonIgnore
    @Setter
    protected boolean isPublishedBySource = true;

    public Map<String, List<String>> getInfo(){
        Map<String, List<String>> info = new LinkedHashMap<>();

        List<String> categories = new ArrayList<>();
        categories.add((category == null) ? "N/A" : category);
        info.put("Category", categories);

        List<String> types = new ArrayList<>();
        types.add((type == null) ? "N/A" : type);
        info.put("Type", types);

        List<String> brands = new ArrayList<>();
        brands.add((brand == null) ? "N/A" : brand);
        info.put("Brand", brands);

        List<String> resource = new ArrayList<>();
        resource.add((source == null) ? "N/A" : source);
        info.put("Source", resource);

        return info;
    }


    public Object getPrice(){
        return (price==null)?false:price;
    }

    @JsonIgnore
    public Price getNativePrice(){
        return price;
    }


    public Item() {
    }

    public Item(String name, List<String> imageUrls, String brand, String category,
                String link, Set<String> tagsList, Price price, String source) {
        this.name = name;
        this.imageUrls = imageUrls;
        this.brand = brand;
        this.category = category;
        this.link = link;
        this.tagsList = tagsList;
        this.price = price;
        this.source = source;
    }

    public Item(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return id.equals(item.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public <T extends Item> boolean itemEqual(T newItem) {
        return getName().equals(newItem.getName())
                && getImageUrls().equals(newItem.getImageUrls())
                && getLink().equals(newItem.getLink())
                && getSource().equals(newItem.getSource())
                && getBrand().equals(newItem.getBrand())
                && getTagsList().equals(newItem.getTagsList())
                && getCategory().equals(newItem.getCategory())
                && getType().equals(newItem.getType());
    }
}
