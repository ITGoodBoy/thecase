package ua.softgroup.thecase.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Created by ITGoodBoy
 */

@Data
public class RolePermission {

    @Id
    @Indexed
    private String id;
    private Role role;
    private int casesMaxCount;
    private boolean possibilitySendCaseToEmail;
    private int funnelCountPerDay;

    public RolePermission(Role role, int casesMaxCount, boolean possibilitySendCaseToEmail,
                          int funnelCountPerDay) {
        this.role = role;
        this.casesMaxCount = casesMaxCount;
        this.possibilitySendCaseToEmail = possibilitySendCaseToEmail;
        this.funnelCountPerDay = funnelCountPerDay;
    }
}
