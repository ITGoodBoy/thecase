package ua.softgroup.thecase.monitor;

import org.slf4j.event.Level;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created by Sergey on 05.07.2017.
 */
@Aspect
@Component
public class ControllerMonitor {

    private final Logger logger = Logger.getLogger(ControllerMonitor.class);
    private long startTime;

    @Before("execution(* ua.softgroup.thecase.controller.*Controller.*(..))")
    public void logBeforeMethodCall(JoinPoint joinPoint) {
        startTime = System.currentTimeMillis();
    }

    @AfterReturning("execution(* ua.softgroup.thecase.controller.*Controller.*(..))")
    public void logAfterReturning(JoinPoint joinPoint) {
        long endTime = System.currentTimeMillis() - startTime;

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        String before = "Method Name: " + method.getName() + ", Args: " + Arrays.toString(joinPoint.getArgs());
        logger.info(before + ", Speed of Execution: " + endTime + " milliseconds" + "(" + endTime/1000.0 + " - seconds)");
    }

    @AfterThrowing("execution(* ua.softgroup.thecase.controller.*Controller.*(..))")
    public void logException(JoinPoint joinPoint) {
        System.out.println("Возникла ошибка в " + joinPoint);
    }



}
