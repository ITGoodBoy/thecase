package ua.softgroup.thecase.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Model3D;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by java-Andr on 10.01.2018.
 * SG JAVA TEAM
 */
@Component
public class Odesd2 extends Parser {

    @Value("${app.odesd2.parser.path}")
    private String FOLDER_PATH;
    @Value("${app.parser.is.active}")
    private boolean parserIsActive;
    @Value("${app.parser.thread.number}")
    private int numberOfThreads;

    @Autowired
    private Model3DRepository repository;

    private AtomicInteger addedCount = new AtomicInteger(0);
    private AtomicInteger updatedCount = new AtomicInteger(0);
    private AtomicInteger hiddenCount = new AtomicInteger(0);

    private static final String FILE_BASE_NAME = "model3d";

    private static final List<ParserLink> PARSER_LINKS = new ArrayList<>();
    static {
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Chair", "http://odesd2.com/en/furniture/chairs"));
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Bar stool", "http://odesd2.com/en/furniture/barstool"));
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Bench", "http://odesd2.com/en/furniture/bench"));
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Lounge chair", "http://odesd2.com/en/furniture/lounge"));

        PARSER_LINKS.add(new ParserLink(Categories.TABLES, "Table", "http://odesd2.com/en/furniture/table"));

        PARSER_LINKS.add(new ParserLink(Categories.SOFAS,"Sofa", "http://odesd2.com/en/furniture/sofas"));

        PARSER_LINKS.add(new ParserLink(Categories.STORAGE, "Shelving system", "http://odesd2.com/en/furniture/shelf"));
        PARSER_LINKS.add(new ParserLink(Categories.STORAGE, "Commode", "http://odesd2.com/en/furniture/commode"));
        PARSER_LINKS.add(new ParserLink(Categories.STORAGE, "Bedsides", "http://odesd2.com/en/furniture/bedsides"));

    }

    public Odesd2(GroupRepository groupRepository, TagRepository tagRepository) {
        super(groupRepository, tagRepository);
    }

    public synchronized void loadModels() throws InterruptedException {
        if (!parserIsActive) return;

        log("START");

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(this::parse);

        executorService.shutdown();
        executorService.awaitTermination(5, TimeUnit.DAYS);

        log(" DONE");
        ParserUtils.notifyAdmin(this.getClass().getSimpleName(), addedCount, updatedCount, hiddenCount);

    }

    private void parse() {
        for (ParserLink link : PARSER_LINKS) {
            Set<String> itemsLinksSet = getItemsLinksSet(link.getUrl());
            for (String itemLink : itemsLinksSet) {
                saveItem(link, itemLink);
            }
        }
    }

    private void saveItem(ParserLink link, String itemLink) {
        try {
            Document itemPage = Jsoup.connect(itemLink).timeout(10000).get();
            Model3D item = buildItem(itemPage, link);
            item.setUpdateAt(item.getCreateAt());
            setupKeywords(item);
            addOrUpdateItem(addedCount, updatedCount, item, repository);
            log("Saved: "+item.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Model3D buildItem(Document itemPage, ParserLink link) {
        return Model3D.builder()
                .name(getName(itemPage))
                .category(link.getCategory())
                .type(link.getType())
                .brand(itemPage.getElementsByClass("des-btn").html())
                .source("Odesd2")
                .link(link.getUrl())
                .imageUrls(getImageUrls(itemPage))
                .tagsList(new HashSet<>(getTags(itemPage)))
                .isPublished(true)
                .build();
    }

    private String getName(Document itemPage) {
        return itemPage.getElementsByClass("product-hover").select("h1.name").text();
    }

    private List<String> getTags(Document itemPage) {
        try {
            List<String> tags = Arrays.asList(itemPage.getElementsByClass("material-block").first().ownText().split(","));
            return tags;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    private List<String> getImageUrls(Document itemPage) {
        ArrayList<String> imageUrls = new ArrayList<>();
        Elements images = itemPage.getElementById("images").select("img");
        imageUrls.addAll(images.stream().map(image -> image.absUrl("src")).collect(Collectors.toList()));
        return imageUrls;
    }

    @Override
    Set<String> getItemsLinksSet(String url) {
        Set<String> linkList = new HashSet<>();
        try {
            Document document = Jsoup.connect(url).timeout(15000).get();
            linkList.addAll(getItemLinks(document));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return linkList;
    }

    private List<String> getItemLinks(Document document) {
        List<String> links = new ArrayList<>();

        for (Element element : document.getElementsByClass("item").select("a")) {
            links.add(element.attr("abs:href"));
        }
        return links;
    }

    @Override
    void log(String s) {
        System.out.println(this.getClass().getSimpleName() + " -------> " + s);
    }
}
