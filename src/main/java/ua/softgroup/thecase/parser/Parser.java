package ua.softgroup.thecase.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Tag;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.ItemRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by TLJD Andrew Kobin on 11.10.2017.
 */

public abstract class Parser {

    private final GroupRepository groupRepository;
    private final TagRepository tagRepository;

    public Parser(GroupRepository groupRepository, TagRepository tagRepository) {
        this.groupRepository = groupRepository;
        this.tagRepository = tagRepository;
    }

    int saveItemsInFiles(ParserLink parserLink, String folderPath, String fileBaseName, int numberOfThreads) throws InterruptedException {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        try {
            Set<String> links = getItemsLinksSet(parserLink.getUrl());
            for (String itemLink : links) {
                executorService.submit(() -> {
                    Document document = getDocumentFromLink(itemLink);
                    if(atomicInteger.get() % 100 == 0) log("save to file:  "+ atomicInteger.get()+" / "+links.size());
                    if (document == null) {
                        System.out.println("document is null");
                    } else {
                        ParserUtils.saveTextToFile(folderPath, document.outerHtml(), fileBaseName + "#" + atomicInteger.incrementAndGet());
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        executorService.awaitTermination(5, TimeUnit.DAYS);

        return atomicInteger.get();
    }

    abstract Set<String> getItemsLinksSet(String url);

    protected void setupKeywords(Item item) {
        Set<String> permanentTags = new HashSet<>();

        for (String tag : item.getTagsList()) {
            tag = tag.trim();
            increaseTagCountOrCreate(tag);
            for (Map.Entry<String, String[]> pair : ParserUtils.groupInTagsMap.entrySet()) {
                for (String keyWord : pair.getValue()) {
                    if (tag.toLowerCase().contains(keyWord.toLowerCase())) {
                        permanentTags.add(keyWord);
                        increaseTagCountOrCreate(keyWord, pair.getKey());
                    }
                }
            }
        }
        item.getTagsList().addAll(permanentTags);

        item.getTagsList().remove("");
    }

    private void increaseTagCountOrCreate(String tag) {
        if (tagRepository.existsByName(tag)) {
            Tag tagFromBd = tagRepository.findByName(tag);
            tagFromBd.setFrequency(tagFromBd.getFrequency() + 1);
            tagRepository.save(tagFromBd);
        } else {
            tagRepository.save(new Tag(tag, 1));
        }
    }

    private void increaseTagCountOrCreate(String tag, String groupName) {
        if (tagRepository.existsByName(tag)) {
            Tag tagFromBd = tagRepository.findByName(tag);
            Group group = groupRepository.findByName(groupName);
            tagFromBd.setGroup(group);
            tagFromBd.setFrequency(tagFromBd.getFrequency() + 1);
            tagRepository.save(tagFromBd);
        } else {
            Tag newTag = new Tag(tag, 1);
            Group group = groupRepository.findByName(groupName);
            newTag.setGroup(group);
            newTag.setCombinable(true);
            tagRepository.save(newTag);
        }
    }

    <T extends Item, E extends ItemRepository<T>> void addOrUpdateItem(AtomicInteger addedCount, AtomicInteger updatedCount, T item, E repository) {
        if (repository.existsByLink(item.getLink())) {
            T itemFromDB = repository.findByLink(item.getLink());
            item.setCreateAt(itemFromDB.getCreateAt());
            if (itemFromDB.isChangedByAdmin()) {
                item.setId(itemFromDB.getId());
                item.setType(itemFromDB.getType());
                item.setCategory(itemFromDB.getCategory());
                item.setTagsList(itemFromDB.getTagsList());
                item.setPublished(itemFromDB.isPublished());
                repository.save(item);
                if (item.isPublished()) updatedCount.incrementAndGet();
            } else if (!item.itemEqual(itemFromDB)) {
                item.setId(itemFromDB.getId());
                repository.save(item);
                if (item.isPublished()) updatedCount.incrementAndGet();
            }
        } else {
            repository.save(item);
            if (item.isPublished()) addedCount.incrementAndGet();
        }
    }

    private Document getDocumentFromLink(String link) {
        try {
            return Jsoup.connect(link).timeout(15000).get();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    abstract void log(String s);
}
