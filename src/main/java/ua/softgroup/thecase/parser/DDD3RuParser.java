package ua.softgroup.thecase.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Model3D;
import ua.softgroup.thecase.model.Price;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.ItemRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by java-1-07 on 07.08.2017.
 */
@Component
public class DDD3RuParser extends Parser{

    @Value("${app.ddd3Ru.parser.path}")
    private String FOLDER_PATH;

    @Value("${app.parser.is.active}")
    private boolean parserIsActive;

    @Value("${app.parser.thread.number}")
    private int numberOfThreads;

//    public static final AtomicBoolean PARSER_IS_FINISHED = new AtomicBoolean(false);
//
//    private final static String BASE_URL = "https://3ddd.ru";

    private static final List<ParserLink> PARSER_LINKS = new ArrayList<>();
    static {
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Armchair", "https://3dsky.org/3dmodels/category/kresla"));
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Lounge chair", "https://3dsky.org/search?query=Lounge+chair"));
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Bar stool", "https://3dsky.org/search?query=bar+stool"));
//
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Beanbag", "https://3dsky.org/search?query=beanbag"));
//
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Pouf", "https://3dsky.org/search?query=Pouf"));
//
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Bench", "https://3dsky.org/search?query=bench"));
//
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Chair", "https://3dsky.org/3dmodels/category/stulia"));
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Stool", "https://3dsky.org/search?query=stool"));
//
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Easy chair", "https://3dsky.org/search?query=easy+chair"));
        PARSER_LINKS.add(new ParserLink(Categories.SEATING, "Ottoman", "https://3dsky.org/search?query=Ottoman"));
//
//        //TABLES

        PARSER_LINKS.add(new ParserLink("TABLES","Table", "https://3dsky.org/3dmodels/category/stoli"));
        PARSER_LINKS.add(new ParserLink("TABLES","Coffee table", "https://3dsky.org/search?query=coffee+table"));
        PARSER_LINKS.add(new ParserLink("TABLES","Console table", "https://3dsky.org/search?query=Console+table"));
        PARSER_LINKS.add(new ParserLink("TABLES","Dining table", "https://3dsky.org/search?query=Dining+table"));
        PARSER_LINKS.add(new ParserLink("TABLES","Side table", "https://3dsky.org/search?query=Side+table"));
        PARSER_LINKS.add(new ParserLink("TABLES","Writing desk", "https://3dsky.org/search?query=Writing+desk"));
        PARSER_LINKS.add(new ParserLink("TABLES","Dressing table", "https://3dsky.org/search?query=Dressing+table"));
        PARSER_LINKS.add(new ParserLink("TABLES","Nesting table", "https://3dsky.org/search?query=Nesting+table"));
        PARSER_LINKS.add(new ParserLink("TABLES","Occasional table", "https://3dsky.org/search?query=Occasional+table"));
//
//        //SOFAS
        PARSER_LINKS.add(new ParserLink("SOFAS","Sofa", "https://3dsky.org/3dmodels/category/divani"));
        PARSER_LINKS.add(new ParserLink("SOFAS","Couch", "https://3dsky.org/search?query=Couch"));
        PARSER_LINKS.add(new ParserLink("SOFAS","Couch", "https://3dsky.org/search?query=daybed"));
        PARSER_LINKS.add(new ParserLink("SOFAS","Couch", "https://3dsky.org/search?query=day+bed"));
        PARSER_LINKS.add(new ParserLink("SOFAS","Sectional sofa", "https://3dsky.org/search?query=Sectional+sofa"));
//
//        //SLEEPING
        PARSER_LINKS.add(new ParserLink("SLEEPING","Bed", "https://3dsky.org/3dmodels/category/krovati"));
//
//        //STORAGE
        PARSER_LINKS.add(new ParserLink("STORAGE","Bookcase", "https://3dsky.org/search?query=Bookcase"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Chest of drawers", "https://3dsky.org/search?query=Chest+of+drawers"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Display cabinet", "https://3dsky.org/search?query=Display+cabinet"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Highboard", "https://3dsky.org/search?query=Highboard"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Shelving system", "https://3dsky.org/search?query=Shelving+system"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Sideboard", "https://3dsky.org/search?query=Sideboard"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Storage wall", "https://3dsky.org/search?query=Storage+wall"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Wardrobe", "https://3dsky.org/3dmodels/category/shakfi"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Dressers", "https://3dsky.org/search?query=Dressers"));

    }



    private static final String FILE_BASE_NAME = "model3d";

    @Autowired
    private ItemRepository<Model3D> itemRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private TagRepository tagRepository;

    public DDD3RuParser(GroupRepository groupRepository, TagRepository tagRepository) {
        super(groupRepository, tagRepository);
    }

    @SuppressWarnings("Duplicates")
//    @Scheduled(cron = "${app.cron.expression.every.day.in.2.hour.AM}")
    public synchronized void loadModels() throws InterruptedException {
        if (!parserIsActive) return;

        AtomicInteger addedCount = new AtomicInteger(0);
        AtomicInteger updatedCount = new AtomicInteger(0);
        AtomicInteger hiddenCount = new AtomicInteger(0);

        log("START");

        PARSER_LINKS.forEach(parserLink -> {
            log(parserLink.getCategory() + " | " + parserLink.getType());
            try {
                int lastItem = saveItemsInFiles(parserLink, FOLDER_PATH, FILE_BASE_NAME, numberOfThreads);
                ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);

                for (int i = 1; i <= lastItem; i++) {
                    int finalI = i;
                    executorService.execute(() -> {
                        if (finalI%100==0) System.out.println("DDD3Parser " + ((float) (finalI * 100) / lastItem) + "%");
                        String htmlModel3dName = FOLDER_PATH + "/" + FILE_BASE_NAME + "#" + finalI + "/" + FILE_BASE_NAME + "#" + finalI + ".html";
                        Document document = null;
                        try {
                            document = Jsoup.parse(new File(htmlModel3dName), "UTF-8");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Model3D model3D =
                                Model3D.builder()
                                        .name(getModel3dName(document))
                                        .category(parserLink.getCategory())
                                        .type(parserLink.getType())
                                        .brand(getModel3dDesigner(document))
                                        .source("3ddd")
                                        .link(getModel3dLink(document))
                                        .imageUrls(getModel3dImageUrls(document))
                                        .tagsList(new HashSet<>(getModel3dTags(document)))
                                        .price(getModel3dPrice(document))
                                        .isPublished(true)
                                        .build();

                        model3D.setUpdateAt(model3D.getCreateAt());

                        Collections.addAll(model3D.getTagsList(),
                                model3D.getName().trim().replaceAll(" +", " ").split(" "));

                        model3D.getTagsList().add(getModel3dSubCategory(document));
                        model3D.getTagsList().add(getModel3dCategory(document));

                        super.setupKeywords(model3D);

                        addOrUpdateItem(addedCount, updatedCount, model3D, itemRepository);
                    });
                }
                executorService.shutdown();
                executorService.awaitTermination(5, TimeUnit.DAYS);

            }catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        log(" DONE");
        ParserUtils.notifyAdmin("3ddd", addedCount, updatedCount, hiddenCount);
    }

    @Override
    Set<String> getItemsLinksSet(String url) {
        Set<String> linkList = new HashSet<>();
        String nextLink = url;
        try {
            do {
                Document document = Jsoup.connect(nextLink).timeout(15000).get();
                linkList.addAll(getModelLinks(document));
                nextLink = document.select("div.paginator_block").select("ul.paginator").select("li.next").select("a").attr("abs:href");
                System.out.println("next: " + nextLink);
            }while (nextLink!=null&&!nextLink.isEmpty());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return linkList;
    }

    @Override
    void log(String s) {
        System.out.println("3DDD Parser -------> " + s);
    }

    private static String getModel3dLink(Document document) {
        String link = "https://3ddd.ru" + document.getElementById("bredcramps").select("ul").select("li").select("a").last().attr("href");
        if (link.equals("https://3ddd.ru")) return "link not found";
        return link;
    }

    private static List<String> getModelLinks(Document pageDocument) {
        List<String> modelLinks = new ArrayList<>();
        for (Element element : pageDocument.select("div.model_list").select("div.item").select("a.link")) {
            modelLinks.add(element.attr("abs:href"));
        }
        return modelLinks;
    }

    private static Price getModel3dPrice(Document model3dDocument) {
        try {
            for (Element element : model3dDocument.select("div.characteristics_block").select("a")) {
                if (element.hasAttr("class")) {
                    String priceFromHtml = element.html();
                    long amount = Long.parseLong(priceFromHtml.substring(0, priceFromHtml.indexOf(' ')));
                    String currency = priceFromHtml.substring(priceFromHtml.indexOf(' ') + 1, priceFromHtml.length());
                    return new Price(amount, currency);
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    private static String getModel3dSubCategory(Document model3dDocument) {
        return model3dDocument.getElementById("bredcramps").select("li").select("a").get(1).html();
    }

    private static String getModel3dCategory(Document model3dDocument) {
        return model3dDocument.getElementById("bredcramps").select("li").select("a").first().html();
    }

    private static Set<String> getModel3dTags(Document model3dDocument) {
        Set<String> model3dTags = new HashSet<>();
        for (Element element : model3dDocument.select("li.icon_tags").select("a")) {
            model3dTags.add(element.html());
        }
        return model3dTags;
    }

    private static String getModel3dName(Document model3dDocument) {
        return model3dDocument.select("div.user").select("div.title").select("h1").html();
    }

    private static String getModel3dDescription(Document model3dDocument) {
        return model3dDocument.select("div.show_pre_description").html();
    }

    private static String getModel3dDesigner(Document model3dDocument) {
        return model3dDocument.select("div.username.icon_autor_dark").select("a").html();
    }

    private static List<String> getModel3dImageUrls(Document model3dDocument) {
        List<String> model3dImageUrls = new ArrayList<>();
        for (Element element : model3dDocument.select("ul.little_fotos").select("li").select("a")) {
            model3dImageUrls.add(element.attr("href"));
        }
        if (model3dImageUrls.size() == 0) {
            model3dImageUrls.add(model3dDocument.select("div.big_foto").select("img").attr("src"));
        }

        return model3dImageUrls;
    }
}
