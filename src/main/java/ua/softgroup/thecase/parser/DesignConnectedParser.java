package ua.softgroup.thecase.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Model3D;
import ua.softgroup.thecase.model.Price;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by java-1-07 on 24.07.2017.
 */
@Component
public class DesignConnectedParser {

    public static final AtomicBoolean PARSER_IS_FINISHED = new AtomicBoolean(false);

    private final Model3DRepository itemRepository;
    private final GroupRepository groupRepository;
    private final TagRepository tagRepository;

    @Value("${app.designconnected.parser.path}")
    private String FOLDER_PATH;
    @Value("${app.parser.is.active}")
    private boolean parserIsActive;
    private static final String FILE_BASE_NAME = "model3d";

    @Autowired
    public DesignConnectedParser(Model3DRepository itemRepository, GroupRepository groupRepository, TagRepository tagRepository) {
        this.itemRepository = itemRepository;
        this.groupRepository = groupRepository;
        this.tagRepository = tagRepository;
    }

    private void driverQuit(WebDriver driver) {
        try {
            driver.quit();
        } catch (Exception e) {
        }
    }

    @SuppressWarnings("Duplicates")
    public synchronized void loadModels() throws InterruptedException, IOException {
        if (!parserIsActive) return;
        AtomicInteger addedCount = new AtomicInteger(0);
        AtomicInteger updatedCount = new AtomicInteger(0);
        AtomicInteger hiddenCount = new AtomicInteger(0);

        log("START");
        ParserLink.PARSER_LINKS.forEach(parserLink -> {
            log(parserLink.getCategory() + " | " + parserLink.getType());
            saveModels3dInOneFile(parserLink);
            log("save in one file done");
            try {
                Document mainDocument = Jsoup.parse(new File(parserLink.getFileName()), "UTF-8");
                log("save model to files start");
                int lastModel3d = saveModels3dInFiles(16, mainDocument);
                log("save model to files done");
                log("lastModel3d="+lastModel3d);
                ExecutorService executorService = Executors.newFixedThreadPool(2);
                for (int i = 1; i <= lastModel3d; i++) {
                    int finalI = i;
                    executorService.execute(() -> {
                        if (finalI%100==0) System.out.println("DesignConnectedParser " + ((float) (finalI * 100) / lastModel3d) + "%");
                        String htmlModel3dName = FOLDER_PATH + "/" + FILE_BASE_NAME + "#" + finalI + "/" + FILE_BASE_NAME + "#" + finalI + ".html";
                        Document document = null;
                        try {
                            document = Jsoup.parse(new File(htmlModel3dName), "UTF-8");

                            Model3D model3d =
                                    Model3D.builder()
                                            .name(getModel3Name(document))
                                            .category(parserLink.getCategory())
                                            .type(parserLink.getType())
                                            .brand(getModel3dBrand(document))
                                            .source("Designconnected")
                                            .link(getModel3dLink(document))
                                            .price(new Price(0L, "No Currency"))
                                            .designer(getModel3dDesigner(document))
                                            .imageUrls(getModel3dImageUrls(document))
                                            .isPublished(true)
                                            .build();
                            model3d.setUpdateAt(model3d.getCreateAt());

                            Set<String> model3dTags = new HashSet<>();
                            model3dTags.add(model3d.getName());
                            model3dTags.add(model3d.getBrand());
                            model3dTags.add(parserLink.getCategory());
                            model3dTags.add(parserLink.getType());
                            model3dTags.add(model3d.getDesigner());
                            model3d.setTagsList(new HashSet<>(model3dTags));

                            if (itemRepository.existsByLink(model3d.getLink())) {
                                Model3D model3DFromBD = itemRepository.findByLink(model3d.getLink());
                                model3d.setCreateAt(model3DFromBD.getCreateAt());
                                if (model3DFromBD.isChangedByAdmin()) {
                                    model3d.setId(model3DFromBD.getId());
                                    model3d.setType(model3DFromBD.getType());
                                    model3d.setCategory(model3DFromBD.getCategory());
                                    model3d.getTagsList().addAll(model3DFromBD.getTagsList());
                                    itemRepository.save(model3d);
                                    if (model3d.isPublished()) updatedCount.incrementAndGet();
                                }
                                else if (!model3d.model3DEqual(model3DFromBD)) {
                                    model3d.setId(model3DFromBD.getId());
                                    itemRepository.save(model3d);
                                    if (model3d.isPublished()) updatedCount.incrementAndGet();
                                }
                            } else {
                                itemRepository.save(model3d);
                                if (model3d.isPublished()) addedCount.incrementAndGet();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }

                executorService.shutdown();
                executorService.awaitTermination(5, TimeUnit.DAYS);


//                log("clean started");
//                ParserUtils.cleanDeletedModels3dFromDatabase(itemRepository);
//                log(" clean end");

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
        log(" DONE");
        ParserUtils.notifyAdmin("DesignConnected", addedCount, updatedCount, hiddenCount);

    }

    private void saveModels3dInOneFile(ParserLink parserLink)  {

        WebDriver driver = ParserUtils.webDriverInitialize(parserLink.getUrl());
        WebDriverWait webDriverWait = new WebDriverWait(driver, 100);
        Actions actions = new Actions(driver);

        int timeOut = 15000;
        try {
            for (int i = 0; i < 10000; i++) {
                By by = By.className("load_more_btn");
                List<WebElement> elements = driver.findElements(by);
                System.out.println(elements.size());

                if(elements.size()!=0){
                    WebElement webElement = elements.get(elements.size() - 1);
                    webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
                    actions.moveToElement(webElement).perform();
                    for (int j = 0; j < 30; j++) {
                        actions.sendKeys(Keys.ARROW_DOWN).build().perform();
                    }
                    webElement.click();
                    Thread.sleep(timeOut += 20);
                    ParserUtils.saveTextToFile(driver.getPageSource(), parserLink.getFileName());
                }else{
                    ParserUtils.saveTextToFile(driver.getPageSource(), parserLink.getFileName());
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            driverQuit(driver);
            return;
        }
        driverQuit(driver);
    }

    private String getModel3dLink(Document document) {
        for (Element element : document.select("head").select("meta")) {
            if (element.hasAttr("property") && element.attr("property").equals("og:url")) {
                return element.attr("content");
            }
        }
        return "";
    }

    @SuppressWarnings("Duplicates")
    private static List<String> getModel3dImageUrls(Document document) {
        List<String> model3dImageUrls = new ArrayList<>();
        for (Element element : document.select("div.dc4_bigImgHolder").select("img")) {
            String imageUrl = element.attr("src");
            model3dImageUrls.add(imageUrl);
        }
        return model3dImageUrls;
    }

    private static String getModel3dDesigner(Document document) {
        return document
                .select("div.dcr_product_description_info")
                .select("span.dc4_mdCell")
                .select("span.dc4_mdText")
                .select("span")
                .select("a")
                .html();
    }

    private static String getModel3dBrand(Document document) {
        return document
                .select("h3.dc4_productDesigner")
                .select("a")
                .first()
                .html()
                .replace("amp;", "");
    }

    private static String getModel3Name(Document document) {
        return document
                .select("div.panel-body")
                .select("h2.dc4_productTitle")
                .select("span")
                .html()
                .replace("amp;", "");
    }

    private int saveModels3dInFiles(int numberOfThreads, Document mainDocument) throws InterruptedException {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);

        for (String productLink : getModel3dUrls(mainDocument)) {
            executorService.submit(() -> {
                Document document = getDocumentFromHtml(productLink);
                if (document == null) System.out.println("null");
                else
                    ParserUtils.saveTextToFile(FOLDER_PATH, document.outerHtml(), FILE_BASE_NAME + "#" + atomicInteger.incrementAndGet());
            });
        }
        executorService.shutdown();
        executorService.awaitTermination(5, TimeUnit.DAYS);
        return atomicInteger.get();
    }

    private static Document getDocumentFromHtml(String productLink) {
        try {
            return Jsoup.connect(productLink).timeout(15000).get();
        } catch (Exception e) {
            return null;
        }
    }


    private static List<String> getModel3dUrls(Document document) {
        List<String> model3dUrls = new ArrayList<>();
        Elements elements = document.getElementsByClass("modelName");

        for (Element element : elements) {
            String url = "https://www.designconnected.com" + element.select("a").attr("href");
            model3dUrls.add(url);
        }

        return model3dUrls;
    }

    private void log(String s){
        System.out.println("DC Parser -------> " + s);
    }

}
