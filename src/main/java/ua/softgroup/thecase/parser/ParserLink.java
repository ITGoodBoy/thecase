package ua.softgroup.thecase.parser;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class ParserLink {

    public static final List<ParserLink> PARSER_LINKS = new ArrayList<>();
    static {
        //SEATING
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Armchair",         "https://www.designconnected.com/catalog/3D-Models/all/category/Armchairs__14"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Lounge chair",     "https://www.designconnected.com/catalog/3D-Models/all/category/Lounge-chairs__17"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Bar stool",        "https://www.designconnected.com/catalog/3D-Models/all/category/Bar-stools__16"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Modular seating",  "https://www.designconnected.com/catalog/3D-Models/all/category/Modular-seating__83"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Beanbag",          "https://www.designconnected.com/catalog/3D-Models/all/category/Beanbag__58"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Pouf",             "https://www.designconnected.com/catalog/3D-Models/all/category/Poufs__19"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Bench",            "https://www.designconnected.com/catalog/3D-Models/all/category/Benches__18"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Seating object",   "https://www.designconnected.com/catalog/3D-Models/all/category/Seating-objects__85"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Chair",            "https://www.designconnected.com/catalog/3D-Models/all/category/Chairs__13"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Stool",            "https://www.designconnected.com/catalog/3D-Models/all/category/Stools__15"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Easy chair",       "https://www.designconnected.com/catalog/3D-Models/all/category/Easy-chairs__59"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(),"Ottoman",          "https://www.designconnected.com/catalog/3D-Models/all/category/Footstools--Ottomans__60"));
        //TABLES
        PARSER_LINKS.add(new ParserLink("TABLES","Coffee table", "https://www.designconnected.com/catalog/3D-Models/all/category/Coffee-tables__21"));
        PARSER_LINKS.add(new ParserLink("TABLES","Console table", "https://www.designconnected.com/catalog/3D-Models/all/category/Console-tables__77"));
        PARSER_LINKS.add(new ParserLink("TABLES","Dining table", "https://www.designconnected.com/catalog/3D-Models/all/category/Dining-tables__20"));
        PARSER_LINKS.add(new ParserLink("TABLES","Nesting table", "https://www.designconnected.com/catalog/3D-Models/all/category/Nesting-tables__78"));
        PARSER_LINKS.add(new ParserLink("TABLES","Occasional table", "https://www.designconnected.com/catalog/3D-Models/all/category/Occasional-tables__79"));
        PARSER_LINKS.add(new ParserLink("TABLES","Side table", "https://www.designconnected.com/catalog/3D-Models/all/category/Side-tables__80"));
        PARSER_LINKS.add(new ParserLink("TABLES","Writing desk", "https://www.designconnected.com/catalog/3D-Models/all/category/Writing-desks__22"));
        //SOFAS
        PARSER_LINKS.add(new ParserLink("SOFAS","Sofa", "https://www.designconnected.com/catalog/3D-Models/all/category/Sofas__2"));
        PARSER_LINKS.add(new ParserLink("SOFAS","Couch", "https://www.designconnected.com/catalog/3D-Models/all/category/Daybeds--Sofa-Beds__73"));
        PARSER_LINKS.add(new ParserLink("SOFAS","Sectional sofa", "https://www.designconnected.com/catalog/3D-Models/all/category/Modular--Sectional-sofas__75"));
        //SLEEPING
        PARSER_LINKS.add(new ParserLink("SLEEPING","Bed", "https://www.designconnected.com/catalog/3D-Models/all/category/Sleep-furniture__6"));
        //STORAGE
        PARSER_LINKS.add(new ParserLink("STORAGE","Bookcase", "https://www.designconnected.com/catalog/3D-Models/all/category/Bookcases__51"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Chest of drawers", "https://www.designconnected.com/catalog/3D-Models/all/category/Chests-of-drawers__61"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Display cabinet", "https://www.designconnected.com/catalog/3D-Models/all/category/Display-cabinets__62"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Highboard", "https://www.designconnected.com/catalog/3D-Models/all/category/Highboards__63"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Shelving system", "https://www.designconnected.com/catalog/3D-Models/all/category/Shelving-systems__66"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Sideboard", "https://www.designconnected.com/catalog/3D-Models/all/category/Sideboards__65"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Storage wall", "https://www.designconnected.com/catalog/3D-Models/all/category/Storage-walls__64"));
        PARSER_LINKS.add(new ParserLink("STORAGE","Wardrobe", "https://www.designconnected.com/catalog/3D-Models/all/category/Wardrobes__50"));
    }

    private String category;
    private String type;
    private String url;

    public String getFileName(){
        String name = "designconnected-"+category+"-"+type+".html";
        name = name.replaceAll(" ", "");
        return name;
    }

}
