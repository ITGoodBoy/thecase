package ua.softgroup.thecase.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.model.Product;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.ItemRepository;
import ua.softgroup.thecase.repository.TagRepository;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


@Component
public class ArchiproductsParser extends Parser {

    public static final AtomicBoolean PARSER_IS_FINISHED = new AtomicBoolean(false);

    @Value("${app.archiproducts.parser.path}")
    private String FOLDER_PATH;
    @Value("${app.parser.is.active}")
    private boolean parserIsActive;
    @Value("${app.parser.thread.number}")
    private int numberOfThreads;

    private static final String FILE_BASE_NAME = "product";

    @Autowired
    private ItemRepository<Product> itemRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private TagRepository tagRepository;

    private static final List<ParserLink> PARSER_LINKS = new ArrayList<>();

    static {
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Armchair", "http://www.archiproducts.com/en/products/armchairs"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Lounge chair", "http://www.archiproducts.com/en/products/lounge-chairs"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Bar stool", "http://www.archiproducts.com/en/products/stools/barstools"));

        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Beanbag", "http://www.archiproducts.com/en/products/garden-armchairs/bean-bags"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Beanbag", "http://www.archiproducts.com/en/products/kids-poufs/bean-bags"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Beanbag", "http://www.archiproducts.com/en/products?q=Beanbag"));

        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Pouf", "http://www.archiproducts.com/en/products?q=Pouf"));

        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Bench", "http://www.archiproducts.com/en/products/bench-seatings"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Bench", "http://www.archiproducts.com/en/products/benches"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Bench", "http://www.archiproducts.com/en/products/indoor-benches"));

        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Chair", "http://www.archiproducts.com/en/products/chairs"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Stool", "http://www.archiproducts.com/en/products/stools"));

        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Easy chair", "http://www.archiproducts.com/en/products/easy-chairs"));
        PARSER_LINKS.add(new ParserLink("seating".toUpperCase(), "Ottoman", "http://www.archiproducts.com/en/products/footstools"));

        //TABLES
        PARSER_LINKS.add(new ParserLink("TABLES", "Coffee table", "http://www.archiproducts.com/en/products/coffee-tables"));
        PARSER_LINKS.add(new ParserLink("TABLES", "Console table", "http://www.archiproducts.com/en/products/console-tables"));
        PARSER_LINKS.add(new ParserLink("TABLES", "Dining table", "http://www.archiproducts.com/en/products/tables/dining-tables"));
        PARSER_LINKS.add(new ParserLink("TABLES", "Side table", "http://www.archiproducts.com/en/products/coffee-tables/side-tables"));
        PARSER_LINKS.add(new ParserLink("TABLES", "Writing desk", "http://www.archiproducts.com/en/products/writing-desks"));
        PARSER_LINKS.add(new ParserLink("TABLES", "Dressing table", "http://www.archiproducts.com/en/products/dressing-tables"));

        //SOFAS
        PARSER_LINKS.add(new ParserLink("SOFAS", "Sofa", "http://www.archiproducts.com/en/products/sofas"));
        PARSER_LINKS.add(new ParserLink("SOFAS", "Couch", "http://www.archiproducts.com/en/products?q=Couch"));
        PARSER_LINKS.add(new ParserLink("SOFAS", "Couch", "http://www.archiproducts.com/en/products/day-beds"));
        PARSER_LINKS.add(new ParserLink("SOFAS", "Sectional sofa", "http://www.archiproducts.com/en/products/sofas/sectional"));

        //SLEEPING
        PARSER_LINKS.add(new ParserLink("SLEEPING", "Bed", "http://www.archiproducts.com/en/products/beds"));

        //STORAGE
        PARSER_LINKS.add(new ParserLink("STORAGE", "Bookcase", "http://www.archiproducts.com/en/products/bookcases"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Chest of drawers", "http://www.archiproducts.com/en/products/chests-of-drawers"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Display cabinet", "http://www.archiproducts.com/en/products/display-cabinets"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Highboard", "http://www.archiproducts.com/en/products/highboards"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Shelving system", "http://www.archiproducts.com/en/products/shelveing-systems"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Sideboard", "http://www.archiproducts.com/en/products/sideboards"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Storage wall", "http://www.archiproducts.com/en/products/storage-walls"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Wardrobe", "http://www.archiproducts.com/en/products/wardrobes"));
        PARSER_LINKS.add(new ParserLink("STORAGE", "Dressers", "http://www.archiproducts.com/en/products/dressers"));

    }

    public ArchiproductsParser(GroupRepository groupRepository, TagRepository tagRepository) {
        super(groupRepository, tagRepository);
    }


    @SuppressWarnings("Duplicates")
    private void createGroups() {
        for (Map.Entry<String, String> pair : ParserUtils.allGroups.entrySet()) {
            Group group = groupRepository.findByName(pair.getKey());
            if (group == null)
                groupRepository.save(new Group(pair.getKey(), pair.getValue()));
        }
    }

    @SuppressWarnings("Duplicates")
//        @Scheduled(cron = "${app.cron.expression.every.day.in.2.hour.AM}")
    public synchronized void loadProducts() throws InterruptedException {
        if (!parserIsActive) return;
        createGroups();

        AtomicInteger addedCount = new AtomicInteger(0);
        AtomicInteger updatedCount = new AtomicInteger(0);
        AtomicInteger hiddenCount = new AtomicInteger(0);

        log("START");
        PARSER_LINKS.forEach(parserLink -> {
            log(parserLink.getCategory() + " | " + parserLink.getType());
            try {
                int lastProduct = saveItemsInFiles(parserLink, FOLDER_PATH, FILE_BASE_NAME, numberOfThreads);

                System.out.println("lastProduct=" + lastProduct);

                ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);

                for (int i = 1; i <= lastProduct; i++) {
                    int finalI = i;
                    executorService.execute(() -> {
                        if (finalI % 100 == 0)
                            System.out.println("ArchiproductsParser " + ((float) (finalI * 100) / lastProduct) + "%");
                        String htmlProductName = FOLDER_PATH + "/" + FILE_BASE_NAME + "#" + finalI + "/" + FILE_BASE_NAME + "#" + finalI + ".html";

                        Document document = null;
                        try {
                            document = Jsoup.parse(new File(htmlProductName), "UTF-8");

                            Product product =
                                    Product.builder()
                                            .name(getProductName(document))
                                            .category(parserLink.getCategory())
                                            .type(parserLink.getType())
                                            .brand(getProductBrand(document))
                                            .source("Archiproducts")
                                            .link(getProductLink(document))
                                            .imageUrls(getProductImageUrls(document))
                                            .tagsList(new HashSet<>(getProductTags(document)))
                                            .isPublished(true)
                                            .build();

                            product.setUpdateAt(product.getCreateAt());

                            Collections.addAll(product.getTagsList(),
                                    product.getName().trim().replaceAll(" +", " ").split(" "));

                            product.getTagsList().add(getProductType(document));
                            product.getTagsList().add(getProductSubCategory(document));

                            super.setupKeywords(product);

                            addOrUpdateItem(addedCount, updatedCount, product, itemRepository);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }

                executorService.shutdown();
                executorService.awaitTermination(5, TimeUnit.DAYS);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        log(" DONE");
        ParserUtils.notifyAdmin("Archiproducts", addedCount, updatedCount, hiddenCount);
    }

    @Override
    Set<String> getItemsLinksSet(String url) {
        Set<String> productsLinksSet = new LinkedHashSet<>();
        Element next = null;
        String link = url;
        try {
            do {
                Document document = Jsoup.connect(link).timeout(15000).get();
                for (Element element : document.select("div.row.large-up-3.medium-up-2.small-up-2").select("a._search-item-anchor")) {
                    productsLinksSet.add(element.attr("abs:href"));
                }
                System.out.println("part productsLinksSet = " + productsLinksSet.size());
                next = document.select("div.pagination-container").select("li.pagination-next").select("a").first();
                link = next.attr("abs:href");
                System.out.println("next: " + link);
            } while (link != null && !link.isEmpty());
        } catch (IOException e) {
            e.printStackTrace();
        }
        log("productsLinksSet size=" + productsLinksSet.size());
        return productsLinksSet;
    }


    private String getProductName(Document document) {
        String productName = "";
        for (Element element : document.getElementsByClass("medium-9")) {
            if (element.hasAttr("itemprop")) {
                productName = element.select("h2").html().replace("amp;", "");
                break;
            }
        }
        return productName;
    }

    private String getProductBrand(Document document) {
        return document.getElementsByClass("mobile-padding").select("h1").select("span").select("a").html().replace("amp;", "");
    }

    private Set<String> getProductTags(Document document) {
        Set<String> set = new HashSet<>();
        for (Element element : document.select("div.accordion-item").select("div.accordion-content").last().select("a").select("span")) {
            set.add(element.html().replaceAll(",", ""));
        }
        return set;
    }

    private List<String> getProductImageUrls(Document document) {
        List<String> list = new ArrayList<>();
        for (Element element : document.getElementsByClass("productpage-carousel").select("span").select("img")) {
            String imageUrl = element.attr("content");
            list.add(imageUrl);
        }

        return list;
    }

    private String getProductLink(Document document) {
        String link = document.select("link").attr("href");
        if (link == null || link.equals("")) return "link not found";
        else return link;
    }

    private String getProductType(Document document) {
        try {

            return document.select("div.row").select("ul.breadcrumbs.column.small-10").select("li").get(1).select("span").html();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getProductSubCategory(Document document) {
        try {

            Elements liElements = document.select("div.row").select("ul.breadcrumbs.column.small-10").select("li");
            return liElements.get(liElements.size() - 2).select("span").html();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    void log(String s) {
        System.out.println("AP Parser -------> " + s);
    }
}
