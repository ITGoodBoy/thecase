package ua.softgroup.thecase.parser;

import io.github.bonigarcia.wdm.PhantomJsDriverManager;
import org.jsoup.Jsoup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.repository.ItemRepository;
import ua.softgroup.thecase.service.EmailService;

import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by java-1-07 on 26.07.2017.
 */

@Component
public class ParserUtils {

    private static EmailService emailService;

    @Autowired
    public ParserUtils(EmailService emailService) {
        ParserUtils.emailService = emailService;
    }

    @Value("${app.parser.thread.number}")
    private static int numberOfThreads;

//    public static final String ADMIN_EMAIL = "sg.serhii.lomachenko@gmail.com";
    public static final String ADMIN_EMAIL = "ankobiss@gmail.com";

//    public static final Map<String, String[]> categoryMap = new ConcurrentHashMap<>();
    public static final Map<String, String[]> groupInTagsMap = new ConcurrentHashMap<>();
    public static final Map<String, String> allGroups = new ConcurrentHashMap<>();
//    public static final List<String> blackList = new ArrayList<>();

//    public static final AtomicInteger ITEMS_ADDED_COUNT = new AtomicInteger(0);
//    public static final AtomicInteger ITEMS_UPDATED_COUNT = new AtomicInteger(0);
//    public static final AtomicInteger ITEMS_HIDDEN_COUNT = new AtomicInteger(0);

//
//    public static final String CATEGORY_SEATING = "SEATING";
//    public static final String CATEGORY_SOFAS = "SOFAS";
//    public static final String CATEGORY_TABLES = "TABLES";
//    public static final String CATEGORY_STORAGE = "STORAGE";
//    public static final String CATEGORY_SLEEPING = "SLEEPING";
    public static final List<String> defaultCombinableMaterials = new ArrayList<>();


    public static final String GROUP_MATERIAL = "Material";
    public static final String GROUP_ROOM = "Room";
//    public static final String GROUP_TYPE = "Type";
    public static final String GROUP_VOLUME = "Volume";
    public static final String GROUP_STATUS = "Status";

//    public static final String[] CATEGORY_SEATING_VALUE = new String[]{"Chair", "Armchair", "Pouf", "Bar chair", "Bench"};
//    public static final String[] CATEGORY_SOFAS_VALUE = new String[]{"Sofa", "Couch", "Ottoman"};
//    public static final String[] CATEGORY_TABLES_VALUE = new String[]{"Coffee table", "Work-table", "Dining table", "Dressing table", "Table"};
//    public static final String[] CATEGORY_STORAGE_VALUE = new String[]{"Commode", "Wardrobe", "Showcase", "TV stand", "Console", "Kitchen STORAGE", "Stand", "Shelf", "Bar", "Sideboard"};
//    public static final String[] CATEGORY_SLEEPING_VALUE = new String[]{"Bed"};

    static {
//        categoryMap.put(CATEGORY_SEATING, CATEGORY_SEATING_VALUE);
//        categoryMap.put(CATEGORY_SOFAS, CATEGORY_SOFAS_VALUE);
//        categoryMap.put(CATEGORY_TABLES, CATEGORY_TABLES_VALUE);
//        categoryMap.put(CATEGORY_STORAGE, CATEGORY_STORAGE_VALUE);
//        categoryMap.put(CATEGORY_SLEEPING, CATEGORY_SLEEPING_VALUE);

        defaultCombinableMaterials.add("Metal");
        defaultCombinableMaterials.add("Wooden");
        defaultCombinableMaterials.add("Leather");
        defaultCombinableMaterials.add("Fabric");
        defaultCombinableMaterials.add("Plastic");


        groupInTagsMap.put(GROUP_MATERIAL, new String[]{"Ceramic", "Fabric", "Felt", "Forging", "Glass", "Leather", "MDF",
                "Metal", "Mirror", "Plastic", "Plywood", "Rottang", "Silver", "Stone", "Wood", "Wool"});
        groupInTagsMap.put(GROUP_ROOM, new String[]{"Bedroom", "Dining", "Kids", "Kitchen", "Living", "Office",
                "Outdoor", "HoReCa"});

        allGroups.put(GROUP_MATERIAL, "meetup");
        allGroups.put(GROUP_ROOM, "file-text");
        allGroups.put(GROUP_VOLUME, "ravelry");
        allGroups.put(GROUP_STATUS, "plus-square");
//
//        blackList.add("Classic");
//        blackList.add("Kids (детская)");
//        blackList.add("Wallpapers");
//        blackList.add("Closets");
//        blackList.add("lockers");
//        blackList.add("cupboard");
//        blackList.add("Кухни");
    }

    public static synchronized void notifyAdmin(String parserName, AtomicInteger addedCount,
                                                AtomicInteger updatedCount, AtomicInteger hiddenCount) {
            emailService.sendNotifyToEmail(
                    "Админ",
                    ADMIN_EMAIL,
                    "Парсинг \"" + parserName + "\" завершён",
                    "Парсинг \"" + parserName + "\" успешно завершён, элементов добавлено - "
                            + addedCount + "\n" + "элементов обновлено - "
                            + updatedCount + "\n" + "элементов скрыто - "
                            + hiddenCount);
            addedCount.set(0);
            updatedCount.set(0);
            hiddenCount.set(0);
    }

    public static <T extends Item, E extends ItemRepository<T>> AtomicInteger cleanDeletedFromDatabase(E repository) {
        AtomicInteger hiddenCount = new AtomicInteger(0);
        final int pageLimit = 1000;
        int pageNumber = 0;
        Page<T> page = repository.findByIsPublishedTrueAndIsPublishedBySourceTrue(new PageRequest(pageNumber, pageLimit));
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        while (page.hasNext()) {
            List<T> itemList = page.getContent();
            for (T item : itemList) {
                Page<T> finalPage = page;
                executorService.submit(() -> {
                    System.out.println(" clean " + ((float) (pageNumber * 100) / finalPage.getTotalPages()) + "%");
                    if ((!isStatusOk(item.getLink()) && !item.isChangedByAdmin())
//                            || item.getTagsList().containsAll(blackList)
                            ) {
                        item.setPublished(false);
                        hiddenCount.incrementAndGet();
                        repository.save(item);
                    }
                });
            }
            page = repository.findByIsPublishedTrueAndIsPublishedBySourceTrue(new PageRequest(page.nextPageable().getPageNumber(), pageLimit));
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(5, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return hiddenCount;
    }

    private static volatile int internetCheckCount = 0;
    private static final int INTERNET_CHECK_COUNT_SKIP = 100;

    private static boolean isStatusOk(String link) {
        try {
            if (internetCheckCount >= INTERNET_CHECK_COUNT_SKIP) {
                if (isActiveInternetConnection())
                    internetCheckCount = 0;
                else {
                    Thread.sleep(10000);
                    isStatusOk(link);
                }
            }
            internetCheckCount++;
            return Jsoup.connect(link).timeout(4000).execute().statusCode() == 200;
        } catch (Exception e) {
            return false;
        }
    }


    private static boolean isActiveInternetConnection() {
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName("www.google.com");
            if (inetAddress.isReachable(5000)) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


//    public static WebDriver webDriverInitialize(String website) {
//        ChromeDriverManager.getInstance().setup();
//        WebDriver driver;
//
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--lang=en");
//
//        driver = new ChromeDriver(options);
//        driver.manage().window().maximize();
//        driver.get(website);
//        return driver;
//    }

    public static WebDriver webDriverInitialize(String website) {
        PhantomJsDriverManager.getInstance().setup();

        PhantomJSDriverService service = new PhantomJSDriverService.Builder()
                .usingAnyFreePort()
                .usingPhantomJSExecutable(
                        Paths.get(System.getProperty("phantomjs.binary.path"))
                                .toFile())
                .usingCommandLineArguments(new String[]{
                        "--ignore-ssl-errors=true", "--ssl-protocol=tlsv1",
                        "--web-security=false", "--webdriver-loglevel=INFO"})
                .build();

        DesiredCapabilities desireCaps = new DesiredCapabilities();
        WebDriver driver = new PhantomJSDriver(service, desireCaps);
        driver.manage().window().maximize();
        driver.get(website);
        return driver;
    }

    public static void saveTextToFile(String folder, String text, String fileName) {
        try {
            File file = new File(folder + "/" + fileName);

            if (!file.exists()) {
                file.mkdirs();
            }
            PrintWriter writer = new PrintWriter(file.getAbsoluteFile() + "/" + fileName + ".html", "UTF-8");
            writer.println(text);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveTextToFile(String text, String fileName) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new PrintWriter(fileName, "UTF-8"));
            bufferedWriter.write(text);
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveImage(String imageUrl, String folder, String imageName) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os;
        try {
            ClassPathResource classPathResource = new ClassPathResource("images/static");
            File file = new File(classPathResource.getURI().getPath() + "/" + folder + "/" + imageName + ".jpg");
            os = new FileOutputStream(file);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }
}
