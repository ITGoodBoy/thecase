package ua.softgroup.thecase.serviceImpl;

import lombok.NonNull;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.config.TokenHandler;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.MessageRequest;
import ua.softgroup.thecase.model.message.UserResponse;
import ua.softgroup.thecase.repository.CaseRepository;
import ua.softgroup.thecase.repository.RelationshipRepository;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.repository.UserTokenRepository;
import ua.softgroup.thecase.service.EmailService;
import ua.softgroup.thecase.service.UserService;
import ua.softgroup.thecase.utils.*;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Value("${app.reg.token.livetime.in.milliseconds}")
    private long expireAt;
    @Value("${app.auth.token.livetime.in.minutes}")
    private long authTokenLiveTimeInMinutes;
    @Value("${app.page.items.count}")
    private int PAGE_SIZE;

    private UserRepository userRepository;
    private EmailService emailService;
    private UserTokenRepository userTokenRepository;
    private CaseRepository caseRepository;
    private final RelationshipRepository relationshipRepository;
    private final OnlineUserCache onlineUserCache;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, EmailService emailService,
                           UserTokenRepository userTokenRepository, CaseRepository caseRepository,
                           RelationshipRepository relationshipRepository, OnlineUserCache onlineUserCache) {
        this.userRepository = userRepository;
        this.emailService = emailService;
        this.userTokenRepository = userTokenRepository;
        this.caseRepository = caseRepository;
        this.relationshipRepository = relationshipRepository;
        this.onlineUserCache = onlineUserCache;
    }

    @Override
    public String login(User user, String email, String password) {

        TokenHandler tokenHandler = new TokenHandler();
        return tokenHandler.generateAccessToken(user, LocalDateTime.now().plusMinutes(authTokenLiveTimeInMinutes));
    }

    @Override
    public ResponseEntity<String> registration(String userName, String email, String password) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            return new ResponseEntity<>("User already registered", HttpStatus.CONFLICT);
        }

        user = new User(userName, email, new BCryptPasswordEncoder().encode(password), Role.USER, Role.FREE);
        user.setCreatedAt(new Date(System.currentTimeMillis()));

        String uuid = UUID.randomUUID().toString() + UUID.randomUUID().toString();
        String token = uuid.toUpperCase() + System.currentTimeMillis();

        UserToken userToken = new UserToken(user, token, expireAt);

        emailService.sendVerificationEmail(userName, email, token);

        userRepository.save(user);
        userTokenRepository.save(userToken);

        return new ResponseEntity<>("A confirmation email has been sent to your email.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> registration(String userName, String email, String password, String referrer) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            return new ResponseEntity<>("User already registered", HttpStatus.CONFLICT);
        }

        user = new User(userName, email, new BCryptPasswordEncoder().encode(password), referrer, Role.USER, Role.FREE);
        user.setCreatedAt(new Date(System.currentTimeMillis()));

        String uuid = UUID.randomUUID().toString() + UUID.randomUUID().toString();
        String token = uuid.toUpperCase() + System.currentTimeMillis();

        UserToken userToken = new UserToken(user, token, expireAt);

        emailService.sendVerificationEmail(userName, email, token);

        userRepository.save(user);
        userTokenRepository.save(userToken);

        return new ResponseEntity<>("A confirmation email has been sent to your email.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> registrationConfirm(String token) {
        UserToken userToken = userTokenRepository.findByToken(token);
        if (userToken != null) {

            User user = userToken.getUser();
            user.setEnabled(true);
            user.setAccountNonLocked(true); // for time
            user.setAccountNonExpired(true);
            user.setCredentialsNonExpired(true);

            //init default case for new user
            Case caSe = Case.builder()
                        .name("default").owner(user).description("default case")
                        .products(new TreeSet<>(new Comparators.TimeDescComparator()))
                        .models3D(new TreeSet<>(new Comparators.TimeDescComparator()))
                    .build();
            caseRepository.save(caSe);

            user.setActiveCaseId(caseRepository.findByNameAndOwner(caSe.getName(), user).getId());
            userRepository.save(user);
            userTokenRepository.delete(userToken.getId());

            if (user.getReferrer() != null && userRepository.findOne(user.getReferrer()) != null) {
                User referrer = userRepository.findOne(user.getReferrer());
                Relationship relationship = new Relationship(user, referrer);
                Relationship relationshipMirror = new Relationship(referrer, user);
                relationshipRepository.save(relationship);
                relationshipRepository.save(relationshipMirror);
            }

            return new ResponseEntity<>("You have successfully registered a user.", HttpStatus.OK);
        }

        else return new ResponseEntity<>("Such an email or a token does not exist.", HttpStatus.CONFLICT);
    }

    @Override
    public ResponseEntity<String> resetForgottenPassword(String email) {
        User user = userRepository.findByEmail(email);

        if (user == null) return new ResponseEntity<>("user not found.", HttpStatus.NOT_FOUND);

        String uuid = UUID.randomUUID().toString() + UUID.randomUUID().toString();
        String token = uuid.toUpperCase() + System.currentTimeMillis();

        emailService.sendForgottenPasswordRequest(user.getUsername(), user.getEmail(), token);
        userTokenRepository.save(new UserToken(user, token, expireAt));

        return new ResponseEntity<>("Request for a password change has been sent to you by email", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> resetForgottenPasswordConfirm(String token) {
        UserToken userToken = userTokenRepository.findByToken(token);
        if (userToken == null) return new ResponseEntity<>(HttpStatus.CONFLICT);

        SecureRandom random = new SecureRandom();
        String password = new BigInteger(130, random).toString(32);

        User user = userToken.getUser();
        user.setPassword(new BCryptPasswordEncoder().encode(password));

        emailService.sendNewRandomPasswordToUserEmail(user.getUsername(), user.getEmail(), password);

        userRepository.save(user);
        userTokenRepository.delete(userToken.getId());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> replacePassword(String email, String oldPassword, String newPassword) {
        User user = userRepository.findByEmail(email);
        if (user == null) return new ResponseEntity<>("Such email is not registered", HttpStatus.NOT_FOUND);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) return new ResponseEntity<>("Passwords not equals", HttpStatus.CONFLICT);

        user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
        userRepository.save(user);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public User getLoggedUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userPrincipal = (User) auth.getPrincipal();
        return userPrincipal;
    }

    @Override
    public boolean isPasswordMatch(User user, String password) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String dbPassword = user.getPassword();

        return passwordEncoder.matches(password, dbPassword);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return user;
    }

    @Override
    public Optional<User> findById(@NonNull ObjectId id) {
        return Optional.ofNullable(userRepository.findById(id));
    }

    @Override
    public List<UserResponse> getAllUsers(int pageNumber) {
        User user = getLoggedUser();

        Pageable request = new PageRequest(pageNumber, PAGE_SIZE);
        Page<User> users = userRepository.findDistinctByIdNot(user.getId(), request);

        List<UserResponse> userResponses = new ArrayList<>();
        List<User> userList = users.getContent();

        userList = setIsOnline(userList); // set "isOnline" flag to users

        userList.forEach(u -> {
            userResponses.add(new UserResponse(u, relationshipRepository.existsByCurrentUserAndTargetUser(user, u)));
        });

        return userResponses;
    }

    @Override
    public List<UserResponse> getUsersStartingWith(String name, int pageNumber) {
        User user = getLoggedUser();

        Pageable request = new PageRequest(pageNumber, PAGE_SIZE);
        Page<User> users = userRepository.findByUsernameStartingWithOrEmailStartingWith(name, name, request);

        List<UserResponse> userResponses = new ArrayList<>();
        List<User> userList = users.getContent();

        userList = setIsOnline(userList); // set "isOnline" flag to users

        userList.forEach(u -> {
            userResponses.add(new UserResponse(u, relationshipRepository.existsByCurrentUserAndTargetUser(user, u)));
        });

        return userResponses;
    }

    @Override
    public ResponseEntity addUserToContactList(String userId) {
        User currentUser = getLoggedUser();
        User targetUser = userRepository.findOne(userId);
        if (targetUser == null)
            return ResponseEntity.noContent().build();

        if (relationshipRepository.existsByCurrentUserAndTargetUser(currentUser, targetUser))
            return ResponseEntity.status(HttpStatus.CONFLICT).build();

        Relationship relationship = new Relationship(currentUser, targetUser);
        relationshipRepository.save(relationship);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity removeUserFromContactList(String userId) {
        User currentUser = getLoggedUser();
        User targetUser = userRepository.findOne(userId);
        if (targetUser == null)
            return ResponseEntity.noContent().build();

        Relationship relationship = relationshipRepository.findByCurrentUserAndTargetUser(currentUser, targetUser);
        if (relationship == null)
            return ResponseEntity.status(HttpStatus.CONFLICT).build();

        relationshipRepository.delete(relationship);
        return ResponseEntity.ok().build();
    }

    @Override
    public List<User> getContactList(int pageNumber) {
        User currentUser = getLoggedUser();
        Pageable request = new PageRequest(pageNumber, PAGE_SIZE);
        Page<Relationship> relationshipPage = relationshipRepository.findByCurrentUser(currentUser, request);

        List<User> contactList = new ArrayList<>();
        if (relationshipPage != null)
            relationshipPage.getContent().forEach(relationship -> contactList.add(relationship.getTargetUser()));
        return setIsOnline(contactList);
    }

    @Override
    public ResponseEntity<String> inviteUserToSystem(String email) {
        if (email.isEmpty() || !Validator.isEmailValid(email))
            return new ResponseEntity<>("Bad email!", HttpStatus.BAD_REQUEST);

        emailService.sendInviteUserToSystem(getLoggedUser(), email);
        return new ResponseEntity<>("Invitation sent to " + email, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> sendMessage(String email, MessageRequest messageRequest) {
        User currentUser = getLoggedUser();

        User userTo = userRepository.findByEmail(email);
        if (userTo == null)
            return new ResponseEntity<>("Bad email!", HttpStatus.BAD_REQUEST);

        if (messageRequest.getFileName()==null && messageRequest.getFile()==null && (messageRequest.getMessage()==null || messageRequest.getMessage().isEmpty()))
            return new ResponseEntity<>("Empty message and attachment file", HttpStatus.CONFLICT);

        emailService.sendMessage(currentUser, userTo, messageRequest);
        return new ResponseEntity<>("Email sent to " + email, HttpStatus.OK);
    }

    @Override
    public List<User> setIsOnline(List<User> users) {
        return users.stream().peek(user -> user.setOnline(onlineUserCache.isUserOnline(user))).collect(Collectors.toList());
    }

    @Override
    public User setIsUserOnline(User user) {
        user.setOnline(onlineUserCache.isUserOnline(user));
        return user;
    }
}
