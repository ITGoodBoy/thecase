package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.Subscription;
import ua.softgroup.thecase.repository.SubscriptionRepository;
import ua.softgroup.thecase.service.SubscriptionService;

/**
 * Created by java-1-07 on 18.07.2017.
 */
@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    private SubscriptionRepository subscriptionRepository;

    @Autowired
    public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository) {
        this.subscriptionRepository = subscriptionRepository;
    }


    @Override
    public Subscription getById(String id) {
        return subscriptionRepository.findOne(id);
    }
}
