package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.ItemStatistic;
import ua.softgroup.thecase.repository.ItemStatisticRepository;
import ua.softgroup.thecase.service.ItemStatisticService;

import java.util.List;

/**
 * Created by java-1-03 on 18.09.2017.
 */
@Service
public class ItemStatisticServiceImpl implements ItemStatisticService {

    private final ItemStatisticRepository itemStatisticRepository;

    @Autowired
    public ItemStatisticServiceImpl(ItemStatisticRepository itemStatisticRepository) {
        this.itemStatisticRepository = itemStatisticRepository;
    }

    @Override
    public void addViewCount(List<? extends Item> items) {
        items.forEach(p -> {
            ItemStatistic itemStatistic = itemStatisticRepository.findByItem(p);
            if (itemStatistic == null)
                itemStatistic = new ItemStatistic(p);
            itemStatistic.addView();
            itemStatisticRepository.save(itemStatistic);
        });
    }

    @Override
    public void addCaseCount(Item item) {
        ItemStatistic itemStatistic = itemStatisticRepository.findByItem(item);
        if (itemStatistic == null){
            itemStatistic = new ItemStatistic(item);
        }
        itemStatistic.addCase();
        itemStatisticRepository.save(itemStatistic);
    }

    @Override
    public void removeCaseCount(Item item) {
        ItemStatistic itemStatistic = itemStatisticRepository.findByItem(item);
        if (itemStatistic != null){
            itemStatistic.removeCase();
            itemStatisticRepository.save(itemStatistic);
        }
    }

    @Override
    public void removeCaseCount(List<? extends Item> items) {
        items.forEach(this::removeCaseCount);
    }

    @Override
    public void addOpeningCount(Item item) {
        ItemStatistic itemStatistic = itemStatisticRepository.findByItem(item);
        if (itemStatistic == null){
            itemStatistic = new ItemStatistic(item);
        }
        itemStatistic.addOpening();
        itemStatisticRepository.save(itemStatistic);
    }

    @Override
    public void addLikeCount(Item item) {
        ItemStatistic itemStatistic = itemStatisticRepository.findByItem(item);
        if (itemStatistic == null){
            itemStatistic = new ItemStatistic(item);
        }
        itemStatistic.addLike();
        itemStatisticRepository.save(itemStatistic);
    }

    @Override
    public void removeLikeCount(Item item) {
        ItemStatistic itemStatistic = itemStatisticRepository.findByItem(item);
        if (itemStatistic != null){
            itemStatistic.removeLike();
            itemStatisticRepository.save(itemStatistic);
        }
    }


}
