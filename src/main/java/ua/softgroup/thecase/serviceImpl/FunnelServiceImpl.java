package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.FunnelFieldsResponse;
import ua.softgroup.thecase.parser.ParserUtils;
import ua.softgroup.thecase.repository.*;
import ua.softgroup.thecase.service.FilterService;
import ua.softgroup.thecase.service.FunnelService;
import ua.softgroup.thecase.service.ItemService;
import ua.softgroup.thecase.service.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by java-1-03 on 29.08.2017.
 */
@Service
public class FunnelServiceImpl implements FunnelService {

    private TagRepository tagRepository;
    private FunnelRepository funnelRepository;
    private UserService userService;
    private GroupRepository groupRepository;
    private final ProductRepository productRepository;
    private final Model3DRepository model3DRepository;
    private final ItemService itemService;
    private final MongoTemplate mongoTemplate;

    private final FilterService filterService;

    @Autowired
    public FunnelServiceImpl(TagRepository tagRepository, FunnelRepository funnelRepository, UserService userService,
                             GroupRepository groupRepository, Model3DRepository model3DRepository, ProductRepository productRepository,
                             ItemService itemService, MongoTemplate mongoTemplate, FilterService filterService) {
        this.tagRepository = tagRepository;
        this.funnelRepository = funnelRepository;
        this.userService = userService;
        this.groupRepository = groupRepository;
        this.model3DRepository = model3DRepository;
        this.productRepository = productRepository;
        this.itemService = itemService;
        this.mongoTemplate = mongoTemplate;
        this.filterService = filterService;
    }

    @Override
    public ResponseEntity<List<Funnel>> activateFunnelByGroup(Group group) {
        User user = userService.getLoggedUser();

        Funnel userFunnel = funnelRepository.findByUserAndGroup(user, group);

        if (userFunnel == null) {
            List<String> tagsByGroup = tagRepository.findByGroupAndCombinableTrue(group).stream().map(Tag::getName).collect(Collectors.toList());
            if (tagsByGroup.size() < 1)
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            userFunnel = Funnel.createFunnelByGroup(user, group, tagsByGroup);
        } else if (userFunnel.isCreatedByGroup()){
            userFunnel.changeTags();
        } else {
            funnelRepository.delete(userFunnel);
            List<String> tagsByGroup = tagRepository.findByGroupAndCombinableTrue(group).stream().map(Tag::getName).collect(Collectors.toList());
            if (tagsByGroup.size() < 1)
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            userFunnel = Funnel.createFunnelByGroup(user, group, tagsByGroup);
        }
        funnelRepository.save(userFunnel);
        return ResponseEntity.ok(funnelRepository.findByUser(user));
    }

    @Override
    public void deactivateFunnel(Group group) {
        User user = userService.getLoggedUser();

        Funnel funnel = funnelRepository.findByUserAndGroup(user, group);
        if (funnel != null)
            funnelRepository.delete(funnel);

    }

    @Override
    public void deactivateFunnels() {

        User user = userService.getLoggedUser();

        List<Funnel> funnels = funnelRepository.findByUser(user);
        if (funnels != null && funnels.size() > 0)
            funnelRepository.delete(funnels);
    }

    @Override
    public void deleteAllFunnelsByGroup(Group group) {
        List<Funnel> funnels = funnelRepository.findByGroup(group);
        funnelRepository.delete(funnels);
    }


    @Override
    public ResponseEntity<List<Funnel>> activateFunnelsByItem(User user, String id) {

        Item item = itemService.getItemById(id);

        filterService.deactivateFilter();
        filterService.changeFilter(Item.Field.type.name(), item.getType(), true);

        Set<String> itemTagsList = item.getTagsList().stream().collect(Collectors.toSet());

        removeRoomTagsFromList(itemTagsList);

        List<Funnel> funnels = generateFunnelsByTags(user, itemTagsList);
        if (funnels == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        funnelRepository.save(funnels);

        return ResponseEntity.ok(funnels);
    }

    private void removeRoomTagsFromList(Set<String> allTags){
        Group room = groupRepository.findByName(ParserUtils.GROUP_ROOM);
        List<Tag> roomTags = tagRepository.findByGroup(room);
        roomTags.forEach(tag -> {
            if (allTags.contains(tag.getName()))
                allTags.remove(tag.getName());
        });
    }

    @Override
    public ResponseEntity<List<FunnelFieldsResponse>> getFunnels(User user) {
        List<Group> groups = groupRepository.findByPublishedTrue();

        List<Funnel> activeFunnels = getActiveFunnels(user);

        List<FunnelFieldsResponse> responses = new ArrayList<>();
        groups.forEach(group -> {
            List<String> tagsByGroup = tagRepository.findByGroup(group).stream().map(Tag::getName).collect(Collectors.toList());
            responses.add(new FunnelFieldsResponse(group, tagsByGroup, getCurrentCombinations(activeFunnels, group)));
        });

        return ResponseEntity.ok(responses);
    }

    private List<String> getCurrentCombinations(List<Funnel> funnels, Group group){
        List<String> result = new ArrayList<>();
        funnels.stream().filter(funnel -> funnel.getName().equals(group.getName())).forEach(funnel -> result.addAll(funnel.getCurrentCombination()));
        return result;
    }

    @Override
    public List<Funnel> getActiveFunnels(User user) {
        return funnelRepository.findByUser(user);
    }

    @Override
    public ResponseEntity<List<FunnelFieldsResponse>> manualSelecting(User user, List<String> tags) {

        Set<String> tagsSet = tags.stream().collect(Collectors.toSet());
        List<Funnel> funnels = generateFunnelsByTags(user, tagsSet);
        if (funnels == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        funnelRepository.save(funnels);

        return getFunnels(user);
    }

    private List<Funnel> generateFunnelsByTags(User user, Set<String> tags){

        List<Tag> allTagsFromItem = new ArrayList<>();
        Set<Group> allGroupsFromItem = new HashSet<>();
        tags.forEach(s -> {
            Tag tag = tagRepository.findByName(s);
            if (tag != null) {
                allTagsFromItem.add(tag);
                if (tag.getGroup() != null && tag.getGroup().isPublished())
                    allGroupsFromItem.add(tag.getGroup());
            }
        });

        List<Funnel> oldFunnels = funnelRepository.findByUser(user);
        if (oldFunnels != null) {
            funnelRepository.delete(oldFunnels);
        }

        List<Funnel> funnels = new ArrayList<>();
        List<Group> allGroupsFromItemList = allGroupsFromItem.stream().collect(Collectors.toList());

        for (int i = 0; i < allGroupsFromItemList.size(); i++) {
            if (allGroupsFromItemList.get(i) != null){
                Group g = allGroupsFromItemList.get(i);

                List<Tag> curTagsByGroup = allTagsFromItem.stream().filter(t -> t.getGroup() != null && t.getGroup().getName().equals(g.getName()))
                        .collect(Collectors.toList());
                List<Tag> tagsByGroup = tagRepository.findByGroup(g);

                List<String> curTagsByGroupName = curTagsByGroup.stream().map(Tag::getName).collect(Collectors.toList());
                List<String> tagsByGroupName = tagsByGroup.stream().map(Tag::getName).collect(Collectors.toList());

                if (tagsByGroup.size()<1)
                    return null;

                funnels.add(Funnel.createFunnelByItem(user, g, tagsByGroupName, curTagsByGroupName));
            }
        }
        return funnels;
    }
}