//package ua.softgroup.thecase.serviceImpl;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.annotation.Cacheable;
//import org.springframework.stereotype.Service;
//import ua.softgroup.thecase.model.Brand;
//import ua.softgroup.thecase.repository.BrandRepository;
//import ua.softgroup.thecase.service.BrandService;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * Created by java-1-03 on 06.07.2017.
// */
//@Service
//public class BrandServiceImpl implements BrandService {
//
//    @Autowired
//    public BrandServiceImpl(BrandRepository brandRepository) {
//        this.brandRepository = brandRepository;
//    }
//
//    @Override
//    public List<Brand> getAll() {
//        return brandRepository.findAll();
//    }
//
//    @Override
//    @Cacheable("brand")
//    public List<String> getAllNames() {
//        return brandRepository.findAll().stream().map(Brand::getName).collect(Collectors.toList());
//    }
//}
