package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.CaseChange;
import ua.softgroup.thecase.model.CaseHistory;
import ua.softgroup.thecase.repository.CaseHistoryRepository;
import ua.softgroup.thecase.service.CaseHistoryService;

/**
 * Created by java-1-03 on 16.09.2017.
 */
@Service
public class CaseHistoryServiceImpl implements CaseHistoryService {

    private CaseHistoryRepository caseHistoryRepository;
    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    public CaseHistoryServiceImpl(CaseHistoryRepository caseHistoryRepository) {
        this.caseHistoryRepository = caseHistoryRepository;
    }

    @Override
    public void addCaseChange(Case aCase, String action, String editorEmail) {
        CaseHistory caseHistory = caseHistoryRepository.findByACase(aCase);
        CaseChange caseChange = new CaseChange(aCase.getOwner().getEmail(), action, editorEmail);
        if (caseHistory == null)
            caseHistory = new CaseHistory(aCase, caseChange);
        else {
            caseHistory.addCaseChange(caseChange);
        }
        caseHistoryRepository.save(caseHistory);
        publisher.publishEvent(caseChange);
    }
}
