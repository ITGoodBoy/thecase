package ua.softgroup.thecase.serviceImpl;

import com.mongodb.BasicDBObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.Filter;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.FilterFieldsResponse;
import ua.softgroup.thecase.repository.FilterRepository;
import ua.softgroup.thecase.service.FilterService;
import ua.softgroup.thecase.service.ItemService;
import ua.softgroup.thecase.service.UserService;
import ua.softgroup.thecase.utils.DBObjectFlagFilter;
import ua.softgroup.thecase.utils.FilterCacheble;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by java-1-03 on 26.09.2017.
 */
@Service
@Slf4j
public class FilterServiceImpl implements FilterService {

    private final UserService userService;
    private final FilterRepository filterRepository;
    private final ItemService itemService;
    private final MongoTemplate mongoTemplate;

    @Autowired
    private FilterCacheble filterCacheble;



    @Autowired
    public FilterServiceImpl(UserService userService, FilterRepository filterRepository,
                              ItemService itemService, MongoTemplate mongoTemplate) {
        this.userService = userService;
        this.filterRepository = filterRepository;
        this.itemService = itemService;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public ResponseEntity<Filter> changeFilter(String filterName, String name, boolean isAdd) {
        User user = userService.getLoggedUser();
        Filter filter = filterRepository.findByUser(user);
        if (filter == null)
            filter = new Filter(user);

        switch (filterName){
            case "type":
                if (isAdd) {
                    filter.getType().add(name);
                    List<String> strings = mongoTemplate.getCollection("product").distinct("category", new BasicDBObject("type", name));
                    strings.addAll(mongoTemplate.getCollection("model3D").distinct("category", new BasicDBObject("type", name)));
                    if (strings!=null && strings.size()>0) filter.getCategory().add(strings.get(0)); // strings.get(0) because can be only one category in type
                }
                else filter.getType().remove(name);
                break;
            case "category":
                if (isAdd) filter.getCategory().add(name);
                else {

                    if (filter.getType() != null && filter.getType().size() > 0) {
                        Set<String> typesByCategory = new HashSet<>();
                        typesByCategory.addAll(mongoTemplate.getCollection("product").distinct("type", new BasicDBObject("category", name)));
                        typesByCategory.addAll(mongoTemplate.getCollection("model3D").distinct("type", new BasicDBObject("category", name)));

                        List<String> list = new ArrayList<>();
                        filter.getType().forEach(s -> {
                            if (typesByCategory.contains(s))
                                list.add(s);
                        });

                        for (int i = 0; i < list.size(); i++) {
                            filter.getType().remove(list.get(i));
                        }

                    }

                    filter.getCategory().remove(name);
                }
                break;
            case "brand":
                if (isAdd) filter.getBrand().add(name);
                else filter.getBrand().remove(name);
                break;
            case "source":
                if (isAdd) filter.getSource().add(name);
                else filter.getSource().remove(name);
                break;
            default:
                return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        filterRepository.save(filter);
        return ResponseEntity.ok(filterRepository.findByUser(user));
    }

    @Override
    public ResponseEntity<Filter> deActivateFilterByType(String filterName) {
        User user = userService.getLoggedUser();
        Filter filter = filterRepository.findByUser(user);
        if (filter == null)
            filter = new Filter(user);

        switch (filterName){
            case "type":
                filter.setType(new HashSet<>());
                break;
            case "category":
                filter.setCategory(new HashSet<>());
                break;
            case "brand":
                filter.setBrand(new HashSet<>());
                break;
            case "source":
                filter.setSource(new HashSet<>());
                break;
            default:
                return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        filterRepository.save(filter);
        return ResponseEntity.ok(filterRepository.findByUser(user));

    }

    @Override
    public ResponseEntity<FilterFieldsResponse> getFilterFields(String brandStartingWith) {

        User user = userService.getLoggedUser();
        Filter filter = filterRepository.findByUser(user);

        Query published = DBObjectFlagFilter.published();

        BasicDBObject brandBasicDBObject = DBObjectFlagFilter.visible();
        if (brandStartingWith != null && !brandStartingWith.isEmpty())
            brandBasicDBObject.put("startingWith", brandStartingWith.toLowerCase());

        List<String> types;
        List<String> categories;
        List<String> sources;
        List<String> brands;

        categories = filterCacheble.findDistinct(Item.Field.category, published);

        if (filter == null){
            types = filterCacheble.findDistinctAllTypes(Item.Field.type);
            sources = filterCacheble.findDistinct(Item.Field.source, published);
            brands = filterCacheble.findBrandDistinct(brandBasicDBObject);
            return ResponseEntity.ok(new FilterFieldsResponse(types, categories, sources, brands));
        }

        brands = filterCacheble.findBrandDistinct(generateBasicDBObjectToBrands(brandBasicDBObject, filter));
        sources = filterCacheble.findDistinct(Item.Field.source, generateQueryWithAllFields(filter, Item.Field.source));
        types = filterCacheble.findDistinct(Item.Field.type, generateQueryForTypes(filter));

        return ResponseEntity.ok(new FilterFieldsResponse(types, categories, sources, brands, filter));
    }

    private BasicDBObject generateBasicDBObjectToBrands(BasicDBObject brandBasicDBObject, Filter filter){

        if (filter != null && filter.generateMap() != null) {
            System.out.println(" ------------------------------- generateBasicDBObjectToBrands   = MAPA=    filter : " + filter.generateMap().toString());

            filter.generateMap().forEach((s, strings) -> {
                if (!s.equals("brand"))
                    brandBasicDBObject.put(s, strings);
//                    strings.forEach(s1 -> basicDBObject.put(s, s1));
            });
        }

        System.out.println(" ------------------------------- generateBasicDBObjectToBrands   = basicDBObject=  : " + brandBasicDBObject.toString());
        return brandBasicDBObject;
    }

    private Query generateQueryWithAllFields(Filter filter, Item.Field notField){
        Query published = DBObjectFlagFilter.published();

        if (filter != null && filter.generateMap() != null) {
            System.out.println(" ------------------------------- generateQueryWithAllFields   = MAPA=    filter : " + filter.generateMap().toString());

            filter.generateMap().forEach((s, strings) -> {
                if (!s.equals(notField.name()))
                    published.addCriteria(Criteria.where(s).in(strings));
            });
        }

        System.out.println(" ------------------------------- generateQueryWithAllFields   = query=  : " + published.toString());
        return published;
    }

    private Query generateQueryForTypes(Filter filter){

        Query published = DBObjectFlagFilter.published();

        System.out.println(" ------------------------------- generateTo   =TYPE=    filter : " + filter.getCategory().toString());

        if (filter.getCategory() != null && filter.getCategory().size() > 0)
            published.addCriteria(Criteria.where("category").in(filter.getCategory()));

        System.out.println(" ------------------------------- generateQueryForTypes   =TYPE=    query : " + published.toString());

        return published;
    }





    @Override
    public ResponseEntity<Filter> deactivateFilter() {
        long start = System.currentTimeMillis();

        User user = userService.getLoggedUser();
        Filter filter = filterRepository.findByUser(user);
        if (filter != null)
            filterRepository.delete(filter);
        log.info("findDistinctAllTypes time: " + (System.currentTimeMillis() - start));

        return ResponseEntity.ok(new Filter());
    }

    @Override
    public ResponseEntity<Filter> getActiveFilter() {
        long start = System.currentTimeMillis();

        User user = userService.getLoggedUser();
        Filter filter = filterRepository.findByUser(user);
        if (filter == null)
            filter = new Filter();
        log.info("findDistinctAllTypes time: " + (System.currentTimeMillis() - start));

        return ResponseEntity.ok(filter);
    }


}
