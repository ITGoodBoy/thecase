package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.TeamResponse;
import ua.softgroup.thecase.model.message.TeamUser;
import ua.softgroup.thecase.repository.CaseRepository;
import ua.softgroup.thecase.repository.InviteCaseRepository;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.service.*;
import ua.softgroup.thecase.utils.OnlineUserCache;
import ua.softgroup.thecase.utils.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;


/**
 * Created by java-1-03 on 11.09.2017.
 */
@Service
public class TeamServiceImpl implements TeamService {

    private CaseRepository caseRepository;
    private UserService userService;
    private UserRepository userRepository;
    private InviteCaseRepository inviteCaseRepository;
    private EmailService emailService;
    private CaseHistoryService caseHistoryService;
    private ItemService itemService;
    private final OnlineUserCache onlineUserCache;

    @Autowired
    public TeamServiceImpl(CaseRepository caseRepository, UserService userService,
                           UserRepository userRepository, InviteCaseRepository inviteCaseRepository,
                           EmailService emailService, CaseHistoryService caseHistoryService,
                           ItemService itemService, OnlineUserCache onlineUserCache) {
        this.caseRepository = caseRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.inviteCaseRepository = inviteCaseRepository;
        this.emailService = emailService;
        this.caseHistoryService = caseHistoryService;
        this.itemService = itemService;
        this.onlineUserCache = onlineUserCache;
    }

    @Override
    public ResponseEntity addUserToCaseOrChangePermission(String caseId, String userId, int permissionNumber) {

        int permission = permissionNumber;
        if (permission < 1 || permission > 4)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        User user = userService.getLoggedUser();
        Case aCase = caseRepository.findOne(caseId);
        User inviteToUser = userRepository.findOne(userId);

        if (aCase == null || !aCase.getOwner().equals(user) || inviteToUser == null)
            return ResponseEntity.noContent().build();

        if (aCase.getOwner().equals(inviteToUser))
            return new ResponseEntity(HttpStatus.FAILED_DEPENDENCY);

        if (aCase.getOwner().equals(user) || isCanAddUserToCase(user, aCase, permissionNumber)){
            InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(inviteToUser, aCase);
            if (inviteCase != null)
                inviteCase.changeParmissions(permissionNumber);
            else {
                inviteCase = new InviteCase(user, inviteToUser, aCase, permissionNumber);
                userService.addUserToContactList(userId);

                if (!aCase.getOwner().equals(user))
                    caseHistoryService.addCaseChange(aCase, CaseChange.INVITE_USER_TO_CASE + inviteToUser.getEmail(), user.getEmail());

            }
            inviteCaseRepository.save(inviteCase);
            return ResponseEntity.ok().build();
        }

        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public ResponseEntity removeUserFromCase(String caseId, String userId) {
        User user = userService.getLoggedUser();
        User invitedUser = userRepository.findOne(userId);
        Case aCase = caseRepository.findOne(caseId);

        if (aCase == null || invitedUser == null)
            return new ResponseEntity(HttpStatus.NO_CONTENT);

        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(invitedUser, aCase);
        if (inviteCase == null || !aCase.getOwner().equals(user))
            return new ResponseEntity(HttpStatus.CONFLICT);

        inviteCaseRepository.delete(inviteCase);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity confirmInvite(String caseId) {
        User user = userService.getLoggedUser();
        Case aCase = caseRepository.findOne(caseId);

        if (aCase == null)
            return ResponseEntity.noContent().build();

        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        if (inviteCase == null)
            return new ResponseEntity(HttpStatus.CONFLICT);

        inviteCase.setConfirmed(true);
        inviteCaseRepository.save(inviteCase);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<String> sendCaseToEmail(String caseId, String emailTo) {
        User user = userService.getLoggedUser();
        Case aCase = caseRepository.findOne(caseId);

        if (aCase == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        else if (aCase.getProducts().size() == 0 && aCase.getModels3D().size() == 0)
            return new ResponseEntity<>("Case is empty!", HttpStatus.NOT_ACCEPTABLE);

        if (emailTo.isEmpty() || !Validator.isEmailValid(emailTo)) {
            return new ResponseEntity<>("Bad email!", HttpStatus.BAD_REQUEST);
        }

        if (aCase.getOwner().equals(user) || isCanSend(user, aCase)) {
            emailService.createAndSend(aCase, emailTo);
            return ResponseEntity.ok().build();
        }

        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public ResponseEntity sendItemsToEmail(Case aCase, String emailTo, List<String> itemsId) {
        User user = userService.getLoggedUser();

        if (aCase == null || itemsId.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        else if (aCase.getProducts().size() == 0 || aCase.getModels3D().size() == 0)
            return new ResponseEntity<>("Case is empty!", HttpStatus.NOT_ACCEPTABLE);

        if (emailTo.isEmpty() || !Validator.isEmailValid(emailTo)) {
            return new ResponseEntity<>("Bad email!", HttpStatus.BAD_REQUEST);
        }

        if (aCase.getOwner().equals(user) || isCanSend(user, aCase)) {

            TreeSet<ItemHolder> productsSet = new TreeSet<>();
            TreeSet<ItemHolder> modelsSet = new TreeSet<>();

            itemsId.forEach(s -> {
                Item item = itemService.findItemById(s);
                if (item!=null && item instanceof Product)
                    productsSet.add(new ItemHolder(item, ""));
                else if (item!=null && item instanceof Model3D)
                    modelsSet.add(new ItemHolder(item, ""));
            });

            aCase.setProducts(productsSet);
            aCase.setModels3D(modelsSet);

            emailService.createAndSend(aCase, emailTo);
            return ResponseEntity.ok().build();
        }
        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public ResponseEntity<TeamResponse> getTeamByCase(String caseId) {
        Case aCase = caseRepository.findOne(caseId);

        if (aCase == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        List<InviteCase> inviteCases = inviteCaseRepository.findByACase(aCase);

        List<TeamUser> teamUsers = new ArrayList<>();
        if (inviteCases != null)
            inviteCases.forEach(inviteCase -> {
                if (inviteCase.getInvitedUser() != null)
                    teamUsers.add(new TeamUser(userService.setIsUserOnline(inviteCase.getInvitedUser()), inviteCase.getPermissionNumber()));
            });


//            inviteCases.forEach(inviteCase -> teamUsers.add(new TeamUser(userService.setIsUserOnline(inviteCase.getInvitedUser()), inviteCase.getPermissionNumber())));

        return ResponseEntity.ok(new TeamResponse(caseId, teamUsers)); // userService making check for online/offline user
    }

    @Override
    public ResponseEntity<List<User>> getTeamAllCases() {
        User currentUser = userService.getLoggedUser();
        List<User> users = new ArrayList<>();

        List<Case> cases = caseRepository.findByOwnerOrderByCreatedAtDesc(currentUser);
        for (Case aCase : cases) {
            List<InviteCase> inviteCases = inviteCaseRepository.findByACase(aCase);
            if (inviteCases != null)
                inviteCases.forEach(inviteCase -> users.add(inviteCase.getInvitedUser()));
        }
        return ResponseEntity.ok(userService.setIsOnline(users)); // userService making check for online/offline user
    }

    @Override
    public boolean isConfirmed(User user, Case aCase) {
        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        return inviteCase != null && inviteCase.isConfirmed();
    }

    @Override
    public boolean isCanAddItem(User user, Case aCase) {
        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        return inviteCase != null && inviteCase.getPermissionNumber() > 2;
    }

    @Override
    public boolean isCanRemoveItem(User user, Case aCase, ItemHolder itemHolder) {
        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        if (inviteCase == null)
            return false;
        else if (inviteCase.getPermissionNumber() == 4)
            return true;
        else if (inviteCase.getPermissionNumber() == 3 && itemHolder.getEditorName().equals(user.getUsername()))
            return true;
        return false;
    }

    @Override
    public boolean isCanRename(User user, Case aCase) {
        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        return inviteCase != null && inviteCase.getPermissionNumber() == 4;
    }

    @Override
    public boolean isCanSend(User user, Case aCase) {
        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        return inviteCase != null;
    }

    @Override
    public boolean isCanAddUserToCase(User user, Case aCase, int permissionNumber) {
        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        if (inviteCase == null)
            return false;
        else if (inviteCase.getPermissionNumber() < 2)
            return false;
        else if (inviteCase.getPermissionNumber() == 2 && permissionNumber == 2)
            return true;
        else if (inviteCase.getPermissionNumber() == 3 && permissionNumber == 1)
            return true;
        else if (inviteCase.getPermissionNumber() == 4 && permissionNumber < 4)
            return true;
        else
            return false;
    }

    @Override
    public boolean isCanChangeCount(User user, Case aCase, ItemHolder itemHolder) {
       return isCanRemoveItem(user, aCase, itemHolder);
    }

    @Override
    public boolean isCanChangeCountAndPrice(User user, Case aCase, ItemHolder itemHolder) {
        InviteCase inviteCase = inviteCaseRepository.findByInvitedUserAndACase(user, aCase);
        return inviteCase != null && inviteCase.getPermissionNumber() == 2;
    }


}
