package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.RolePermission;
import ua.softgroup.thecase.repository.RolePermissionRepository;
import ua.softgroup.thecase.service.RolePermissionService;

/**
 * Created by java-1-07 on 18.07.2017.
 */
@Service
public class RolePermissionServiceImpl implements RolePermissionService {

    private RolePermissionRepository rolePermissionRepository;

    @Autowired
    public RolePermissionServiceImpl(RolePermissionRepository rolePermissionRepository) {
        this.rolePermissionRepository = rolePermissionRepository;
    }


    @Override
    public RolePermission getById(String id) {
        return rolePermissionRepository.findOne(id);
    }
}
