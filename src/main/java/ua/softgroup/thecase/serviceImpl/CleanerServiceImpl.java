package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.BlackWord;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Model3D;
import ua.softgroup.thecase.model.Product;
import ua.softgroup.thecase.repository.BlackListRepository;
import ua.softgroup.thecase.repository.ItemRepository;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.ProductRepository;
import ua.softgroup.thecase.service.CleanerService;
import ua.softgroup.thecase.service.ItemService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TLJD Andrew Kobin on 04.10.2017.
 */
@Service
@EnableAsync
public class CleanerServiceImpl implements CleanerService {

    private final BlackListRepository blackListRepository;
    private final ProductRepository productRepository;
    private final Model3DRepository model3DRepository;

    @Autowired
    private ItemService itemService;


    @Autowired
    public CleanerServiceImpl(ProductRepository productRepository, Model3DRepository model3DRepository, BlackListRepository blackListRepository) {
        this.productRepository = productRepository;
        this.model3DRepository = model3DRepository;
        this.blackListRepository = blackListRepository;
    }

    @Override
    public List<BlackWord> getBlackList() {
        return blackListRepository.findAll();
    }

    @Override
    public boolean addToBlackList(String word) {
        if(!blackListRepository.existsByWord(word)){
            blackListRepository.save(new BlackWord(word));
            return true;
        }
        return false;
    }

    @Override
    public boolean removeFromBlackList(String word) {
        if(!blackListRepository.existsByWord(word)){
            blackListRepository.delete(blackListRepository.findByWord(word));
            return true;
        }
        return false;
    }

    @Override
    public synchronized int cleanUp() {
        System.out.println("===> CLEANER START");
        final int[] i = {0};
        List<BlackWord> blackWords = blackListRepository.findAll();
        blackWords.forEach(blackWord -> {

            System.out.println("---------------------------- " + blackWord + " ------------------------------");
            List<Product> products = productRepository.findByTagsListInAndIsPublishedTrueAndIsPublishedBySourceTrue(blackWord.getWord());
            products.forEach(p -> p.setPublished(false));
            productRepository.save(products);
            System.out.println("Cleaner: " + products.size() + " products clean by BL word " + blackWord);
            i[0] +=products.size();


            List<Model3D> model3Ds = model3DRepository.findByTagsListInAndIsPublishedTrueAndIsPublishedBySourceTrue(blackWord.getWord());
            model3Ds.forEach(m -> m.setPublished(false));
            model3DRepository.save(model3Ds);
            System.out.println("Cleaner: " + model3Ds.size() + " models clean  by BL word " + blackWord);
            i[0] +=model3Ds.size();
        });

        System.out.println("<=== CLEANER STOP");
        return i[0];
    }

    @Async
    @Override
    public void cleanUpSource() {
        System.out.println("===> SOURCE CLEANER START");

        proceed(productRepository, new CleanSource());
        proceed(model3DRepository, new CleanSource());

        System.out.println("<=== SOURCE CLEANER STOP");
    }

//    @Override
//    public void clean3dddBrands() {
//        System.out.println("===> 3ddd BRAND CLEANER START");
//        proceed(model3DRepository, new Model3dddBrandCleaner());
//        System.out.println("<=== 3ddd BRAND CLEANER STOP");
//    }

    private <T extends Item, E extends ItemRepository<T>> void proceed(E repository, CleanerExecutor executor) {
        final int pageLimit = 1000;
        int pageNumber = 0;

        Page<T> page = repository.findAll(new PageRequest(pageNumber, pageLimit));
        while (page.hasNext()) {
            executor.execute(page, repository);
            page = repository.findAll(new PageRequest(++pageNumber, pageLimit));
        }
        executor.execute(page, repository);
    }


    private interface CleanerExecutor {
        <T extends Item, E extends ItemRepository<T>> void execute(Page<T> page, E repository);
    }

    private class CleanSource implements CleanerExecutor{
        @Override
        public  <T extends Item, E extends ItemRepository<T>> void execute(Page<T> page, E repository) {
            List<T> items = new ArrayList<>();
            System.out.println("cleanUpSource: executing " + ((page.getNumber() * 100) / page.getTotalPages()) + "%");
            page.getContent().forEach(items::add);
            repository.save(items);
        }
    }

//    private class Model3dddBrandCleaner implements CleanerExecutor{
//        @Override
//        public  <T extends Item, E extends ItemRepository<T>> void execute(Page<T> page, E repository) {
//            List<T> items = new ArrayList<>();
//            System.out.println("cleanUpSource: executing " + ((page.getNumber() * 100) / page.getTotalPages()) + "%");
//            // TODO: 26.10.2017
//            page.getContent().stream().filter(t -> t.getSource().equals("3ddd")).peek(t -> t.setBrand("")).
//            repository.save(items);
//        }
//    }

}
