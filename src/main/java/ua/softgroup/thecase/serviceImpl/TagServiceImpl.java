package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Tag;
import ua.softgroup.thecase.model.message.TagByGroupResponce;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.repository.TagRepository;
import ua.softgroup.thecase.service.ItemService;
import ua.softgroup.thecase.service.TagService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final GroupRepository groupRepository;
    private final ItemService itemService;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, GroupRepository groupRepository, ItemService itemService) {
        this.tagRepository = tagRepository;
        this.groupRepository = groupRepository;
        this.itemService = itemService;
    }

    @Override
    public ResponseEntity<List<Tag>> findByNameStartingWith(String name) {

        List<Tag> tagList = tagRepository.findByGroupAndNameStartingWithIgnoreCaseOrderByFrequencyDesc(null, name);
        if (tagList == null || tagList.size() == 0) {
            tagList = tagRepository.findByGroupAndNameContainingIgnoreCaseOrderByFrequencyDesc(null, name);
        }
        return ResponseEntity.ok(tagList);
    }

    @Override
    public ResponseEntity<List<Tag>> findByNameStartingWith(String name, int limit) {
        List<Tag> tagList = tagRepository.findByGroupAndNameStartingWithIgnoreCaseOrderByFrequencyDesc(null, name);
        if (tagList == null || tagList.size() == 0) {
            tagList = tagRepository.findByGroupAndNameContainingIgnoreCaseOrderByFrequencyDesc(null, name);
        }
        return ResponseEntity.ok(tagList.stream().limit(limit).collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<List<Tag>> getTags(String groupName, int size, int page) {
        Group group = groupRepository.findByName(groupName);
        Pageable request = new PageRequest(page, size, Sort.Direction.DESC, "frequency");
        Page<Tag> tags = tagRepository.findByGroup(group, request);
        return ResponseEntity.ok(tags.getContent());
    }

    @Override
    public ResponseEntity addTags(String groupName, List<String> tags) {
        Group group = groupRepository.findByName(groupName);
        if (group.getName().equals("Type")) return new ResponseEntity<>("Can not add tags to this group", HttpStatus.CONFLICT);
        try {
            tags.forEach(t -> {
                Tag tag = tagRepository.findByName(t);
                if (tag==null) {
                    tag = new Tag(t);
                }
                tag.setGroup(group);
                tagRepository.save(tag);
            });

            // TODO: 30.01.2018 clear cache with this group


        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity removeTags(List<String> tags) {
        tags.forEach(t -> {
            Tag tag = tagRepository.findByName(t);
            List<Item> items = itemService.findByTagsListIn(t);
            if (items != null) {
                items.forEach(item -> item.getTagsList().remove(t));
                itemService.save(items);
            }
            tagRepository.delete(tag);
        });
        return ResponseEntity.ok().build();
    }

    @Override
    public List<Tag> getTagsByGroup(String groupName) {
        Group group = groupRepository.findByName(groupName);
        return tagRepository.findByGroup(group);
    }

    @Override
    public void saveTags(List<Tag> tags) {
        tagRepository.save(tags);
    }

    @Override
    public ResponseEntity renameTag(String oldTagName, String newTagName) {
        if (!tagRepository.existsByName(oldTagName))
            return ResponseEntity.noContent().build();

        Tag tag = tagRepository.findByName(oldTagName);

        // TODO: 28.08.2017 Протестить

        List<Item> items = itemService.findByTagsListIn(oldTagName);
        if (items != null) {
            items.forEach(item -> {
                item.getTagsList().remove(oldTagName);
                item.getTagsList().add(newTagName);
            });
            itemService.save(items);
        }
        tag.setName(newTagName);
        tagRepository.save(tag);

        return ResponseEntity.ok().build();
    }

    @Override
    public List<Tag> getFirst100UnsortedTag() {
        return tagRepository.findTop100ByGroupOrderByFrequencyDesc(null);
    }

    @Override
    public ResponseEntity<List<TagByGroupResponce>> getSortedTags() {
        List<TagByGroupResponce> tagByGroupResponse = new ArrayList<>();
        List<Group> groupList = groupRepository.findDistinctByNameNot("Type");

        for (Group group: groupList) {
            tagByGroupResponse.add(new TagByGroupResponce(group.getName(), group.isPublished(), tagRepository.findByGroup(group)));
        }

        return ResponseEntity.ok(tagByGroupResponse);
    }
}
