package ua.softgroup.thecase.serviceImpl;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;
import ua.softgroup.thecase.Utils;
import ua.softgroup.thecase.service.ImageService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by TLJD Andrew Kobin on 06.07.2017.
 */
@Service
public class ImageServiceImpl implements ImageService {

    private final String BASE_IMG_URL = "/api/image/";
    public ResponseEntity<byte[]> getImage(@PathVariable String id) throws IOException {
        try {
            File file = new File(Utils.imagesPath +id);

            if(!file.exists())
                return ResponseEntity.noContent().build();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            byte[] bytes = IOUtils.toByteArray(bufferedInputStream);
            bufferedInputStream.close();
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(bytes);
        } catch (IOException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    public void removeImage(String image) throws IOException {
        Path path = Paths.get(Utils.imagesPath + image);
        Files.deleteIfExists(path);
    }

    @Override
    public List<String> uploadImages(MultipartFile[] files) {

        List<String> imageUrls = new ArrayList<>();

        for (int i = 0; i < files.length; i++) {
            if (!files[i].isEmpty()){
                MultipartFile file = files[i];
                String workingDir = Utils.imagesPath;
                String imageName = file.getOriginalFilename();

                boolean b = checkName(workingDir, imageName);

                while (b){
                    Random random = new Random();
                    imageName = imageName.replace(".", random.nextInt(100)+".");
                    b = checkName(workingDir, imageName);
                }
                System.out.println(imageName);

                try {
                    byte[] bytes = file.getBytes();
                    Path path = Paths.get(workingDir + imageName);
                    Files.write(path, bytes);
                    imageUrls.add(BASE_IMG_URL + imageName);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                System.out.println("FILE EMPTY");
            }
        }
        return imageUrls;
    }


    private boolean checkName(String workingDir, String name){
        File workingDirFile = new File(workingDir);
        File testFile = new File(workingDirFile, name);
        return testFile.exists();
    }

}
