package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.ItemResponse;
import ua.softgroup.thecase.model.message.ListItemsResponse;
import ua.softgroup.thecase.repository.*;
import ua.softgroup.thecase.service.ItemService;
import ua.softgroup.thecase.service.ItemStatisticService;
import ua.softgroup.thecase.service.UserService;
import ua.softgroup.thecase.utils.DBObjectFlagFilter;

import java.util.*;

/**
 * Created by java-1-07 on 04.07.2017.
 */

@Service
public class ItemServiceImpl implements ItemService {

    private MongoTemplate mongoTemplate;
    private FunnelRepository funnelRepository;
    private ProductRepository productRepository;
    private Model3DRepository model3DRepository;
    private UserService userService;
    private GroupRepository groupRepository;
    private LikeRepository likeRepository;
    private ItemStatisticService itemStatisticService;
    private final FilterRepository filterRepository;

    @Autowired
    public ItemServiceImpl(MongoTemplate mongoTemplate, FunnelRepository funnelRepository,
                           ProductRepository productRepository, Model3DRepository model3DRepository,
                           UserService userService, GroupRepository groupRepository,
                           LikeRepository likeRepository, ItemStatisticService itemStatisticService,
                           FilterRepository filterRepository) {
        this.mongoTemplate = mongoTemplate;
        this.funnelRepository = funnelRepository;
        this.productRepository = productRepository;
        this.model3DRepository = model3DRepository;
        this.userService = userService;
        this.groupRepository = groupRepository;
        this.likeRepository = likeRepository;
        this.itemStatisticService = itemStatisticService;
        this.filterRepository = filterRepository;
    }

    private long productsCount;
    private long modelsCount;

    @Value("${app.page.items.count}")
    private int PAGE_SIZE;



    @Override
    public Item getItemById(String id) {
        if(productRepository.exists(id)){
            return productRepository.findOne(id);
        }else{
            return model3DRepository.findOne(id);
        }
    }

    @Override
    public List<Item> getAll() {
        return mongoTemplate.findAll(Item.class);
    }

    @Override
    public <T extends Item> List<T> getItem(Class<T> tClass) {
        Query query = new Query();
        query.restrict(tClass, tClass);

        return mongoTemplate.find(query, tClass);
    }

    @Override
    public <T extends Item> List<T> getItemPaginated(int pageNumber, Class<T> tClass) {
        Query query = new Query();
        query.restrict(tClass, tClass).with(new PageRequest(pageNumber, PAGE_SIZE));

        return mongoTemplate.find(query, tClass);
    }

//    private List<Brand> convertToBrands(List<String> brandsName){
//        List<Brand> brands = new ArrayList<>();
//        brandsName.forEach(n -> {
//            Brand b = brandRepository.findOneByName(n);
//            if (b != null)
//                brands.add(b);
//        });
//        return brands;
//    }

    @Override
    public <T extends Item> ListItemsResponse getItems(int pageNumber, Class<T> tClass) {

        User user = userService.getLoggedUser();
        Filter filter = filterRepository.findByUser(user);
        List<Funnel> funnels = funnelRepository.findByUser(user);

        Query queryFunnels = queryByFunnels(funnels, pageNumber);
        Query fullQuery = queryByFilters(queryFunnels, filter);


        List<T> items = mongoTemplate.find(fullQuery, tClass);
        long sizeByQuery = mongoTemplate.count(fullQuery, tClass);

        List<ItemResponse> itemResponseList = new ArrayList<>();
        items.forEach(item -> {
            itemResponseList.add(new ItemResponse(item, isLiked(item, user), likesCount(item)));
        });

        return new ListItemsResponse(sizeByQuery, itemResponseList);
    }

    private Query queryByFunnels(List<Funnel> funnels, int pageNumber){

        Query query;
        if (funnels.size() > 0) {
            List<String> allTags = new ArrayList<>();
            funnels.forEach(f -> allTags.addAll(f.getCurrentCombination()));

            TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matchingAny(allTags.toString());

            query = new TextQuery(textCriteria).sortByScore();
        } else
            query = new Query();

        query.with(new PageRequest(pageNumber, PAGE_SIZE));
        query.addCriteria(Criteria.where("isPublished").is(true));
        query.addCriteria(Criteria.where("isPublishedBySource").is(true));


        return query;
    }

    private Query queryByFilters(Query query, Filter filter){

        if (filter != null && filter.generateMap() != null) {
            filter.generateMap().forEach((s, s2) -> {
                System.out.println("queryByFilters: " + s + " | " + s2);
                query.addCriteria(Criteria.where(s).in(s2));
            });
        }
        return query;
    }

    @Override
    public ListItemsResponse findProductByNameStartingWith(String name, int pageNumber) {
        Pageable request = new PageRequest(pageNumber, PAGE_SIZE);
        Page<Product> products = productRepository.findByNameStartingWithIgnoreCase(name, request);

        return new ListItemsResponse(products.getTotalElements(), convertToListItemResponse(products));
    }

    @Override
    public ListItemsResponse findModel3DByNameStartingWith(String name, int pageNumber) {
        Pageable request = new PageRequest(pageNumber, PAGE_SIZE);
        Page<Model3D> model3Ds = model3DRepository.findByNameStartingWithIgnoreCase(name, request);

        return new ListItemsResponse(model3Ds.getTotalElements(), convertToListItemResponse(model3Ds));
    }

    @Override
    public Page<? extends Item> findAll(int page, int page_size, Item.Type type) {
        Pageable request = new PageRequest(page, page_size);
        if(type.equals(Item.Type.model3D))
            return model3DRepository.findAll(request);
        return productRepository.findAll(request);

    }

    private List<ItemResponse> convertToListItemResponse(Page<? extends Item> page){
        User user = userService.getLoggedUser();
        List<ItemResponse> itemResponseList = new ArrayList<>();
        page.getContent().forEach(product -> {
            itemResponseList.add(new ItemResponse(product, isLiked(product, user), likesCount(product)));
        });
        return itemResponseList;
    }



//    @Override
//    public List<Item> findAll(int page) {
//        Query query = new Query();
//        query.restrict(Item.class).with(new PageRequest(page, PAGE_SIZE));
//        return mongoTemplate.find(query, Item.class);
//    }

    @Override
    public <T extends Item> long getCount(Class<T> tClass) {
        Query query = new Query();
        query.restrict(tClass, tClass);
        return mongoTemplate.count(query, tClass);
    }

    @Override
    public Map<String, Long> countsInfo() {
        Map<String, Long> counts = new HashMap<>();

        if (productsCount == 0 && modelsCount == 0) {
            productsCount = productRepository.countByIsPublishedTrueAndIsPublishedBySourceTrue();
            modelsCount = model3DRepository.countByIsPublishedTrueAndIsPublishedBySourceTrue();
        }

        counts.put("products", productsCount);
        counts.put("models3d", modelsCount);
        counts.put("items", productsCount+modelsCount);
        return counts;
    }

    @Override
    public List<Item> findByTagsListIn(String tagName) {
        List<Item> items = new ArrayList<>();
        items.addAll(productRepository.findByTagsListIn(tagName));
        items.addAll(model3DRepository.findByTagsListIn(tagName));
        return items;
    }

    @Override
    public void save(List<Item> items) {
        items.forEach(this::save);
    }

    @Override
    public void save(Item item) {
        if(item instanceof Product){
            productRepository.save((Product) item);
        }else if(item instanceof Model3D){
            model3DRepository.save((Model3D) item);
        }
    }

    @Override
    public Item findItemById(String id) {
        Item item = productRepository.findOne(id);
        if (item == null)
            item = model3DRepository.findOne(id);
        return item;
    }

    @Override
    public ItemResponse likeItem(Item item) {
        User user = userService.getLoggedUser();
        Like like = likeRepository.findByItemAndLiker(item, user);
        if (like != null){
            likeRepository.delete(like);
            itemStatisticService.removeLikeCount(item); // remove countLike in statistic
        } else {
            like = new Like(item, user);
            likeRepository.save(like);
            itemStatisticService.addLikeCount(item); // add countLike in statistic
        }
        return new ItemResponse(item, isLiked(item, user), likesCount(item));
    }

    @Override
    public boolean isLiked(Item item, User user) {
        return likeRepository.existsByItemAndLiker(item, user);
    }

    @Override
    public int likesCount(Item item) {
        return (int) likeRepository.countByItem(item);
    }

    @Override
    public List<String> findDistinctItemFilterField(Item.Field field) {
        Set<String> types = new HashSet<>();
        types.addAll(mongoTemplate.getCollection(Item.Type.product.name()).distinct(field.name(), DBObjectFlagFilter.visible()));
        types.addAll(mongoTemplate.getCollection(Item.Type.model3D.name()).distinct(field.name(), DBObjectFlagFilter.visible()));
        types.remove("");
        return new ArrayList<>(types);
    }
}
