package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.Utils;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.CaseChange;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.MessageRequest;
import ua.softgroup.thecase.service.EmailService;
import ua.softgroup.thecase.utils.PdfGenerator;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by java-1-03 on 10.07.2017.
 */
@Service
@EnableAsync
public class EmailServiceImpl implements EmailService {

    @Value("${app.url}")
    private String host;

    private JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendVerificationEmail(String name, String emailTo, String token) {
        String letter = Utils.redirectToHTML("template/confirmLater.html");
        letter = letter.replaceAll("@username@", name);
        letter = letter.replaceAll("@link@", host + "/api/users/registration/" + token);

        MimeMessage message = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, true);
            mimeMessageHelper.setTo(emailTo);
            mimeMessageHelper.setSubject("Verification");
            mimeMessageHelper.setText(letter, true);
            javaMailSender.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendForgottenPasswordRequest(String name, String emailTo, String token) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(emailTo);
        mailMessage.setSubject("Forgotten Password");
        mailMessage.setText("Hello, Dear " + name + "\nPlease, click to this link for replace password:\n" + host + "/api/users/resetForgottenPasswordConfirm/" + token);
        javaMailSender.send(mailMessage);
    }

    @Override
    public void sendNewRandomPasswordToUserEmail(String name, String emailTo, String password) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(emailTo);
        mailMessage.setSubject("New Password");
        mailMessage.setText("Hello, Dear " + name + "\nThis is your new password:\n" + password);
        javaMailSender.send(mailMessage);
    }

    @Override
    public void sendNotifyToEmail(String name, String emailTo, String title, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(emailTo);
        mailMessage.setSubject(title);
        mailMessage.setText(message);
        javaMailSender.send(mailMessage);
    }

    @Override
    public void sendInviteUserToSystem(User fromUser, String emailTo) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(emailTo);
        mailMessage.setSubject("Invitation to TheCase");
        mailMessage.setText("Hello! \nUser " + fromUser.getEmail() + " invites you to join TheCase\nPlease, click to this link for registration:\n" + host + "/ref/" + fromUser.getId());
        javaMailSender.send(mailMessage);
    }

    @Override
    public void sendCaseToEmail(String emailTo, byte[] bytes) {

        MimeMessage message = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, true);
            mimeMessageHelper.setTo(emailTo);
            mimeMessageHelper.setSubject("Sharing Case");
            mimeMessageHelper.setText("Sharing Case");
            mimeMessageHelper.addAttachment("case.pdf", new ByteArrayResource(bytes));
            javaMailSender.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void createAndSend(Case aCase, String emailTo) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfGenerator.pdfFromCase(aCase, outputStream);
        sendCaseToEmail(emailTo, outputStream.toByteArray());
    }

    @Override
    public void sendChangesOnCase(Set<CaseChange> caseChanges) {
        if (caseChanges != null && caseChanges.size() > 0) {

            List<CaseChange> list = caseChanges.stream().collect(Collectors.toList());

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(list.get(0).getOwnerEmail());
            mailMessage.setSubject("Inform about changing in your Case");

            StringBuilder stringBuilder = new StringBuilder();
            list.forEach(caseChange -> {
                stringBuilder.append(caseChange.getWhoChangeEmail());
                stringBuilder.append(" - ");
                stringBuilder.append(caseChange.getWhichChange());
                stringBuilder.append("\n");
            });

            mailMessage.setText(stringBuilder.toString());
            javaMailSender.send(mailMessage);
        }
    }

    @Override
    public void sendMessage(User fromUser, User toUser, MessageRequest messageRequest) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            if(messageRequest.getSender()!=null&&!messageRequest.getSender().isEmpty()){
                mimeMessageHelper.setFrom(messageRequest.getSender());
                mimeMessage.setFrom(messageRequest.getSender());
                mimeMessageHelper.setFrom(messageRequest.getSender(), messageRequest.getSender());
            }
            mimeMessageHelper.setTo(toUser.getEmail());
            mimeMessageHelper.setSubject("Message from - " + fromUser.getEmail());

            if (messageRequest.getMessage() != null && !messageRequest.getMessage().isEmpty())
                mimeMessageHelper.setText(messageRequest.getMessage());
            else
                mimeMessageHelper.setText(fromUser.getUsername() + " sent you a file.");

            if (messageRequest.getFile()!=null && messageRequest.getFileName()!=null && !messageRequest.getFile().isEmpty() && !messageRequest.getFileName().isEmpty()){
                String file = messageRequest.getFile().substring(messageRequest.getFile().indexOf(",") + 1);
                byte[] decodedFile = Base64.getDecoder().decode(file.getBytes(StandardCharsets.UTF_8));
                mimeMessageHelper.addAttachment(messageRequest.getFileName(), new ByteArrayResource(decodedFile));
            }
            javaMailSender.send(mimeMessage);
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
