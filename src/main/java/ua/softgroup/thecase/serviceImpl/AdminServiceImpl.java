package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.Utils;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.ItemRequest;
import ua.softgroup.thecase.model.message.SourceResponse;
import ua.softgroup.thecase.parser.ParserUtils;
import ua.softgroup.thecase.repository.Model3DRepository;
import ua.softgroup.thecase.repository.ProductRepository;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.service.*;
import ua.softgroup.thecase.utils.DBObjectFlagFilter;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by java-1-07 on 08.08.2017.
 */
@Service
public class AdminServiceImpl implements AdminService {

    private UserService userService;
    private UserRepository userRepository;
    private GroupService groupService;
    private TagService tagService;
    private final ProductRepository productRepository;
    private final Model3DRepository model3DRepository;
    private final MongoTemplate mongoTemplate;
    private final ImageService imageService;

    @Value("${app.page.items.count}")
    private int PAGE_SIZE;

    @Autowired
    public AdminServiceImpl(UserService userService, UserRepository userRepository,
                            GroupService groupService, TagService tagService,
                            ProductRepository productRepository, Model3DRepository model3DRepository, MongoTemplate mongoTemplate,
                            ImageService imageService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.groupService = groupService;
        this.tagService = tagService;
        this.productRepository = productRepository;
        this.model3DRepository = model3DRepository;
        this.mongoTemplate = mongoTemplate;
        this.imageService = imageService;
    }

    @Override
    public ResponseEntity removeUser(String id) {
        User user = userRepository.findOne(id);
        if (user == null) return ResponseEntity.noContent().build();
        userRepository.delete(user);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity banUser(String id) {
        User user = userRepository.findOne(id);
        if (user == null) return ResponseEntity.noContent().build();
        user.setAccountNonLocked(false);
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity unbanUser(String id) {
        User user = userRepository.findOne(id);
        if (user == null) return ResponseEntity.noContent().build();
        user.setAccountNonLocked(true);
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity removeItem(Item item) {
        if (item instanceof Product) {
            productRepository.delete((Product) item);
        } else {
            model3DRepository.delete((Model3D) item);
        }
        return ResponseEntity.ok().build();
    }

//    @Override
//    public ResponseEntity removeBrand(Brand brand) {
//        brandRepository.delete(brand);
//        return ResponseEntity.ok().build();
//    }

    @Override
    public ResponseEntity<List<Tag>> findByNameStartingWith(String name) {
        return tagService.findByNameStartingWith(name);
    }

    @Override
    public ResponseEntity<List<Tag>> findByNameStartingWith(String name, int limit) {
        return tagService.findByNameStartingWith(name, limit);
    }

    @Override
    public ResponseEntity getUserRoles(String id) {
        User user = userRepository.findOne(id);
        if (user == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(user.getSecondaryRoles());
    }

    @Override
    public ResponseEntity giveAdminRightsToUser(String id, boolean give) {
        User currentUser = userService.getLoggedUser();
        if (!currentUser.isAdmin())
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        User targetUser = userRepository.findOne(id);
        if (targetUser == null)
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        if (targetUser.isAdmin() == give)
            return new ResponseEntity<>("the target user already has/hasn`t Admin rights", HttpStatus.ALREADY_REPORTED);

        targetUser.addToRoles(Role.ADMIN);
        userRepository.save(targetUser);
        if (targetUser.isAdmin() == give)
            return ResponseEntity.ok().build();
        else
            return new ResponseEntity(HttpStatus.ALREADY_REPORTED);
    }

    @Override
    public ResponseEntity addProduct(ItemRequest itemRequest) {
        return createItem(itemRequest, Type.PRODUCT);
    }

    @Override
    public ResponseEntity addModel3D(ItemRequest itemRequest) {
        return createItem(itemRequest, Type.MODEL3D);
    }

    @Override
    public ResponseEntity editProduct(String itemId, ItemRequest itemRequest) {
        Product product = productRepository.findOne(itemId);
        return editItem(product, itemRequest);
    }

    @Override
    public ResponseEntity editModel3D(String itemId, ItemRequest itemRequest) {
        Model3D model3D = model3DRepository.findOne(itemId);
        return editItem(model3D, itemRequest);
    }

    @Override
    public List<SourceResponse> getSources() {

        List<SourceResponse> sourceResponses = new ArrayList<>();

        Set<String> hideSources = getHideSources();
        Set<String> allSources = getAllSources();

        if (hideSources.isEmpty()){
            allSources.forEach(s -> {
                sourceResponses.add(new SourceResponse(s, true));
            });
        } else {
            allSources.forEach(s -> {
                if (hideSources.contains(s))
                    sourceResponses.add(new SourceResponse(s, false));
                else
                    sourceResponses.add(new SourceResponse(s, true));
            });
        }
        return sourceResponses;
    }

    @Override
    public void editAllSourceItemsPublishedBySource(String source, boolean published) {
        mongoTemplate.getCollection("product").updateMulti(DBObjectFlagFilter.searchBySource(source), DBObjectFlagFilter.updatePublishedBySource(published));
        mongoTemplate.getCollection("model3D").updateMulti(DBObjectFlagFilter.searchBySource(source), DBObjectFlagFilter.updatePublishedBySource(published));
    }

    @Override
    public ResponseEntity hideShowItemsBySource(String source) {
        boolean contains = false;
        boolean published = false;
        List<SourceResponse> allSources = getSources();
        for (int i = 0; i < allSources.size(); i++) {
            if (allSources.get(i).getSource().equals(source)) {
                contains = true;
                published = !allSources.get(i).isPublished();
            }
        }
        if (!contains)
            return ResponseEntity.notFound().build();

        editAllSourceItemsPublishedBySource(source, published);
        return ResponseEntity.ok().build();
    }

    @Override
    public void setDefaultCombinableTags() {
        List<String> defaultCombinableTags = ParserUtils.defaultCombinableMaterials;
        List<Tag> materials = tagService.getTagsByGroup(ParserUtils.GROUP_MATERIAL);

        System.out.println("==== defaultCombinableTags:" + defaultCombinableTags);

        for (int i = 0; i < materials.size(); i++) {
            Tag tag = materials.get(i);

            System.out.println("==== TAG:" + tag.getName());

            if (defaultCombinableTags.contains(tag.getName().trim()))
                tag.setCombinable(true);
            else
                tag.setCombinable(false);
        }
        tagService.saveTags(materials);

        List<Group> groups = groupService.getAllGroups();
        groups.forEach(group -> {
            if (!group.getName().equals(ParserUtils.GROUP_MATERIAL)) {
                List<Tag> tags = tagService.getTagsByGroup(group.getName());
                tags.forEach(tag -> tag.setCombinable(true));
                tagService.saveTags(tags);
            }
        });


    }

    private Set<String> getAllSources(){
        List<String> allSourcesModels = mongoTemplate.getCollection("model3D").distinct("source");
        List<String> allSourcesProducts = mongoTemplate.getCollection("product").distinct("source");
        Set<String> allSources = new HashSet<>();
        allSources.addAll(allSourcesModels);
        allSources.addAll(allSourcesProducts);

        return allSources;
    }

    private Set<String> getHideSources(){
        Criteria criteria = Criteria.where("isPublishedBySource").is(false);
        Query query = new Query();
        query.addCriteria(criteria);
        List<String> hideSourceProducts = mongoTemplate.getCollection("product").distinct("source",query.getQueryObject());
        List<String> hideSourceModels = mongoTemplate.getCollection("model3D").distinct("source",query.getQueryObject());
        Set<String> hide = new HashSet<>();
        hide.addAll(hideSourceModels);
        hide.addAll(hideSourceProducts);

        return hide;
    }

    private ResponseEntity createItem(ItemRequest itemRequest, Type type) {
        if (itemRequest.getName() == null || itemRequest.getName().isEmpty())
            return new ResponseEntity<>("The name should be define at least", HttpStatus.CONFLICT);

        if (!getItemsTypesDistinct().contains(itemRequest.getType()))
            return new ResponseEntity<>("wrong items type", HttpStatus.BAD_REQUEST);

        try {
            if (type.equals(Type.PRODUCT)) {
                Product product = buildProduct(itemRequest);
                productRepository.save(product);
                return ResponseEntity.ok(product);
            } else {
                Model3D model3D = buildModel3D(itemRequest);
                model3DRepository.save(model3D);
                return ResponseEntity.ok(model3D);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    private Product buildProduct(ItemRequest itemRequest) throws Exception {
        Price price = itemRequest.getPrice();
        Product.ProductBuilder productBuilder = Product.builder();
        productBuilder.name(itemRequest.getName());
        productBuilder.isPublished(itemRequest.isPublished());
        productBuilder.category(itemRequest.getCategory());
        productBuilder.type(itemRequest.getType());
        productBuilder.imageUrls(decodeToImageAndUpload(itemRequest.getUploadImgs(), itemRequest.getName()));
        productBuilder.link(itemRequest.getUrl());
        productBuilder.price(price);
        productBuilder.brand(itemRequest.getBrand());
        if (itemRequest.getTags() != null) {
            productBuilder.tagsList(new HashSet<>(itemRequest.getTags()));
        }
        Product product = productBuilder.build();
        product.setChangedByAdmin(true);
        return product;
    }

    private Model3D buildModel3D(ItemRequest itemRequest) throws Exception {
        Price price = itemRequest.getPrice();
        Model3D.Model3DBuilder model3DBuilder = Model3D.builder();
        model3DBuilder.name(itemRequest.getName());
        model3DBuilder.isPublished(itemRequest.isPublished());
        model3DBuilder.category(itemRequest.getCategory());
        model3DBuilder.type(itemRequest.getType());
        model3DBuilder.imageUrls(decodeToImageAndUpload(itemRequest.getUploadImgs(), itemRequest.getName()));
        model3DBuilder.link(itemRequest.getUrl());
        model3DBuilder.price(price);
        model3DBuilder.brand(itemRequest.getBrand());
        if (itemRequest.getTags() != null) {
            model3DBuilder.tagsList(new HashSet<>(itemRequest.getTags()));
        }
        Model3D model3D = model3DBuilder.build();

        updateFileFieldModel3D(model3D, itemRequest);

        model3D.setChangedByAdmin(true);
        return model3D;
    }

    private ResponseEntity editItem(Item item, ItemRequest itemRequest) {
        if (item == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        if (itemRequest.getName() == null || itemRequest.getName().isEmpty())
            return new ResponseEntity<>("The name should be define at least", HttpStatus.CONFLICT);

        if (itemRequest.getType() != null && !getItemsTypesDistinct().contains(itemRequest.getType()))
            return new ResponseEntity<>("wrong items type", HttpStatus.BAD_REQUEST);

        Price price = itemRequest.getPrice();

        String type = item.getType();
        if (itemRequest.getType() !=null)
            type = itemRequest.getType();


        removeImagesFromItemByRequest(item, itemRequest);

        try {
            item.setName(itemRequest.getName());
            item.setPublished(itemRequest.isPublished());

            checkHideAllItemsBySource(item, itemRequest);

            item.setCategory(itemRequest.getCategory());
            item.setType(type);
            item.setLink(itemRequest.getUrl());
            item.setBrand(itemRequest.getBrand());
            if (itemRequest.getTags() != null) {
                item.setTagsList(new HashSet<>(itemRequest.getTags()));
            }
            List<String> images = updateItemImagesByRequest(itemRequest);
            item.setImageUrls(images);
            item.setPrice(price);
            item.setChangedByAdmin(true);

            if (item instanceof Product){
                productRepository.save((Product) item);
            } else {
                Model3D model3D = (Model3D) item;
                updateFileFieldModel3D(model3D, itemRequest);
                model3DRepository.save((Model3D) item);
            }
            return ResponseEntity.ok(item);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    private void updateFileFieldModel3D(Model3D model3D, ItemRequest itemRequest) throws IOException {
        String file = itemRequest.getFile();
        if (file != null && !file.equals(model3D.getFile()))
            model3D.setFile(decodeToFileAndUpload(file, model3D.getName(), itemRequest.getFileType()));

    }

    private List<String> updateItemImagesByRequest(ItemRequest itemRequest) throws Exception {
        List<String> images = new ArrayList<>();
        if (itemRequest.getUploadImgs() != null && itemRequest.getUploadImgs().size() > 0)
            images.addAll(decodeToImageAndUpload(itemRequest.getUploadImgs(), itemRequest.getName()));
        if (itemRequest.getImgs() != null && itemRequest.getImgs().size() > 0)
            images.addAll(itemRequest.getImgs());
        return images;
    }

    private void checkHideAllItemsBySource(Item item, ItemRequest itemRequest){
        if (item.isPublishedBySource() != itemRequest.isPublishedBySource()){
            item.setPublishedBySource(itemRequest.isPublishedBySource());
            editAllSourceItemsPublishedBySource(item.getSource(), itemRequest.isPublishedBySource());
        }
    }

    private void removeImagesFromItemByRequest(Item item, ItemRequest itemRequest){
        List<String> imagesForRemove = item.getImageUrls();
        imagesForRemove.removeAll(itemRequest.getImgs());
        imagesForRemove.forEach(s -> {
            try {
                imageService.removeImage(s.replace("/api/image/", ""));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private String decodeToFileAndUpload(String base64, String itemName, String fileType) throws IOException {
        final String BASE_FILE_URL = "/api/file/";
        String filesDir = Utils.filesPath;

        String fileDataBytes = base64.substring(base64.indexOf(",") + 1);
        byte[] decoded = Base64.getDecoder().decode(fileDataBytes.getBytes(StandardCharsets.UTF_8));
        String name = getFileName(filesDir, itemName, fileType);
        Path destinationFile = Paths.get(filesDir, name);
        Files.write(destinationFile, decoded);
        return BASE_FILE_URL + name;
    }

    private List<String> decodeToImageAndUpload(List<String> imageStrings, String itemName) throws Exception {
        final String BASE_IMG_URL = "/api/image/";
        List<String> imageURLs = new ArrayList<>();

        String workingDir = Utils.imagesPath;

        for (int i = 0; i < imageStrings.size(); i++) {
            String imageDataBytes = imageStrings.get(i).substring(imageStrings.get(i).indexOf(",") + 1);

            byte[] decodedImg = Base64.getDecoder().decode(imageDataBytes.getBytes(StandardCharsets.UTF_8));
            String imageFormat = imageStrings.get(i).split("/")[1].split(";")[0];

            String name = getImageName(itemName, imageFormat);
            Path destinationFile = Paths.get(workingDir, name);

            Files.write(destinationFile, decodedImg);
            imageURLs.add(BASE_IMG_URL + name);
        }
        return imageURLs;
    }

    private String getFileName(String workDir, String itemName, String fileType){
        String fileName = itemName.replace(" ", "").trim().toLowerCase() + fileType;
        boolean b = checkName(workDir, fileName);

        while (b) {
            Random random = new Random();
            fileName = fileName.replace(".", random.nextInt(100) + ".");
            b = checkName(workDir, fileName);
        }
        return fileName;
    }

    private String getImageName(String itemName, String format) throws IOException {
        String workingDir = Utils.imagesPath;

        String imageName = itemName.replaceAll("[^A-Za-z0-9]","").toLowerCase() + "." + format;

        boolean b = checkName(workingDir, imageName);

        while (b) {
            Random random = new Random();
            imageName = imageName.replace(".", random.nextInt(100) + ".");
            b = checkName(workingDir, imageName);
        }
        return imageName;
    }

    private boolean checkName(String workingDir, String name) {
        File workingDirFile = new File(workingDir);
        File testFile = new File(workingDirFile, name);
        return testFile.exists();
    }

    @Override
    public List<User> getUsers(User user, int page) {
        Pageable request = new PageRequest(page, PAGE_SIZE);
        Page<User> userPage = userRepository.findDistinctByIdNot(user.getId(), request);
        return userService.setIsOnline(userPage.getContent());
    }

    private List<String> getItemsTypesDistinct() {
        Set<String> types = new HashSet<>();
        types.addAll(mongoTemplate.getCollection("product").distinct("type"));
        types.addAll(mongoTemplate.getCollection("model3D").distinct("type"));
        types.remove(null);
        return new ArrayList<>(types);

    }
}
