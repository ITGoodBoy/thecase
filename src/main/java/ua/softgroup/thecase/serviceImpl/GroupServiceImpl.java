package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.service.FunnelService;
import ua.softgroup.thecase.service.GroupService;

import java.util.List;

/**
 * Created by java-1-07 on 11.08.2017.
 */
@Service
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final FunnelService funnelService;

    @Autowired
    public GroupServiceImpl(GroupRepository groupRepository, FunnelService funnelService) {
        this.groupRepository = groupRepository;
        this.funnelService = funnelService;
    }

    @Override
    public ResponseEntity<String> addGroup(String groupName, String groupIconNameFromMaterialIcons) {
        Group group = groupRepository.findByNameIgnoreCase(groupName);
        if (group != null) return new ResponseEntity<>("group name exist", HttpStatus.CONFLICT);

        group = new Group(groupName, groupIconNameFromMaterialIcons);
        groupRepository.save(group);

        return new ResponseEntity<>("group added", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> removeGroup(String groupName) {
       /* if (groupName.equals("Type"))
            return new ResponseEntity<>("Can not delete this group", HttpStatus.CONFLICT);*/

        Group group = groupRepository.findByName(groupName);
        if (group == null)
            return new ResponseEntity<>("group not exist", HttpStatus.NO_CONTENT);

        groupRepository.delete(group);

        return ResponseEntity.ok("group deleted");
    }

    @Override
    public ResponseEntity<String> renameGroup(String groupOldName, String groupNewName) {
        /*if (groupOldName.equals("Type"))
            return new ResponseEntity<>("Can not rename this group", HttpStatus.CONFLICT);*/

        Group group = groupRepository.findByName(groupOldName);
        if (group == null) return new ResponseEntity<>("group not exist", HttpStatus.NO_CONTENT);

        group.setName(groupNewName);
        groupRepository.save(group);

        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @Override
    public ResponseEntity hideOrShowGroup(String groupName) {
        Group group = groupRepository.findByName(groupName);
        if (group == null) return new ResponseEntity<>("group not exist", HttpStatus.NO_CONTENT);

        boolean published = !group.isPublished();
        if (!published)
            funnelService.deleteAllFunnelsByGroup(group);

        group.setPublished(published);
        groupRepository.save(group);
        return ResponseEntity.ok().build();
    }

    @Override
    public List<Group> getAllGroups() {
        return groupRepository.findAll();
    }

}
