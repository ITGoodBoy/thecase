package ua.softgroup.thecase.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.ShortCase;
import ua.softgroup.thecase.repository.*;
import ua.softgroup.thecase.service.*;
import ua.softgroup.thecase.utils.Comparators;

import java.util.*;

@Service
public class CaseServiceImpl implements CaseService {

    private CaseRepository caseRepository;
    private RolePermissionRepository rolePermissionRepository;
    private InviteCaseRepository inviteCaseRepository;
    private UserService userService;
    private UserRepository userRepository;
    private ItemService itemService;
    private TeamService teamService;
    private CaseHistoryService caseHistoryService;
    private ItemStatisticService itemStatisticService;

    private final LikeRepository likeRepository;

    @Autowired
    public CaseServiceImpl(CaseRepository caseRepository, RolePermissionRepository rolePermissionRepository,
                           InviteCaseRepository inviteCaseRepository, UserService userService,
                           UserRepository userRepository, ItemService itemService, TeamService teamService,
                           CaseHistoryService caseHistoryService, ItemStatisticService itemStatisticService, LikeRepository likeRepository) {
        this.caseRepository = caseRepository;
        this.rolePermissionRepository = rolePermissionRepository;
        this.inviteCaseRepository = inviteCaseRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.itemService = itemService;
        this.teamService = teamService;
        this.caseHistoryService = caseHistoryService;
        this.itemStatisticService = itemStatisticService;
        this.likeRepository = likeRepository;
    }

    @Override
    public Case getActiveCase(User user) {
        Case activeCase = caseRepository.findOne(user.getActiveCaseId());
        if (activeCase == null && caseRepository.findByOwnerOrderByCreatedAtDesc(user).size()>0) {
            activeCase = caseRepository.findByOwnerOrderByCreatedAtDesc(user).get(0);
            user.setActiveCaseId(activeCase.getId());
            userRepository.save(user);
        } else if (activeCase == null && caseRepository.findByOwnerOrderByCreatedAtDesc(user).size()<1){
            Case caSe = Case.builder()
                    .name("default").owner(user).description("default case")
                    .products(new TreeSet<>(new Comparators.TimeDescComparator()))
                    .models3D(new TreeSet<>(new Comparators.TimeDescComparator()))
                    .build();
            caseRepository.save(caSe);

            user.setActiveCaseId(caseRepository.findByNameAndOwner(caSe.getName(), user).getId());
            userRepository.save(user);
            activeCase = caseRepository.findByNameAndOwner(caSe.getName(), caSe.getOwner());
        }
        //Dirty
        activeCase.setMine(activeCase.getOwner().equals(user));
        activeCase.setPermission(getPermission(user, activeCase));

        return activeCase;
    }

    private Integer getPermission(User user, Case activeCase) {
        Optional<InviteCase> optionalProject =
                Optional.ofNullable(inviteCaseRepository.findByInvitedUserAndACase(user, activeCase));
        Optional<Integer> permission =
                optionalProject.map(InviteCase::getPermissionNumber);
        return permission.orElse(0);
    }

    @Override
    public List<ShortCase> getCasesNames(User user) {
        List<Case> cases = caseRepository.findByOwnerOrderByCreatedAtDesc(user);
        List<InviteCase> inviteCases = inviteCaseRepository.findByInvitedUser(user);

        if ((cases == null || cases.size() == 0) && (inviteCases == null || inviteCases.size() == 0))
            return null;

        List<ShortCase> shortCases = new ArrayList<>();
        for (Case aCase1 : cases) {
            boolean isActive = aCase1.getId().equals(user.getActiveCaseId());
            shortCases.add(new ShortCase(aCase1, isActive));
        }

        inviteCases.forEach(i -> {
            boolean isActive = i.getACase().getId().equals(user.getActiveCaseId());
            shortCases.add(new ShortCase(i.getACase(), isActive, i.isConfirmed(), i.getPermissionNumber()));
        });
        return shortCases;
    }

    @Override
    public Case getCaseById(String caseId) {
        return caseRepository.findOne(caseId);
    }

    @Transactional
    @Override
    public ResponseEntity<Case> getCaseByIdAndActivateIt(String caseId) {
        Case aCase = caseRepository.findOne(caseId);
        if (aCase == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        User user = userService.getLoggedUser();

        if (aCase.getOwner().equals(user) || teamService.isConfirmed(user, aCase)) {
            user.setActiveCaseId(aCase.getId());
            userRepository.save(user);

            //Dirty
            aCase.setMine(aCase.getOwner().equals(user));
            aCase.setPermission(getPermission(user, aCase));

            return ResponseEntity.ok(aCase);
        }

        return new ResponseEntity<>(HttpStatus.PRECONDITION_REQUIRED);
    }

    @Override
    public ResponseEntity<Case> addItem(String caseId, String itemId) {
        User user = userService.getLoggedUser();
        Case aCase = caseRepository.findOne(caseId);
        Item item = itemService.getItemById(itemId);

        if (aCase == null || item == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        if (isContain(aCase, item))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        if (aCase.getOwner().equals(user) || teamService.isCanAddItem(user, aCase)) {
            SortedSet<ItemHolder> itemHolders = aCase.getItemHolders(item.getClass());
            itemHolders.stream().filter(p -> p.getItem().equals(item))
                    .findFirst()
                    .orElseGet(() -> {
                        ItemHolder holder = new ItemHolder(item, user.getUsername(), isLiked(item, user));
                        itemHolders.add(holder);
                        return holder;
                    })
                    .setAddedAt(System.currentTimeMillis());

            if (!aCase.getOwner().equals(user))
                caseHistoryService.addCaseChange(aCase, CaseChange.ADD_ITEM + item.getName(), user.getEmail());

            caseRepository.save(aCase);

            itemStatisticService.addCaseCount(item); // add countOfCases to statistic

            return ResponseEntity.ok(aCase);
        }

        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public ResponseEntity<Case> changeItemCount(String itemId, int count) {
        User user = userService.getLoggedUser();
        Case aCase = getActiveCase(user);
        Item item = itemService.getItemById(itemId);

        if (aCase == null || item == null)
            return ResponseEntity.noContent().build();

        ItemHolder itemHolder = aCase.getItemHolders(item.getClass()).stream().filter(i -> i.getItem().equals(item)).findFirst().orElse(null) ;
        if (itemHolder == null)
            return ResponseEntity.noContent().build();

        if (aCase.getOwner().equals(user) || teamService.isCanChangeCount(user, aCase, itemHolder)) {
            itemHolder.setCount(count);
            caseRepository.save(aCase);

            if (!aCase.getOwner().equals(user))
                caseHistoryService.addCaseChange(aCase, CaseChange.CHANGE_COUNT, user.getEmail());

            return ResponseEntity.ok(caseRepository.findOne(aCase.getId()));
        }
        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public ResponseEntity<Case> changeItemCountAndPrice(String itemId, int count, long amount, String currency) {
        User user = userService.getLoggedUser();
        Case aCase = getActiveCase(user);
        Item item = itemService.getItemById(itemId);

        if (aCase == null || item == null)
            return ResponseEntity.noContent().build();

        ItemHolder itemHolder = aCase.getItemHolders(item.getClass()).stream().filter(i -> i.getItem().equals(item)).findFirst().orElse(null) ;
        if (itemHolder == null)
            return ResponseEntity.noContent().build();

        if (teamService.isCanChangeCountAndPrice(user, aCase, itemHolder)){
            itemHolder.setCount(count);
            itemHolder.setPrice(new Price(amount, currency));

            caseHistoryService.addCaseChange(aCase, CaseChange.CHANGE_COUNT_AND_PRICE, user.getEmail());

            caseRepository.save(aCase);
            return ResponseEntity.ok(caseRepository.findOne(aCase.getId()));
        }
        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public ResponseEntity<Case> removeItem(String itemId) {
        User user = userService.getLoggedUser();
        Case aCase = getActiveCase(user);
        Item item = itemService.getItemById(itemId);

        if (aCase == null || item == null || !isContain(aCase, item))
            return ResponseEntity.noContent().build();

        SortedSet<ItemHolder> itemHolders = aCase.getItemHolders(item.getClass());
        ItemHolder itemHolder = itemHolders.stream()
                .filter(h -> h.getItem().equals(item)).findFirst().orElse(null);

        if (itemHolder == null)
            return ResponseEntity.noContent().build();

        if (aCase.getOwner().equals(user) || teamService.isCanRemoveItem(user, aCase, itemHolder)){
            itemHolders.remove(itemHolder);

            if (!aCase.getOwner().equals(user))
                caseHistoryService.addCaseChange(aCase, CaseChange.REMOVE_ITEM + item.getName(), user.getEmail());

            caseRepository.save(aCase);

            itemStatisticService.removeCaseCount(item); // remove countOfCase from statistic count

            return ResponseEntity.ok(caseRepository.findOne(aCase.getId()));
        }
        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public ResponseEntity<Case> makeItemFirstInCase(String itemId) {
        User user = userService.getLoggedUser();
        Case aCase = getActiveCase(user);
        Item item = itemService.getItemById(itemId);

        if (aCase == null || item == null || !isContain(aCase, item))
            return ResponseEntity.noContent().build();

        Set<ItemHolder> products = aCase.getProducts();
        Set<ItemHolder> models = aCase.getModels3D();
        products.forEach(itemHolder -> {
            if (itemHolder.getItem().equals(item))
                itemHolder.setAddedAt(System.currentTimeMillis());
        });
        models.forEach(itemHolder -> {
            if (itemHolder.getItem().equals(item))
                itemHolder.setAddedAt(System.currentTimeMillis());
        });

        caseRepository.save(aCase);
        return ResponseEntity.ok(caseRepository.findOne(aCase.getId()));
    }

    @Override
    public boolean isContain(Case caSe, Item item) {
        return caSe.getItemHolders(item.getClass()).stream().anyMatch(i -> i.getItem().equals(item));
    }

    @Override
    public List<Case> remove(Case aCase) {

        itemStatisticService.removeCaseCount(aCase.getAllItems()); // remove countOfCase from statistic count

        User user = aCase.getOwner();
        inviteCaseRepository.delete(inviteCaseRepository.findByACase(aCase));
        caseRepository.delete(aCase);
        return caseRepository.findByOwnerOrderByCreatedAtDesc(user);
    }

    @Override
    public Case removeAllItems(Case aCase) {

        itemStatisticService.removeCaseCount(aCase.getAllItems()); // remove countOfCase from statistic count

        aCase.clearBoth();
        caseRepository.save(aCase);
        return aCase;
    }

    @Override
    public Case removeAllProducts(Case aCase) {

        itemStatisticService.removeCaseCount(aCase.getItems(Product.class)); // remove countOfCase from statistic count

        aCase.clearProducts();
        caseRepository.save(aCase);
        return aCase;
    }

    @Override
    public Case removeAllModels3D(Case aCase) {

        itemStatisticService.removeCaseCount(aCase.getItems(Model3D.class)); // remove countOfCase from statistic count

        aCase.clearModels();
        caseRepository.save(aCase);
        return aCase;
    }

    @Override
    public Case create(User owner, String name, String description) {
        Case caSe =
                Case.builder()
                        .name(name.trim())
                        .owner(owner)
                        .description(description)
                        .products(new TreeSet<>(new Comparators.TimeDescComparator()))
                        .models3D(new TreeSet<>(new Comparators.TimeDescComparator()))
                        .build();
        caseRepository.save(caSe);
        caSe = caseRepository.findByNameAndOwner(caSe.getName(), owner);
        owner.setActiveCaseId(caSe.getId());
        return caSe;
    }

    @Override
    public ResponseEntity<Case> rename(String caseId, String name, String description) {

        Case aCase = caseRepository.findOne(caseId);
        if (aCase == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        User user = userService.getLoggedUser();

        if (!isCaseNameUnique(user, name))
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

        if (aCase.getOwner().equals(user) || teamService.isCanRename(user, aCase)) {
            aCase.setName(name);
            if (description != null)
                aCase.setDescription(description);

            if (!aCase.getOwner().equals(user))
                caseHistoryService.addCaseChange(aCase, CaseChange.RENAME_CASE + aCase.getName(), user.getEmail());

            caseRepository.save(aCase);
            return ResponseEntity.ok(aCase);
        }
        return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
    }

    @Override
    public boolean isCanCreateNewCase(User owner) {
        List<Case> cases = caseRepository.findByOwnerOrderByCreatedAtDesc(owner);

        List<Role> roles = owner.getSecondaryRoles();
        int maxcount = 0;

        for (Role role : roles) {
            RolePermission rolePermission = rolePermissionRepository.findByRole(role);
            if (maxcount < rolePermission.getCasesMaxCount()) {
                maxcount = rolePermission.getCasesMaxCount();
            }
        }

        return cases.size() < maxcount;
    }

    @Override
    public boolean isCaseNameUnique(User owner, String name) {
        List<Case> cases = caseRepository.findByOwnerOrderByCreatedAtDesc(owner);
        Case mCase = cases.stream().filter(c -> c.getName().toLowerCase().equals(name.trim().toLowerCase())).findFirst().orElse(null);
        return mCase == null;
    }

    public boolean isLiked(Item item, User user) {
        return likeRepository.existsByItemAndLiker(item, user);
    }
}
