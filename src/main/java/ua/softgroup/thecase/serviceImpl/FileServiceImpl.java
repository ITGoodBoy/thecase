package ua.softgroup.thecase.serviceImpl;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import ua.softgroup.thecase.Utils;
import ua.softgroup.thecase.service.FileService;

import java.io.File;


/**
 * Created by java-1-03 on 24.01.2018.
 */
@Service
public class FileServiceImpl implements FileService {
    private final String BASE_IMG_URL = "/api/file/";

    @Override
    public File getFile(@PathVariable String id) {
        return new File(Utils.imagesPath +id);
    }
}
