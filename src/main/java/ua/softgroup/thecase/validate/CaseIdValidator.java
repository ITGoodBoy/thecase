package ua.softgroup.thecase.validate;

import org.springframework.beans.factory.annotation.Autowired;
import ua.softgroup.thecase.repository.CaseRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by java-1-03 on 04.08.2017.
 */
public class CaseIdValidator implements ConstraintValidator<CaseLive, String> {

    @Autowired
    private CaseRepository caseRepository;

    @Override
    public void initialize(CaseLive caseLive) {

    }

    @Override
    public boolean isValid(String caseId, ConstraintValidatorContext constraintValidatorContext) {

        System.out.println("vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaalidation");

        return caseId != null && caseRepository.findOne(caseId) != null;
    }
}
