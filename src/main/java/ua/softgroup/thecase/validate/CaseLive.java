package ua.softgroup.thecase.validate;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by java-1-03 on 04.08.2017.
 */
@Documented
@Constraint(validatedBy = CaseIdValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CaseLive {
    String message() default "Invalid caseID";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
