package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.softgroup.thecase.Utils;
import ua.softgroup.thecase.service.ImageService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

/**
 * Created by TLJD Andrew Kobin on 06.07.2017.
 */
@RestController
@RequestMapping("/api")
public class ImageController {

    private ImageService imageService;

    private final String BASE_IMG_URL = "/api/image/";

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @RequestMapping(value = "/image/{id:.+}", method = RequestMethod.GET,  produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE})
    public ResponseEntity<byte[]> getImage(@PathVariable String id) throws IOException {
        return imageService.getImage(id);
    }

    @RequestMapping(value = "/image/upload", method = RequestMethod.POST)
    public ResponseEntity<String> uploadImage(@RequestParam("image") MultipartFile file) throws IOException {

        if (!file.isEmpty()){
            String workingDir = Utils.imagesPath;
            String imageName = file.getOriginalFilename();

            boolean b = checkName(workingDir, imageName);

            while (b){
                Random random = new Random();
                imageName = imageName.replace(".", random.nextInt(100)+".");
                b = checkName(workingDir, imageName);
            }
            System.out.println(imageName);

            byte[] bytes = file.getBytes();
            Path path = Paths.get(workingDir + imageName);
            Files.write(path, bytes);

            return new ResponseEntity<>(BASE_IMG_URL + imageName, HttpStatus.CREATED);

        } else {
            return new ResponseEntity<>("You failed to upload", HttpStatus.NO_CONTENT);
        }
    }

    private boolean checkName(String workingDir, String name){
        File workingDirFile = new File(workingDir);
        File testFile = new File(workingDirFile, name);
        return testFile.exists();
    }
}
