package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.RolePermission;
import ua.softgroup.thecase.service.RolePermissionService;

/**
 * Created by java-1-07 on 18.07.2017.
 */
@RestController
@RequestMapping(value = "/api/rolePermissions", headers = TokenAuthService.AUTH_HEADER_NAME)
public class RolePermissionController {

    private RolePermissionService rolePermissionService;

    @Autowired
    public RolePermissionController(RolePermissionService rolePermissionService) {
        this.rolePermissionService = rolePermissionService;
    }

    @RequestMapping(name = "/getById/{id}", method = RequestMethod.GET)
    public ResponseEntity<RolePermission> getById(@PathVariable String id) {
        RolePermission rolePermission = rolePermissionService.getById(id);

        if (rolePermission == null) return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        return ResponseEntity.ok(rolePermission);
    }
}
