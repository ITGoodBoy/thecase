package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Filter;
import ua.softgroup.thecase.model.message.FilterFieldsResponse;
import ua.softgroup.thecase.service.FilterService;

/**
 * Created by java-1-03 on 26.09.2017.
 */
@RestController
@RequestMapping(value = "/api/filters", headers = TokenAuthService.AUTH_HEADER_NAME)
public class FilterController {

    private final FilterService filterService;

    @Autowired
    public FilterController(FilterService filterService) {
        this.filterService = filterService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<FilterFieldsResponse> getFilterFields() {
        return filterService.getFilterFields(null);
    }

    @RequestMapping(value = "/brands/{name}", method = RequestMethod.GET)
    public ResponseEntity<FilterFieldsResponse> getFilterFields(@PathVariable String name) {
        return filterService.getFilterFields(name);
    }

    @RequestMapping(value = "/active", method = RequestMethod.GET)
    public ResponseEntity<Filter> getActiveFilter() {
        return filterService.getActiveFilter();
    }

    @RequestMapping(value = "/active", method = RequestMethod.DELETE)
    public ResponseEntity<Filter> deActivateFilter() { return filterService.deactivateFilter(); }

    @RequestMapping(value = "/{filter}/{name}", method = RequestMethod.POST)
    public ResponseEntity<Filter> activateFilter(@PathVariable String filter, @PathVariable String name) {
        return filterService.changeFilter(filter, name, true);
    }

    @RequestMapping(value = "/{filter}/{name}", method = RequestMethod.DELETE)
    public ResponseEntity<Filter> deActivateFilter(@PathVariable String filter, @PathVariable String name) {
        return filterService.changeFilter(filter, name, false);
    }

    @RequestMapping(value = "/{filter}", method = RequestMethod.DELETE)
    public ResponseEntity<Filter> deActivateFilterByType(@PathVariable String filter) {
        return filterService.deActivateFilterByType(filter);
    }

}
