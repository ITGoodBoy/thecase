package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.service.CleanerService;

/**
 * Created by TLJD Andrew Kobin on 04.10.2017.
 */

@RestController
@RequestMapping(value = "/api/admin/blacklist", headers = TokenAuthService.AUTH_HEADER_NAME)
public class CleanerController {

    @Autowired
    private CleanerService cleanerService;

    @GetMapping
    public ResponseEntity getBlackList(){
        return ResponseEntity.ok(cleanerService.getBlackList());
    }

    @DeleteMapping
    public ResponseEntity deleteWord(@RequestParam String word){
        return ResponseEntity.ok(cleanerService.removeFromBlackList(word));
    }

    @PostMapping
    public ResponseEntity addBlackList(@RequestParam String word){
        return (cleanerService.addToBlackList(word))?ResponseEntity.ok().build():ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/cleanup")
    public ResponseEntity cleanUp(){
        int i = cleanerService.cleanUp();
        return ResponseEntity.ok(i);
    }

    @GetMapping(value = "/cleanup/source")
    public ResponseEntity cleanUpSource(){
        cleanerService.cleanUpSource();
        return ResponseEntity.ok().build();
    }


}
