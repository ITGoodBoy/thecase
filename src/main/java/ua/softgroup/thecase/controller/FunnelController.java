package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Funnel;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.FunnelFieldsResponse;
import ua.softgroup.thecase.repository.GroupRepository;
import ua.softgroup.thecase.service.FunnelService;
import ua.softgroup.thecase.service.ItemService;
import ua.softgroup.thecase.service.UserService;

import java.util.List;

/**
 * Created by TLJD Andrew Kobin on 28.08.2017.
 */
@RestController
@RequestMapping(value = "/api/funnels", headers = TokenAuthService.AUTH_HEADER_NAME)
public class FunnelController {

    private final GroupRepository groupRepository;
    private final FunnelService funnelService;
    private final UserService userService;

    private final ItemService itemService;

    @Autowired
    public FunnelController(GroupRepository groupRepository, FunnelService funnelService, UserService userService, ItemService itemService) {
        this.groupRepository = groupRepository;
        this.funnelService = funnelService;
        this.userService = userService;
        this.itemService = itemService;
    }

    @GetMapping()
    public ResponseEntity<List<FunnelFieldsResponse>> getFunnels() {
        User user = userService.getLoggedUser();
        if (user == null)
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        return funnelService.getFunnels(user);
    }

    @GetMapping(value = "/active")
    public ResponseEntity<List<Funnel>> getActiveFunnels() {

        User user = userService.getLoggedUser();
        if (user == null)
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        return ResponseEntity.ok(funnelService.getActiveFunnels(user));
    }


    @GetMapping(value = "/activateByItem")
    public ResponseEntity<List<Funnel>> activateFunnelByItem(@RequestParam String itemId) {

        User user = userService.getLoggedUser();
        if (user == null)
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        Item item = itemService.getItemById(itemId);
        if(item==null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return funnelService.activateFunnelsByItem(user, itemId);
    }

    @GetMapping(value = "/activateByGroup")
    public ResponseEntity<List<Funnel>> activateFunnelByGroup(@RequestParam String groupId) {

        Group group = groupRepository.findOne(groupId);
        if (group == null)
            return ResponseEntity.noContent().build();
        else
            return funnelService.activateFunnelByGroup(group);
    }

    @RequestMapping(value = "/deactivateFunnel", method = RequestMethod.DELETE)
    public ResponseEntity deactivateFunnel(@RequestParam String groupId) {
        Group group = groupRepository.findOne(groupId);
        if (group == null)
            return ResponseEntity.noContent().build();
        else{
            funnelService.deactivateFunnel(group);
            return ResponseEntity.ok().build();
        }
    }

    @RequestMapping(value = "/deactivateFunnels", method = RequestMethod.DELETE)
    public ResponseEntity deactivateFunnel() {
        funnelService.deactivateFunnels();
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/manual")
    public ResponseEntity<List<FunnelFieldsResponse>> manualSelecting(@RequestBody List<String> tags){
        User user = userService.getLoggedUser();
        if (user == null)
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        return funnelService.manualSelecting(user, tags);
    }

}
