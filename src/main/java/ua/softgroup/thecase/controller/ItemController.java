package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Model3D;
import ua.softgroup.thecase.model.Product;
import ua.softgroup.thecase.model.message.ItemResponse;
import ua.softgroup.thecase.service.ItemService;
import ua.softgroup.thecase.service.ItemStatisticService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/items", headers = TokenAuthService.AUTH_HEADER_NAME)
public class ItemController {

    private final ItemService itemService;
    private final ItemStatisticService itemStatisticService;


    @Autowired
    public ItemController(ItemService itemService, ItemStatisticService itemStatisticService) {
        this.itemService = itemService;
        this.itemStatisticService = itemStatisticService;
    }

//    @Deprecated
//    @GetMapping(value = {"/all/{page}", "/all"})
//    public ResponseEntity<?> getAllItems(@PathVariable Map<String, String> pathVariables) {
//        int page = 0;
//        if (pathVariables.containsKey("page")) {
//            try {
//                page = Integer.parseInt(pathVariables.get("page"));
//            } catch (NumberFormatException e) {
//                e.printStackTrace();
//            }
//        }
//        List<Item> items = itemService.findAll(page);
//        return ResponseEntity.ok(items);
//    }

    @GetMapping(value = {"/products/{page}/{name}", "/products/{page}", "/products"})
    public ResponseEntity<?> getProducts(@PathVariable Map<String, String> pathVariables) {
        int page = 0;

        if (pathVariables.containsKey("page")) {
            try {
                page = Integer.parseInt(pathVariables.get("page"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        if (page < 0) return new ResponseEntity<>("Page #" + page + " doesn`t exist", HttpStatus.BAD_REQUEST);

        if (!pathVariables.containsKey("name"))
            return ResponseEntity.ok(itemService.getItems(page, Product.class));

        return ResponseEntity.ok(itemService.findProductByNameStartingWith(pathVariables.get("name"), page));
    }

    @GetMapping(value = {"/models/{page}/{name}", "/models/{page}", "/models"})
    public ResponseEntity<?> getModels(@PathVariable Map<String, String> pathVariables) {
        int page = 0;
        if (pathVariables.containsKey("page")) {
            try {
                page = Integer.parseInt(pathVariables.get("page"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        if (page < 0) return new ResponseEntity<>("Page #" + page + " doesn`t exist", HttpStatus.BAD_REQUEST);
        if (!pathVariables.containsKey("name"))
            return ResponseEntity.ok(itemService.getItems(page, Model3D.class));

        return ResponseEntity.ok(itemService.findModel3DByNameStartingWith(pathVariables.get("name"), page));
    }

    @RequestMapping(value = "/{id}/like", method = RequestMethod.POST)
    public ResponseEntity<ItemResponse> likeItem(@PathVariable String id) {

        Item item = itemService.findItemById(id);
        if (item == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return ResponseEntity.ok(itemService.likeItem(item));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ResponseEntity openItem(@PathVariable String id) {

        Item item = itemService.findItemById(id);
        if (item == null)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        itemStatisticService.addOpeningCount(item);
        return ResponseEntity.ok().build();
    }

}