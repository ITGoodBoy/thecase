package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.MessageRequest;
import ua.softgroup.thecase.model.message.PermissionResponse;
import ua.softgroup.thecase.model.message.UserResponse;
import ua.softgroup.thecase.repository.UserRepository;
import ua.softgroup.thecase.service.CaseService;
import ua.softgroup.thecase.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;
    private UserRepository userRepository;

    private CaseService caseService;

    @Autowired
    public UserController(UserService userService, UserRepository userRepository, CaseService caseService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.caseService = caseService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<String> login(@RequestParam String email, @RequestParam String password) {

        User user = userRepository.findByEmail(email);

        System.out.println(" -------- " + email + " - " + password);

        if (user == null || !userService.isPasswordMatch(user, password))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        if (!user.isEnabled())
            return new ResponseEntity<>("Not confirmed email", HttpStatus.NOT_ACCEPTABLE);

        if (!user.isAccountNonLocked())
            return new ResponseEntity<>("The user is blocked", HttpStatus.LOCKED);

        return ResponseEntity.ok(userService.login(user, email, password));
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public ResponseEntity<String> logoutPage (HttpServletRequest request, HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return ResponseEntity.ok().build();
    }



   /* @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ResponseEntity<String> registration(@RequestParam String userName,
                                               @RequestParam String email,
                                               @RequestParam String password) {
        return userService.registration(userName, email, password);
    }*/

    // TODO: 08.09.2017 test with Vasya

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ResponseEntity<String> registration(@RequestParam String userName,
                                               @RequestParam String email,
                                               @RequestParam String password,
                                               @RequestParam(required = false) String referrer) {
        if (referrer != null)
            return userService.registration(userName, email, password, referrer);
        else
            return userService.registration(userName, email, password);
    }

    @RequestMapping(value = "/registration/{token}", method = RequestMethod.GET)
    public ResponseEntity<String> registrationConfirm(@PathVariable String token) {
        return userService.registrationConfirm(token);
    }

    @RequestMapping(value = "/resetForgottenPassword", method = RequestMethod.PUT)
    public ResponseEntity<String> resetForgottenPassword(@RequestParam String email) {
        return userService.resetForgottenPassword(email);
    }

    @RequestMapping(value = "/resetForgottenPasswordConfirm/{token}", method = RequestMethod.GET)
    public ResponseEntity<String> resetForgottenPasswordConfirm(@PathVariable String token) {
        return userService.resetForgottenPasswordConfirm(token);
    }

    @RequestMapping(value = "/replacePassword", method = RequestMethod.PUT)
    public ResponseEntity<String> replacePassword(@RequestParam String email,
                                                  @RequestParam String oldPassword,
                                                  @RequestParam String newPassword) {
        return userService.replacePassword(email, oldPassword, newPassword);
    }

    @GetMapping(value = {"/{page}/{name}", "/{page}", ""}, headers = TokenAuthService.AUTH_HEADER_NAME)
    public ResponseEntity<List<UserResponse>> findUsersStartingWith(@PathVariable Map<String, String> pathVariables) {
        int page = 0;
        if (pathVariables.containsKey("page")) {
            try {
                page = Integer.parseInt(pathVariables.get("page"));
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
        }
        if(page<0) return ResponseEntity.badRequest().build();

        if (!pathVariables.containsKey("name"))
            return ResponseEntity.ok(userService.getAllUsers(page));

        return ResponseEntity.ok(userService.getUsersStartingWith(pathVariables.get("name"), page));
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.POST, headers = TokenAuthService.AUTH_HEADER_NAME)
    public ResponseEntity addUserToContactList(@RequestParam String userId) {
        return ResponseEntity.ok(userService.addUserToContactList(userId));
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.DELETE, headers = TokenAuthService.AUTH_HEADER_NAME)
    public ResponseEntity removeUserFromContactList(@RequestParam String userId) {
        return ResponseEntity.ok(userService.removeUserFromContactList(userId));
    }

    @GetMapping(value = {"/contacts/{page}", "/contacts"}, headers = TokenAuthService.AUTH_HEADER_NAME)
    public ResponseEntity getContactList(@PathVariable Map<String, String> pathVariables) {
        int page = 0;
        if (pathVariables.containsKey("page")) {
            try {
                page = Integer.parseInt(pathVariables.get("page"));
            } catch (NumberFormatException e) {
//                e.printStackTrace();
            }
        }
        if(page<0) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(userService.getContactList(page));
    }

    @RequestMapping(value = "/invite", method = RequestMethod.POST, headers = TokenAuthService.AUTH_HEADER_NAME)
    public ResponseEntity<String> inviteUserToSystem(@RequestParam String email) {
        return userService.inviteUserToSystem(email);
    }

    @RequestMapping(value = "/message", method = RequestMethod.POST, headers = TokenAuthService.AUTH_HEADER_NAME)
    public ResponseEntity<String> sendMessage(@RequestParam String email, @RequestBody MessageRequest messageRequest) {
        return userService.sendMessage(email, messageRequest);
    }

    @GetMapping(value = "/access", headers = TokenAuthService.AUTH_HEADER_NAME)
    public ResponseEntity<PermissionResponse> isCanCreateNewCase(){
        PermissionResponse response = new PermissionResponse();
        response.setCan_create_case(caseService.isCanCreateNewCase(userService.getLoggedUser()));
        return ResponseEntity.ok(response);
    }


}
