package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Subscription;
import ua.softgroup.thecase.service.SubscriptionService;

/**
 * Created by java-1-07 on 18.07.2017.
 */
@RestController
@RequestMapping(value = "/api/subscriptions", headers = TokenAuthService.AUTH_HEADER_NAME)
public class SubscriptionController {

    private SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @RequestMapping(name = "/getById/{id}", method = RequestMethod.GET)
    public ResponseEntity<Subscription> getById(@PathVariable String id) {
        Subscription subscription = subscriptionService.getById(id);

        if (subscription == null) return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        return ResponseEntity.ok(subscription);
    }
}
