package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.TeamResponse;
import ua.softgroup.thecase.service.CaseService;
import ua.softgroup.thecase.service.TeamService;
import ua.softgroup.thecase.service.UserService;

import java.util.List;

/**
 * Created by java-1-03 on 11.09.2017.
 */
@RestController
@RequestMapping(value = "/api/team", headers = TokenAuthService.AUTH_HEADER_NAME)
public class TeamController {

    private TeamService teamService;
    private CaseService caseService;
    private UserService userService;

    @Autowired
    public TeamController(TeamService teamService, CaseService caseService, UserService userService) {
        this.teamService = teamService;
        this.caseService = caseService;
        this.userService = userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity addUserToCase(@RequestParam String caseId, @RequestParam String userId, @RequestParam int permissionNumber){
        return teamService.addUserToCaseOrChangePermission(caseId, userId, permissionNumber);
    }

    @RequestMapping(value = "/users", method = RequestMethod.DELETE)
    public ResponseEntity removeUserFromCase(@RequestParam String caseId, @RequestParam String userId){
        return teamService.removeUserFromCase(caseId, userId);
    }

    @RequestMapping(value = "/cases/{caseId}", method = RequestMethod.PUT)
    public ResponseEntity confirmInvite(@PathVariable String caseId){
        return teamService.confirmInvite(caseId);
    }

    @RequestMapping(value = "/cases/{caseId}/send", method = RequestMethod.POST)
    public ResponseEntity sendCaseToEmail(@PathVariable String caseId, @RequestParam String emailTo){
        return teamService.sendCaseToEmail(caseId, emailTo);
    }

    @RequestMapping(value = "/items/send", method = RequestMethod.POST)
    public ResponseEntity sendItemsToEmail(@RequestParam String emailTo, @RequestBody List<String> itemsId){
        User user = userService.getLoggedUser();
        Case aCase = caseService.getActiveCase(user);

        return teamService.sendItemsToEmail(aCase, emailTo, itemsId);
    }

    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.PUT)
    public ResponseEntity<Case> changeItemCountAnPrice(@PathVariable String itemId, @RequestParam int count, @RequestParam long amount, @RequestParam String currency) {
        return caseService.changeItemCountAndPrice(itemId, count, amount, currency);
    }

    @RequestMapping(value = "/users/{caseId}", method = RequestMethod.GET)
    public ResponseEntity<TeamResponse> getTeamByCase(@PathVariable String caseId) {
        return teamService.getTeamByCase(caseId);
    }

    @RequestMapping(value = "/users/cases", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getTeamAllCases() {
        return teamService.getTeamAllCases();
    }

}
