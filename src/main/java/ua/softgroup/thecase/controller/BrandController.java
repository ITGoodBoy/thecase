package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.service.ItemService;

import java.util.List;

/**
 * Created by java-1-03 on 10.08.2017.
 */

@RestController
@RequestMapping(value = "/api/brands", headers = TokenAuthService.AUTH_HEADER_NAME)
public class BrandController {

    private final ItemService itemService;

    @Autowired
    public BrandController(ItemService itemService) {
        this.itemService = itemService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<String>> getBrands(){
        return ResponseEntity.ok(itemService.findDistinctItemFilterField(Item.Field.brand));
    }

}
