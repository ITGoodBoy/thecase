package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.ShortCase;
import ua.softgroup.thecase.service.CaseService;
import ua.softgroup.thecase.service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/api", headers = TokenAuthService.AUTH_HEADER_NAME)
public class CaseController {

    private CaseService caseService;
    private UserService userService;

    @Autowired
    public CaseController(CaseService caseService, UserService userService) {
        this.caseService = caseService;
        this.userService = userService;
    }

    @RequestMapping(value = "/cases/active", method = RequestMethod.GET)
    public ResponseEntity<Case> getActiveCase() {
        User user = userService.getLoggedUser();
        Case actCase = caseService.getActiveCase(user);
        if (actCase == null)
            return ResponseEntity.noContent().build();

        return ResponseEntity.ok(actCase);
    }

    @RequestMapping(value = "/cases/up/{itemId}", method = RequestMethod.PUT)
    public ResponseEntity<Case> makeItemFirstInCase(@PathVariable String itemId) {
        return caseService.makeItemFirstInCase(itemId);
    }

    @RequestMapping(value = "/cases", method = RequestMethod.GET)
    public ResponseEntity<List<ShortCase>> getCasesNames() {
        User user = userService.getLoggedUser();
        List<ShortCase> shortCases = caseService.getCasesNames(user);
        if (shortCases != null)
            return ResponseEntity.ok(shortCases);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/cases", method = RequestMethod.POST)
    public ResponseEntity<Case> create(@RequestParam String name, @RequestParam(required = false) String description) {
        if (description == null) description = "";

        User user = userService.getLoggedUser();

        if (!caseService.isCanCreateNewCase(user))
            return new ResponseEntity<>(HttpStatus.LOCKED);

        if (!caseService.isCaseNameUnique(user, name))
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

        return ResponseEntity.ok(caseService.create(user, name, description));
    }

    @RequestMapping(value = "/cases/{caseId}", method = RequestMethod.PATCH)
    public ResponseEntity<Case> rename(@PathVariable String caseId, @RequestParam String name, @RequestParam(required = false) String description) {
        return caseService.rename(caseId, name, description);
    }

    @RequestMapping(value = "/cases/{caseId}", method = RequestMethod.DELETE)
    public ResponseEntity<List<Case>> remove(@PathVariable String caseId) {

        User user = userService.getLoggedUser();
        Case aCase = caseService.getCaseById(caseId);

        if (aCase == null)
            return ResponseEntity.noContent().build();

        if (caseService.getCasesNames(user).size() <= 1)
            return new ResponseEntity<>(HttpStatus.LOCKED);

        if (!aCase.getOwner().equals(user))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        return ResponseEntity.ok(caseService.remove(aCase));
    }

    @RequestMapping(value = "/cases/{caseId}", method = RequestMethod.GET)
    public ResponseEntity<Case> getCase(@PathVariable String caseId) {
        return caseService.getCaseByIdAndActivateIt(caseId);
    }

    @RequestMapping(value = "/cases/{caseId}/items", method = RequestMethod.POST)
    public ResponseEntity<Case> addItem(@PathVariable String caseId, @RequestParam String itemId) {
        return caseService.addItem(caseId, itemId);
    }

    @RequestMapping(value = "/cases/{itemId}", method = RequestMethod.PUT)
    public ResponseEntity<Case> changeItemCount(@PathVariable String itemId, @RequestParam int count) {
        return caseService.changeItemCount(itemId, count);
    }

    @RequestMapping(value = "/cases/items/{itemId}", method = RequestMethod.DELETE)
    public ResponseEntity<Case> removeItem(@PathVariable String itemId) {
        return caseService.removeItem(itemId);
    }

    @RequestMapping(value = "/cases/items", method = RequestMethod.DELETE)
    public ResponseEntity<Case> removeAllItems() {

        User user = userService.getLoggedUser();
        Case aCase = caseService.getActiveCase(user);

        if (aCase == null) // never Null
            return ResponseEntity.noContent().build();

        if (!aCase.getOwner().equals(user))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        return ResponseEntity.ok(caseService.removeAllItems(aCase));
    }

    @RequestMapping(value = "/cases/items/products", method = RequestMethod.DELETE)
    public ResponseEntity<Case> removeAllProducts() {

        User user = userService.getLoggedUser();
        Case aCase = caseService.getActiveCase(user);

        if (aCase == null)
            return ResponseEntity.noContent().build();

        if (!aCase.getOwner().equals(user))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        return ResponseEntity.ok(caseService.removeAllProducts(aCase));
    }

    @RequestMapping(value = "/cases/items/models3d", method = RequestMethod.DELETE)
    public ResponseEntity<Case> removeAllModels3D() {

        User user = userService.getLoggedUser();
        Case aCase = caseService.getActiveCase(user);

        if (aCase == null)
            return ResponseEntity.noContent().build();

        if (!aCase.getOwner().equals(user))
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        return ResponseEntity.ok(caseService.removeAllModels3D(aCase));
    }




}