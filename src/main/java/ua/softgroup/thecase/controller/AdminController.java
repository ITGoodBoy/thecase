package ua.softgroup.thecase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.softgroup.thecase.config.TokenAuthService;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Tag;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.ItemRequest;
import ua.softgroup.thecase.model.message.SourceResponse;
import ua.softgroup.thecase.model.message.TagByGroupResponce;
import ua.softgroup.thecase.parser.ArchiproductsParser;
import ua.softgroup.thecase.parser.DDD3RuParser;
import ua.softgroup.thecase.parser.DesignConnectedParser;
import ua.softgroup.thecase.parser.Odesd2;
import ua.softgroup.thecase.service.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by java-1-07 on 08.08.2017.
 */
@RestController
@RequestMapping(value = "/api/admin", headers = TokenAuthService.AUTH_HEADER_NAME)
public class AdminController {

    private AdminService adminService;
    private ItemService itemService;

    private UserService userService;
    private final GroupService groupService;
    private final TagService tagService;

    private final DesignConnectedParser designConnectedParser;
    private final ArchiproductsParser archiproductsParser;
    private final DDD3RuParser ddd3RuParser;

    private final Odesd2 odesd2;

    @Autowired
    public AdminController(AdminService adminService, ItemService itemService, UserService userService,
                           GroupService groupService, TagService tagService, DesignConnectedParser designConnectedParser,
                           ArchiproductsParser archiproductsParser, DDD3RuParser ddd3RuParser,
                           Odesd2 odesd2) {
        this.adminService = adminService;
        this.itemService = itemService;
        this.userService = userService;
        this.groupService = groupService;
        this.tagService = tagService;
        this.designConnectedParser = designConnectedParser;
        this.archiproductsParser = archiproductsParser;
        this.ddd3RuParser = ddd3RuParser;
        this.odesd2 = odesd2;
    }

    /*--------------------------------Users-------------------------------*/

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeUser(@PathVariable String id) {
        return adminService.removeUser(id);
    }

    @RequestMapping(value = "/users/ban", method = RequestMethod.PUT)
    public ResponseEntity banUser(@RequestParam String id) {
        return adminService.banUser(id);
    }

    @RequestMapping(value = "/users/unban", method = RequestMethod.PUT)
    public ResponseEntity unbanUser(@RequestParam String id) {
        return adminService.unbanUser(id);
    }

    @RequestMapping(value = "/users/role/{id}", method = RequestMethod.GET)
    public ResponseEntity getUserRoles(@PathVariable String id) {
        return adminService.getUserRoles(id);
    }

    @RequestMapping(value = "/users/role/giveRights", method = RequestMethod.PUT)
    public ResponseEntity giveAdminRightsToUser(@RequestParam String id, @RequestParam boolean give) {
        return adminService.giveAdminRightsToUser(id, give);
    }

    @RequestMapping(value = "/users/{page}", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsers(@PathVariable int page) {
        User user = userService.getLoggedUser();
        if (page < 0) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(adminService.getUsers(user, page));
    }

    /*--------------------------------Parser-------------------------------*/

    @RequestMapping(value = "/parser/start/all", method = RequestMethod.GET)
    public ResponseEntity startParserAll() {
        try {
            new Thread(() -> {
                try {
                    ddd3RuParser.loadModels();
                    archiproductsParser.loadProducts();
                    designConnectedParser.loadModels();
                    odesd2.loadModels();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }).start();


            return ResponseEntity.ok("All parser start");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/parser/start/dc", method = RequestMethod.GET)
    public ResponseEntity startParserDC() {
        try {
            new Thread(() -> {
                try {
                    designConnectedParser.loadModels();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }).start();
            return ResponseEntity.ok("DC parser start");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/parser/start/ap", method = RequestMethod.GET)
    public ResponseEntity startParserAP() {
        try {
            new Thread(() -> {
                try {
                    archiproductsParser.loadProducts();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
            return ResponseEntity.ok("AP parser start");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/parser/start/3ddd", method = RequestMethod.GET)
    public ResponseEntity startParser3ddd() {
        try {
            new Thread(() -> {
                try {
                    ddd3RuParser.loadModels();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
            return ResponseEntity.ok("3DDD parser start");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/parser/start/odesd2", method = RequestMethod.GET)
    public ResponseEntity startParserOdesd2() {
        try {
            new Thread(() -> {
                try {
                    odesd2.loadModels();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
            return ResponseEntity.ok("Odesd2 parser start");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*--------------------------------Tags-------------------------------*/

    @RequestMapping(value = "/getTags", method = RequestMethod.GET)
    public ResponseEntity<List<Tag>> getGroup(@RequestParam String groupName, @RequestParam int size, @RequestParam int page) {
        return tagService.getTags(groupName, size, page);
    }

    @RequestMapping(value = "/addTags", method = RequestMethod.PUT)
    public ResponseEntity addTags(@RequestParam String groupName, @RequestBody List<String> tags) {
        return tagService.addTags(groupName, tags);
    }

    @RequestMapping(value = "/removeTags", method = RequestMethod.PUT)
    public ResponseEntity removeTags(@RequestBody List<String> tags) {
        return tagService.removeTags(tags);
    }

    @RequestMapping(value = "/renameTag", method = RequestMethod.PUT)
    public ResponseEntity renameTag(@RequestParam String oldTagName, @RequestParam String newTagName) {
        return tagService.renameTag(oldTagName, newTagName);
    }

    @Deprecated
    @RequestMapping(value = "/findByNameStartingWith/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<Tag>> findByNameStartingWith(@PathVariable String name) {
        return adminService.findByNameStartingWith(name);
    }

    @RequestMapping(value = "/findByNameStartingWith/{name}/{size}", method = RequestMethod.GET)
    public ResponseEntity<List<Tag>> findByNameStartingWithLimit(@PathVariable String name, @PathVariable String size) {
        int limit = 0;
        try {
            limit = Integer.parseInt(size);
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().build();
        }
        return adminService.findByNameStartingWith(name, limit);
    }

    @RequestMapping(value = "/getUnsortedTags", method = RequestMethod.GET)
    public ResponseEntity<List<Tag>> getUnsortedTags() {
        return ResponseEntity.ok(tagService.getFirst100UnsortedTag());
    }

    @RequestMapping(value = "/getSortedTags", method = RequestMethod.GET)
    public ResponseEntity<List<TagByGroupResponce>> getSortedTags() {
        return tagService.getSortedTags();
    }

    @PutMapping(value = "/tags/combinable/default")
    public ResponseEntity setDefaultCombinableTags(){
        adminService.setDefaultCombinableTags();
        return ResponseEntity.ok().build();
    }


//    /*--------------------------------Groups-------------------------------*/

    @RequestMapping(value = "/addGroup", method = RequestMethod.POST)
    public ResponseEntity<String> addGroup(@RequestParam String groupName, @RequestParam String groupIcon) {
        return groupService.addGroup(groupName, groupIcon);
    }

    @RequestMapping(value = "/removeGroup", method = RequestMethod.DELETE)
    public ResponseEntity<String> removeGroup(@RequestParam String groupName) {
        return groupService.removeGroup(groupName);
    }

    @RequestMapping(value = "/renameGroup", method = RequestMethod.PUT)
    public ResponseEntity<String> renameGroup(@RequestParam String groupOldName, @RequestParam String groupNewName) {
        return groupService.renameGroup(groupOldName, groupNewName);
    }

    @PutMapping(value = "/groups/hide")
    public ResponseEntity hideOrShowGroup(@RequestParam String groupName) {
        return groupService.hideOrShowGroup(groupName);
    }

    /*--------------------------------Items-------------------------------*/

    @GetMapping(value = "/items/models/{page}/{size}")
    public Page<? extends Item> getModels(@PathVariable int page, @PathVariable int size) {
        return itemService.findAll(page, size, Item.Type.model3D);
    }

    @RequestMapping(value = "/items/product", method = RequestMethod.POST)
    public ResponseEntity addProduct(@RequestBody ItemRequest itemRequest) {
        System.out.println("product = " + itemRequest);
        return adminService.addProduct(itemRequest);
    }

    @RequestMapping(value = "/items/model3d", method = RequestMethod.POST)
    public ResponseEntity addModel3D(@RequestBody ItemRequest itemRequest) {
        System.out.println("model3d = " + itemRequest);
        return adminService.addModel3D(itemRequest);
    }

    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.DELETE)
    public ResponseEntity removeItem(@PathVariable String itemId) {
        Item item = itemService.getItemById(itemId);
        if (item == null) return ResponseEntity.noContent().build();
        return adminService.removeItem(item);
    }

    @RequestMapping(value = "/items/product/{id}", method = RequestMethod.PUT)
    public ResponseEntity editProduct(@PathVariable String id, @RequestBody ItemRequest itemRequest) {
        return adminService.editProduct(id, itemRequest);
    }

    @RequestMapping(value = "/items/model3d/{id}", method = RequestMethod.PUT)
    public ResponseEntity editItem(@PathVariable String id, @RequestBody ItemRequest itemRequest) {
        return adminService.editModel3D(id, itemRequest);
    }

    @Deprecated
    @RequestMapping(value = "/brands/{brandId}", method = RequestMethod.DELETE)
    public ResponseEntity removeBrand(@PathVariable String brandId) {
//        Brand brand = brandRepository.findOne(brandId);
//        if (brand == null) return ResponseEntity.noContent().build();
//
        return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
    }

    /*--------------------------------Types-------------------------------*/

    @RequestMapping(value = "/type", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getTypes() {
        List<String> types = itemService.findDistinctItemFilterField(Item.Field.type);
        return ResponseEntity.ok(types);
    }

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getCategory() {
        List<String> categories = itemService.findDistinctItemFilterField(Item.Field.category);
        return ResponseEntity.ok(categories);
    }

    @RequestMapping(value = "/brand/search/{name}/{size}", method = RequestMethod.GET)
    public ResponseEntity<List<String>> findBrandByNameStartingWithLimit(@PathVariable String name, @PathVariable String size) {
        int limit = 10;
        try {
            limit = Integer.parseInt(size);
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().build();
        }
        List<String> brands = itemService.findDistinctItemFilterField(Item.Field.brand).stream()
                .filter(s -> s.startsWith(name)).collect(Collectors.toList());
        return ResponseEntity.ok(brands.stream().limit(limit).collect(Collectors.toList()));
    }

    /*--------------------------------Sources-------------------------------*/
    @GetMapping(value = "/source")
    public ResponseEntity<List<SourceResponse>> getSources(){
        return ResponseEntity.ok(adminService.getSources());
    }

    @PutMapping(value = "/source/{source}")
    public ResponseEntity hideShowItemsBySource(@PathVariable String source){
        return adminService.hideShowItemsBySource(source);
    }

}
