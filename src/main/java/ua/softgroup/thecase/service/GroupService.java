package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import ua.softgroup.thecase.model.Group;

import java.util.List;

/**
 * Created by java-1-07 on 11.08.2017.
 */
public interface GroupService {


    ResponseEntity<String> addGroup(String groupName, String groupIconNameFromMaterialIcons);
    ResponseEntity<String> removeGroup(String groupName);
    ResponseEntity<String> renameGroup(String groupOldName, String groupNewName);

    ResponseEntity hideOrShowGroup(String groupName);

    List<Group> getAllGroups();


}
