package ua.softgroup.thecase.service;

import ua.softgroup.thecase.model.Case;

/**
 * Created by java-1-03 on 16.09.2017.
 */
public interface CaseHistoryService {

    void addCaseChange(Case aCase, String action, String editorEmail);

}
