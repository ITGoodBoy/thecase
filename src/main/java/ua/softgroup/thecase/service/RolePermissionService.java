package ua.softgroup.thecase.service;

import ua.softgroup.thecase.model.RolePermission;

/**
 * Created by java-1-07 on 18.07.2017.
 */
public interface RolePermissionService {

    RolePermission getById(String id);
}
