package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.TeamResponse;

import java.util.List;

/**
 * Created by java-1-03 on 11.09.2017.
 */
public interface TeamService {


    ResponseEntity addUserToCaseOrChangePermission(String caseId, String userId, int permissionNumber);
    ResponseEntity removeUserFromCase(String caseId, String userId);
    ResponseEntity confirmInvite(String caseId);

    ResponseEntity sendCaseToEmail(String caseId, String emailTo);
    ResponseEntity sendItemsToEmail(Case aCase, String emailTo, List<String> itemsId);

    ResponseEntity<TeamResponse> getTeamByCase(String caseId);
    ResponseEntity<List<User>> getTeamAllCases();

    boolean isConfirmed(User user, Case aCase);
    boolean isCanAddItem(User user, Case aCase);
    boolean isCanRemoveItem(User user, Case aCase, ItemHolder itemHolder);
    boolean isCanRename(User user, Case aCase);
    boolean isCanSend(User user, Case aCase);
    boolean isCanAddUserToCase(User user, Case aCase, int permissionNumber);
    boolean isCanChangeCount(User user, Case aCase, ItemHolder itemHolder);
    boolean isCanChangeCountAndPrice(User user, Case aCase, ItemHolder itemHolder);

}
