package ua.softgroup.thecase.service;

import ua.softgroup.thecase.model.BlackWord;

import java.util.List;

/**
 * Created by TLJD Andrew Kobin on 04.10.2017.
 */
public interface CleanerService {

    List<BlackWord> getBlackList();
    boolean addToBlackList(String word);
    boolean removeFromBlackList(String word);

    int cleanUp();
    void cleanUpSource();

}
