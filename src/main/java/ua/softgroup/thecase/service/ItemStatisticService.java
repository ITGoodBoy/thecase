package ua.softgroup.thecase.service;

import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Model3D;
import ua.softgroup.thecase.model.Product;

import java.util.List;

/**
 * Created by java-1-03 on 18.09.2017.
 */
public interface ItemStatisticService {


    void addViewCount(List<? extends Item> items);
    void addCaseCount(Item item);
    void removeCaseCount(Item item);
    void removeCaseCount(List<? extends Item> items);
    void addOpeningCount(Item item);
    void addLikeCount(Item item);
    void removeLikeCount(Item item);

}
