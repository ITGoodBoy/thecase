package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import ua.softgroup.thecase.model.Filter;
import ua.softgroup.thecase.model.message.FilterFieldsResponse;

/**
 * Created by java-1-03 on 26.09.2017.
 */
public interface FilterService {

    ResponseEntity<Filter> changeFilter(String filterName, String name, boolean isAdd);

    ResponseEntity<Filter> deActivateFilterByType(String filterName);

    ResponseEntity<FilterFieldsResponse> getFilterFields(String brandStartingWith);

    ResponseEntity<Filter> deactivateFilter();
    ResponseEntity<Filter> getActiveFilter();


}
