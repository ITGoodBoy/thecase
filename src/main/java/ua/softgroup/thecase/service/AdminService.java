package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.model.message.ItemRequest;
import ua.softgroup.thecase.model.message.SourceResponse;

import java.util.List;

/**
 * Created by java-1-07 on 08.08.2017.
 */
public interface AdminService{
    ResponseEntity<String> removeUser(String email);
    ResponseEntity<String> banUser(String email);
    ResponseEntity<String> unbanUser(String email);

    ResponseEntity removeItem(Item item);


//    ResponseEntity removeBrand(Brand brand);

    List<User> getUsers(User user, int page);

    ResponseEntity<List<Tag>> findByNameStartingWith(String name);
    ResponseEntity<List<Tag>> findByNameStartingWith(String name, int limit);

    ResponseEntity getUserRoles(String id);

    ResponseEntity giveAdminRightsToUser(String id, boolean give);

    ResponseEntity addProduct(ItemRequest itemRequest);
    ResponseEntity addModel3D(ItemRequest itemRequest);

    ResponseEntity editProduct(String itemId, ItemRequest itemRequest);
    ResponseEntity editModel3D(String itemId, ItemRequest itemRequest);

    List<SourceResponse> getSources();

    void editAllSourceItemsPublishedBySource(String source, boolean published);
    ResponseEntity hideShowItemsBySource(String source);

    void setDefaultCombinableTags();
}
