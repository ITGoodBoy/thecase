package ua.softgroup.thecase.service;

import lombok.NonNull;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.MessageRequest;
import ua.softgroup.thecase.model.message.UserResponse;

import java.util.List;
import java.util.Optional;

/**
 * Created by java-1-07 on 10.07.2017.
 */
@Service
public interface UserService extends UserDetailsService {
    ResponseEntity<String> registration(String userName, String email, String password);
    ResponseEntity<String> registration(String userName, String email, String password, String referrer);
    ResponseEntity<String> registrationConfirm(String token);
    ResponseEntity<String> resetForgottenPassword(String email);
    ResponseEntity<String> resetForgottenPasswordConfirm(String token);
    ResponseEntity<String> replacePassword(String email, String oldPassword, String newPassword);

    String login(User user, String email, String password);
    User getLoggedUser();

    boolean isPasswordMatch(User user, String password);

    @Override
    UserDetails loadUserByUsername(String s) throws UsernameNotFoundException;
    Optional<User> findById(@NonNull ObjectId id);

    List<UserResponse> getAllUsers(int pageNumber);
    List<UserResponse> getUsersStartingWith(String name, int pageNumber);

    ResponseEntity addUserToContactList(String userId);
    ResponseEntity removeUserFromContactList(String userId);

    List<User> getContactList(int pageNumber);

    ResponseEntity<String> inviteUserToSystem(String email);
    ResponseEntity<String> sendMessage(String email, MessageRequest messageRequest);

    List<User> setIsOnline(List<User> users);
    User setIsUserOnline(User user);

}
