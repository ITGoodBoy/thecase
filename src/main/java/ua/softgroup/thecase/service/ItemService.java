package ua.softgroup.thecase.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.ItemResponse;
import ua.softgroup.thecase.model.message.ListItemsResponse;

import java.util.List;
import java.util.Map;

/**
 * Created by TLJD Andrew Kobin on 06.07.2017.
 */
@Service
public interface ItemService {

    Item getItemById(String id);
    List<Item> getAll();

    <T extends Item> List<T> getItem(Class<T> tClass);
    <T extends Item> List<T> getItemPaginated(int pageNumber, Class<T> tClass);

    <T extends Item> ListItemsResponse getItems(int pageNumber, Class<T> tClass);

    ListItemsResponse findProductByNameStartingWith(String name, int pageNumber);
    ListItemsResponse findModel3DByNameStartingWith(String name, int pageNumber);

    Page<? extends Item> findAll(int page, int page_size, Item.Type type);

    <T extends Item> long getCount(Class<T> tClass);

    Map<String, Long> countsInfo();

    List<Item> findByTagsListIn(String tagName);

    void save(List<Item> items);

    void save(Item item);

    Item findItemById(String id);

    ItemResponse likeItem(Item item);
    boolean isLiked(Item item, User user);
    int likesCount(Item item);

    List<String> findDistinctItemFilterField(Item.Field field);
}
