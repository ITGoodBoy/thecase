package ua.softgroup.thecase.service;

import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.CaseChange;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.MessageRequest;

import java.util.Set;

/**
 * Created by java-1-03 on 10.07.2017.
 */
public interface EmailService {

    void sendVerificationEmail(String name, String emailTo, String token);
    void sendForgottenPasswordRequest(String name, String emailTo, String token);
    void sendNewRandomPasswordToUserEmail(String name, String emailTo, String password);
    void sendNotifyToEmail(String name, String emailTo, String title, String message);
    void sendInviteUserToSystem(User fromUser, String emailTo);

    void sendCaseToEmail(String emailTo, byte[] bytes);

    void createAndSend(Case aCase, String emailTo);

    void sendChangesOnCase(Set<CaseChange> caseChanges);

    void sendMessage(User fromUser, User toUser, MessageRequest messageRequest);

}
