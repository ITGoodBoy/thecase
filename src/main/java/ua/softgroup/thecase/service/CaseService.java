package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.ShortCase;

import java.util.List;

/**
 * Created by java-1-07 on 13.07.2017.
 */
public interface CaseService {

    Case getActiveCase(User user);
//    Case activateCase(User user, Case caSe);

    List<ShortCase> getCasesNames(User user);
    Case getCaseById(String caseId);

    ResponseEntity<Case> getCaseByIdAndActivateIt(String caseId);

    ResponseEntity<Case> addItem(String caseId, String itemId);
    ResponseEntity<Case> changeItemCount(String itemId, int count);
    ResponseEntity<Case> changeItemCountAndPrice(String itemId, int count, long amount, String currency);
    ResponseEntity<Case> removeItem(String itemId);

    ResponseEntity<Case> makeItemFirstInCase(String itemId);


    boolean isContain(Case caSe, Item item);

    List<Case> remove(Case aCase);
    Case removeAllItems(Case aCase);
    Case removeAllProducts(Case aCase);
    Case removeAllModels3D(Case aCase);
    //List<Case> createFromSidebar(User user, String name, String description);
    //Case updateFromSidebar(Case caSe, Sidebar sidebar);

    ResponseEntity<Case> rename(String caseId, String name, String description);
    Case create(User owner, String name, String description);
    boolean isCaseNameUnique(User owner, String name);

    boolean isCanCreateNewCase(User owner);
}
