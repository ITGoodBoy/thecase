package ua.softgroup.thecase.service;

import org.springframework.web.bind.annotation.PathVariable;

import java.io.File;
import java.io.IOException;

/**
 * Created by java-1-03 on 24.01.2018.
 */
public interface FileService {

    File getFile(@PathVariable String id);

}
