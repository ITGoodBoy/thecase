package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import ua.softgroup.thecase.model.Tag;
import ua.softgroup.thecase.model.message.TagByGroupResponce;

import java.util.List;

/**
 * Created by java-1-07 on 21.08.2017.
 */
public interface TagService {
    ResponseEntity<List<Tag>> findByNameStartingWith(String name);
    ResponseEntity<List<Tag>> findByNameStartingWith(String name, int limit);

    ResponseEntity<List<Tag>> getTags(String groupName, int size, int page);

    ResponseEntity addTags(String groupName, List<String> tags);

    ResponseEntity renameTag(String oldTagName, String newTagName);

    ResponseEntity<List<TagByGroupResponce>> getSortedTags();

    List<Tag> getFirst100UnsortedTag();

    ResponseEntity removeTags(List<String> tags);

    List<Tag> getTagsByGroup(String groupName);
    void saveTags(List<Tag> tags);
}
