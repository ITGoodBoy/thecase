package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import ua.softgroup.thecase.model.Funnel;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.message.FunnelFieldsResponse;

import java.util.List;

/**
 * Created by java-1-03 on 29.08.2017.
 */
public interface FunnelService {

    ResponseEntity<List<Funnel>> activateFunnelByGroup(Group group);

    void deactivateFunnel(Group group);
    void deactivateFunnels();

    void deleteAllFunnelsByGroup(Group group);

    ResponseEntity<List<Funnel>> activateFunnelsByItem(User user, String id);

    ResponseEntity<List<FunnelFieldsResponse>> getFunnels(User user);

    List<Funnel> getActiveFunnels(User user);

    ResponseEntity<List<FunnelFieldsResponse>> manualSelecting(User user, List<String> tags);
}
