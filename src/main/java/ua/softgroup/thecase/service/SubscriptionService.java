package ua.softgroup.thecase.service;

import ua.softgroup.thecase.model.Subscription;

/**
 * Created by java-1-07 on 18.07.2017.
 */
public interface SubscriptionService {

    Subscription getById(String id);
}
