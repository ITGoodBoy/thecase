package ua.softgroup.thecase.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by TLJD Andrew Kobin on 06.07.2017.
 */
public interface ImageService {
    ResponseEntity<byte[]> getImage(@PathVariable String id) throws IOException;
    void removeImage(String image) throws IOException;
    List<String> uploadImages(MultipartFile[] files);
}
