package ua.softgroup.thecase.utils;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sf.ehcache.Cache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.User;

/**
 * Created by java-1-03 on 16.10.2017.
 */
@Component
public class OnlineUserCacheImpl implements OnlineUserCache {

    private static final Logger logger = LoggerFactory.getLogger(OnlineUserCacheImpl.class);
    private static final Cache onlineUsersCache = CacheManager.getInstance().getCache("onlineUsersCache");


    @Value(value = "temp.token.storage.clear.cache.scheduler")
    private static final int SCHEDULER_RATE = 60 * 1000;


    @Scheduled(fixedRate = SCHEDULER_RATE)
    public void evictExpiredUser() {
        logger.info("Evicting expired users");
        onlineUsersCache.evictExpiredElements();
        logSize();
    }

    @Override
    public void addUser(String email) {
        Element element = new Element(email, String.valueOf(System.currentTimeMillis()));
        onlineUsersCache.put(element);
        logSize();
    }

    @Override
    public void removeUser(String email) {
        try {
            logger.info("isRemove: " + onlineUsersCache.remove(onlineUsersCache.get(email).getObjectKey()));
        } finally {
            logSize();
        }
    }

    @Override
    public boolean isUserOnline(User user) {
        return onlineUsersCache.isKeyInCache(user.getEmail());
    }

    private void logSize() {
        logger.info("Online users in cache: " + onlineUsersCache.getSize());
    }
}
