package ua.softgroup.thecase.utils;

import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.ItemHolder;
import ua.softgroup.thecase.model.Tag;

import java.util.Comparator;

/**
 * Created by java-1-03 on 02.08.2017.
 */
public class Comparators {

    public static class ByName implements Comparator<Item> {

        @Override
        public int compare(Item o1, Item o2) {

            String s1 = o1.getName();
            String s2 = o2.getName();

            return s1.compareTo(s2);
        }
    }

    public static class TimeDescComparator implements Comparator<ItemHolder> {
        @Override
        public int compare(ItemHolder h1, ItemHolder h2) {

            if (h1.getItem().equals(h2.getItem())) {
                return 0;
            } else {
                if (h1.getAddedAt() > h2.getAddedAt())
                    return -1;
                else return 1;
            }
        }

    }

    private static Comparator<Tag> numberOfRepetitionsComparator() {
        return (o1, o2) -> {
            if (o1.getFrequency() <= o2.getFrequency()) return 1;
            else return -1;
        };
    }
}
