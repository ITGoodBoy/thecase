package ua.softgroup.thecase.utils;

import ua.softgroup.thecase.model.User;

/**
 * Created by java-1-03 on 16.10.2017.
 */
public interface OnlineUserCache {

    void addUser(String email);
    void removeUser(String email);

    boolean isUserOnline(User user);

}
