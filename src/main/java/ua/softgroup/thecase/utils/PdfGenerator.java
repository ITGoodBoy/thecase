package ua.softgroup.thecase.utils;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.core.io.ClassPathResource;
import ua.softgroup.thecase.Utils;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.ItemHolder;
import ua.softgroup.thecase.model.Model3D;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by java-1-03 on 13.09.2017.
 */
public class PdfGenerator {

    public static void pdfFromCase(Case aCase, OutputStream outputStream) {

        Document document = new Document(PageSize.A4.rotate());
        try {
            PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream /*new FileOutputStream("TestPdf.pdf")*/);
            document.open();

            ClassPathResource imgFile = new ClassPathResource(Utils.imagesPath + "Thecase_Logo.png");
            Image image = Image.getInstance(imgFile.getPath());
            image.scaleAbsolute(110, 40);
            document.add(image);
            document.add(new Paragraph("Case: " + aCase.getName()));
            document.add(new Paragraph("Owner: " + aCase.getOwner().getUsername() + ", " + aCase.getOwner().getEmail()));


            document.add(createTable(aCase));

            document.close();
            pdfWriter.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static PdfPTable createTable(Case aCase) throws Exception {
        PdfPTable table = new PdfPTable(9); // 9 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        Font bold = FontFactory.getFont(FontFactory.COURIER_BOLD);

        List<String> fields = new ArrayList<>();
        fields.add("#");
        fields.add("IMAGE");
        fields.add("NAME");
        fields.add("CATEGORY");
        fields.add("BRAND");
        fields.add("TYPE");
        fields.add("QUANTITY");
        fields.add("STATUS");
        fields.add("PRICE");

        //Set Column widths
        float[] columnWidths = {3f, 10f, 10f, 7f, 7f, 7f, 6f, 10f, 6f};
        table.setWidths(columnWidths);

        List<ItemHolder> productsHolder = aCase.getProducts().stream().collect(Collectors.toList());
        //List<Product> products = new ArrayList<>();
        //productsHolder.forEach(itemHolder -> products.add((Product) itemHolder.getItem()));

        if (productsHolder.size() > 0) {

            PdfPCell cell0 = new PdfPCell(new Paragraph("PRODUCTS", bold));
            cell0.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell0.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell0.setColspan(9);
            table.addCell(cell0);

            table = stat(table, fields);

            for (int i = 0; i < productsHolder.size(); i++) {

                List<String> itemsInfo = new ArrayList<>();
                itemsInfo.add(String.valueOf(i+1));
                itemsInfo.add(productsHolder.get(i).getItem().getImageUrls().get(0));
                itemsInfo.add(productsHolder.get(i).getItem().getName());

                String category = "n/a";
                if (productsHolder.get(i).getItem().getCategory() != null)
                    category = productsHolder.get(i).getItem().getBrand();
                itemsInfo.add(category);

                String brand = "n/a";
                if (productsHolder.get(i).getItem().getBrand() != null)
                    brand = productsHolder.get(i).getItem().getBrand();
                itemsInfo.add(brand);

                String type = "n/a";
                if (productsHolder.get(i).getItem().getType() != null && !productsHolder.get(i).getItem().getType().isEmpty())
                    type = productsHolder.get(i).getItem().getType();
                itemsInfo.add(type);

                String quantity = String.valueOf(productsHolder.get(i).getCount());
                itemsInfo.add(quantity);

                String status = "n/a";
               /* if (items.get(i).getType() !=null && !items.get(i).getType().isEmpty())
                    status = items.get(i).getType();*/
                itemsInfo.add(status);

                String price = "n/a";
                if (productsHolder.get(i).getItem().getNativePrice() != null)
                    price = String.valueOf((productsHolder.get(i).getItem().getNativePrice()).getAmount() + (productsHolder.get(i).getItem().getNativePrice()).getCurrency());
                itemsInfo.add(price);

                table = info(table, itemsInfo);
            }
        }

        List<ItemHolder> modelsHolder = aCase.getModels3D().stream().collect(Collectors.toList());
        /*List<Model3D> models = new ArrayList<>();
        modelsHolder.forEach(itemHolder -> models.add((Model3D) itemHolder.getItem()));*/

        if (modelsHolder.size() > 0) {
            PdfPCell cell0 = new PdfPCell(new Paragraph("MODELS", bold));
            cell0.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell0.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell0.setColspan(9);
            table.addCell(cell0);

            table = stat(table, fields);

            for (int i = 0; i < modelsHolder.size(); i++) {

                List<String> itemsInfo = new ArrayList<>();
                itemsInfo.add(String.valueOf(i+1));
                itemsInfo.add(modelsHolder.get(i).getItem().getImageUrls().get(0));
                itemsInfo.add(modelsHolder.get(i).getItem().getName());

                String category = "n/a";
                if (modelsHolder.get(i).getItem().getCategory() != null)
                    category = modelsHolder.get(i).getItem().getCategory();
                itemsInfo.add(category);

                String brand = "n/a";
                if (modelsHolder.get(i).getItem().getBrand() != null)
                    brand = modelsHolder.get(i).getItem().getBrand();
                else if (((Model3D) (modelsHolder.get(i).getItem())).getDesigner() != null)
                    brand = ((Model3D) (modelsHolder.get(i).getItem())).getDesigner();
                itemsInfo.add(brand);

                String type = "n/a";
                if (modelsHolder.get(i).getItem().getType() != null && !modelsHolder.get(i).getItem().getType().isEmpty())
                    type = modelsHolder.get(i).getItem().getType();
                itemsInfo.add(type);

                String quantity = "n/a";
                /*if (items.get(i).getType() !=null && !items.get(i).getType().isEmpty())
                    quantity = items.get(i).getType();*/
                itemsInfo.add(quantity);

                String status = "n/a";
               /* if (items.get(i).getType() !=null && !items.get(i).getType().isEmpty())
                    status = items.get(i).getType();*/
                itemsInfo.add(status);

                String price = "n/a";
                if (modelsHolder.get(i).getItem().getNativePrice() != null)
                    price = String.valueOf((modelsHolder.get(i).getItem().getNativePrice()).getAmount() + " " + (modelsHolder.get(i).getItem().getNativePrice()).getCurrency());
                itemsInfo.add(price);

                table = info(table, itemsInfo);
            }
        }
        return table;
    }

    private static PdfPTable stat(PdfPTable table, List<String> fields) {

        Font courier = FontFactory.getFont(FontFactory.COURIER);

        for (int i = 0; i < fields.size(); i++) {

            PdfPCell cell;
            if (i == 0)
                cell = new PdfPCell(new Paragraph(fields.get(i)));
            else
                cell = new PdfPCell(new Paragraph(fields.get(i), courier));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

            table.addCell(cell);

        }
        return table;
    }

    private static PdfPTable info(PdfPTable table, List<String> fields) throws IOException, DocumentException {

        Font courier = FontFactory.getFont(FontFactory.COURIER);

        ClassPathResource imgFile = new ClassPathResource("src/main/resources/fonts/FreeSans.ttf");

        BaseFont bf3 = BaseFont.createFont(imgFile.getPath(), "Cp1251", BaseFont.EMBEDDED);
        Font russian = new Font(bf3);

        for (int i = 0; i < fields.size(); i++) {

            PdfPCell cell;

            if (i == 1) {
                cell = new PdfPCell();
                if (fields.get(i).startsWith("/api/image/"))
                    cell.setImage(Image.getInstance(Utils.imagesPath + fields.get(i).replace("/api/image/", "")));
                else
                    cell.setImage(Image.getInstance(new URL(validateUrl(fields.get(i)))));
            } else if (i == 8)
                cell = new PdfPCell(new Paragraph(fields.get(i), russian));
            else
                cell = new PdfPCell(new Paragraph(fields.get(i), courier));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

            table.addCell(cell);

        }
        return table;
    }

    private static String validateUrl(String url){

        if (url.startsWith("//"))
            return "http:" + url;

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            return "http://" + url;
        return url;
    }

}