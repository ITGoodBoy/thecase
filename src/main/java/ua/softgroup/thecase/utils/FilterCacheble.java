package ua.softgroup.thecase.utils;

import com.mongodb.BasicDBObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Model3D;
import ua.softgroup.thecase.model.Product;
import ua.softgroup.thecase.serviceImpl.FilterServiceImpl;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;

/**
 * Created by java-1-03 on 29.01.2018.
 */
@Component
@Slf4j
@CacheConfig(cacheManager = "bCacheManager")
public class FilterCacheble {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Cacheable(value = "all_type", key = "#field.name()")
    public List<String> findDistinctAllTypes(Item.Field field) {

        System.out.println("===================== findDistinctAllTypes fieldName: " + field.name());

        Set<String> types = new HashSet<>();
        types.addAll(mongoTemplate.getCollection(Item.Type.product.name()).distinct(field.name()));
        types.addAll(mongoTemplate.getCollection(Item.Type.model3D.name()).distinct(field.name()));
        types.remove("");
        return new ArrayList<>(types);
    }

    @Data
    private class BrandCount{
        private String brand;
        private long total;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BrandCount that = (BrandCount) o;

        return brand != null ? brand.equals(that.brand) : that.brand == null;

    }

    @Override
    public int hashCode() {
        return brand != null ? brand.hashCode() : 0;
    }
}

    @Cacheable(value = "brands", key = "#basicDBObject.toString()")
    public List<String> findBrandDistinct(BasicDBObject basicDBObject) {
        Criteria criteria = new Criteria();

        List<Criteria> andCriterias = new ArrayList<>();
        andCriterias.add(Criteria.where("isPublished").is(true));
        andCriterias.add(Criteria.where("isPublishedBySource").is(true));

        String startingWith = (String) basicDBObject.get("startingWith");



        if (startingWith != null && !startingWith.isEmpty())
            andCriterias.add(Criteria.where("brand").regex(Pattern.compile("^" + startingWith, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)));

        criteria.andOperator(andCriterias.toArray(new Criteria[0]));

        List<String> category = (List<String>) basicDBObject.get("category");
        List<String> types = (List<String>) basicDBObject.get("type");

        if (types != null && types.size() > 0){
            List<Criteria> orCriterias = new ArrayList<>();
            types.forEach(s -> orCriterias.add(Criteria.where("type").is(s)));

            criteria.orOperator(orCriterias.toArray(new Criteria[0]));

        } else if (category != null && category.size() > 0){
            List<Criteria> orCriterias = new ArrayList<>();
            category.forEach(s -> orCriterias.add(Criteria.where("category").is(s)));

            criteria.orOperator(orCriterias.toArray(new Criteria[0]));
        }

        Aggregation agg = Aggregation.newAggregation(
                match(criteria),
                group("brand").count().as("total"),
                sort(Sort.Direction.DESC, "total"),
                project("total").and("brand").previousOperation(),
                limit(25)
        );
        AggregationResults<BrandCount> modelGroupResults = mongoTemplate.aggregate(agg, Model3D.class, BrandCount.class);
        AggregationResults<BrandCount> productGroupResults = mongoTemplate.aggregate(agg, Product.class, BrandCount.class);

        List<String> brands = joinAndSortBrandCount(modelGroupResults, productGroupResults);

        return brands;
    }

    private List<String> joinAndSortBrandCount(AggregationResults<BrandCount> a1, AggregationResults<BrandCount> a2){
        Map<String, Long> brandsMap = a1.getMappedResults().stream().collect(Collectors.toMap(BrandCount::getBrand, BrandCount::getTotal));

        a2.getMappedResults().forEach(b -> {
            if (brandsMap.containsKey(b.getBrand()))
                brandsMap.put(b.getBrand(), b.getTotal() + brandsMap.get(b.getBrand()));
            else
                brandsMap.put(b.getBrand(), b.getTotal());
        });

        Stream<Map.Entry<String,Long>> sorted =
                brandsMap.entrySet().stream()
                        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));

        return sorted.map(Map.Entry::getKey).collect(Collectors.toList());
    }


    @Cacheable(value = "fields", key = "#query.toString().concat('-').concat(#field.name())")
    public List<String> findDistinct(Item.Field field, Query query) {
        System.out.println("===================== findDistinct field.name: " + field.name() + " basicDBObject: " + query.toString());
        Set<String> types = new HashSet<>();
        types.addAll(mongoTemplate.getCollection(Item.Type.product.name()).distinct(field.name(), query.getQueryObject()));
        types.addAll(mongoTemplate.getCollection(Item.Type.model3D.name()).distinct(field.name(), query.getQueryObject()));
        types.remove("");
        return new ArrayList<>(types);
    }

}
