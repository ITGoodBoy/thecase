package ua.softgroup.thecase.utils;

import com.mongodb.BasicDBObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

/**
 * Created by java-1-03 on 19.01.2018.
 */
public class DBObjectFlagFilter {


    public static Query published(){
        Query query = new Query();
        query.addCriteria(Criteria.where("isPublished").is(true));
        query.addCriteria(Criteria.where("isPublishedBySource").is(true));
        return query;
    }

    public static BasicDBObject visible(){
        BasicDBObject basicDBObject = new BasicDBObject();
        basicDBObject.put("isPublished", true);
        basicDBObject.put("isPublishedBySource", true);
        return basicDBObject;
    }

    public static BasicDBObject searchBySource(String source){
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.append("source", source);
        return searchQuery;
    }

    public static BasicDBObject updatePublishedBySource(boolean published){
        BasicDBObject updateQuery = new BasicDBObject();
        updateQuery.append("$set", new BasicDBObject().append("isPublishedBySource", published));
        return updateQuery;
    }

}
