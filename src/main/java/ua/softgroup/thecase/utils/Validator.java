package ua.softgroup.thecase.utils;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Created by java-1-03 on 14.09.2017.
 */
public class Validator {

    public static boolean isEmailValid(String email){
        boolean result = true;
        try {
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
        } catch (AddressException e){
            result = false;
        }
        return result;
    }

}
