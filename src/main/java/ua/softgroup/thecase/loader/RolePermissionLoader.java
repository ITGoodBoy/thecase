package ua.softgroup.thecase.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Role;
import ua.softgroup.thecase.model.RolePermission;
import ua.softgroup.thecase.repository.RolePermissionRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by java-1-03 on 19.07.2017.
 */
@Component
public class RolePermissionLoader {

    private RolePermissionRepository rolePermissionRepository;

    @Autowired
    public RolePermissionLoader(RolePermissionRepository rolePermissionRepository) {
        this.rolePermissionRepository = rolePermissionRepository;
    }


    public void init(){

        if (rolePermissionRepository.findAll().size() > 0)
            return;

        List<RolePermission> rolePermissions = new ArrayList<>();
        rolePermissions.add(new RolePermission(Role.FREE, 3, false, 9));
        rolePermissions.add(new RolePermission(Role.BASIC, 6, true, Integer.MAX_VALUE));
        rolePermissions.add(new RolePermission(Role.PRO, 20, true, Integer.MAX_VALUE));
        rolePermissions.add(new RolePermission(Role.PREMIUM, Integer.MAX_VALUE, true, Integer.MAX_VALUE));
        rolePermissions.add(new RolePermission(Role.ADMIN, Integer.MAX_VALUE, true, Integer.MAX_VALUE));

        rolePermissionRepository.save(rolePermissions);
    }
}
