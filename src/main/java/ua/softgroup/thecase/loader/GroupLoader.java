package ua.softgroup.thecase.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.repository.GroupRepository;

/**
 * Created by java-1-07 on 16.08.2017.
 */
@Component
public class GroupLoader {

    private final GroupRepository groupRepository;

    @Autowired
    public GroupLoader(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public void init() {
        createTagsGroup();
    }

    private void createTagsGroup() {
        if (!groupRepository.existsByName("Material")) {
            Group group = new Group("Material", "meetup");
            groupRepository.save(group);
        }
        if (!groupRepository.existsByName("Volume")) {
            Group group = new Group("Volume", "th-large");
            groupRepository.save(group);
        }
        if (!groupRepository.existsByName("Room")) {
            Group group = new Group("Room", "file-text");
            groupRepository.save(group);
        }
        if (!groupRepository.existsByName("Status")) {
            Group group = new Group("Status", "plus-square");
            groupRepository.save(group);
        }
    }
}
