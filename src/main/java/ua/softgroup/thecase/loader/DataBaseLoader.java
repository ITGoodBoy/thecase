//package ua.softgroup.thecase.loader;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.stereotype.Component;
//import ua.softgroup.thecase.model.*;
//import ua.softgroup.thecase.service.ItemService;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
////TODO Delete after migrate to NO-embedded MongoDB
//@Component
//public class DataBaseLoader {
//
//    private final MongoTemplate mongoTemplate;
//    private final ItemService itemService;
//
//    @Autowired
//    public DataBaseLoader(ItemService itemService, MongoTemplate mongoTemplate) {
//        this.itemService = itemService;
//        this.mongoTemplate = mongoTemplate;
//    }
//
//    public void init() {
//
//        if (mongoTemplate.findAll(Product.class).size() > 0 || mongoTemplate.findAll(Model3D.class).size() > 0)
//            return;
//
//        List<String> categories = itemService.findDistinctItemFilterField(Item.Field.category);
//
//        List<Item> items = new ArrayList<>();
//
//        items.add(Product.builder().name("Chair S").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img1.jpg"))).brand(brands.get(0)).category(categories.get(0)).price(new Price((long) 19, Price.EUR)).build());
//        items.add(Product.builder().name("Chair L").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img2.jpg"))).brand(brands.get(1)).category(categories.get(1)).price(new Price((long) 29, Price.USD)).build());
//        items.add(Product.builder().name("Chair M").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img3.jpg"))).brand(brands.get(2)).category(categories.get(2)).build());
//        items.add(Product.builder().name("The best Sofa").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img4.jpg"))).brand(brands.get(3)).category(categories.get(0)).build());
//        items.add(Product.builder().name("Small Sofafa").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img5.jpg"))).brand(brands.get(4)).category(categories.get(1)).build());
//        items.add(Product.builder().name("Table S").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img6.jpg"))).brand(brands.get(0)).category(categories.get(2)).price(new Price((long) 69, Price.USD)).build());
//        items.add(Model3D.builder().name("Table L").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img7.jpg"))).brand(brands.get(1)).category(categories.get(0)).build());
//        items.add(Model3D.builder().name("Table M").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img8.jpg"))).brand(brands.get(2)).category(categories.get(1)).build());
//        items.add(Model3D.builder().name("GlassTable").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img9.jpg"))).brand(brands.get(3)).category(categories.get(2)).build());
//        items.add(Model3D.builder().name("Sofa-Table").imageUrls(new ArrayList<>(Arrays.asList("/api/image/img10.jpg"))).brand(brands.get(4)).category(categories.get(0)).build());
//
////        items.parallelStream().forEach(Item::mapInit);
//
//
////        itemService.save(items);
//
//    }
//}
