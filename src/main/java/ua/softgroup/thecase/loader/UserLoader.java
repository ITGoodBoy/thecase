package ua.softgroup.thecase.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.*;
import ua.softgroup.thecase.repository.CaseRepository;
import ua.softgroup.thecase.repository.UserRepository;

import java.util.*;

@Component
public class UserLoader {

    private UserRepository userRepository;
    private CaseRepository caseRepository;

    @Autowired
    public UserLoader(UserRepository userRepository, CaseRepository caseRepository) {
        this.userRepository = userRepository;
        this.caseRepository = caseRepository;
    }

    public void init(){

        if (userRepository.findAll().size() > 0)
            return;

        List<User> users = new ArrayList<>();

        users.add(constructUser("user-free", "email1@gmail.com", "password", Role.USER, Role.FREE));
        users.add(constructUser("user-admin", "email2@gmail.com", "password", Role.USER, Role.ADMIN));

        userRepository.save(users);

        List<Case> cases = new ArrayList<>();
        cases.add(Case.builder()
                .owner(users.get(0))
                .name("default")
                .description("default case")
                .products(new TreeSet<>((o1, o2) -> (o1.getAddedAt() >= o2.getAddedAt()) ? 1 : -1))
                .models3D(new TreeSet<>((o1, o2) -> (o1.getAddedAt() >= o2.getAddedAt()) ? 1 : -1))
                .build());
        cases.add(Case.builder()
                .owner(users.get(1))
                .name("default")
                .description("default case")
                .products(new TreeSet<>((o1, o2) -> (o1.getAddedAt() >= o2.getAddedAt()) ? 1 : -1))
                .models3D(new TreeSet<>((o1, o2) -> (o1.getAddedAt() >= o2.getAddedAt()) ? 1 : -1))
                .build());

        caseRepository.save(cases);


        users.get(0).setActiveCaseId(caseRepository.findByNameAndOwner(cases.get(0).getName(), users.get(0)).getId());
        users.get(1).setActiveCaseId(caseRepository.findByNameAndOwner(cases.get(1).getName(), users.get(0)).getId());

        userRepository.save(users);

    }

    private User constructUser(String name, String email, String pass, Role... roles) {
        return User.builder()
            .username(name)
            .email(email)
            .password(new BCryptPasswordEncoder().encode(pass))
            .authorities(Arrays.asList(roles))
            .createdAt(new Date(System.currentTimeMillis()))
            .enabled(true)
            .accountNonExpired(true)
            .accountNonLocked(true)
            .credentialsNonExpired(true).build();
    }
}
