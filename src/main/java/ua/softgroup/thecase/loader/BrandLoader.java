//package ua.softgroup.thecase.loader;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
////import ua.softgroup.thecase.model.Brand;
//import ua.softgroup.thecase.repository.BrandRepository;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by java-1-03 on 06.07.2017.
// */
//@Component
//public class BrandLoader {
//
//    private BrandRepository brandRepository;
//
//    @Autowired
//    public BrandLoader(BrandRepository brandRepository) {
//        this.brandRepository = brandRepository;
//    }
//
//
//    public void init() {
//
//        if (brandRepository.findAll().size() > 0)
//            return;
//
//        List<Brand> brands = new ArrayList<>();
//        brands.add(new Brand("Rowe", "American furniture brand. Started in 1970. Very big fabric."));
//        brands.add(new Brand("Palecek", "Alan Palacek created this brand 40 years ago."));
//        brands.add(new Brand("Maison 55", " American furniture brand"));
//        brands.add(new Brand("BOYD", "Another American brand which create furniture."));
//        brands.add(new Brand("Sterling", "Big and famous American brand!"));
//
//        brandRepository.save(brands);
//
//    }
//}
