package ua.softgroup.thecase.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.repository.TagRepository;
import ua.softgroup.thecase.service.ItemService;

import java.util.List;
import java.util.Map;

/**
 * Created by java-1-07 on 16.08.2017.
 */
@Component
public class TagLoader {

    private final ItemService itemService;
    private final TagRepository tagRepository;

    @Autowired
    public TagLoader(ItemService itemService, TagRepository tagRepository) {
        this.itemService = itemService;
        this.tagRepository = tagRepository;
    }

    public void init() {
        createUnsortedTagsGroup();
    }


    private void createUnsortedTagsGroup() {
//        tagRepository.deleteAll();
//        Map<String, Integer> allTags = new HashMap<>();
//
//        final int pageLimit = 1000;
//        int pageNumber = 0;
//        Page<Item> page = itemService.findAll(new PageRequest(pageNumber, pageLimit));
//        while (page.hasNext()) {
//            System.out.println("TagLoader: loading " + ((page.getNumber()*100)/page.getTotalPages()) + "%");
//            populateTags(allTags, page.getContent());
//            page = itemService.findAll(new PageRequest(++pageNumber, pageLimit));
//        }
//        // process last page
//        populateTags(allTags, page.getContent());
//
//        allTags.entrySet().forEach(e -> tagRepository.save(new Tag(e.getKey(), e.getValue())));
//
//        System.out.println("Tag loading done");
    }

    private void populateTags(Map<String, Integer> allTags, List<Item> items) {
        for (Item item : items) {
            for (String strTag : item.getTagsList()) {
                if (allTags.containsKey(strTag)) {
                    allTags.put(strTag, allTags.get(strTag) + 1);
                } else allTags.put(strTag, 1);
            }
        }
    }
}
