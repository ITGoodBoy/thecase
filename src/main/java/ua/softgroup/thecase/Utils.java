package ua.softgroup.thecase;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @Author ITGoodBoy
 **/
public class Utils {

    public static String imagesPath = "images/";
    public static String filesPath = "files/";

    public static String redirectToHTML(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext();

        Resource resource =
                appContext.getResource("classpath:" + path);

        try{
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            br.close();

        }catch(IOException e){
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }

}
