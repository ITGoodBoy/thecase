package ua.softgroup.thecase.config;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;


@Component
public class TokenAuthService {

    public static final String AUTH_HEADER_NAME = "X-Auth-Token";

    private final TokenHandler tokenHandler;
    private final UserService userService;

    @Autowired
    public TokenAuthService(TokenHandler tokenHandler, UserService userService) {
        this.tokenHandler = tokenHandler;
        this.userService = userService;
    }


    public Optional<Authentication> getAuthentication(@NonNull HttpServletRequest request) {
        UserAuthentication userAuthentication =
                Optional.ofNullable(request.getHeader(AUTH_HEADER_NAME))
                .flatMap(tokenHandler::extractUserId)
                .flatMap(userService::findById)
                .map(UserAuthentication::new)
                        .orElse(null);
        return Optional.ofNullable(userAuthentication);
    }


}
