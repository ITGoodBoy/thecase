package ua.softgroup.thecase.config;

import lombok.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import ua.softgroup.thecase.utils.OnlineUserCache;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class StatelessAuthFilter extends GenericFilterBean {

    private final TokenAuthService tokenAuthService;
    private final OnlineUserCache onlineUserCache;

    public StatelessAuthFilter(@NonNull TokenAuthService tokenAuthService, OnlineUserCache onlineUserCache) {
        this.tokenAuthService = tokenAuthService;
        this.onlineUserCache = onlineUserCache;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        Authentication authentication = tokenAuthService.getAuthentication((HttpServletRequest)servletRequest).orElse(null);

        if(authentication!=null){
            SecurityContextHolder.getContext().setAuthentication(
                    tokenAuthService.getAuthentication((HttpServletRequest)servletRequest).orElse(null));
            //write user to cache of online users
            onlineUserCache.addUser(authentication.getName());
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
