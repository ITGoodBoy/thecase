package ua.softgroup.thecase.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import ua.softgroup.thecase.model.Role;
import ua.softgroup.thecase.service.UserService;
import ua.softgroup.thecase.utils.OnlineUserCache;

@Configuration
@EnableAsync
@EnableScheduling
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final TokenAuthService tokenAuthService;
    private final OnlineUserCache onlineUserCache;

    @Autowired
    public SecurityConfig(UserService userService, TokenAuthService tokenAuthService, OnlineUserCache onlineUserCache) {
        this.userService = userService;
        this.tokenAuthService = tokenAuthService;
        this.onlineUserCache = onlineUserCache;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .csrf().disable()
                //ANONYMOUS PERMISSIONS
                .authorizeRequests()
                .antMatchers("/v2/api-docs/**", "/configuration/ui/**", "/swagger-resources/**",
                        "/configuration/security/**", "/swagger-ui.html**", "/swagger-ui.html", "/webjars/**")
                .permitAll()
                .antMatchers("/api/users/**").permitAll()
                .antMatchers("/api/image/**").permitAll()
                .and()
                .addFilterBefore(new StatelessAuthFilter(tokenAuthService, onlineUserCache), BasicAuthenticationFilter.class)
                //USER PERMISSIONS
                .authorizeRequests()
                .antMatchers("/api/items/all/**").hasRole(Role.USER.name())
                .antMatchers("/api/items/products/**").hasRole(Role.USER.name())
                .antMatchers("/api/items/models/**").hasRole(Role.USER.name())
                .antMatchers("/api/items/{id}/like").hasRole(Role.USER.name())
                .antMatchers("/api/sidebar/").hasRole(Role.USER.name())
                .antMatchers("/api/cases/").hasRole(Role.USER.name())
                .antMatchers("/api/removeUser").hasAnyRole(Role.ADMIN.name())
                .antMatchers("/api/banUser").hasAnyRole(Role.ADMIN.name())
                .antMatchers("/api/unbanUser").hasAnyRole(Role.ADMIN.name())
                .antMatchers("/api/unbanUser").hasAnyRole(Role.ADMIN.name())
                .antMatchers("/api/admin/**").hasAnyRole(Role.ADMIN.name())
                .anyRequest().fullyAuthenticated();
    }

    @Bean
    public PasswordEncoder bcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userService)
                .passwordEncoder(bcryptPasswordEncoder());
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        //TODO --secure origin
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return source;
    }

}
