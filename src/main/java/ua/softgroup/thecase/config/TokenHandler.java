package ua.softgroup.thecase.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bson.types.ObjectId;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Component;
import ua.softgroup.thecase.model.Role;
import ua.softgroup.thecase.model.User;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class TokenHandler {

    private final SecretKey secretKey;

    public TokenHandler() {
        String jwtKey = "jwtKey0123456789";
        byte[] decodeKey = Base64.decode(jwtKey.getBytes());
        this.secretKey = new SecretKeySpec(decodeKey, 0, decodeKey.length, "AES" );
    }

    public Optional<ObjectId> extractUserId(String token) {
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims body = claimsJws.getBody();

            System.out.println("TokenHandler: extractUserId = " + body.getId());
            return Optional
                    .ofNullable(body.get(Claims.ID, String.class))
                    .map(ObjectId::new);

        } catch (RuntimeException e) {
            System.out.println("TokenHandler: USER not found!");
           return Optional.empty();
        }
    }

    public String generateAccessToken(User user, LocalDateTime expires) {

        Map map = new HashMap<String, Object>();
        map.put(Claims.ID, user.getId());
        map.put("role", user.getSecondaryRoles());
        map.put("username", user.getUsername());
        map.put("sign", "Here was Vasia!!!");
        map.put("admin", user.getAuthorities().stream()
                .anyMatch(o -> o.equals(Role.getRole(Role.ADMIN))));

        return Jwts.builder()
                .setClaims(map)
                .setExpiration(Date.from(expires.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }
}