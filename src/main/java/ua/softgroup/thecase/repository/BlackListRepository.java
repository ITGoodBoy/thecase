package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.BlackWord;

/**
 * Created by TLJD Andrew Kobin on 04.10.2017.
 */
@Repository
public interface BlackListRepository extends MongoRepository<BlackWord, String> {
    BlackWord findByWord(String word);
    boolean existsByWord(String word);
}
