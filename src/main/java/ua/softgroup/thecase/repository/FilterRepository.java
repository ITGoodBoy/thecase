package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Filter;
import ua.softgroup.thecase.model.User;

/**
 * Created by java-1-03 on 26.09.2017.
 */
@Repository
public interface FilterRepository extends MongoRepository<Filter, String> {

    Filter findByUser(User user);

}
