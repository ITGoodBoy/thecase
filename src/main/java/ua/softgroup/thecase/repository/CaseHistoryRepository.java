package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.CaseHistory;

import java.util.List;

/**
 * Created by java-1-03 on 16.09.2017.
 */
@Repository
public interface CaseHistoryRepository extends MongoRepository<CaseHistory, String> {

    CaseHistory findByACase(Case aCase);

}
