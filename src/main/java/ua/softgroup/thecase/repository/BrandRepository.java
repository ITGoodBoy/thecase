//package ua.softgroup.thecase.repository;
//
//import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.stereotype.Repository;
//import ua.softgroup.thecase.model.Brand;
//
//import java.util.List;
//
///**
// * Created by java-1-03 on 06.07.2017.
// */
//@Repository
//public interface BrandRepository extends MongoRepository<Brand, String> {
//    Brand findOneByName(String name);
//
//
//    List<Brand> findByNameStartingWith(String name);
//    boolean existsByName(String name);
//}
