package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.User;

import java.util.List;

/**
 * Created by java-1-07 on 13.07.2017.
 */
@Repository
public interface CaseRepository extends MongoRepository<Case, String> {

    List<Case> findByOwnerOrderByCreatedAtDesc(User owner);

    List<Case> findDistinctByOwnerNot(User owner);

    Case findByNameAndOwner(String name, User owner);

}
