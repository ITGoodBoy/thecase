package ua.softgroup.thecase.repository;

import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Product;

/**
 * Created by java-1-03 on 30.08.2017.
 */
@Repository
public interface ProductRepository extends ItemRepository<Product> {
}
