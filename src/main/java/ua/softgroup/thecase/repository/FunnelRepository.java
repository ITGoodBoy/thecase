package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Funnel;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.model.User;

import java.util.List;

/**
 * Created by java-1-03 on 29.08.2017.
 */
@Repository
public interface FunnelRepository extends MongoRepository<Funnel, String> {

    List<Funnel> findByUser(User user);
    Funnel findByUserAndGroup(User user, Group group);

    List<Funnel> findByGroup(Group group);
}
