package ua.softgroup.thecase.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Relationship;
import ua.softgroup.thecase.model.User;

/**
 * Created by java-1-03 on 08.09.2017.
 */
@Repository
public interface RelationshipRepository extends MongoRepository<Relationship, String> {


    Relationship findByCurrentUserAndTargetUser(User currentUser, User targetUser);

    boolean existsByCurrentUserAndTargetUser(User currentUser, User targetUser);

    Page<Relationship> findByCurrentUser(User currentUser, Pageable pageable);

}
