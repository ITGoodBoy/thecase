package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Subscription;

/**
 * Created by java-1-07 on 18.07.2017.
 */
@Repository
public interface SubscriptionRepository extends MongoRepository<Subscription, String> {
}
