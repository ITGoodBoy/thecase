package ua.softgroup.thecase.repository;

import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Model3D;

/**
 * Created by java-1-03 on 30.08.2017.
 */
@Repository
public interface Model3DRepository extends ItemRepository<Model3D> {
}
