package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.User;
import ua.softgroup.thecase.model.UserToken;

/**
 * @Author ITGoodBoy
 **/
@Repository
public interface UserTokenRepository extends MongoRepository<UserToken, String> {
    UserToken findByUser(User user);
    UserToken findByToken(String token);
}
