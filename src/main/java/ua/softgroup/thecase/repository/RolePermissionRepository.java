package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Role;
import ua.softgroup.thecase.model.RolePermission;

/**
 * Created by java-1-07 on 18.07.2017.
 */
@Repository
public interface RolePermissionRepository extends MongoRepository<RolePermission, String> {
    RolePermission findByRole(Role role);
}
