package ua.softgroup.thecase.repository;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.User;


/**
 * Created by java-1-03 on 10.07.2017.
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findByEmail(String email);
    User findById(ObjectId id);
    User findByEmailAndPassword(String email, String password);

    User findDistinctByEmailNot(String email);

    Page<User> findDistinctByIdNot(String id, Pageable pageable);
    Page<User> findByUsernameStartingWithOrEmailStartingWith(String name, String email, Pageable pageable);

    //User findByUsername(String userName);
}
