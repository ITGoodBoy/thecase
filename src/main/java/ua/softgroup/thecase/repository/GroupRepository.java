package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Group;

import java.util.List;

@Repository
public interface GroupRepository extends MongoRepository<Group, String> {

    Group findByName(String name);
    Group findByNameIgnoreCase(String name);
    boolean existsByName(String name);
    List<Group> findDistinctByNameNot(String name);

    List<Group> findByPublishedTrue();
}
