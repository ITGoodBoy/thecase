package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.ItemStatistic;

/**
 * Created by java-1-03 on 18.09.2017.
 */
@Repository
public interface ItemStatisticRepository extends MongoRepository<ItemStatistic, String> {

    ItemStatistic findByItem(Item item);

}
