package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Like;
import ua.softgroup.thecase.model.User;

import java.util.List;

/**
 * Created by java-1-03 on 14.09.2017.
 */
@Repository
public interface LikeRepository extends MongoRepository<Like, String> {

    Like findByItemAndLiker(Item item, User liker);
    boolean existsByItemAndLiker(Item item, User Liker);
    long countByItem(Item item);

    List<Like> findByItem(Item item);

}
