package ua.softgroup.thecase.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ua.softgroup.thecase.model.Case;
import ua.softgroup.thecase.model.InviteCase;
import ua.softgroup.thecase.model.User;

import java.util.List;

/**
 * Created by java-1-03 on 11.09.2017.
 */
public interface InviteCaseRepository extends MongoRepository<InviteCase, String> {

    InviteCase findByInvitedUserAndACase(User invitedUser, Case aCase);
    List<InviteCase> findByInvitedUser(User invitedUser);
    List<InviteCase> findByACase(Case aCase);


}
