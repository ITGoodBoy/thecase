package ua.softgroup.thecase.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.softgroup.thecase.model.Group;
import ua.softgroup.thecase.model.Tag;

import java.util.List;

/**
 * Created by java-1-07 on 16.08.2017.
 */
@Repository
public interface TagRepository extends MongoRepository<Tag, String> {

    List<Tag> findByGroupAndNameStartingWithIgnoreCaseOrderByFrequencyDesc(Group group, String name);
    List<Tag> findByGroupAndNameContainingIgnoreCaseOrderByFrequencyDesc(Group group, String name);

    boolean existsByName(String name);
    Tag findByName(String name);

    Page<Tag> findByGroup(Group group, Pageable request);
    List<Tag> findByGroup(Group group);
    List<Tag> findByGroupAndCombinableTrue(Group group);


    Page<Tag> findAll(Pageable pageable);

    List<Tag> findTop100ByGroupOrderByFrequencyDesc(Group group);
}
