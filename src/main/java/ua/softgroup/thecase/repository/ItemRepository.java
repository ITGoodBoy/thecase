package ua.softgroup.thecase.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import ua.softgroup.thecase.model.Item;
import ua.softgroup.thecase.model.Model3D;

import java.util.List;

/**
 * Created by java-1-03 on 05.07.2017.
 */

public interface ItemRepository<T extends Item> extends MongoRepository<T, String> {

    T findFirstByOrderById();
    Page<T> findAllByOrderByScoreDesc(TextCriteria criteria, Pageable pageable);
    List<T> findByTagsListIn(String tagName);
    Page<T> findAll(Pageable pageable);
    List<T> findByIsPublishedTrueAndIsPublishedBySourceTrue();
//    Page<T> findByIsPublishedTrueOrderByScoreDesc(TextCriteria criteria, Pageable request);

//    Page<T> findByIsPublishedTrueOrderByScoreDesc(Criteria criteria, TextCriteria textCriteria, Pageable request);
    Page<T> findByIsPublishedTrueAndIsPublishedBySourceTrue(Criteria criteria, Pageable request);

    Page<T> findByTypeAndIsPublishedTrueAndIsPublishedBySourceTrueOrderByScoreDesc(String type, TextCriteria criteria, Pageable pageable);
    Page<T> findByIsPublishedTrueAndIsPublishedBySourceTrue(Pageable pageable);
    List<T> findByTagsListInAndIsPublishedTrueAndIsPublishedBySourceTrue(String name);
    Page<T> findByNameStartingWithIgnoreCase(String name, Pageable pageable);
    long countByIsPublishedTrueAndIsPublishedBySourceTrue();
    long countByIsPublishedTrueAndIsPublishedBySourceTrueOrderByScore(TextCriteria textCriteria);
    boolean existsByLink(String link);
    T findByLink(String link);
    int countByTagsListIn(String name);

    Page<Model3D> findBySourceStartingWithIgnoreCase(String source, Pageable pageRequest);

}
