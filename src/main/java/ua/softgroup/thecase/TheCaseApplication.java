package ua.softgroup.thecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import ua.softgroup.thecase.loader.*;

@SpringBootApplication
@EnableAsync
@EnableCaching
public class TheCaseApplication implements CommandLineRunner{

	@Value("${app.use.loaders}")
	private boolean USE_LOADER;

//	private BrandLoader brandLoader;
//	private DataBaseLoader dataBaseLoader;
	private UserLoader userLoader;
	private RolePermissionLoader rolePermissionLoader;
	private GroupLoader groupLoader;
	private final TagLoader tagLoader;

	@Autowired
	public TheCaseApplication(
//			BrandLoader brandLoader,
//							  DataBaseLoader dataBaseLoader,
							  UserLoader userLoader,
							  RolePermissionLoader rolePermissionLoader, GroupLoader groupLoader, TagLoader tagLoader) {
//			this.brandLoader = brandLoader;
//		this.dataBaseLoader = dataBaseLoader;
		this.userLoader = userLoader;
		this.rolePermissionLoader = rolePermissionLoader;
		this.groupLoader = groupLoader;
		this.tagLoader = tagLoader;
	}

	public static void main(String[] args) {
		SpringApplication.run(TheCaseApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		rolePermissionLoader.init();
		if (USE_LOADER) {
//			System.out.println("brandLoader");
//			brandLoader.init();
//
//			System.out.println("materialLoader");
//			materialLoader.init();
//
//			System.out.println("categoryLoader");
//			categoryLoader.init();
//
//			System.out.println("roomLoader");
//			roomLoader.init();

			System.out.println("userLoader");
			userLoader.init();

			System.out.println("rolePermissionLoader");
			rolePermissionLoader.init();

//			System.out.println("dataBaseLoader");
//			dataBaseLoader.init();

			System.out.println("groupLoader");
			groupLoader.init();

//			System.out.println("tagLoader");
//			tagLoader.init();
		}
	}
}
