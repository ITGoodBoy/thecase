import {changeQuantityLoader} from '../config/actionEvents';

export function quantityLoaderFunc (value) {
    return dispatch => {
        dispatch({
            type: changeQuantityLoader,
            state: {
                isQuantityLoader: value
            }
        })
    }
}