import { toggleSideBar, rebaseData, addToFolder, toggleSideBarFull, toggleProjects, toggleFunnel, funnelEvent, toggleList, toggleModal } from '../config/actionEvents';

export function toggleSideBarFunc (value) {
    return dispatch => {
        dispatch({
            type: toggleSideBar,
            state: {
                isSidebar: value,
            }
        })
    }
}

export function toggleModalFunc (value) {
    return dispatch => {
        dispatch({
            type: toggleModal,
            state: {
                modalWindow: value,
            }
        })
    }
}

export function toggleSideBarFullFunc (value) {
    return dispatch => {
        dispatch({
            type: toggleSideBarFull,
            state: {
                isSidebarFull: value,
            }
        })
    }
}

export function toggleProjectsFunc (value) {
    return dispatch => {
        dispatch({
            type: toggleProjects,
            state: {
                isProjects: value,
            }
        })
    }
}
export function toggleFunnelFunc (value) {
    return dispatch => {
        dispatch({
            type: toggleFunnel,
            state: {
                isFunnel: value,
            }
        })
    }
}
export function funnelEventFunc (value) {
    return dispatch => {
        dispatch({
            type: funnelEvent,
            state: {
                funnelActive: value,
            }
        })
    }
}
export function toggleListFunc (value) {
    return dispatch => {
        dispatch({
            type: toggleList,
            state: {
                isList: value,
            }
        })
    }
}

export function rebaseDataFunc (key, value, activeCase) {
    return dispatch => {
        dispatch({
            type: rebaseData,
            state: {
                key,
                value
            },
            payload: activeCase
        })
    }
}
export function addToFolderFunc (type, element) {
    return dispatch => {
        dispatch({
            type: addToFolder,
            state: {
                type,
                element
            }
        })
    }
}