import axios from 'axios';

axios.defaults.baseURL = 'http://185.86.77.92:8080/api/';
axios.defaults.timeout = 5000;

export function parseJwt () {
    let base64Url = localStorage.getItem('X-Auth-Token').split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}
export function Logout () {
    return  localStorage.clear() + sessionStorage.clear();
}
// Получить всех юзеров
export function getUsers () {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/users`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
// Заблокировать юзера
export function blockUser (id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/users/ban?id=${id}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
// Заблокировать юзера
export function unblockUser (id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/users/unban?id=${id}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
// Удалить юзера
export function removeUser (id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/users/${id}`,
        method: "delete",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
// Изменить статус юзера на админа или мембера
export function updateUserRole (id, give) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/users/role/giveRights`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            id: id,
            give: give
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
// Определить роль юзера
export function userRole (id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/users/role/${id}`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
export function getTags(name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/findByNameStartingWith/${name}/5`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            name: name
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            console.log('Error on Authentication: ' + error);
        });
}
// Отредактировать теги или добавить в другую группу
export function getSortedTags() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/getSortedTags`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            console.log('Error: ' + error);
        });
}
// Отредактировать теги или добавить в другую группу
export function addTags(name, tag) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/addTags?groupName=${name}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken,
            'Content-Type': 'application/json'
        },
        data: tag
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Удалить теги
export function removeTags(tag) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/removeTags`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken,
            'Content-Type': 'application/json'
        },
        data: tag
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Создать группу
export function createGroup(groupName, groupIcon) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/addGroup`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken,
            'Content-Type': 'application/json'
        },
        params: {
            groupName: groupName,
            groupIcon: groupIcon
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Изменить группу
export function renameGroup(groupOldName, groupNewName) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/renameGroup`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken,
            'Content-Type': 'application/json'
        },
        params: {
            groupOldName: groupOldName,
            groupNewName: groupNewName
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Удалить группу
export function removeGroup(groupName) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/removeGroup`,
        method: "delete",
        headers: {
            'X-Auth-Token': authToken,
            'Content-Type': 'application/json'
        },
        params: {
            groupName: groupName
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Получить Все Элементы
export function getAllElements (page, name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    let requestStatus;
    if(name != undefined) {
        requestStatus = `${page}/${name}`;
    } else if(page != undefined) {
        requestStatus = `${page}`;
    } else {
        requestStatus = ``;
    }
    return axios.get(`items/all/${requestStatus}`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Получить продукты
export function getItems (page, name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    let requestStatus;
    if(name != undefined) {
        requestStatus = `${page}/${name}`;
    } else if(page != undefined) {
        requestStatus = `${page}`;
    } else {
        requestStatus = ``;
    }
    return axios.get(`items/products/${requestStatus}`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Получить модели
export function getModels (page, name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    let requestStatus;
    if(name != undefined) {
        requestStatus = `${page}/${name}`;
    } else if(page != undefined) {
        requestStatus = `${page}`;
    } else {
        requestStatus = ``;
    }
    return axios.get(`items/models/${requestStatus}`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Like item
export function liked (id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `items/${id}/like`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Получить все Cases
export function getCases() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.get(`cases`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
    .then(response =>  {
        return response.data;
    })
    .catch(error => {
        console.log(error)
    });
}
// Delete Case
export function removeCases(id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.delete(`cases/${id}`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
    .then(response =>  {
        return response.data;
    })
    .catch(error => {
        console.log(error)
    });
}
export function getActivateCase(id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.get(`cases/${id}`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
    .then(response =>  {
        return response.data;
    })
    .catch(error => {
        console.log(error)
    });
}
export function upItem(id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `cases/up/${id}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        }
    })
    .then(response =>  {
        return response.data;
    })
    .catch(error => {
        console.log(error)
    });
}
// Создать Cases
export function createCases(name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `cases`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            name: name,
            description: '123'
        }
    })
    .then(response =>  {
        return response.data;
    })
    .catch(error => {
        if(error.response === undefined) {
            let verif = document.querySelector('body');
            let errorEl = document.createElement('div');
            errorEl.className = "error server-status";
            errorEl.style = 'color: #fff; background: #f44336;';
            errorEl.innerHTML = 'Error: Server not responding, please try again later';
            verif.appendChild(errorEl);
            setTimeout(function(){
                document.querySelector(".error").remove();
            }, 4000);
        }
    });
}
// Редактировать Cases
export function editCases(id, name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `cases/${id}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            name: name,
            description: '123'
        }
    })
        .then(response =>  {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
// Получить активный Case
export function focusCases() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.get(`cases/active`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
    .then(response =>  {
        return response.data;
    })
    .catch(error => {
        console.log(error)
    });
}
// Отправить item в case
export function postCaseItem(caseId, id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `cases/${caseId}/items`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            itemId: id
        }
    })
    .then(response => {
        return response.data;
    })
    .catch(error => {
        console.log(error)
    });
}
// Удалить item из Case
export function deleteCaseItem(id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `cases/items/${id}`,
        method: "delete",
        headers: {
            'X-Auth-Token': authToken
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}

export function Login(email, password) {
    return axios.request({
        url: `users/login`,
        method: "post",
        params: {
            email,
            password
        }
    })
    .then(function(response){
        localStorage.setItem('X-Auth-Token', response.data);
        setTimeout(function(){
            window.location.reload();
        }, 150);
        return response.data;

    })
    .catch(function (error){
        let verif = document.querySelector('#login-form form');
        let errorEl = document.createElement('div');
        errorEl.className = "error-status";
        verif.appendChild(errorEl);
        let remove = setTimeout(function(){
            document.querySelector(".error-status").remove();
        }, 3000);
        if(error.response === undefined) {
            errorEl.innerHTML = 'Server not responding, please try again later';
            remove
        } else if (error.response.status === 409) {
            errorEl.innerHTML = 'Incorrect email or password, please try again';
            remove
        } else if (error.response.status === 423) {
            errorEl.innerHTML = 'Sorry user is blocked';
            remove
        }
    });
}
// create Item (product and model) in admin panel
export function createItem(selectedOption, name, uploadImgs, published, category, type, brand, sourceLink, allPrice, selectedTags) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/items/${selectedOption}`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
        data: {
            name,
            selectedOption,
            uploadImgs,
            published,
            category,
            type,
            brand,
            sourceLink,
            price: allPrice,
            tags: selectedTags
        }
    })
        .then(response => {
            console.log(`create new ${selectedOption}`, response.data);
            return response.data;
        })
        .catch(error => {
            console.log(error)
        });
}
// edit Item (product and model) in admin panel
export function editItems(id, selectedOption, name, oldImage, newImage, published, category, type, brand, sourceLink, allPrice, selectedTags) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/items/${selectedOption}/${id}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
        data: {
            name,
            selectedOption,
            imgs: oldImage,
            uploadImgs: newImage,
            published,
            category,
            type,
            brand,
            sourceLink,
            price: allPrice,
            tags: selectedTags
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response.status === 400) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error 400: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        })
}
// Регистрация Юзера
export function Register(userName, email, password) {
    return axios.request({
        url: `users/registration`,
        method: "post",
        params: {
            userName: userName,
            email: email,
            password: password,
        }
    })
        .then(function(response){
            let verif = document.querySelector('#modal_1 form');
            console.log(verif);
            verif.innerHTML = 'Confirmation email has been successfully sent to your email address';
            document.querySelector('#modal_1 .darken-2').remove();
            setTimeout(function(){
                window.location.reload();
            }, 4000);
            return response.data;
        })
        .catch(function (error){
            if(error.response.status === 409) {
                let verif = document.querySelector('#modal_1 form');
                let errorEl = document.createElement('div');
                errorEl.className = "col input-field s12 error";
                errorEl.innerHTML = 'Error: Email address is already registered';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 3000);
            } else if(error.response.status === 500) {
                let verif = document.querySelector('#modal_1 form');
                let errorEl = document.createElement('div');
                errorEl.className = "col input-field s12 error";
                errorEl.innerHTML = 'Error: Incomplete fields';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 3000);
            } else {
                let verif = document.querySelector('#modal_1 form');
                let errorEl = document.createElement('div');
                errorEl.className = "col input-field s12 error";
                errorEl.innerHTML = "Error: No access to the server :-(" + "<br/>" + "Try to reload the page ;-)";
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 3000);
            }
        });
}
//  reset user's password
export function resetPassword(email) {
    return axios.request({
        url: `users/resetForgottenPassword`,
        method: "put",
        params: {
            email
        }
    })
        .then(function(response){
            let verif = document.querySelector('#reset-password form');
            let successEl = document.createElement('div');
            successEl.className = "success-status center-align";
            successEl.innerHTML = 'Temporary password has been successfully sent to your email address';
            verif.appendChild(successEl);
            setTimeout(function(){
                window.location.reload();
            }, 2000);
            return response.data;
        })
        .catch(function (error){
            let verif = document.querySelector('#reset-password form');
            let errorEl = document.createElement('div');
            errorEl.className = "error-status";
            verif.appendChild(errorEl);
            let remove = setTimeout(function(){
                document.querySelector(".error-status").remove();
            }, 3000);
            if(error.response.status === 404) {
                errorEl.innerHTML = 'Error: There is no user registered with that email address';
                remove
            } else if(error.response.status === 500) {
                errorEl.innerHTML = 'Error: Incomplete field';
                remove
            } else {
                errorEl.innerHTML = "Error: No access to the server :-(" + "<br/>" + "Try to reload the page ;-)";
                remove
            }
        });
}
//  replace user's password
export function replacePassword(email, oldPassword, newPassword) {
    return axios.request({
        url: `users/replacePassword`,
        method: "put",
        params: {
            email,
            oldPassword,
            newPassword
        }
    })
        .then(function (response){
            let verif = document.querySelector('#replace-password form');
            let successEl = document.createElement('div');
            successEl.className = "success-status center-align";
            successEl.innerHTML = 'You successfully changed your password!';
            verif.appendChild(successEl);
            setTimeout(function(){
                window.location.reload();
            }, 2000);
            return response.data;
        })
        .catch(function (error){
            let verif = document.querySelector('#replace-password form');
            let errorEl = document.createElement('div');
            errorEl.className = "error-status";
            verif.appendChild(errorEl);
            let remove = setTimeout(function(){
                document.querySelector(".error-status").remove();
            }, 3000);
            if(error.response === undefined) {
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                remove
            } else if (error.response.status === 404) {
                errorEl.innerHTML = 'Error: There is no user registered with that email address';
                remove
            } else if (error.response.status === 409) {
                errorEl.innerHTML = 'Error: Incorrect field Old Password';
                remove
            }
        });
}
// get user's accessibility to create case
export function getAccess() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `users/access`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(function(response){
            return response.data;
        })
        .catch(function (error){
            let verif = document.querySelector('body');
            let errorEl = document.createElement('div');
            errorEl.className = "error-status";
            verif.appendChild(errorEl);
            let remove = setTimeout(function(){
                document.querySelector(".error-status").remove();
            }, 3000);
            if(error.response.status === 401) {
                errorEl.innerHTML = 'Error: Email address is already registered';
                remove
            } else {
                errorEl.innerHTML = "Error: No access to the server :-(" + "<br/>" + "Try to reload the page ;-)";
                remove
            }
        });
}
// Получить воронки
export function getFunnels () {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.get(`funnels`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Згенерит воронки
export function funnelsSearch (id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `funnels/activateByGroup?groupId=${id}`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
        param: {
            groupId: id
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Фильтр айтемов
export function itemsSearch (id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `funnels/activateByItem?itemId=${id}`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
        param: {
            groupId: id
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}

// Сбросить поиск по ворнкам
export function deactivateFunnels() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `funnels/deactivateFunnels`,
        method: "delete",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// get types
export function getTypes () {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.get(`admin/type`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// get categories
export function getCategories () {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.get(`admin/category`, {
        headers: {
            'X-Auth-Token': authToken
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// get brands by search results
export function getBrands(name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/brand/search/${name}/5`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            name: name
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(function (error){
            console.log('Error on Authentication: ' + error);
        });
}

// Command Works ---------------------------------------------------------------------------------------------------------------------
// Get users list
export function usersAllList(page, name) {
    let authToken = localStorage.getItem('X-Auth-Token');
    let requestStatus;
    if(name != undefined) {
        requestStatus = `${page}/${name}`;
    } else if(page != undefined) {
        requestStatus = `${page}`;
    } else {
        requestStatus = ``;
    }
    return axios.request({
        url: `users/${requestStatus}`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Add user to contact list
export function addToContact(id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `users/contacts`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            userId: id
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Remove user from contact list
export function removeFromContact(id) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `users/contacts`,
        method: "delete",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            userId: id
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// User contact list
export function userContacts(page) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `users/contacts/${page}`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Follow from contact user to case
export function followToCase(caseId, userId, permission) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `team/users`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            caseId: caseId,
            userId: userId,
            permissionNumber: permission,
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Accept command work
export function acceptFollow(caseId) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `team/cases/${caseId}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Cancel command work
export function cancelFollow(caseId) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `team/cases/${caseId}`,
        method: "delete",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Get users from command work
export function getUserFromTeamWork(caseId) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `team/users/${caseId}`,
        method: "get",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Remove user from command work
export function removeUserFromTeamWork(caseId) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `team/cases/${caseId}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Send Case to email
export function sendFullCase(caseId, email) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `team/cases/${caseId}/send`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            emailTo: email
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Send Case elements to email
export function sendCaseElements(email, items) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `team/items/send`,
        method: "post",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            emailTo: email,
        },
        data: items
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Edit Item count
export function editActiveItem(itemId, count) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `cases/items/${itemId}`,
        method: "put",
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            count: count
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// *************************************** FILTERS *******************************************
// Get filters
export function getFilters() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `filters`,
        method: 'get',
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Get filters
export function getActiveFilters() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `filters/active`,
        method: 'get',
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Add and remove filter
export function filterEvent(filter, name, method) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `filters/${filter}/${name}`,
        method: method,
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Deactivate Filters
export function deactivateFilters() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `filters/active`,
        method: 'delete',
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Deactivate Filters
export function deactivateFilter(filter) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `filters/${filter}`,
        method: 'delete',
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
// Block Words
export function blockWord(word) {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/blacklist`,
        method: 'post',
        headers: {
            'X-Auth-Token': authToken
        },
        params: {
            word: word
        }
    })
        .then(response => {
            if(response) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "successfull server-status";
                errorEl.style = 'color: #fff; background: green;';
                errorEl.innerHTML = word + ' Deleted';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".successfull").remove();
                }, 4000);
                return response.data;
            }
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}
export function clearWords() {
    let authToken = localStorage.getItem('X-Auth-Token');
    return axios.request({
        url: `admin/blacklist/cleanup`,
        method: 'get',
        timeout: 100000,
        headers: {
            'X-Auth-Token': authToken
        },
    })
        .then(response => {
            if(response) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "successfull server-status";
                errorEl.style = 'color: #fff; background: green;';
                errorEl.innerHTML = 'Clean up successfully';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".successfull").remove();
                }, 4000);
                return response.data;
            }
        })
        .catch(error => {
            if(error.response === undefined) {
                let verif = document.querySelector('body');
                let errorEl = document.createElement('div');
                errorEl.className = "error server-status";
                errorEl.style = 'color: #fff; background: #f44336;';
                errorEl.innerHTML = 'Error: Server not responding, please try again later';
                verif.appendChild(errorEl);
                setTimeout(function(){
                    document.querySelector(".error").remove();
                }, 4000);
            }
        });
}