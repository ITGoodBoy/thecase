import React, {Component} from "react";
import {Col, Icon, Row, Input, Chip, Collapsible, CollapsibleItem, Button} from 'react-materialize';

import '../CardFlip/CardFlip.scss';
import IconFont from '../IconFont/IconFont';
import { postCaseItem, getCases, itemsSearch, getItems, getModels, focusCases, deactivateFilters, filterEvent, getFilters, getActiveFilters, liked, getTags, getBrands, editItems, getActivateCase, parseJwt } from '../../services/http';
import LazyLoad from 'react-lazy-load';
import OwlCarousel from 'react-owl-carousel2';
import Dropzone from 'react-dropzone';
import { WithContext as ReactTags } from 'react-tag-input';

class Model extends Component {
    constructor(props) {
        super(props);
        let tags;
        if(this.props.item.item.tagsList !== null) {
            tags = this.props.item.item.tagsList.map((item, index) => {return {id: index, text: item};})
        } else {
            tags = [];
        }
        let image = this.props.item.item.imageUrls.map((image, index) => {return {id: index, file: image}});
        let isOpen = [];
        this.props.sortedTags.forEach(function (i) {
            isOpen.push(i)
        });
        this.state = {
            buttonStatus: [],
            itemEdit: [],
            isFront: true,
            activeCase: [],
            itemNo: 1,
            loop: true,
            nav: true,
            rewind: true,
            autoplay: true,
            lazyLoad: true,
            liked: this.props.item.liked,
            tagsView: this.props.getTags,
            isOpen: isOpen,
            selectedOption: 'model3d',
            tags: tags,
            imgs: [],
            files: image,
            blobs: [],
            updFiles: [],
            name: this.props.item.item.name,
            published: this.props.item.item.published,
            category: this.props.item.item.info.Category.toString(),
            type: this.props.item.item.info.Type.toString(),
            brand: this.props.item.item.info.Brand.toString(),
            brandsView: this.props.getBrands,
            sourceLink: this.props.item.item.link || '',
            selectedTags: this.props.item.item.tagsList,
            priceCurrency: this.props.item.item.price.currency || 'USD',
            priceAmount: this.props.item.item.price.amount || '',
            caseId: this.props.activeCase.id
        };

        this.toggle = this.toggle.bind(this);
        this.toggleEl = this.toggleEl.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleStarClick = this.handleStarClick.bind(this);
        this.filter = this.filter.bind(this);
        this.itemEdit = this.itemEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.updateTags = this.updateTags.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.removeImage = this.removeImage.bind(this);
        this.removeNewImage = this.removeNewImage.bind(this);
        this.newDrop = this.newDrop.bind(this);
        this.updateBrands = this.updateBrands.bind(this);
        this.brandDelete = this.brandDelete.bind(this);
        this.brandAddition = this.brandAddition.bind(this);
        this.editItem = this.editItem.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.targetSortElement = this.targetSortElement.bind(this);
    }
    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];
        this.setState({
            buttonStatus: newStatus,
        });
    }
    itemEdit(i) {
        const newStatus = this.state.itemEdit.slice();
        newStatus[i] = !this.state.itemEdit[i];
        this.setState({
            itemEdit: newStatus,
        });
    }

    toggle() {
        this.setState({
            isFront: !this.state.isFront
        });
    }

    filter(filter, name) {
        let f = filter.toLowerCase();
        let n = name.toString();
        let m;
        if(Object.keys(this.props.activeFilters).map((key) => this.props.activeFilters[key].map((el) => el)).toString().indexOf(n) > -1) {
            m = 'delete';
        } else {
            m = 'post';
        }
        deactivateFilters()
            .then(() => {
                filterEvent(f, n, m)
                    .then(() => {
                        getItems()
                            .then((data) => {
                                this.props.rebaseData('items', data)
                            });
                        getModels()
                            .then((data) => {
                                this.props.rebaseData('models', data)
                            })
                    })
                    .then(() => {
                        let scrollElement = document.querySelector('.infinite-scroll-component');
                        scrollElement.scrollTop = 0;
                    })
                    .then(() => {
                        if(this.props.funnelActive == false) {
                            return false;
                        } else {
                            this.props.funnelEvent(!this.props.funnelActive);
                        }
                    })
                    .then(() => {
                        getFilters()
                            .then((data) => {
                                if(data) {
                                    this.props.rebaseData('filters', data);
                                }
                            });
                        getActiveFilters()
                            .then((data) => {
                                if(data) {
                                    this.props.rebaseData('activeFilters', data);
                                }
                            });
                    })
            });
    }

    handleStarClick(id) {
        liked(id)
            .then((data) => {
                if(data) {
                    this.setState({
                        liked: !this.state.liked
                    })
                }
            })
            .then(() => {
                focusCases()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('activeCase', data);
                        }
                    });
            })
            .catch((error) => {
                if(error) {
                    this.setState({
                        liked: this.state.liked
                    })
                }
            });
    }
    toggleEl(id) {
        const newStatus = this.state.isOpen.slice();
        newStatus[id] = !this.state.isOpen[id];
        this.setState({
            isOpen: newStatus,
        });
    }
    updateTags(tag)  {
        if(tag == '') {
            getTags('A')
        } else {
            getTags(tag)
                .then(tagsView => {
                    this.setState({tagsView})
                });
        }
    }

    handleDelete(i) {
        let tags = this.state.tags;
        let selectedTags = this.state.selectedTags;
        tags.splice(i, 1);
        selectedTags.splice(i, 1);
        this.setState({
            tags,
            selectedTags
        });
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        let selectedTags = this.state.selectedTags.concat(tag);
        tags.push({
            id: tags.length + 1,
            text: tag
        });
        this.setState({
            tags,
            selectedTags
        });
    }

    handleOptionChange(changeEvent) {
        this.setState({
            selectedOption: changeEvent.target.value
        })
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    };

    updateBrands(tag)  {
        if(tag == '') {
            getBrands('A')
        } else {
            getBrands(tag)
                .then(brandsView => {
                    this.setState({brandsView})
                });
        }
    }

    brandDelete(i) {
        let brands = this.state.brands;
        brands.splice(i, 1);
        this.setState({
            brands
        });
    }

    brandAddition(brand) {
        let brands = [];
        brands.push({
            id: brand.length + 1,
            text: brand
        });
        this.setState({
            brands,
            brand: brand,
        })
    }
    onDrop(files) {
        let filesArray = this.state.imgs.concat(files);
        this.setState({
            imgs: filesArray
        });
        this.newDrop(files);
    }

    newDrop(files) {
        const promise = new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.readAsDataURL(files[0]);

            reader.onload = () => {
                if (!!reader.result) {
                    resolve(reader.result)
                }
                else {
                    reject(Error("Failed converting to base64"))
                }
            }
        });
        promise.then(result => {
            let blobs = this.state.blobs.concat(result);
            this.setState({
                blobs,
            });
        }, err => {
            console.log(err)
        });
    }

    removeImage(event, ind) {
        event.preventDefault();
        let newArray = this.state.files;
        newArray.splice(ind, 1);
        this.setState({
            files: newArray
        });
    }
    removeNewImage(event, ind) {
        event.preventDefault();
        let newArray = this.state.imgs;
        let blob = this.state.blobs;
        newArray.splice(ind, 1);
        blob.splice(ind, 1);
        this.setState({
            imgs: newArray,
            blobs: blob
        });
    }
    targetSortElement(e) {
        const newSelection = e.target.value;
        let newSelectionArray;
        if(this.state.selectedTags.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.selectedTags.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.selectedTags, newSelection];
        }
        this.setState({ selectedTags: newSelectionArray });
    }

    editItem(id) {
        let selectedOption = this.state.selectedOption;
        let name = this.state.name;
        let oldImage = this.state.files.map((image) => image.file);
        let newImage = this.state.blobs;
        let published = this.state.published;
        let category = this.state.category;
        let type = this.state.type;
        let sourceLink = this.state.sourceLink;
        let selectedTags = this.state.selectedTags;
        let brand = this.state.brand;
        let allPrice;
        if(!this.state.priceAmount.length && !this.state.priceCurrency.length) {
            allPrice = null;
        } else {
            allPrice = {
                amount: this.state.priceAmount,
                currency: this.state.priceCurrency,
            };
        }
        editItems(id, selectedOption, name, oldImage, newImage, published, category, type, brand, sourceLink, allPrice, selectedTags)
            .then(() => {
                this.props.updateItem();
            })
            .then(() => {
                this.itemEdit(id);
            })
    }
    cancelEdit(id) {
        let tags;
        if(this.props.item.item.tagsList !== null) {
            tags = this.props.item.item.tagsList.map((item, index) => {return {id: index, text: item};})
        } else {
            tags = [];
        }
        let image = this.props.item.item.imageUrls.map((image, index) => {return {id: index, file: image}});
        let isOpen = [];
        this.props.sortedTags.forEach(function (i) {
            isOpen.push(i)
        });
        this.setState({
            buttonStatus: [],
            itemEdit: [],
            isFront: true,
            activeCase: [],
            itemNo: 1,
            loop: true,
            nav: true,
            rewind: true,
            autoplay: true,
            lazyLoad: true,
            tagsView: this.props.getTags,
            isOpen: isOpen,
            selectedOption: 'model3d',
            tags: tags,
            imgs: [],
            files: image,
            blobs: [],
            updFiles: [],
            name: this.props.item.item.name,
            published: this.props.item.item.published,
            category: this.props.item.item.info.Category.toString(),
            type: this.props.item.item.info.Type.toString(),
            brand: this.props.item.item.info.Brand.toString(),
            brandsView: this.props.getBrands,
            sourceLink: this.props.item.item.link || '',
            selectedTags: this.props.item.item.tagsList,
            priceAmount: this.props.item.item.price.amount || '',
            priceCurrency: this.props.item.item.price.currency || 'USD',
        });
        this.itemEdit(id);
    }

    componentWillReceiveProps(nextProps) {
        let tags;
        if(nextProps.item.item.tagsList !== null) {
            tags = nextProps.item.item.tagsList.map((item, index) => {return {id: index, text: item};})
        } else {
            tags = [];
        }
        let image = nextProps.item.item.imageUrls.map((image, index) => {return {id: index, file: image}});
        let isOpen = [];
        nextProps.sortedTags.forEach(function (i) {
            isOpen.push(i)
        });
        this.setState({
            buttonStatus: [],
            itemEdit: [],
            isFront: true,
            activeCase: [],
            itemNo: 1,
            loop: true,
            nav: true,
            rewind: true,
            autoplay: true,
            lazyLoad: true,
            tagsView: nextProps.getTags,
            isOpen: isOpen,
            selectedOption: 'model3d',
            tags: tags,
            imgs: [],
            files: image,
            blobs: [],
            updFiles: [],
            name: nextProps.item.item.name,
            published: nextProps.item.item.published,
            category: nextProps.item.item.info.Category.toString(),
            type: nextProps.item.item.info.Type.toString(),
            brand: nextProps.item.item.info.Brand.toString(),
            brandsView: nextProps.getBrands,
            sourceLink: nextProps.item.item.link || '',
            selectedTags: nextProps.item.item.tagsList,
            priceAmount: nextProps.item.item.price.amount || '',
            priceCurrency: nextProps.item.item.price.currency || 'USD',
        });
    }

    render() {
        let item = this.props.item.item;
        let editForm, isSidebarFullOpened;
        if(this.state.itemEdit[item.id] === true) {
            editForm = (
                <Row key={item.id} className="item-edit-form">
                    <a className="close-edit-form" onClick={(event) => {event.preventDefault(); this.itemEdit(item.id)}}><Icon>close</Icon></a>
                    <Col s={12}>
                        <Input s={12} type='text' defaultValue={this.state.name} label='Item Name' onChange={(event) => this.setState({name: event.target.value})} />
                    </Col>
                    <Col s={12}>
                        <Input s={4} name='item' type='radio' value='model3d'  label='Model' checked={this.state.selectedOption === 'model3d'} onChange={this.handleOptionChange} className='with-gap' />
                        <Input s={4} name='item' type='radio' value='product' label='Product' checked={this.state.selectedOption === 'product'} onChange={this.handleOptionChange} className='with-gap' />
                        <Input s={4} name='published' type='checkbox' label='Published' className='filled-in' checked={this.state.published} onChange={this.handleChange} />
                    </Col>
                    <Col s={12}>
                        <Input s={6}
                               type='select'
                               label="Category"
                               value={this.state.category}
                               onChange={this.handleChange}
                               name="category"
                        >
                            {this.props.categories.map((key, id) => {
                                return (
                                    <option key={id} value={key}>{key}</option>
                                )
                            })}
                        </Input>
                        <Input s={6}
                               type='select'
                               label="Type"
                               value={this.state.type}
                               onChange={this.handleChange}
                               name="type"
                        >
                            {this.props.types.map((key, id) => {
                                return (
                                    <option key={id} value={key}>{`${key.charAt(0).toUpperCase()}${key.substr(1).toLowerCase()}`}</option>
                                )
                            })}
                        </Input>
                    </Col>
                    <Col s={12}>
                        {this.state.brand.length ? (
                            <Col s={12}>
                                <Chip>{this.state.brand}</Chip>
                            </Col>
                        ) : []}
                        <ReactTags
                            suggestions={
                                this.state.brandsView.map(
                                    (brand) => brand
                                )
                            }
                            placeholder="Add new brand"
                            autofocus={false}
                            handleDelete={this.brandDelete}
                            handleAddition={this.brandAddition}
                            handleInputChange={this.updateBrands}
                            classNames={{
                                tag: 'chip',
                                remove: 'left-padding',
                                suggestions: 'suggestions-container'
                            }}
                        />
                    </Col>
                    <Col s={12}>
                        <Input s={12}
                               label="Source link"
                               name="sourceLink"
                               value={this.state.sourceLink}
                               onChange={this.handleChange}
                        />
                    </Col>
                    <Col s={12} className='edit-tags'>
                        <ReactTags tags={this.state.tags}
                                   suggestions={
                                       this.state.tagsView.map(
                                           (tag) => tag.name
                                       )
                                   }
                                   autofocus={false}
                                   handleDelete={this.handleDelete}
                                   handleAddition={this.handleAddition}
                                   handleInputChange={this.updateTags}
                                   classNames={{
                                       tag: 'chip',
                                       remove: 'left-padding',
                                       suggestions: 'suggestions-container'
                                   }}
                        />
                    </Col>
                    <Col s={12}>
                        {this.props.sortedTags.map((key, id) => {
                            return (
                                <Row className="top-padding" key={id}>
                                    <Collapsible>
                                        <CollapsibleItem
                                            header={key.name}
                                            onClick={() => this.toggleEl(id)}
                                            icon={this.state.isOpen[id] ? 'keyboard_arrow_right' : 'keyboard_arrow_down'}
                                            iconClassName="right"
                                        >

                                            <Row>
                                                {key.tags.map((tag, ind) => {
                                                    let check;
                                                    if(this.state.selectedTags.indexOf(tag.name) > -1 == true) {
                                                        check = 'checked';
                                                    } else {
                                                        check = false;
                                                    }
                                                    return (
                                                        <Input s={4}
                                                               type='checkbox'
                                                               className='filled-in'
                                                               key={ind}
                                                               name={key.name}
                                                               value={tag.name}
                                                               label={`${tag.name} ${tag.frequency}`}
                                                               checked={check}
                                                               onChange={(event) => {this.targetSortElement(event)}}
                                                        />
                                                    )
                                                })}
                                            </Row>
                                        </CollapsibleItem>
                                    </Collapsible>
                                </Row>
                            )
                        })}
                    </Col>
                    <Col s={12}>
                        <Input
                            s={8}
                            type="number"
                            label="Price"
                            name="amount"
                            defaultValue={this.state.priceAmount.toString()}
                            onChange={(event) => this.setState({
                                priceAmount: event.target.value,
                            })}
                        />

                        <Input s={4}
                               type='select'
                               label="Currency"
                               defaultValue={this.state.priceCurrency.toString()}
                               onChange={(event) => this.setState({
                                   priceCurrency: event.target.value,
                               })}
                               name="currency"
                        >
                            <option value=''>Chose Currency</option>
                            <option value='USD'>USD</option>
                            <option value='EUR'>EUR</option>
                            <option value='RUB'>RUB</option>
                        </Input>
                    </Col>
                    <Col s={12}>
                        <div className="dropzone">
                            <Dropzone onDrop={this.onDrop.bind(this)}
                                      accept="image/*"
                                      ref="fileUpload"
                            >
                                Upload your images
                            </Dropzone>
                            {this.state.files.map((file, ind) => {
                                return (
                                    <div key={ind} className="top-indent relative edit-image">
                                        <img src={file.file} className="uploaded-image"/>
                                        <a href="#" onClick={(event) => {
                                            this.removeImage(event, ind)
                                        }}>
                                            <IconFont name="times-circle" size="2x"/>
                                        </a>
                                    </div>
                                )
                            })}
                            {this.state.imgs.map((file, ind) =>
                                <div key={ind} className="top-indent relative edit-image">
                                    <img src={file.preview} className="uploaded-image" />
                                    <a href="#" onClick={(event) => {this.removeNewImage(event, ind)}}>
                                        <IconFont name="times-circle" size="2x"/>
                                    </a>
                                </div>
                            )}
                        </div>
                    </Col>
                    <Col s={4} className='right'>
                        <Button waves='light' className="waves-effect waves-teal btn-flat edit-button-form save-edit-form"  onClick={() => {
                            this.editItem(item.id);
                        }} >Edit</Button>
                        <Button waves='light' className="waves-effect waves-teal btn-flat edit-button-form cancel-edit-form"  onClick={() => {
                            this.cancelEdit(item.id);
                        }} >Cancel</Button>
                    </Col>
                </Row>
            )
        } else {
            editForm = false;
        }
        // let modelLike = this.props.model;
        // console.log('modelLike', modelLike);
        const options = {
            items: this.state.itemNo,
            loop: this.state.loop,
            nav: this.state.nav,
            rewind: this.state.rewind,
            autoplay: this.state.autoplay,
            lazyLoad: this.state.lazyLoad,
            mouseDrag: false,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        };
        if(!this.props.isSidebarFull) {
            isSidebarFullOpened = !this.props.isSidebarFull;
        } else {
            isSidebarFullOpened = this.props.isSidebarFull;
        }

        return (
            <Col s={3} className='grid-card'>
                {editForm}
                {this.state.buttonStatus[23] ?
                    (
                        <div className="to-project">
                            <Row className="choose-projects">
                                <Col s={12} className="projects-head">
                                    <p className="go-to-projects" onClick={() => {
                                        this.handleClick(23);
                                        this.props.toggleProjects(!this.props.isProjects);
                                        this.props.toggleSideBarFull(isSidebarFullOpened);
                                    }}>Projects   <IconFont name="arrow-left" /></p>
                                    <p className="heading">SAVE IN PROJECT</p>
                                    <a onClick={() => {this.handleClick(23); this.setState({caseId: this.props.activeCase.id})}}><IconFont name="times" /></a>
                                </Col>
                                {this.props.cases.map((key, ind) => (
                                    <Col s={12} className={key.id.indexOf(this.state.caseId) ? 'modal-projects' : 'modal-projects projects-is-active'} key={ind} onClick={(event) => {
                                        event.preventDefault();
                                        this.setState({caseId: key.id});
                                    }}>
                                        <IconFont name={key.id.indexOf(this.state.caseId) ? 'folder' : 'folder-open'} />
                                        <p>{key.name}</p>
                                    </Col>
                                ))}
                                <Col s={12} className="projects-buttons">
                                    <button onClick={() => {this.handleClick(23); this.setState({caseId: this.props.activeCase.id})}} className="project-cancel">Cancel</button>
                                    <button className="project-save" onClick={() => {
                                        let self = this;
                                        postCaseItem(this.state.caseId, item.id)
                                            .then(() => {
                                                focusCases()
                                                    .then(data => {
                                                        if(data) {
                                                            setTimeout(function () {
                                                                self.props.rebaseData('activeCase', data);
                                                            }, 300)
                                                        }
                                                    });
                                                getCases()
                                                    .then(data => {
                                                        if(data) {
                                                            setTimeout(function () {
                                                                self.props.rebaseData('cases', data);
                                                            }, 300)
                                                        }
                                                    })
                                                    .then(() => {
                                                        this.handleClick(23);
                                                        this.setState({caseId: this.props.activeCase.id})
                                                    })
                                            })
                                    }}>Save</button>
                                </Col>
                            </Row>
                        </div>
                    )
                    :
                    []
                }
                <div className="card-wrap">
                    <div className={this.state.isFront ? 'card-item' : 'card-item item-transform grey lighten-4'}>
                        <div className="front front-menu">
                            <LazyLoad offsetVertical={300}>
                                <img src={item.imageUrls[0]} alt={this.state.name} />
                            </LazyLoad>
                            <div className="item-buttons">
                                <IconFont id="add-to-card" name="folder" size="2x" border={true} className="icon-left" click={() => {
                                    this.handleClick(23);
                                    }}/>
                                <svg width="100" height="100" viewBox="0 0 100 100" onClick={() => {
                                    let self = this;
                                    itemsSearch(item.id)
                                        .then((data) => {
                                            this.props.rebaseData('funnelData', data)
                                        })
                                        .then(() => {
                                            getItems()
                                                .then(data => {
                                                    if(data) {
                                                        setTimeout(function () {
                                                            self.props.rebaseData('items', data);
                                                        }, 300)
                                                    }
                                                });
                                            getModels()
                                                .then(data => {
                                                    if(data) {
                                                        setTimeout(function () {
                                                            self.props.rebaseData('models', data);
                                                        }, 300)
                                                    }
                                                })
                                                .then(() => {
                                                    let scrollElement = document.querySelector('.infinite-scroll-component');
                                                    scrollElement.scrollTop = 0;
                                                })
                                        });
                                    if(this.props.funnelEvent(!this.props.funnelActive) === false) {
                                        return false;
                                    } else {
                                        this.props.funnelEvent(!this.props.funnelActive);
                                    }
                                }}>
                                    <path d="M33 40 l35 0 l0 2 l-15 14 l0 9 l-6 0 l0 -9 L33 42 Z" className="figure"/>
                                    <g className="shape">
                                        <circle cx="50" cy="50" r="38" fill="transparent"/>
                                        <line x1="0" y1="50" x2="11" y2="50"/>
                                        <line x1="89" y1="50" x2="100" y2="50"/>
                                        <line x1="50" y1="0" x2="50" y2="11"/>
                                        <line x1="50" y1="89" x2="50" y2="100"/>
                                    </g>
                                </svg>
                                <IconFont name="shopping-cart" size="2x" border={true} className="icon-right" click={() => window.open(item.link,'_blank')} />
                                <a className="item-slider" onClick={() => {
                                    this.handleClick(22);
                                }}><Icon small >zoom_in</Icon></a>
                            </div>
                        </div>
                        <div className="back">
                            <div className='yellow accent-4'>
                                <h2 className='grey-text text-darken-4'>{this.state.name}</h2>
                            </div>
                            <div className="info-wrap">
                                {Object.keys(item.info).map((key, ind) => {
                                    return (
                                        <div key={ind} className="back-info" onClick={() => {
                                            this.filter(key, item.info[key].map((value) => value));
                                        }}>
                                            <p className='grey-text text-lighten-1'>{key}</p>
                                            <div className="info-data">
                                                {item.info[key].map((value, id) => {
                                                    return (
                                                        <a key={id} className='grey-text text-darken-4'>{value}</a>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>

                <div className={this.state.isFront ? 'card-panel' : 'card-panel grey lighten-4'}>
                    <a onClick={this.toggle} className={this.state.isFront ? 'grey-text text-lighten-1 card-info flex-end' : 'grey-text text-darken-4 card-info flex-end'}><Icon>info_outline</Icon></a>

                    <div className="flex column">
                        <p className="right-align zero-margin">
                            {parseJwt().admin ? (
                                <a onClick={() => {
                                    this.itemEdit(item.id);
                                }}>
                                    <IconFont id="pencil" name="pencil" border={false} />
                                </a>
                            ) : null}
                            <a onClick={() => {
                                this.handleStarClick(item.id);
                                if(this.props.funnelActive == false) {
                                    this.props.funnelEvent(!this.props.funnelActive);
                                } else {
                                    return false;
                                }
                            }}>
                                <IconFont name={this.state.liked ? 'star yellow-text text-darken-3' : 'star-o grey-text text-lighten-1'} />
                            </a>
                        </p>
                        <p className={this.state.isFront ? 'panel-item grey-text text-lighten-1' : 'panel-item grey-text text-darken-4 card-info'}>
                            <span className='price grey-text text-darken-4'>3d</span>
                            <IconFont name="cube" />
                        </p>
                    </div>

                </div>
                {this.state.buttonStatus[22] ? (
                    <div className={this.state.buttonStatus[22] ? 'fancybox-slider active' : 'fancybox-slider hide'}>
                        <span className="behind" onClick={() => {this.handleClick(22)}}></span>
                        <OwlCarousel
                            ref="car"
                            options={options}
                        >
                            {this.state.buttonStatus[22] ? this.state.files.map((key, ind) => (
                                <div key={ind} className="item-image">
                                    <span className="behind" onClick={() => {this.handleClick(22)}}></span>
                                    <img src={key.file} key={ind} alt={this.state.name}/>
                                </div>)) : []}
                        </OwlCarousel>
                        {/*<a onClick={() => this.handleClick(22)}><IconFont name="times" size="2x" /></a>*/}
                    </div>
                ) : []}
            </Col>
        )
    }
}

export  default Model;
