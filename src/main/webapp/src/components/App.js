import React, { Component } from 'react';

import './App.css';
import Header from './Header/HeaderContainer';
import PanelbarContainer from  './Panelbar/PanelbarContainer';
import MainContainer from  './Main/MainContainer';
import Projects from  './Projects/ProjectsContainer';
import AdminPanelComponent from './Admin/AdminPanelComponent';
import UsersComponent from './Users/UsersComponent';
import UserContactsComponent from './UserContacts/UserContactsComponent';
import { parseJwt, getCases, focusCases, getItems, getModels, getFunnels, userContacts, usersAllList } from '../services/http';
import {
    BrowserRouter as Router,
    Route,
    Redirect,
} from 'react-router-dom'

class App extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const ele = document.getElementById('ipl-progress-indicator');
        if(ele){
            setTimeout(() => {
                ele.classList.add('available')
                setTimeout(() => {
                    ele.outerHTML = ''
                }, 2000)
            }, 1000)
        }
        let didLogin = this.props.didLogin;
        if(didLogin) {
            getCases()
                .then(data => {
                    if(data) {
                        this.props.rebaseData('cases', data);
                    }
                });
            focusCases()
                .then(data => {
                    if(data) {
                        this.props.rebaseData('activeCase', data);
                    }
                });
            getItems(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('items', data);
                    }
                });
            getModels(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('models', data);
                    }
                });
            getFunnels()
                .then(data => {
                    if(data) {
                        this.props.rebaseData('funnels', data);
                    }
                });
            userContacts(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUserContacts', data);
                    }
                });
            usersAllList(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUsersList', data);
                    }
                });
        }
    }
    loadNewData() {
        getCases()
            .then(data => {
                if(data) {
                    this.props.rebaseData('cases', data);
                }
            });
        focusCases()
            .then(data => {
                if(data) {
                    this.props.rebaseData('activeCase', data);
                }
            });
        getItems(0)
            .then(data => {
                if(data) {
                    this.props.rebaseData('items', data);
                }
            });
        getModels(0)
            .then(data => {
                if(data) {
                    this.props.rebaseData('models', data);
                }
            });
        getFunnels()
            .then(data => {
                if(data) {
                    this.props.rebaseData('funnels', data);
                }
            });
        userContacts(0)
            .then(data => {
                if(data) {
                    this.props.rebaseData('commandUserContacts', data);
                }
            });
        usersAllList(0)
            .then(data => {
                if(data) {
                    this.props.rebaseData('commandUsersList', data);
                }
            })
            .then(() => {
                if (localStorage.getItem("X-Auth-Token") && sessionStorage.getItem("reduxPersist:app")) {
                    setTimeout(function () {
                        console.clear();
                        console.log('Hello ' + parseJwt().username)
                    }, 300);
                } else {
                    console.clear();
                    // window.location.assign('');
                }
            })
    }
    render() {
        let didLogin = this.props.didLogin;
        let AdminTools;
        let self = this;
        if(sessionStorage.getItem("reduxPersist:app") === null && localStorage.getItem("X-Auth-Token")) {
            setTimeout(function () {
                self.loadNewData();
            }, 300);
        } else if(localStorage.getItem("X-Auth-Token") === null && sessionStorage.getItem("reduxPersist:app")) {
            console.clear();
            sessionStorage.clear();
            window.location.assign("/main")
        } else if (localStorage.getItem("X-Auth-Token") === null && sessionStorage.getItem("reduxPersist:app") === null) {
            setTimeout(function () {
                console.clear();
            }, 2000);
        } else if (localStorage.getItem("X-Auth-Token") && sessionStorage.getItem("reduxPersist:app")) {
            setTimeout(function () {
                console.clear();
                console.log('Hello ' + parseJwt().username)
            }, 2000);
        } else {
            console.log('Something went wrong')
        }

        if(didLogin) {
            AdminTools = parseJwt()
        } else {
            AdminTools = false
        }
        const Admin = () => (
            <div>
                <AdminPanelComponent />
            </div>
        );
        const Landing = () => (
            <div>
                <Header didLogin={didLogin} rebaseCallback={this.props.rebaseData} />
            </div>
        );
        const Users = () => (
            <div>
                <Header didLogin={didLogin} rebaseCallback={this.props.rebaseData} />
                {didLogin ? <UsersComponent />: ''}
            </div>
        );
        const UserContacts = () => (
            <div>
                <Header didLogin={didLogin} rebaseCallback={this.props.rebaseData} />
                {didLogin ? <UserContactsComponent />: ''}
            </div>
        );
        const Home = () => (
            <div>
                <Header didLogin={didLogin} rebaseCallback={this.props.rebaseData} />
                {didLogin ? <PanelbarContainer />: ''}
                {didLogin ? <MainContainer /> : ''}
                {didLogin ? <Projects />: ''}
            </div>
        );
        const SiteRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={props => (
                didLogin ? (
                    <Component {...props}/>
                ) : (
                    <Redirect to={{
                        pathname: '/main',
                        state: { from: props.location }
                    }}/>
                )
            )}/>
        );
        const UsersRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={props => (
                didLogin ? (
                    <Component {...props}/>
                ) : (
                    <Redirect to={{
                        pathname: '/main',
                        state: { from: props.location }
                    }}/>
                )
            )}/>
        );
        const AdminRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={props => (
                didLogin && AdminTools.admin === true && AdminTools.role.map((key) => { key === 'ADMIN'  }) ? (
                    <Component {...props}/>
                ) : (
                    <Redirect to={{
                        pathname: '/',
                        state: { from: props.location }
                    }}/>
                )
            )}/>
        );
        return (
            <div>
                <Router>
                    <div>
                        <SiteRoute exact path="/" component={Home}/>
                        <Route path="/main" component={Landing}/>
                        <AdminRoute path="/admin.tools" component={Admin}/>
                        <UsersRoute path="/users" component={Users}/>
                        <UsersRoute path="/mycontacts" component={UserContacts}/>
                    </div>
                </Router>
            </div>
        );
    }
    componentDidUpdate(){
        let didLogin = this.props.didLogin;
        if(didLogin) {
            getCases()
                .then(data => {
                    if(data) {
                        this.props.rebaseData('cases', data);
                    }
                });
            focusCases()
                .then(data => {
                    if(data) {
                        this.props.rebaseData('activeCase', data);
                    }
                });
            getItems(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('items', data);
                    }
                });
            getModels(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('models', data);
                    }
                });
            getFunnels()
                .then(data => {
                    if(data) {
                        this.props.rebaseData('funnels', data);
                    }
                });
            userContacts(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUserContacts', data);
                    }
                });
            usersAllList(0)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUsersList', data);
                    }
                });
        }
    }
}

export default App;
