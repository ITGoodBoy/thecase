import {connect} from 'react-redux';
import {toggleSideBarFunc, rebaseDataFunc} from '../actions/index';

import App from './App';

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        didLogin: !!state.app.token,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSideBar (property) {
            toggleSideBarFunc(property)(dispatch)
        },
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        }
    }
};

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

export default AppContainer;