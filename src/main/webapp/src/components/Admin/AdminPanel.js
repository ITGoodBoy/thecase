import React, {Component} from "react";
import './AdminPanel.css';
import {NavItem, Button, Dropdown, SideNav, SideNavItem, Icon, Navbar} from 'react-materialize';

import IconFont from "../IconFont/IconFont"
import NavbarCustom from '../NavbarCustom/NavbarCustom';
import NavItemCustom from '../NavbarCustom/NavItemCustom'
import AdminItems from './AdminItems/AdminItems'
import AdminUsersComponent from './AdminUsers/AdminUsersComponent'
import AdminTagsComponent from './AdminTags/AdminTagsComponent'
import { getUsers, getSortedTags, Logout, parseJwt } from '../../services/http';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'

class AdminPanel extends Component {
    constructor(props) {
        super(props);
        getUsers()
            .then(data => {
                if(data) {
                    this.props.rebaseData('users', data);
                }
            });
        getSortedTags()
            .then(data => {
                if(data) {
                    this.props.rebaseData('sortedTags', data);
                }
            });
    }
    render() {
        const AdminNav = () => (
            <Router>
                <div>
                    <NavbarCustom fixed className="white">
                        <NavItemCustom className="admin-nav-item black-text">
                            <h4 className="admin-title">Admin Panel</h4>
                        </NavItemCustom>

                        <NavItemCustom className="admin-nav-item">
                            <MenuLink activeOnlyWhenExact={true} to="/admin.tools" label="Dashboard"/>
                        </NavItemCustom>

                        <NavItemCustom className="admin-nav-item">
                            <MenuLink to="/admin.tools/items" label="Items"/>
                        </NavItemCustom>

                        <NavItemCustom className="admin-nav-item">
                            <MenuLink to="/admin.tools/users" label="Users"/>
                        </NavItemCustom>
                        <NavItemCustom className="admin-nav-item">
                            <MenuLink to="/admin.tools/tags" label="Tags"/>
                        </NavItemCustom>
                        <NavItemCustom className="admin-nav-item valign-wrapper right">
                            <ul><NavItem href='#' className='user valing-wrapper'>HI, {parseJwt().username}</NavItem></ul>
                            <SideNav
                                trigger={<Button floating large className='btn-flat' icon='sort'></Button>}
                                options={{
                                    closeOnClick: true,
                                    edge: 'right',
                                    draggable: true,
                                }}
                            >
                                <SideNavItem userView
                                             user={{
                                                 background: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfWiLKw38MHb26vmUPn_scmOYMU0djbibmSzNouxISQiHq-lPnSg',
                                                 name: 'Hi, ' + parseJwt().username,
                                             }}
                                />
                                <SideNavItem waves href='/' icon="tab_unselected">The Case</SideNavItem>
                                <SideNavItem waves href='/main' icon="widgets">Landing</SideNavItem>
                                <SideNavItem waves href='/main' onClick={() => Logout()} icon="exit_to_app">Log Out</SideNavItem>
                            </SideNav>
                        </NavItemCustom>
                    </NavbarCustom>
                    <Route exact path="/admin.tools" component={Dashboard}/>
                    <Route path="/admin.tools/items" component={Items}/>
                    <Route path="/admin.tools/users" component={Users}/>
                    <Route path="/admin.tools/tags" component={Tags}/>
                </div>
            </Router>
        );

        const MenuLink = ({ label, to, activeOnlyWhenExact }) => (
            <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
                <div className={match ? 'active' : ''}>
                    {match ? '' : ''}<Link to={to}>{label}</Link>
                </div>
            )}/>
        );
        const Dashboard = () => (
            <div>
                <h2>Dashboard</h2>
            </div>
        );
        const Items = () => (
            <AdminItems />
        );

        const Users = () => (
            <div>
                <AdminUsersComponent />
            </div>
        );

        const Tags = () => (
            <div>
                <AdminTagsComponent />
            </div>
        );

        return (
            <div>
                <AdminNav />
            </div>
        )
    }
}

export default AdminPanel
