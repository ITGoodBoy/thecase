import React, {Component} from "react";
import {Row, Col, Collapsible, CollapsibleItem, Input} from 'react-materialize'
import './AdminUsers.css';
import IconFont from '../../IconFont/IconFont'
import { getUsers, blockUser, unblockUser, removeUser, updateUserRole, userRole } from '../../../services/http';

class AdminUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            changeRole: 2,
        };
        this.changeRole = this.changeRole.bind(this);
        this.adminRole = this.adminRole.bind(this);
        this.userRole = this.userRole.bind(this);
    }

    changeRole(event) {
        this.setState({changeRole: event.target.value})
    }

    adminRole(id) {
        updateUserRole(id, true);
    };
    userRole(id) {
        updateUserRole(id, false);
    };

    render() {
        let h = window.innerHeight - 80;
        return (
            <Row className='users-container' style={{'height': + h}}>
                {this.props.users.map((key, id) => {
                    return (
                        <Col m={12} l={6} key={id}>
                            <Collapsible defaultActiveKey={0}>
                                <CollapsibleItem header={key.username} icon='perm_identity'>
                                    <Row>
                                        <Col s={4} className='user-block'>
                                            <h3>User Info:</h3>
                                            <p>ID: {key.id}</p>
                                            <p>Email: <a href={'mailto:'+key.email}>{key.email}</a></p>
                                        </Col>
                                        <Col s={4} className='user-block user-roles'>
                                            <h3>User Role:</h3>
                                            <Input s={12} type='select' defaultValue={key.admin ? '1' : '2'} onChange={(event) => {
                                                this.changeRole(event);
                                                if(this.state.changeRole == 1) {
                                                    this.adminRole(key.id)
                                                } else if(this.state.changeRole == 2) {
                                                    this.userRole(key.id)
                                                }
                                            }}>
                                                <option value='1'>Admin</option>
                                                <option value='2'>Member</option>
                                            </Input>
                                        </Col>
                                        <Col s={4} className='user-block'>
                                            <h3>User Control:</h3>
                                            <div className="user-control">
                                                <IconFont size="2x" name={key.accountNonLocked ? 'unlock' : 'unlock-alt' } click={() => {
                                                    key.accountNonLocked ?
                                                    blockUser(key.id)
                                                        .then(() => {
                                                            getUsers()
                                                                .then((data) => {
                                                                    if(data) {
                                                                        this.props.rebaseData('users', data)
                                                                    }
                                                                })
                                                        })
                                                        :
                                                        unblockUser(key.id)
                                                            .then(() => {
                                                                getUsers()
                                                                    .then((data) => {
                                                                        if(data) {
                                                                            this.props.rebaseData('users', data)
                                                                        }
                                                                    })
                                                            })
                                                }} />
                                                <IconFont size="2x" name='trash-o' click={() => {
                                                    removeUser(key.id)
                                                        .then(() => {
                                                            getUsers()
                                                                .then((data) => {
                                                                    if(data) {
                                                                        this.props.rebaseData('users', data)
                                                                    }
                                                                })
                                                        })
                                                }}/>
                                            </div>
                                        </Col>
                                    </Row>
                                </CollapsibleItem>
                            </Collapsible>
                        </Col>
                    )
                })}
            </Row>
        )
    }
}

export default AdminUsers
