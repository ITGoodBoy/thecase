import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../../actions/index';

import AdminUsers from './AdminUsers';

const mapStateToProps = ({app: {users}}, ownProps) => {
    return {
        users,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const AdminUsersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminUsers);

export default AdminUsersContainer;