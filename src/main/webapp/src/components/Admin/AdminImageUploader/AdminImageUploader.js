import React, {Component} from "react";
import Dropzone from 'react-dropzone'
import { Row, Col } from 'react-materialize'

import  './AdminImageUploader.css'
import IconFont from '../../IconFont/IconFont';

class AdminImageUploader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            blobs: [],
        };
        this.removeImage = this.removeImage.bind(this);
        this.newDrop = this.newDrop.bind(this);
    }

    onDrop(files) {
        this.props.files(files);
        let filesArray = this.state.files.concat(files);
        this.setState({
            files: filesArray
        });
        this.newDrop(files);
    }

    newDrop(files) {
        const promise = new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.readAsDataURL(files[0]);

            reader.onload = () => {
                if (!!reader.result) {
                    resolve(reader.result)
                }
                else {
                    reject(Error("Failed converting to base64"))
                }
            }
        });
        promise.then(result => {
            let blobs = this.state.blobs.concat(result);
            // console.log('blobs', blobs);
            this.props.image(blobs);
            this.setState({
                blobs
            });
        }, err => {
            console.log(err)
        });

        // let blobs = this.state.blobs.concat(files.map((file) => file.preview));
        // console.log(blobs);
        // this.props.image(blobs);
        // this.setState({
        //     blobs: blobs
        // });
    }

    removeImage(event, ind) {
        event.preventDefault();
        let newArray = this.state.files;
        let blobs = this.state.blobs.splice(ind, 1);
        this.props.image(blobs);
        newArray.splice(ind, 1);
        this.props.image(newArray);
        this.setState({
            files: newArray,
            blobs: blobs
        });
    }
    componentWillReceiveProps() {
        this.setState({files: this.props.updFiles})
    }

    render() {
        // console.log(this.props.updFiles)
        // const fileInputKey = input.value ? input.value.name : +new Date();
        return (
            <div className="dropzone">
                <Dropzone onDrop={this.onDrop.bind(this)}
                          accept="image/*"
                          ref="fileUpload"
                >
                    Upload your images
                </Dropzone>
                <Row>
                    {this.props.updFiles.map((file, ind) =>
                        <Col key={ind} l={3} className="top-indent relative">
                            <img src={file.preview} className="uploaded-image"  />
                            <div className="center-align">{file.name} - {file.size} bytes</div>
                            <a href="#" onClick={(event) => {this.removeImage(event, ind)}}>
                                <IconFont name="times-circle" size="2x"/>
                            </a>
                        </Col>
                    )}
                </Row>

            </div>
        );
    }
}

export default AdminImageUploader
