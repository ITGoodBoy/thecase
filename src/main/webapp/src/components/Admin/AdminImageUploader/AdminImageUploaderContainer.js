import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../../actions/index';

import AdminImageUploader from './AdminImageUploader';

const mapStateToProps = ({app: {imgs}}, ownProps) => {
    return {
        imgs,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const AdminImageUploaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminImageUploader);

export default AdminImageUploaderContainer;