import React, {Component} from "react";

import FormAdminContainer from '../FormAdmin/FormAdminContainer'
import './AdminItemsLeftSidebar.css'

class AdminItemsLeftSidebar extends Component {
    render() {
        return(
            <div className="overflow-y left-sidebar">
                <FormAdminContainer />
            </div>
        )
    }
}

export default AdminItemsLeftSidebar
