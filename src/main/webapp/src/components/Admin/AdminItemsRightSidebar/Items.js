import React, {Component} from "react";
import {Row, Col, Input, Chip} from 'react-materialize'
import './AdminItemsRightSidebar.css';
import IconFont from '../../IconFont/IconFont';
import LazyLoad from 'react-lazy-load';

class Items extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonStatus: [],
        };
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];
        this.setState({
            buttonStatus: newStatus,
        });
    }
    render() {
        let item = this.props.item;
        return (
            <Row className='item-grid'>
                <Col s={3}>
                    <LazyLoad offsetVertical={300}>
                        <img className="item-image" src={item.imageUrls[0]} alt="item"/>
                    </LazyLoad>
                </Col>
                <Col s={7}>
                    <div>
                        <h2>{item.name}</h2>
                    </div>
                    <div className='item-description'>
                        {Object.keys(item.info).map((key, ind) => {
                            return (
                                <div key={ind} className="back-info">
                                    <span className="item-chip-name">{key}:</span>
                                    {item.info[key].map((value, id) => {
                                        return (
                                            <Chip key={id}>
                                                {value}
                                            </Chip>
                                        )
                                    })}
                                </div>
                            )
                        })}
                    </div>
                    <div className='item-description'>
                        <span className="item-chip-name">Tags:</span>
                        {item.tagsList.map((key, id) => {
                            return (
                                <Chip key={id}>
                                    {key}
                                </Chip>
                            )
                        })}
                    </div>
                </Col>
                <Col s={2} className='item-navigation'>
                    <IconFont id="pencil" name="pencil" size="2x" border={false} />
                    <IconFont id="trash" name="trash" size="2x" border={false} />
                </Col>
            </Row>
        )
    }
}

export default Items
