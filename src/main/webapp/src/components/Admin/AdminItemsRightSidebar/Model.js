import React, {Component} from "react";
import {Row, Col, Input, Chip, Icon, Collapsible, CollapsibleItem, Button} from 'react-materialize'
import { WithContext as ReactTags } from 'react-tag-input';
import './AdminItemsRightSidebar.css';
import IconFont from '../../IconFont/IconFont';
import LazyLoad from 'react-lazy-load';
import Dropzone from 'react-dropzone'

import { getTags, getBrands, editItems, getItems, getModels, blockWord } from '../../../services/http'

class Model extends Component {
    constructor(props) {
        super(props);
        let tags;
        if(this.props.item.tagsList !== null) {
            tags = this.props.item.tagsList.map((item, index) => {return {id: index, text: item};})
        } else {
            tags = [];
        }
        let image = this.props.item.imageUrls.map((image, index) => {return {id: index, file: image}});
        let isOpen = [];
        this.props.sortedTags.forEach(function (i) {
            isOpen.push(i)
        });
        this.state = {
            tagsView: this.props.getTags,
            isOpen: isOpen,
            buttonStatus: [],
            selectedOption: 'model3d',
            tags: tags,
            imgs: [],
            files: image,
            blobs: [],
            updFiles: [],
            name: this.props.item.name,
            published: this.props.item.published,
            category: this.props.item.info.Category.toString(),
            type: this.props.item.info.Type.toString(),
            brand: this.props.item.info.Brand.toString(),
            brandsView: this.props.getBrands,
            sourceLink: this.props.item.link || '',
            selectedTags: this.props.item.tagsList,
            priceAmount: this.props.item.price.amount || '',
            priceCurrency: this.props.item.price.currency || '',
        };
        this.toggle = this.toggle.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.updateTags = this.updateTags.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.removeImage = this.removeImage.bind(this);
        this.removeNewImage = this.removeNewImage.bind(this);
        this.newDrop = this.newDrop.bind(this);
        this.updateBrands = this.updateBrands.bind(this);
        this.brandDelete = this.brandDelete.bind(this);
        this.brandAddition = this.brandAddition.bind(this);
        this.editItem = this.editItem.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.targetSortElement = this.targetSortElement.bind(this);
    }
    blockWord(word) {
        blockWord(word)
    }
    toggle(id) {
        const newStatus = this.state.isOpen.slice();
        newStatus[id] = !this.state.isOpen[id];
        this.setState({
            isOpen: newStatus,
        });
    }
    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];
        this.setState({
            buttonStatus: newStatus,
        });
    }
    updateTags(tag)  {
        if(tag == '') {
            getTags('A')
        } else {
            getTags(tag)
                .then(tagsView => {
                    this.setState({tagsView})
                });
        }
    }

    handleDelete(i) {
        let tags = this.state.tags;
        let selectedTags = this.state.selectedTags;
        tags.splice(i, 1);
        selectedTags.splice(i, 1);
        this.setState({
            tags,
            selectedTags
        });
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        let selectedTags = this.state.selectedTags.concat(tag);
        tags.push({
            id: tags.length + 1,
            text: tag
        });
        this.setState({
            tags,
            selectedTags
        });
    }

    handleOptionChange(changeEvent) {
        this.setState({
            selectedOption: changeEvent.target.value
        })
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    };

    updateBrands(tag)  {
        if(tag == '') {
            getBrands('A')
        } else {
            getBrands(tag)
                .then(brandsView => {
                    this.setState({brandsView})
                });
        }
    }

    brandDelete(i) {
        let brands = this.state.brands;
        brands.splice(i, 1);
        this.setState({
            brands
        });
    }

    brandAddition(brand) {
        let brands = [];
        brands.push({
            id: brand.length + 1,
            text: brand
        });
        this.setState({
            brands,
            brand: brand,
        })
    }
    onDrop(files) {
        let filesArray = this.state.imgs.concat(files);
        this.setState({
            imgs: filesArray
        });
        this.newDrop(files);
    }

    newDrop(files) {
        const promise = new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.readAsDataURL(files[0]);

            reader.onload = () => {
                if (!!reader.result) {
                    resolve(reader.result)
                }
                else {
                    reject(Error("Failed converting to base64"))
                }
            }
        });
        promise.then(result => {
            let blobs = this.state.blobs.concat(result);
            this.setState({
                blobs,
            });
        }, err => {
            console.log(err)
        });
    }

    removeImage(event, ind) {
        event.preventDefault();
        let newArray = this.state.files;
        newArray.splice(ind, 1);
        this.setState({
            files: newArray
        });
    }
    removeNewImage(event, ind) {
        event.preventDefault();
        let newArray = this.state.imgs;
        let blob = this.state.blobs;
        newArray.splice(ind, 1);
        blob.splice(ind, 1);
        this.setState({
            imgs: newArray,
            blobs: blob
        });
    }
    targetSortElement(e) {
        const newSelection = e.target.value;
        let newSelectionArray;
        if(this.state.selectedTags.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.selectedTags.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.selectedTags, newSelection];
        }
        this.setState({ selectedTags: newSelectionArray });
    }

    editItem(id) {
        let selectedOption = this.state.selectedOption;
        let name = this.state.name;
        let oldImage = this.state.files.map((image) => image.file);
        let newImage = this.state.blobs;
        let published = this.state.published;
        let category = this.state.category;
        let type = this.state.type;
        let sourceLink = this.state.sourceLink;
        let selectedTags = this.state.selectedTags;
        let brand = this.state.brand;
        let allPrice;
        if(!this.state.priceAmount.length && !this.state.priceCurrency.length) {
            allPrice = null;
        } else {
            allPrice = {
                amount: this.state.priceAmount,
                currency: this.state.priceCurrency,
            };
        }
        editItems(id, selectedOption, name, oldImage, newImage, published, category, type, brand, sourceLink, allPrice, selectedTags)
            .then(() => {
                this.props.updateItem();
            })
            .then(() => {
                this.handleClick(id);
            })
    }

    cancelEdit(id) {
        let tags;
        if(this.props.item.tagsList !== null) {
            tags = this.props.item.tagsList.map((item, index) => {return {id: index, text: item};})
        } else {
            tags = [];
        }
        let image = this.props.item.imageUrls.map((image, index) => {return {id: index, file: image}});
        let isOpen = [];
        this.props.sortedTags.forEach(function (i) {
            isOpen.push(i)
        });
        this.setState({
            tagsView: this.props.getTags,
            isOpen: isOpen,
            buttonStatus: [],
            selectedOption: 'product',
            tags: tags,
            imgs: [],
            files: image,
            blobs: [],
            updFiles: [],
            name: this.props.item.name,
            published: this.props.item.published,
            category: this.props.item.info.Category.toString(),
            type: this.props.item.info.Type.toString(),
            brand: this.props.item.info.Brand.toString(),
            brandsView: this.props.getBrands,
            sourceLink: this.props.item.link || '',
            selectedTags: this.props.item.tagsList,
            priceAmount: this.props.item.price.amount || '',
            priceCurrency: this.props.item.price.currency || '',
        });
        this.handleClick(id);
    }

    componentWillReceiveProps() {
        let tags;
        if(this.props.item.tagsList !== null) {
            tags = this.props.item.tagsList.map((item, index) => {return {id: index, text: item};})
        } else {
            tags = [];
        }
        let image = this.props.item.imageUrls.map((image, index) => {return {id: index, file: image}});
        let isOpen = [];
        this.props.sortedTags.forEach(function (i) {
            isOpen.push(i)
        });
        this.setState({
            tagsView: this.props.getTags,
            isOpen: isOpen,
            buttonStatus: [],
            selectedOption: 'model3d',
            tags: tags,
            imgs: [],
            files: image,
            blobs: [],
            updFiles: [],
            name: this.props.item.name,
            published: this.props.item.published,
            category: this.props.item.info.Category.toString(),
            type: this.props.item.info.Type.toString(),
            brand: this.props.item.info.Brand.toString(),
            brandsView: this.props.getBrands,
            sourceLink: this.props.item.link || '',
            selectedTags: this.props.item.tagsList,
            priceAmount: this.props.item.price.amount || '',
            priceCurrency: this.props.item.price.currency || '',
        });
    }
    render() {
        let item = this.props.item;
        let editForm;
        if(this.state.buttonStatus[item.id] === true) {
            editForm = (
                <Row key={item.id} className="item-edit-form">
                    <a className="close-edit-form" onClick={(event) => {event.preventDefault(); this.handleClick(item.id)}}><Icon>close</Icon></a>
                    <Col s={12}>
                        <Input s={12} type='text' defaultValue={this.state.name} label='Item Name' onChange={(event) => this.setState({name: event.target.value})} />
                    </Col>
                    <Col s={12}>
                        <Input s={4} name='item' type='radio' value='model3d'  label='Model' checked={this.state.selectedOption === 'model3d'} onChange={this.handleOptionChange} className='with-gap' />
                        <Input s={4} name='item' type='radio' value='product' label='Product' checked={this.state.selectedOption === 'product'} onChange={this.handleOptionChange} className='with-gap' />
                        <Input s={4} name='published' type='checkbox' label='Published' className='filled-in' checked={this.state.published} onChange={this.handleChange} />
                    </Col>
                    <Col s={12}>
                        <Input s={6}
                               type='select'
                               label="Category"
                               value={this.state.category}
                               onChange={this.handleChange}
                               name="category"
                        >
                            {this.props.categories.map((key, id) => {
                                return (
                                    <option key={id} value={key}>{key}</option>
                                )
                            })}
                        </Input>
                        <Input s={6}
                               type='select'
                               label="Type"
                               value={this.state.type}
                               onChange={this.handleChange}
                               name="type"
                        >
                            {this.props.types.map((key, id) => {
                                return (
                                    <option key={id} value={key}>{`${key.charAt(0).toUpperCase()}${key.substr(1).toLowerCase()}`}</option>
                                )
                            })}
                        </Input>
                    </Col>
                    <Col s={12}>
                        {this.state.brand.length ? (
                            <Col s={12}>
                                <Chip>{this.state.brand}</Chip>
                            </Col>
                        ) : []}
                        <ReactTags
                            suggestions={
                                this.state.brandsView.map(
                                    (brand) => brand
                                )
                            }
                            placeholder="Add new brand"
                            autofocus={false}
                            handleDelete={this.brandDelete}
                            handleAddition={this.brandAddition}
                            handleInputChange={this.updateBrands}
                            classNames={{
                                tag: 'chip',
                                remove: 'left-padding',
                                suggestions: 'suggestions-container'
                            }}
                        />
                    </Col>
                    <Col s={12}>
                        <Input s={12}
                               label="Source link"
                               name="sourceLink"
                               value={this.state.sourceLink}
                               onChange={this.handleChange}
                        />
                    </Col>
                    <Col s={12} className='edit-tags'>
                        <ReactTags tags={this.state.tags}
                                   suggestions={
                                       this.state.tagsView.map(
                                           (tag) => tag.name
                                       )
                                   }
                                   autofocus={false}
                                   handleDelete={this.handleDelete}
                                   handleAddition={this.handleAddition}
                                   handleInputChange={this.updateTags}
                                   classNames={{
                                       tag: 'chip',
                                       remove: 'left-padding',
                                       suggestions: 'suggestions-container'
                                   }}
                        />
                    </Col>
                    <Col s={12}>
                        {this.props.sortedTags.map((key, id) => {
                            return (
                                <Row className="top-padding" key={id}>
                                    <Collapsible>
                                        <CollapsibleItem
                                            header={key.name}
                                            onClick={() => this.toggle(id)}
                                            icon={this.state.isOpen[id] ? 'keyboard_arrow_right' : 'keyboard_arrow_down'}
                                            iconClassName="right"
                                        >

                                            <Row>
                                                {key.tags.map((tag, ind) => {
                                                    let check;
                                                    if(this.state.selectedTags.indexOf(tag.name) > -1 == true) {
                                                        check = 'checked';
                                                    } else {
                                                        check = false;
                                                    }
                                                    return (
                                                        <Input s={4}
                                                               type='checkbox'
                                                               className='filled-in'
                                                               key={ind}
                                                               name={key.name}
                                                               value={tag.name}
                                                               label={`${tag.name} ${tag.frequency}`}
                                                               checked={check}
                                                               onChange={(event) => {this.targetSortElement(event)}}
                                                        />
                                                    )
                                                })}
                                            </Row>
                                        </CollapsibleItem>
                                    </Collapsible>
                                </Row>
                            )
                        })}
                    </Col>
                    <Col s={12}>
                        <Input
                            s={8}
                            type="number"
                            label="Price"
                            name="amount"
                            defaultValue={this.state.priceAmount.toString()}
                            onChange={(event) => this.setState({
                                priceAmount: event.target.value,
                            })}
                        />

                        <Input s={4}
                               type='select'
                               label="Currency"
                               defaultValue={this.state.priceCurrency.toString()}
                               onChange={(event) => this.setState({
                                   priceCurrency: event.target.value,
                               })}
                               name="currency"
                        >
                            <option value=''>Chose Currency</option>
                            <option value='USD'>USD</option>
                            <option value='EUR'>EUR</option>
                            <option value='RUB'>RUB</option>
                        </Input>
                    </Col>
                    <Col s={12}>
                        <div className="dropzone">
                            <Dropzone onDrop={this.onDrop.bind(this)}
                                      accept="image/*"
                                      ref="fileUpload"
                            >
                                Upload your images
                            </Dropzone>
                            {this.state.files.map((file, ind) => {
                                return (
                                    <div key={ind} className="top-indent relative edit-image">
                                        <img src={file.file} className="uploaded-image"  />
                                        <a href="#" onClick={(event) => {this.removeImage(event, ind)}}>
                                            <IconFont name="times-circle" size="2x"/>
                                        </a>
                                    </div>
                                )
                            }
                            )}
                            {this.state.imgs.map((file, ind) =>
                                <div key={ind} className="top-indent relative edit-image">
                                    <img src={file.preview} className="uploaded-image" />
                                    <a href="#" onClick={(event) => {this.removeNewImage(event, ind)}}>
                                        <IconFont name="times-circle" size="2x"/>
                                    </a>
                                </div>
                            )}
                        </div>
                    </Col>
                    <Col s={2} className='right'>
                        <Button waves='light' className="waves-effect waves-teal btn-flat edit-button-form save-edit-form"  onClick={() => {
                            this.editItem(item.id);
                        }} >Edit</Button>
                        <Button waves='light' className="waves-effect waves-teal btn-flat edit-button-form cancel-edit-form"  onClick={() => {
                            this.cancelEdit(item.id);
                        }} >Cancel</Button>
                    </Col>
                </Row>
            )
        } else {
            editForm = false;
        }
        let tagsList;
        if(item.tagsList !== null) {
            tagsList = (
                item.tagsList.map((key, id) => {
                    return (
                        <Chip key={id}>
                            {key}
                            <IconFont name="remove" size="1x" click={() => {this.blockWord(key)}}/>
                        </Chip>
                    )
                })
            )
        } else {
            tagsList = '';
        }
        return (
            <Row className='item-grid'>
                {editForm}
                <Col s={3}>
                    <LazyLoad offsetVertical={300}>
                        <img className="item-image" src={item.imageUrls[0]} alt="item"/>
                    </LazyLoad>
                </Col>
                <Col s={7}>
                    <div>
                        <h2>{item.name}</h2>
                    </div>
                    <div className='item-description'>
                        {Object.keys(item.info).map((key, ind) => {
                            return (
                                <div key={ind} className="back-info">
                                    <span className="item-chip-name">{key}:</span>
                                    {item.info[key].map((value, id) => {
                                        return (
                                            <Chip key={id}>
                                                {value}
                                            </Chip>
                                        )
                                    })}
                                </div>
                            )
                        })}
                    </div>
                    <div className='item-description'>
                        <span className="item-chip-name">Tags:</span>
                        {tagsList}
                    </div>
                </Col>
                <Col s={2} className='item-navigation'>
                    <IconFont id="pencil" name="pencil" size="2x" border={false} click={() => {
                        this.handleClick(item.id);
                    }} />
                    <IconFont id="trash" name="trash" size="2x" border={false} />
                </Col>
            </Row>
        )
    }
}

export default Model
