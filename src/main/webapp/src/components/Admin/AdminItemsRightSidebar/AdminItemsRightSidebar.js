import React, {Component} from "react";
import {Row, Input} from 'react-materialize';
import InfiniteScroll from 'react-infinite-scroll-component';
import DebounceInput from 'react-debounce-input';
import { getItems, getModels, clearWords } from '../../../services/http';
import Product from './Product';
import Model from './Model';
import IconFont from '../../IconFont/IconFont';

class AdminItemsRightSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            filter: 'Products',
            selectedOption: 'checked',
            items: [],
            models: [],
            itemCount: 1,
            modelCount: 1,
        };
        this.clearWords = this.clearWords.bind(this);
        this.updateItem = this.updateItem.bind(this);
        this.updateSearch = this.updateSearch.bind(this);
        this.onFilterChanged = this.onFilterChanged.bind(this);
        this.generateItems = this.generateItems.bind(this);
        this.generateModels = this.generateModels.bind(this);
    }
    clearWords() {
        clearWords()
            .then(() => {
                getItems(0)
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('items', data);
                        }
                    });
                getModels(0)
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('models', data)
                        }
                    })
                    .then(() => {
                        let scrollElement = document.querySelector('.infinite-scroll-component');
                        scrollElement.scrollTop = 0;
                    })
                    .then(() => {
                        this.setState({
                            items: this.props.items.items.map((key) => key.item),
                            models: this.props.models.items.map((key) => key.item),
                        })
                    })
            })
    }
    componentDidMount() {
        getItems(0)
            .then((data) => {
                this.props.rebaseData('items', data);
            });
        getModels(0, this.state.search)
            .then((data) => {
                this.props.rebaseData('models', data);
            })
            .then(() => {
                this.setState({
                    items: this.props.items.items.map((key) => key.item),
                    models: this.props.models.items.map((key) => key.item),
                })
            });
    }
    updateItem() {
        if(this.state.search.length && this.state.filter === 'Products') {
            getItems(0, this.state.search)
                .then((data) => {
                    this.props.rebaseData('items', data);
                })
                .then(() => {
                    this.setState({
                        items: this.props.items.items.map((key) => key.item),
                        itemCount: 1,
                    })
                })
        } else if(this.state.filter === 'Products') {
            getItems(0)
                .then((data) => {
                    this.props.rebaseData('items', data);
                })
                .then(() => {
                    this.setState({
                        items: this.props.items.items.map((key) => key.item),
                        itemCount: 1,
                    })
                })
        } else if(this.state.search.length && this.state.filter === 'Models') {
            getModels(0, this.state.search)
                .then((data) => {
                    this.props.rebaseData('models', data);
                })
                .then(() => {
                    this.setState({
                        models: this.props.models.items.map((key) => key.item),
                        modelCount: 1,
                    })
                })
        } else if(this.state.filter === 'Models') {
            getModels(0)
                .then((data) => {
                    this.props.rebaseData('models', data);
                })
                .then(() => {
                    this.setState({
                        models: this.props.models.items.map((key) => key.item),
                        modelCount: 1,
                    })
                })
        } else {
            return false;
        }
    }
    updateSearch() {
        let self = this;
        setTimeout(function () {
            if(self.state.filter === 'Products') {
                getItems(0, self.state.search)
                    .then((data) => {
                        self.props.rebaseData('items', data);
                    })
                    .then(() => {
                        self.setState({
                            items: self.props.items.items.map((key) => key.item),
                            itemCount: 1,
                        })
                    })
                    .then(() => {
                        let scrollElement = document.querySelector('.infinite-scroll-component');
                        scrollElement.scrollTop = 0;
                    })
                    .then(() => {
                        getModels(0, self.state.search)
                            .then((data) => {
                                self.props.rebaseData('models', data);
                                self.setState({
                                    models: data.items.map((key) => key.item),
                                    modelCount: 1,
                                })
                            })
                    })
            }
            if(self.state.filter === 'Models') {
                getModels(0, self.state.search)
                    .then((data) => {
                        self.props.rebaseData('models', data);
                    })
                    .then(() => {
                        self.setState({
                            models: self.props.models.items.map((key) => key.item),
                            modelCount: 1,
                        });
                    })
                    .then(() => {
                        let scrollElement = document.querySelector('.infinite-scroll-component');
                        scrollElement.scrollTop = 0;
                    })
                    .then(() => {
                        getItems(0, self.state.search)
                            .then((data) => {
                                self.props.rebaseData('items', data);
                                self.setState({
                                    items: data.items.map((key) => key.item),
                                    itemCount: 1,
                                })
                            })
                    })
            }
        }, 400);
    }
    onFilterChanged(event) {
        if(this.state.filter === 'Models') {
            let scrollElement = document.querySelector('.infinite-scroll-component');
            scrollElement.scrollTop = 0;
        } else if(this.state.filter === 'Products') {
            let scrollElement = document.querySelector('.infinite-scroll-component');
            scrollElement.scrollTop = 0;
        } else {
            return false
        }
        this.setState({filter: event.target.value});
    }
    generateItems () {
        let moreItems = [];
        let self = this;
        let count = this.state.itemCount;
        if(!this.props.items.items.map((key) => key.item).length) {
            let infiniteScroll = document.querySelector('.admin-items-container .infinite-scroll-component');
            let noMore = document.createElement('p');
            noMore.className = "no-more-elements";
            noMore.innerHTML = 'Yay! You have seen it all';
            infiniteScroll.appendChild(noMore);
            setTimeout(function(){
                noMore.remove();
            }, 5000);
        } else {
            if(this.state.search.length) {
                getItems(count, this.state.search)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('items', data);
                        }
                    })
                    .then(() => {
                        this.setState({itemCount: this.state.itemCount + 1})
                    })
            } else {
                getItems(count)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('items', data);
                        }
                    })
                    .then(() => {
                        this.setState({itemCount: this.state.itemCount + 1})
                    })
            }
        }
        setTimeout(function () {
            for (let i = 0; i < 1; i++) {
                if(self.props.items == undefined || self.state.items == undefined  || self.state.items.length === 0) {
                    return false
                } else {
                    moreItems = (
                        self.props.items.items.map((key) => key.item)
                    );
                }
            }
            self.setState({items: self.state.items.concat(moreItems)});
        }, 500)
    }
    generateModels () {
        let moreModels = [];
        let self = this;
        let count = this.state.modelCount;
        if(!this.props.models.items.map((key) => key.item).length) {
            let infiniteScroll = document.querySelector('.admin-items-container .infinite-scroll-component');
            let noMore = document.createElement('p');
            noMore.className = "no-more-elements";
            noMore.innerHTML = 'Yay! You have seen it all';
            infiniteScroll.appendChild(noMore);
            setTimeout(function(){
                noMore.remove();
            }, 5000);
        } else {
            if(this.state.search.length) {
                getModels(count, this.state.search)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('models', data);
                        }
                    })
                    .then(() => {
                        this.setState({modelCount: this.state.modelCount + 1})
                    })
            } else {
                getModels(count)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('models', data);
                        }
                    })
                    .then(() => {
                        this.setState({modelCount: this.state.modelCount + 1})
                    })
            }
        }
        setTimeout(function () {
            for (let i = 0; i < 1; i++) {
                if(self.props.models == undefined || self.state.models == undefined  || self.state.models.length === 0) {
                    return false
                } else {
                    moreModels = (
                        self.props.models.items.map((key) => key.item)
                    )
                }
            }
            self.setState({models: self.state.models.concat(moreModels)});
        }, 500);
    }
    render() {
        let itemsCount;
        let itemsFilter;
        if(this.state.filter === 'Products' && this.state.items !== undefined) {
            itemsCount = this.props.items.count;
            itemsFilter = (
                <InfiniteScroll
                    next={this.generateItems}
                    hasMore={true}
                    scrollThreshold={0.3}
                    height={783}
                    style={{height: '86vh'}}
                >
                    {this.state.items.map((item, index) => (<Product item={item} key={index} baseImageUrl={this.props.baseImageUrl} updateItem={this.updateItem} rebaseData={this.props.rebaseData} getTags={this.props.getTags} getBrands={this.props.getBrands} types={this.props.types} categories={this.props.categories} sortedTags={this.props.sortedTags} />))}
                </InfiniteScroll>
            )
        } else if(this.state.filter === 'Models' && this.state.models !== undefined) {
            itemsCount = this.props.models.count;
            itemsFilter = (
                <InfiniteScroll
                    next={this.generateModels}
                    hasMore={true}
                    height={783}
                    scrollThreshold={0.3}
                    style={{height: '86vh'}}
                >
                    {this.state.models.map((item, index) => (<Model item={item} key={index} baseImageUrl={this.props.baseImageUrl} updateItem={this.updateItem} rebaseData={this.props.rebaseData} getTags={this.props.getTags} getBrands={this.props.getBrands} types={this.props.types} categories={this.props.categories} sortedTags={this.props.sortedTags} />))}
                </InfiniteScroll>
            )
        } else {
            return false;
        }
        return(
            <div>
                <div className="item-top-bar">
                    <h1>Items<sup>{itemsCount}</sup>:</h1>
                    <Row className='item-filter-bar'>
                        <Input name='items' type='radio' value='Models' label='Models' className='with-gap' onClick={(event) => this.onFilterChanged(event)} />
                        <Input name='items' type='radio' value='Products' label='Products' className='with-gap' defaultChecked={this.state.selectedOption} onClick={(event) => this.onFilterChanged(event)} />
                    </Row>
                    <Row className='item-search-bar'>
                        <DebounceInput
                            minLength={2}
                            debounceTimeout={600}
                            onChange={event => {
                                this.setState({search: event.target.value});
                                this.updateSearch()
                            }} />
                    </Row>
                </div>
                <div className="admin-items-container">
                    {itemsFilter}
                    <IconFont name="free-code-camp" size="2x" click={() => {this.clearWords()}}/>
                </div>
            </div>
        )
    }
}

export default AdminItemsRightSidebar
