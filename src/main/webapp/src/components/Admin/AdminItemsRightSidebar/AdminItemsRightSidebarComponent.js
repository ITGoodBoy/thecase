import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../../actions/index';

import AdminItemsRightSidebar from './AdminItemsRightSidebar';

const mapStateToProps = ({app: {counts, getTags, allElements, items, models, types, categories, getBrands, sortedTags, baseImageUrl}}, ownProps) => {
    return {
        counts,
        getTags,
        allElements,
        items,
        models,
        types,
        categories,
        getBrands,
        sortedTags,
        baseImageUrl,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const AdminItemsRightSidebarContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminItemsRightSidebar);

export default AdminItemsRightSidebarContainer;