import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../actions/index';

import AdminPanel from './AdminPanel';

const mapStateToProps = ({app: {}}, ownProps) => {
    return {
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const AdminPanelContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminPanel);

export default AdminPanelContainer;