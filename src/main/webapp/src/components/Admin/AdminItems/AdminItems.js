import React, {Component} from "react";
import {Row, Col} from 'react-materialize'
import './AdminItems.css';

import AdminItemsLeftSidebar from '../AdminItemsLeftSidebar/AdminItemsLeftSidebar'
import AdminItemsRightSidebarComponent from '../AdminItemsRightSidebar/AdminItemsRightSidebarComponent'

class AdminItems extends Component {
    render() {
        return (
            <div className="admin-items">
                <Row>
                    <Col l={6} m={12}>
                        <AdminItemsLeftSidebar />
                    </Col>

                    <Col l={6} m={12} className='right-component'>
                        <AdminItemsRightSidebarComponent />
                    </Col>
                </Row>
            </div>
        )
    }
}

export default AdminItems
