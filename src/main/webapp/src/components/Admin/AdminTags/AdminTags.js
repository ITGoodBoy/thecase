import React, {Component} from "react";
import {Row, Col, Input, Button, Modal, Collapsible, CollapsibleItem} from 'react-materialize'
import './AdminTags.css';
import { WithContext as ReactTags } from 'react-tag-input';
import IconFont from '../../IconFont/IconFont'
import { getTags, addTags, getSortedTags, removeTags, createGroup, removeGroup, getFunnels, renameGroup } from '../../../services/http';

class AdminTags extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tagsView: this.props.getTags,
            isChecked: false,
            tags: [],
            sortElement: [],
            selectedTags: [],
            groupName: [],
            groupNewName: '',
            groupOldName: '',
            groupNameToRemove: '',
            newGroupName: '',
            newGroupIcon: '',
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.updateTags = this.updateTags.bind(this);
        this.targetSortElement = this.targetSortElement.bind(this);
        this.pushTags = this.pushTags.bind(this);
        this.createGroup = this.createGroup.bind(this);
        this.removeGroup = this.removeGroup.bind(this);

        getTags()
            .then(data => {
                if(data) {
                    this.props.rebaseData('getTags', data);
                }
            });
    }

    updateTags(tag)  {
        if(tag == '') {
            getTags('A')
        } else {
            getTags(tag)
                .then(tagsView => {
                    this.setState({tagsView})
                });
        }
    }

    handleDelete(i) {
        let tags = this.state.tags;
        let selectedTags = this.state.selectedTags;
        tags.splice(i, 1);
        selectedTags.splice(i, 1);
        this.setState({
            tags,
            selectedTags
        });
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        tags.push({
            id: tags.length + 1,
            text: tag
        });
        const newSelection = tag;
        let newSelectionArray;
        if(this.state.selectedTags.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.selectedTags.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.selectedTags, newSelection];
        }
        this.setState({
            tags,
            selectedTags: newSelectionArray
        });
    }

    handleDrag(tag, currPos, newPos) {
        let tags = this.state.tags;

        // mutate array
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);

        // re-render
        this.setState({
            tags
        });
    }
    targetSortElement(e) {
        const newSelection = e.target.value;
        let newSelectionArray;
        if(this.state.selectedTags.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.selectedTags.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.selectedTags, newSelection];
        }
        this.setState({ selectedTags: newSelectionArray });
    }
    pushTags() {
        let name = this.state.groupName;
        let tags = this.state.selectedTags;
        addTags(name, tags)
            .then(() => {
                getTags()
                    .then((data) => {
                        this.props.rebaseData('getTags', data);
                    });
                getSortedTags()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('sortedTags', data);
                        }
                    });
            })
            .then(() => {
                this.setState({
                    tags: [],
                    sortElement: [],
                    selectedTags: [],
                    groupNameToRemove: '',
                })
            });
    }
    createGroup() {
        let name = this.state.newGroupName;
        let icon = this.state.newGroupIcon;
        createGroup(name, icon)
            .then(() => {
                getTags()
                    .then((data) => {
                        this.props.rebaseData('getTags', data);
                    });
                getSortedTags()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('sortedTags', data);
                        }
                    });
                getFunnels()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('funnels', data);
                        }
                    });
            })
            .then(() => {
                this.setState({
                    tags: [],
                    sortElement: [],
                    selectedTags: [],
                    newGroupName: '',
                    newGroupIcon: '',
                    groupNameToRemove: '',
                })
            });
    }

    renameGroup() {
        let newName = this.state.groupNewName;
        let oldName = this.state.groupOldName;
        renameGroup(oldName, newName)
            .then(() => {
                getTags()
                    .then((data) => {
                        this.props.rebaseData('getTags', data);
                    });
                getSortedTags()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('sortedTags', data);
                        }
                    });
                getFunnels()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('funnels', data);
                        }
                    });
            })
            .then(() => {
                this.setState({
                    tags: [],
                    sortElement: [],
                    selectedTags: [],
                    newGroupName: '',
                    newGroupIcon: '',
                    renameGroup: '',
                    groupNewName: '',
                    groupOldName: '',
                    groupNameToRemove: '',
                })
            });
    }

    removeGroup() {
        let name = this.state.groupNameToRemove;
        removeGroup(name)
            .then(() => {
                getTags()
                    .then((data) => {
                        this.props.rebaseData('getTags', data);
                    });
                getSortedTags()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('sortedTags', data);
                        }
                    });
                getFunnels()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('funnels', data);
                        }
                    });
            })
            .then(() => {
                this.setState({
                    tags: [],
                    sortElement: [],
                    selectedTags: [],
                    newGroupName: '',
                    newGroupIcon: '',
                    groupNameToRemove: '',
                })
            });
    }
    componentWillReceiveProps() {
        this.setState({
            tagsView: this.props.getTags
        })
    }

    render() {
        let w;
        if (window.matchMedia("(min-width: 1920px)").matches) {
            w = window.innerWidth / 6;
        } else if (window.matchMedia("(min-width: 1360px)").matches) {
            w = window.innerWidth / 4;
        } else if (window.matchMedia("(min-width: 980px)").matches) {
            w = window.innerWidth / 3;
        } else {
            w = window.innerWidth / 2;
        }

        let h = window.innerHeight - 80;
        return (
            <div className='admin-tags-container'>
                <div className='admin-tags-element tags-not-sorted' style={{'width': + w, 'height': + h}}>
                    <h5>Unsorted</h5>
                    <ReactTags tags={this.state.tags}
                               suggestions={
                                   this.state.tagsView.map(
                                       (tag) => tag.name
                                   )
                               }
                               autofocus={false}
                               handleDelete={this.handleDelete}
                               handleAddition={this.handleAddition}
                               handleDrag={this.handleDrag}
                               handleInputChange={this.updateTags}
                               classNames={{
                                   tag: 'chip',
                                   remove: 'left-padding',
                                   suggestions: 'suggestions-container'
                               }}
                    />
                </div>
                {this.props.sortedTags.map((key, id) => {
                    return (
                        <div className='admin-tags-element tags-sorted' key={id} style={{'width': + w, 'height': + h}}>
                            <Collapsible>
                                <CollapsibleItem header={key.name}>
                                    <Row>
                                        <Input key={id} placeholder={key.name} value={this.state.groupNewName} s={12} onChange={(event) => this.setState({
                                            groupNewName: event.target.value,
                                            groupOldName: key.name
                                        })}/>
                                    </Row>
                                    <Button waves='light' className="add rename-group" onClick={() => {
                                        this.setState({
                                            groupNewName: '',
                                            groupOldName: key.name
                                        })
                                    }}>Cancel</Button>
                                    <Button waves='light' className="add rename-group" onClick={() => {
                                        this.renameGroup();
                                    }}>
                                        Rename
                                    </Button>
                                </CollapsibleItem>
                            </Collapsible>
                            <Row className='admin-tag-element'>
                                {key.tags.map((tag) => {
                                    return (
                                        <Input key={tag.id} s={12} name={key.name} type='checkbox' value={tag.name} label={tag.name + ' ' + tag.frequency} defaultChecked={this.state.isChecked} onChange={(event) => {
                                            this.targetSortElement(event)
                                        }} />
                                    )
                                })}
                            </Row>
                        </div>
                    )
                })}
                <div className="tags-control">
                    <Modal
                        header='Remove Tags'
                        id='tags-remove'
                        actions={[<Button className='close-modal' waves='light' modal='close' flat>No</Button>,
                            <Button className='remove' modal='close' waves='light' onClick={() => {
                                removeTags(this.state.selectedTags)
                                    .then(() => {
                                        this.setState({
                                            tags: [],
                                            sortElement: [],
                                            selectedTags: [],
                                        })
                                    })
                                    .then(() => {
                                        getTags()
                                            .then((data) => {
                                                this.props.rebaseData('getTags', data);
                                            });
                                        getSortedTags()
                                            .then(data => {
                                                if(data) {
                                                    this.props.rebaseData('sortedTags', data);
                                                }
                                            });
                                    })
                            }}>Yes</Button>
                        ]}
                        trigger={<Button className='btn-edit-tags'>Remove Tags</Button>}>
                        <Row>
                            Do you really want to delete this tags: {this.state.selectedTags.map((key) => {return (<span className="tags-remove-view">{key}</span>)})}?
                        </Row>
                    </Modal>
                    <Modal
                        header='Move Tags'
                        id='tags-move'
                        actions={[<Button className='close-modal' waves='light' modal='close' flat>Close Modal</Button>,
                            <Button waves='light' className="add" modal='close' onClick={this.pushTags}>
                                Submit
                            </Button>
                        ]}
                        trigger={<Button className='btn-edit-tags'>Move tags</Button>}>
                        <Row>
                            {this.props.sortedTags.map((key, id) => {
                                return (
                                    <Input s={12} key={id} name='groupName' type='radio' value={key.name} label={key.name} className='with-gap' onClick={(event) => {
                                        this.setState({
                                            groupName: event.target.value
                                        })
                                    }}  />
                                )
                            })}
                        </Row>
                    </Modal>
                </div>
                <div className="group-control">
                    <Modal
                        header='Create Group'
                        id='group-control-create'
                        actions={[<Button className='close-modal' waves='light' modal='close' flat>Cancel</Button>,
                            <Button waves='light' className="add" modal='close' onClick={() => this.createGroup()}>
                                Create
                            </Button>
                        ]}
                        trigger={<Button className='btn-edit-group'>Create Group</Button>}>
                        <Row>
                            <Input placeholder="Group Name" value={this.state.newGroupName} s={12} onChange={(event) => this.setState({newGroupName: event.target.value})}/>
                            <Input label="Icon Name" value={this.state.newGroupIcon} s={12} onChange={(event) => this.setState({newGroupIcon: event.target.value})} />
                        </Row>
                    </Modal>
                    <Modal
                        header='Remove Group'
                        id='group-control-remove'
                        actions={[<Button className='close-modal' waves='light' modal='close' flat>Cancel</Button>,
                            <Button className='remove' modal='close' waves='light' onClick={() => {this.removeGroup()}}>
                                Remove
                            </Button>
                        ]}
                        trigger={<Button className='btn-edit-group'>Remove Group</Button>}>
                        <Row>
                            {this.props.sortedTags.map((key, id) => {
                                return (
                                    <Input s={12} key={id} name='groupName' type='radio' value={key.name} label={key.name} className='with-gap' onClick={(event) => {
                                        this.setState({
                                            groupNameToRemove: event.target.value
                                        })
                                    }}  />
                                )
                            })}
                        </Row>
                    </Modal>
                </div>
            </div>
        )
    }
}

export default AdminTags
