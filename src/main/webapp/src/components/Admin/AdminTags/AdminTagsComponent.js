import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../../actions/index';

import AdminTags from './AdminTags';

const mapStateToProps = ({app: {getTags, sortedTags}}, ownProps) => {
    return {
        getTags,
        sortedTags,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const AdminTagsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminTags);

export default AdminTagsContainer;