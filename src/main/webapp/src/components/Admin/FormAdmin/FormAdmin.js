import React, {Component} from "react";
import {Row, Col, Input, Collapsible, CollapsibleItem, Button, Chip} from 'react-materialize'
import { WithContext as ReactTags } from 'react-tag-input';

import './FormAdmin.css'
import '../../../styles/index.css'
import AdminImageUploader from '../AdminImageUploader/AdminImageUploader'
import { getTags, createItem, getCategories, getTypes, getBrands } from '../../../services/http'

class FurnitureAdmin extends Component {
    constructor(props) {
        super(props);
        let isOpen = [];
        this.props.sortedTags.forEach(function (i) {
            isOpen.push(i)
        });
        this.state = {
            tagsView: this.props.getTags,
            isOpen: isOpen,
            selectedOption: 'product',
            tags: [],
            name: '',
            uploadImgs: [],
            published: true,
            files: [],
            category: this.props.categories[0],
            type: this.props.types[0],
            brands: [],
            brandsView: this.props.getBrands,
            sourceLink: '',
            selectedTags: [],
            brand: '',
            allPrice: [{
                amount: '0',
                currency: '',
            }],
        };
        this.toggle = this.toggle.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.updateTags = this.updateTags.bind(this);
        this.imageUploader = this.imageUploader.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.imageUpdate = this.imageUpdate.bind(this);
        this.targetSortElement = this.targetSortElement.bind(this);
        this.updateBrands = this.updateBrands.bind(this);
        this.brandDelete = this.brandDelete.bind(this);
        this.brandAddition = this.brandAddition.bind(this);

        getTags()
            .then(data => {
                if(data) {
                    this.props.rebaseData('getTags', data);
                }
            });

        getTypes()
            .then(data => {
                if(data) {
                    this.props.rebaseData('types', data);
                }
            });

        getCategories()
            .then(data => {
                if(data) {
                    this.props.rebaseData('categories', data);
                }
            });

        getBrands()
            .then(data => {
                if(data) {
                    this.props.rebaseData('getBrands', data);
                }
            });
    }

    toggle(id) {
        const newStatus = this.state.isOpen.slice();
        newStatus[id] = !this.state.isOpen[id];
        this.setState({
            isOpen: newStatus,
        });
    }

    updateTags(tag)  {
        if(tag == '') {
            getTags('A')
        } else {
            getTags(tag)
                .then(tagsView => {
                        this.setState({tagsView})
                    });
        }
    }

    handleDelete(i) {
        let tags = this.state.tags;
        tags.splice(i, 1);
        this.setState({
            tags
        });
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        tags.push({
            id: tags.length + 1,
            text: tag
        });
        const newSelection = tag;
        let newSelectionArray;
        if(this.state.selectedTags.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.selectedTags.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.selectedTags, newSelection];
        }
        this.setState({
            tags,
            selectedTags: newSelectionArray
        });
    }

    targetSortElement(e) {
        const newSelection = e.target.value;
        let newSelectionArray;
        if(this.state.selectedTags.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.selectedTags.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.selectedTags, newSelection];
        }
        this.setState({ selectedTags: newSelectionArray });
    }

    handleDrag(tag, currPos, newPos) {
        let tags = this.state.tags;

        // mutate array
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);

        // re-render
        this.setState({
            tags
        });
    }

    handleOptionChange(changeEvent) {
        this.setState({
            selectedOption: changeEvent.target.value
        })
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }

    imageUploader(data) {
        this.setState({
            uploadImgs: data
        });
    }

    imageUpdate(data) {
        let filesArray = this.state.files.concat(data);
        this.setState({
            files: filesArray
        });
    }

    updateBrands(tag)  {
        if(tag == '') {
            getBrands('A')
        } else {
            getBrands(tag)
                .then(brandsView => {
                    this.setState({brandsView})
                });
        }
    }

    brandDelete(i) {
        let brands = this.state.brands;
        brands.splice(i, 1);
        this.setState({
            brands
        });
    }

    brandAddition(brand) {
        let brands = [];
        brands.push({
            id: brand.length + 1,
            text: brand
        });
        this.setState({
            brands,
            brand: brand,
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        let selectedOption = this.state.selectedOption;
        let name = this.state.name;
        let uploadImgs = this.state.uploadImgs;
        let published = this.state.published;
        let category = this.state.category;
        let type = this.state.type;
        let sourceLink = this.state.sourceLink;
        let selectedTags = this.state.selectedTags;
        let brand = this.state.brand;
        let allPrice = this.state.allPrice;
        // console.log('all submit data', selectedOption, name, uploadImgs, published, category, type, brand, sourceLink, selectedTags, allPrice);
        createItem(selectedOption, name, uploadImgs, published, category, type, brand, sourceLink, allPrice, selectedTags)
            .then(() => {
                this.setState({
                    selectedOption: 'product',
                    tags: [],
                    name: '',
                    uploadImgs: [],
                    files: [],
                    published: true,
                    category: this.props.categories[0],
                    type: this.props.types[0],
                    brands: [],
                    brand: '',
                    sourceLink: '',
                    allPrice: [{
                        amount: '0',
                        currency: '',
                    }],
                    selectedTags: []
                });
            });
    }

    componentWillReceiveProps() {
        this.setState({files: []})
    }

    render() {
        return(
            <div>
                <h1 className="leading-size center-align">Create Item</h1>
                <form onSubmit={this.handleSubmit}>
                    <Row className="top-padding">
                        <Input
                            s={12}
                            label="Name"
                            name="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                        />
                    </Row>

                    <Row className="top-padding">
                        <Input s={2}
                               name='items'
                               type='radio'
                               value='model3d'
                               label='Model'
                               checked={this.state.selectedOption === 'model3d'}
                               onChange={this.handleOptionChange}
                               className='with-gap'
                        />

                        <Input s={2}
                               name='items'
                               type='radio'
                               value='product'
                               label='Product'
                               checked={this.state.selectedOption === 'product'}
                               onChange={this.handleOptionChange}
                               className='with-gap'
                        />

                        <Input s={2}
                               name='published'
                               type='checkbox'
                               label='Published'
                               className='filled-in'
                               checked={this.state.published}
                               onChange={this.handleChange}
                        />
                    </Row>

                    <Row className="top-padding">
                        <Input s={12}
                               type='select'
                               label="Category"
                               value={this.state.category}
                               onChange={this.handleChange}
                               name="category"
                        >
                            {this.props.categories.map((key, id) => {
                                return (
                                    <option key={id} value={key}>{key}</option>
                                )
                            })}
                        </Input>
                    </Row>

                    <Row className="top-padding">
                        <Input s={12}
                               type='select'
                               label="Type"
                               value={this.state.type}
                               onChange={this.handleChange}
                               name="type"
                        >
                            {this.props.types.map((key, id) => {
                                return (
                                    <option key={id} value={key}>{`${key.charAt(0).toUpperCase()}${key.substr(1).toLowerCase()}`}</option>
                                )
                            })}
                        </Input>
                    </Row>

                    <Row className="top-padding">
                        <Col s={this.state.brand.length ? 6 : 12} >
                            <ReactTags
                                       suggestions={
                                           this.state.brandsView.map(
                                               (brand) => brand
                                           )
                                       }
                                       placeholder="Add new brand"
                                       autofocus={false}
                                       handleDelete={this.brandDelete}
                                       handleAddition={this.brandAddition}
                                       handleInputChange={this.updateBrands}
                                       classNames={{
                                           tag: 'chip',
                                           remove: 'left-padding',
                                           suggestions: 'suggestions-container'
                                       }}
                            />
                        </Col>
                        {this.state.brand.length ? (
                            <Col s={6} className="center-align">
                                <Chip>{this.state.brand}</Chip>
                            </Col>
                        ) : []}
                    </Row>

                    <Row className="top-padding">
                        <Input s={12}
                               label="Source link"
                               name="sourceLink"
                               value={this.state.sourceLink}
                               onChange={this.handleChange}
                        />
                        <p className="grey-color">URl to the page where is possible to purchase/download the model/product</p>
                    </Row>

                    <Row className="top-padding">
                        <Col s={12}>
                            <ReactTags tags={this.state.tags}
                                       suggestions={
                                           this.state.tagsView.map(
                                               (tag) => tag.name
                                           )
                                       }
                                       autofocus={false}
                                       handleDelete={this.handleDelete}
                                       handleAddition={this.handleAddition}
                                       handleDrag={this.handleDrag}
                                       handleInputChange={this.updateTags}
                                       classNames={{
                                           tag: 'chip',
                                           remove: 'left-padding',
                                           suggestions: 'suggestions-container'
                                       }}
                            />
                        </Col>
                    </Row>

                    {this.props.sortedTags.map((key, id) => {
                        return (
                            <Row className="top-padding" key={id}>
                                <Collapsible>
                                    <CollapsibleItem
                                        header={key.name}
                                        onClick={() => this.toggle(id)}
                                        icon={this.state.isOpen[id] ? 'keyboard_arrow_right' : 'keyboard_arrow_down'}
                                        iconClassName="right"
                                    >

                                        <Row>
                                            {key.tags.map((tag) => {
                                                let check;
                                                if(this.state.selectedTags.indexOf(tag.name) > -1 == true) {
                                                    check = 'checked';
                                                } else {
                                                    check = false;
                                                }
                                                return (
                                                    <Input s={4}
                                                        type='checkbox'
                                                        className='filled-in'
                                                        key={tag.id}
                                                        name={key.name}
                                                        value={tag.name}
                                                        label={`${tag.name} ${tag.frequency}`}
                                                        checked={check}
                                                        onChange={(event) => {this.targetSortElement(event)}}
                                                    />
                                                )
                                            })}
                                        </Row>
                                    </CollapsibleItem>
                                </Collapsible>
                            </Row>
                        )
                    })}

                    <Row className="top-padding">
                        <Input
                            s={8}
                            type="number"
                            label="Price"
                            name="amount"
                            value={this.state.allPrice.amount || ''}
                            onChange={(event) => this.setState({
                                allPrice: {
                                    amount: event.target.value,
                                    currency: this.state.allPrice.currency || '',
                                }
                            })}
                        />

                        <Input s={4}
                               type='select'
                               label="Currency"
                               value={this.state.allPrice.currency || ''}
                               onChange={(event) => this.setState({
                                   allPrice: {
                                       amount: this.state.allPrice.amount || '',
                                       currency: event.target.value,
                                   }
                               })}
                               name="currency"
                        >
                            <option value=''>Chose Currency</option>
                            <option value='USD'>USD</option>
                            <option value='EUR'>EUR</option>
                            <option value='RUB'>RUB</option>
                        </Input>
                    </Row>

                    <Row className="top-padding">
                        <AdminImageUploader image={this.imageUploader} files={this.imageUpdate} updFiles={this.state.files} />
                    </Row>

                    <Row className="top-padding">
                        <Button waves='light' className="btn-primary margin-right">Create Item</Button>
                    </Row>
                </form>
            </div>
        )
    }

}

export default FurnitureAdmin
