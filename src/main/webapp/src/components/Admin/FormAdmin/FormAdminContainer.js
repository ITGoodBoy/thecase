import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../../actions/index';

import FurnitureAdmin from './FormAdmin';

const mapStateToProps = ({app: {getTags, sortedTags, types, categories, getBrands}}, ownProps) => {
    return {
        getTags,
        sortedTags,
        types,
        categories,
        getBrands,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const FurnitureAdminContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(FurnitureAdmin);

export default FurnitureAdminContainer;