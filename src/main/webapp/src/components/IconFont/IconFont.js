import React from 'react';
import FontAwesome from 'react-fontawesome';

const IconFont = (props) => {
    return(
        <FontAwesome
            name={props.name}
            size={props.size}
            className={props.className}
            border={props.border}
            onClick={() => {
                if (props.click) {
                    props.click();
                }
            }}
        />
    )
};

export default IconFont;