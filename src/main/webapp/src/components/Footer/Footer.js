import React, {Component} from "react";
import {Button} from 'react-materialize'

import { parseJwt, Logout } from '../../services/http';

class Header extends Component {
    render() {
       return (
           <Button floating icon='apps' className='red footer-app' large style={{bottom: '45px', right: '24px'}}>
           </Button>
        );
    }
}

export default Header;