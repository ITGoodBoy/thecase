import React, { Component } from 'react';
import {Button, Modal, Row, Input} from 'react-materialize'

import { Register } from '../../services/http';
import './Log.css'

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            email: '',
            password: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.Registration = this.Registration.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }

    Registration() {
        let userName = this.state.userName;
        let email = this.state.email;
        let password = this.state.password;
        Register(userName, email, password)
    }

    render() {
        return(
            <Modal
                header='Sign up'
                actions={[<Button waves='light' modal='close' flat>Close</Button>,
                    <Button waves='light' className="btn blue darken-2" onClick={this.Registration} disabled={!this.state.userName || !this.state.email || !this.state.password}>Submit</Button>]}
                trigger={
                    <Button waves='light' className="btn-flat">Sign up</Button>
                }>
                <Row>
                    <form className="col s12">
                        <Input s={12}
                            type="text"
                            label="User Name"
                            name="userName"
                            value={this.state.userName}
                            onChange={this.handleChange}
                        />

                        <Input s={12}
                            type="email"
                            label="Email"
                            name="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />

                        <Input s={12}
                            type="password"
                            label="Password"
                            name="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                        />
                    </form>
                </Row>
            </Modal>
        )
    }
}

export  default SignUp;