import React, { Component } from 'react';
import {Button, Modal, Row, Input} from 'react-materialize'

import { Login } from '../../services/http';
import './Log.css';
import ResetPassword from './ResetPassword'

class LogIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
        this.logIn = this.logIn.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }

    logIn() {
        let email = this.state.email;
        let password = this.state.password;
        Login(email, password)
            .then(token => {
                this.props.rebaseCallback('token', token)
            })

    }

    render() {

        return(
            <div>

                <Modal
                    id="login-form"
                    header='Log in'
                    actions={[<Button waves='light' modal='close' flat>Close</Button>,
                        <Button waves='light' className="btn blue darken-2 user-login" onClick={this.logIn} disabled={!this.state.email || !this.state.password}>Submit</Button>
                    ]}
                    trigger={
                        <Button waves='light' className="btn-flat">Log in</Button>
                    }>
                    <Row>
                        <form className="col s12">
                            <Input s={12} label="Email"
                                   type='email'
                                   name='email'
                                   value={this.state.email}
                                   onChange={this.handleChange}
                            />
                            <Input s={12} label="Password"
                                   type='password'
                                   name='password'
                                   value={this.state.password}
                                   onChange={this.handleChange}
                            />
                        </form>
                    </Row>
                    <ResetPassword modal='close' />
                </Modal>
            </div>

        )
    }
}

export  default LogIn;