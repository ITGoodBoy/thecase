import React, { Component } from 'react';
import {Button, Modal, Row, Input} from 'react-materialize'

import './Log.css';
import {replacePassword} from '../../services/http'

class ReplacePassword extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			oldPassword: '',
			newPassword: ''
		};
		this.Replace = this.Replace.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;
		this.setState({
			[name]: value
		})
	}

	Replace() {
		let email = this.state.email;
		let oldPassword = this.state.oldPassword;
		let newPassword = this.state.newPassword;
		replacePassword(email, oldPassword, newPassword)
			.then(() => {
				this.setState({
					email: '',
					oldPassword: '',
					newPassword: ''
				})
			})
	}

	render() {
		return (
			<Modal
				id="replace-password"
				header='Replace Password'
				actions={[<Button waves='light' modal='close' flat>Close</Button>,
					<Button waves='light' className="btn blue darken-2" onClick={this.Replace} disabled={!this.state.email || !this.state.oldPassword || !this.state.newPassword}>
						Submit
					</Button>
				]}
				trigger={
					<Button waves='light' className="btn-flat" modal='close'>Replace password</Button>
				}>
				<Row>
					<form className="col s12">
						<Input s={12} label="Email"
						       type='email'
						       name='email'
						       value={this.state.email}
						       onChange={this.handleChange}
						/>

						<Input s={12}
						       type="password"
						       label="Old Password"
						       name="oldPassword"
						       value={this.state.oldPassword}
						       onChange={this.handleChange}

						/>

						<Input s={12}
						       type="password"
						       label="New Password"
						       name="newPassword"
						       value={this.state.newPassword}
						       onChange={this.handleChange}
						/>
					</form>
				</Row>
			</Modal>
		)
	}
}

export  default ReplacePassword
