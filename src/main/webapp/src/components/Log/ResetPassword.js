import React, { Component } from 'react';
import {Button, Modal, Row, Input} from 'react-materialize'

import './Log.css';
import {resetPassword} from '../../services/http'

class ResetPassword extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: ''
		};
		this.Reset = this.Reset.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;
		this.setState({
			[name]: value
		})
	}

	Reset() {
		let email = this.state.email;
		resetPassword(email)
			.then(() => {
				this.setState({ email: ''})
			})
	}

	render() {
		return (
			<Modal
				id="reset-password"
				header='Reset Forgotten Password'
				actions={[<Button waves='light' modal='close' flat>Close</Button>,
					<Button waves='light' className="btn blue darken-2" onClick={this.Reset} disabled={!this.state.email}>
						Submit
					</Button>
				]}
				trigger={
					<Button waves='light' className="btn-flat" modal='close'>Forgot password?</Button>
				}>
				<Row>
					<form className="col s12">
						<Input s={12} label="Email"
						       type='email'
						       name='email'
						       value={this.state.email}
						       onChange={this.handleChange}
						/>
					</form>
				</Row>
			</Modal>
		)
	}
}

export  default ResetPassword