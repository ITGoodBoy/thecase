import React, {Component} from "react";

import {Row, Col} from 'react-materialize'

import './Main.css';
import SideBarContainer from  '../SideBar/SideBarContainer';
import CardFlipContainer from  '../CardFlip/CardFlipContainer';
import Footer from  '../Footer/Footer';

class Main extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let sidebarContainer;
        let cardflipContainer;

        if(!this.props.isSidebar && !this.props.isSidebarFull) {
            sidebarContainer = 'sidebar-container sidebar-closed';
            cardflipContainer = 'cardflip-container cardflip-full-opened';
        }
        if (!!this.props.isSidebar && !this.props.isSidebarFull) {
            sidebarContainer = 'sidebar-container sidebar-small-opened';
            cardflipContainer = 'cardflip-container cardflip-small-opened';
        }
        if (!this.props.isSidebar && !!this.props.isSidebarFull) {
            sidebarContainer = 'sidebar-container sidebar-full-opened';
            cardflipContainer = 'cardflip-container cardflip-closed closed-full';
        }
        if (!!this.props.isSidebar && !!this.props.isSidebarFull) {
            sidebarContainer = 'sidebar-container sidebar-full-opened';
            cardflipContainer = 'cardflip-container cardflip-closed closed-small';
        }
        console.log(this.props.modalWindow)
        return (
            <Row className={this.props.isProjects ? 'main-container deactive-main-container' : 'main-container active-main-container'}>
                <Col className={sidebarContainer}>
                    <SideBarContainer />
                </Col>
                <Col className={cardflipContainer}>
                    <CardFlipContainer />
                </Col>
                {/*<Footer />*/}
            </Row>
        );
    }
}

export default Main;