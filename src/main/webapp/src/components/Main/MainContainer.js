import {connect} from 'react-redux';

import { toggleSideBarFunc, rebaseDataFunc, addToFolderFunc, toggleSideBarFullFunc, toggleProjectsFunc, toggleModalFunc } from '../../actions/index';
import Main from './Main';

const mapStateToProps = ({app: {isSidebar, isSidebarFull, isProjects, sidebars, modalWindow}}, ownProps) => {
    return {
        isSidebar,
        isSidebarFull,
        isProjects,
        sidebars,
        modalWindow,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSideBar (property) {
            toggleSideBarFunc(property)(dispatch);
        },
        toggleSideBarFull (property) {
            toggleSideBarFullFunc(property)(dispatch);
        },
        toggleProjects (property) {
            toggleProjectsFunc(property)(dispatch);
        },
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
        addToFolder(type, element) {
            addToFolderFunc(type, element)(dispatch);
        },
        toggleModal (property) {
            toggleModalFunc(property)(dispatch);
        },
    }
};

const MainContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Main);

export default MainContainer;