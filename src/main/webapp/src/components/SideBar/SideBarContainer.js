import {connect} from 'react-redux';

import { addToFolderFunc, toggleSideBarFunc, toggleListFunc, rebaseDataFunc, funnelEventFunc } from '../../actions/index';
import SideBar from './SideBar';

const mapStateToProps = ({app: {activeCase, isSidebar, isSidebarFull, selectedItems, selectedModels, isFunnel, isList, funnelActive, cases, filters, activeFilters}}, ownProps) => {
  return {
      activeCase,
      selectedItems,
      selectedModels,
      isSidebar,
      isSidebarFull,
      isFunnel,
      isList,
      funnelActive,
      cases,
      filters,
      activeFilters,
      ...ownProps,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
      addToFolder(type, key, value) {
          addToFolderFunc(type, key, value)(dispatch);
      },
      toggleSideBar (property) {
          toggleSideBarFunc(property)(dispatch);
      },
      toggleList (property) {
          toggleListFunc(property)(dispatch);
      },
      rebaseData (key, value) {
          rebaseDataFunc(key, value)(dispatch);
      },
      funnelEvent (property) {
          funnelEventFunc(property)(dispatch);
      },
  }
};

const SideBarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SideBar);

export default SideBarContainer;