import React, {Component} from "react";
import {Col, Icon, Slider, Slide, Input} from 'react-materialize';
import DebounceInput from 'react-debounce-input';

import '../CardFlip/CardFlip.scss';
import './SideBar.css';
import OwlCarousel from 'react-owl-carousel2';
import IconFont from '../IconFont/IconFont';
import { deleteCaseItem, itemsSearch, getItems, getModels, focusCases, getCases, editActiveItem, deactivateFilters, filterEvent, getFilters, getActiveFilters, liked, upItem } from '../../services/http';

class Models extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonStatus: [],
            isFront: true,
            data: [],
            activeCase: [],
            itemNo: 1,
            loop: true,
            nav: true,
            rewind: true,
            autoplay: true,
            lazyLoad: true,
            quantity: this.props.item.count,
            // price: this.props.item.price.amount.toString() || '0',
            // priceCurrency: this.props.item.price.currency.toString() || 'RUB',
        };

        this.toggle = this.toggle.bind(this);
        this.handleClick = this.handleClick.bind(this);
        // this.handleStarClick = this.handleStarClick.bind(this);
    }

    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];
        this.setState({buttonStatus: newStatus});
    }

    toggle() {
        this.setState({
            isFront: !this.state.isFront
        });
    }
    filter(filter, name) {
        let f = filter.toLowerCase();
        let n = name.toString();
        let m;
        if(Object.keys(this.props.activeFilters).map((key) => this.props.activeFilters[key].map((el) => el)).toString().indexOf(n) > -1) {
            m = 'delete';
        } else {
            m = 'post';
        }
        deactivateFilters()
            .then(() => {
                filterEvent(f, n, m)
                    .then(() => {
                        getItems()
                            .then((data) => {
                                this.props.rebaseData('items', data)
                            });
                        getModels()
                            .then((data) => {
                                this.props.rebaseData('models', data)
                            })
                    })
                    .then(() => {
                        let scrollElement = document.querySelector('.infinite-scroll-component');
                        scrollElement.scrollTop = 0;
                    })
                    .then(() => {
                        if(this.props.funnelActive == false) {
                            return false;
                        } else {
                            this.props.funnelEvent(!this.props.funnelActive);
                        }
                    })
                    .then(() => {
                        getFilters()
                            .then((data) => {
                                if(data) {
                                    this.props.rebaseData('filters', data);
                                }
                            });
                        getActiveFilters()
                            .then((data) => {
                                if(data) {
                                    this.props.rebaseData('activeFilters', data);
                                }
                            });
                    })
            });
    }

    // handleStarClick(id) {
    //     liked(id)
    //         .then(() => {
    //             focusCases()
    //                 .then((data) => {
    //                     if(data) {
    //                         this.props.rebaseData('activeCase', data)
    //                     }
    //                 })
    //         })
    //         .catch((error) => {
    //             if(error) {
    //                 console.log(error)
    //             }
    //         });
    // }

    render() {
        let item = this.props.model;
        let list = this.props.list;
        let name = this.props.items;
        let permission = this.props.permission;
        let userPermission, noPublished, listName;
        let priceIcon, priceAmount;

        let checkedState;
        const options = {
            items: this.state.itemNo,
            loop: this.state.loop,
            nav: this.state.nav,
            rewind: this.state.rewind,
            autoplay: this.state.autoplay,
            lazyLoad: this.state.lazyLoad,
            mouseDrag: false,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        };

        if(name.indexOf(item.id) != -1) {
            checkedState = 'checked';
        } else {
            checkedState = false;
        }

        if(permission.mine === true || permission.mine === false && permission.permission >= 2 ) {
            // Get data from price currency
            if(this.state.priceCurrency === 'USD') {
                priceIcon = <Icon className={this.state.isFront ? 'grey-text text-lighten-1' : 'grey-text text-darken-4'}>attach_money</Icon>;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            } else if(this.state.priceCurrency === 'EUR') {
                priceIcon = <Icon className={this.state.isFront ? 'grey-text text-lighten-1' : 'grey-text text-darken-4'}>euro_symbol</Icon>;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            } else if(this.state.priceCurrency === 'RUB' || this.state.priceCurrency === 'руб.') {
                priceIcon = <IconFont name="rub" size="2x" border={false} className={this.state.isFront ? 'grey-text text-lighten-1' : 'grey-text text-darken-4'} />;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            } else if(this.state.priceCurrency === 'hz') {
                priceIcon = false;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            }
            userPermission = (
                <div className="table-list list-info" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>
                    <DebounceInput
                        type="number"
                        minLength={1}
                        debounceTimeout={600}
                        value={this.props.item.count}
                        onChange={(event) => {
                            editActiveItem(item.id, event.target.value)
                                .then(() => {
                                    focusCases()
                                        .then((data) => {
                                            this.props.rebaseData('activeCase', data);
                                        })
                                });
                        }} />
                </div>
            );
            listName = (
                <div className="table-list list-name"  style={this.props.sidebar ? {width: '100%'} : {width: '15%'}}>
                    <Input key={item.id} s={12} name={item.name} type='checkbox' value={item.id} label={item.name} checked={checkedState} onChange={(event) => {
                        this.props.check(event.target.value);
                    }} />
                </div>
            )
        } else if(permission.mine === false && permission.permission === 1) {
            // Get data from price currency
            if(this.state.priceCurrency === 'USD') {
                priceIcon = <Icon className={this.state.isFront ? 'grey-text text-lighten-1' : 'grey-text text-darken-4'}>attach_money</Icon>;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            } else if(this.state.priceCurrency === 'EUR') {
                priceIcon = <Icon className={this.state.isFront ? 'grey-text text-lighten-1' : 'grey-text text-darken-4'}>euro_symbol</Icon>;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            } else if(this.state.priceCurrency === 'RUB' || this.state.priceCurrency === 'руб.') {
                priceIcon = <IconFont name="rub" size="2x" border={false} className={this.state.isFront ? 'grey-text text-lighten-1' : 'grey-text text-darken-4'} />;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            } else if(this.state.priceCurrency === 'hz') {
                priceIcon = false;
                priceAmount = <span className='price grey-text text-darken-4'>{this.props.item.price.amount || '0'}</span>;
            }
            userPermission = (
                <div className="table-list list-info list-uantity" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>
                    <p>{this.props.item.count}</p>
                </div>
            );
            listName = (
                <div className="table-list list-name"  style={this.props.sidebar ? {width: '100%'} : {width: '15%'}}>
                    <p key={item.id}>{item.name}</p>
                </div>
            )
        }

        return (
            <Col s={list ? 3 : 12} className={list ? 'grid-card' : 'list-card'}>
                {list ?
                    <div>
                        <div className='card-wrap'>
                            <div className={this.state.isFront ? 'card-item' : 'card-item item-transform grey lighten-4'}>
                                <div className="front front-menu">
                                    <img src={item.imageUrls[0]} alt="item"/>
                                    <div className="item-buttons">
                                        <a className='pin-item' onClick={() => {
                                            upItem(item.id)
                                                .then(() => {
                                                    focusCases()
                                                        .then((data) => {
                                                            this.props.rebaseData('activeCase', data)
                                                        })
                                                });
                                        }}>
                                            <IconFont name='thumb-tack' size="2x" />
                                        </a>
                                        <IconFont id="add-to-card" name="trash-o" size="2x" border={true} className="icon-left" click={() => {
                                            deleteCaseItem(item.id)
                                                .then(() => {
                                                    let self = this;
                                                    focusCases()
                                                        .then(data => {
                                                            if(data) {
                                                                setTimeout(function () {
                                                                    self.props.rebaseData('activeCase', data);
                                                                }, 300)
                                                            }
                                                        });
                                                    getCases()
                                                        .then(data => {
                                                            if(data) {
                                                                setTimeout(function () {
                                                                    self.props.rebaseData('cases', data);
                                                                }, 300)
                                                            }
                                                        });
                                                });
                                        }}/>
                                        <svg  width="100" height="100" viewBox="0 0 100 100" onClick={() => {
                                            let self = this;
                                            itemsSearch(item.id)
                                                .then((data) => {
                                                    this.props.rebaseData('funnelData', data)
                                                })
                                                .then(() => {
                                                    getItems()
                                                        .then(data => {
                                                            if(data) {
                                                                setTimeout(function () {
                                                                    self.props.rebaseData('items', data);
                                                                }, 300)
                                                            }
                                                        });
                                                    getModels()
                                                        .then(data => {
                                                            if(data) {
                                                                setTimeout(function () {
                                                                    self.props.rebaseData('models', data);
                                                                }, 300)
                                                            }
                                                        })
                                                        .then(() => {
                                                            let scrollElement = document.querySelector('.infinite-scroll-component');
                                                            scrollElement.scrollTop = 0;
                                                        })
                                                });
                                            if(this.props.funnelEvent(!this.props.funnelActive) === false) {
                                                return false;
                                            } else {
                                                this.props.funnelEvent(!this.props.funnelActive);
                                            }
                                        }}>
                                            <path d="M33 40 l35 0 l0 2 l-15 14 l0 9 l-6 0 l0 -9 L33 42 Z" className="figure"/>
                                            <g className="shape">
                                                <circle cx="50" cy="50" r="38" fill="transparent"/>
                                                <line x1="0" y1="50" x2="11" y2="50"/>
                                                <line x1="89" y1="50" x2="100" y2="50"/>
                                                <line x1="50" y1="0" x2="50" y2="11"/>
                                                <line x1="50" y1="89" x2="50" y2="100"/>
                                            </g>
                                        </svg>
                                        <IconFont name="shopping-cart" size="2x" border={true} className="icon-right" click={() => window.open(item.link,'_blank')} />
                                        <a className="item-slider" onClick={() => this.handleClick(22)}><Icon small>search</Icon></a>
                                    </div>
                                </div>
                                <div className="back">
                                    <div className='yellow accent-4'>
                                        <h2 className='grey-text text-darken-4'>{item.name}</h2>
                                    </div>
                                    <div className="info-wrap">
                                        {Object.keys(item.info).map((key, ind) => {
                                            return (
                                                <div key={ind} className="back-info" onClick={() => {
                                                    this.filter(key, item.info[key].map((value) => value));
                                                }}>
                                                    <p className='grey-text text-lighten-1'>{key}</p>
                                                    <div className="info-data">
                                                        {item.info[key].map((value, id) => {
                                                            return (
                                                                <a key={id} className='grey-text text-darken-4'>{value}</a>
                                                            )
                                                        })}
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={this.state.isFront ? 'card-panel' : 'card-panel grey lighten-4'}>
                            <a onClick={this.toggle} className={this.state.isFront ? 'grey-text text-lighten-1 card-info flex-end' : 'grey-text text-darken-4 card-info flex-end'}><Icon>info_outline</Icon></a>

                            <div className="flex column">
                                <p className={this.state.isFront ? 'panel-item grey-text text-lighten-1' : 'panel-item grey-text text-darken-4 card-info'}>
                                    <span className='price grey-text text-darken-4'>3d</span>
                                    <IconFont name="cube" />
                                </p>
                            </div>
                            {/*<div className="flex column">*/}
                                {/*<p className="right-align zero-margin">*/}
                                    {/*<a onClick={() => {*/}
                                        {/*this.handleStarClick(item.id);*/}
                                    {/*}}>*/}
                                        {/*<IconFont name={this.props.item.liked ? 'star' : 'star-o'} className="color-star" />*/}
                                    {/*</a>*/}
                                {/*</p>*/}
                                {/*<p className={this.state.isFront ? 'panel-item grey-text text-lighten-1' : 'panel-item grey-text text-darken-4 card-info'}>*/}
                                    {/*<span className='price grey-text text-darken-4'>3d</span>*/}
                                    {/*<IconFont name="cube" />*/}
                                {/*</p>*/}
                            {/*</div>*/}

                        </div>
                        {this.state.buttonStatus[22] ? (
                            <div className={this.state.buttonStatus[22] ? 'fancybox-slider active' : 'fancybox-slider hide'}>
                                <span className="behind" onClick={() => this.handleClick(22)}></span>
                                <OwlCarousel
                                    ref="car"
                                    options={options}
                                >
                                    {this.state.buttonStatus[22] ? item.imageUrls.map((key, ind) => (
                                        <div key={ind} className="item-image">
                                            <span className="behind" onClick={() => this.handleClick(22)}></span>
                                            <img src={key}/>
                                        </div>
                                    )) : []}
                                </OwlCarousel>
                                {/*<a onClick={() => this.handleClick(22)}><IconFont name="times" size="2x" /></a>*/}
                            </div>
                        ) : []}
                    </div>
                    :
                    <div className="list-view">
                        <div className="table-list list-number" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>{this.props.number + 1}</div>
                        <div className="table-list list-image" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>
                            <img src={item.imageUrls[0]} alt="item"/>
                        </div>
                        {listName}
                        <div className="table-list list-info" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>{item.info.Category.map((value) => value)}</div>
                        <div className="table-list list-info" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>{item.info.Brand.map((value) => value)}</div>
                        <div className="table-list list-info" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>{item.info.Type.map((value) => value)}</div>
                        {userPermission}
                        <div className="table-list list-status" style={this.props.sidebar ? {display: 'none'} : {display: 'inline-flex'}}>
                            <p>Created: data</p>
                            <p>Requested: data</p>
                            <p>In the work: data</p>
                            <p>Given response: data</p>
                        </div>
                        <div className="table-list list-price" style={this.props.sidebar ? {display: 'none'} : {display: 'flex'}}>{priceAmount}{priceIcon}</div>
                    </div>
                }
            </Col>
        )
    }
}

export  default Models;