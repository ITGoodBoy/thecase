import React, {Component} from "react";
import {Row, Col, Input, Button} from 'react-materialize'
import './SideBar.css';

import '../QuantityLoader/QuantityLoader.css'
import IconFont from '../IconFont/IconFont';
import Products from './Products';
import Models from './Models';
import { sendCaseElements } from '../../services/http';

class SideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: '',
            mode: 'product',
            selectedItems: [],
            email: '',
            buttonStatus: [],
            getNewItem: this.props.activeCase
        };
        this.toggleMode = this.toggleMode.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.targetSortElement = this.targetSortElement.bind(this);
    }

    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];

        this.setState({
            buttonStatus: newStatus,
        });
    }

    toggleMode() {
        this.setState({
            mode: this.state.mode === 'product' ? 'model' : 'product'
        });
    }
    targetSortElement(e) {
        const newSelection = e;
        let newSelectionArray;
        if(this.state.selectedItems.indexOf(newSelection) > -1) {
            newSelectionArray = this.state.selectedItems.filter(s => s !== newSelection)
        } else {
            newSelectionArray = [...this.state.selectedItems, newSelection];
        }
        this.setState({ selectedItems: newSelectionArray });
    }
    componentWillReceiveProps() {
        let self = this;
        setTimeout(function () {
            self.setState({
                getNewItem: self.props.activeCase,
            });
        }, 400);
    }

    render() {
        let productsLength, modelsLength, sendToEmail;
        let list = (
        this.props.isList ?
            <div className="list-card" style={{'padding': '0 15px'}}>
                <div className="list-view">
                    <div className="table-list list-number list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>№</div>
                    <div className="table-list list-image list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>Image</div>
                    <div className="table-list list-name list-description">Name</div>
                    <div className="table-list list-info list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>Category</div>
                    <div className="table-list list-info list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>Brand</div>
                    <div className="table-list list-info list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>Type</div>
                    <div className="table-list list-info list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>Quantity</div>
                    <div className="table-list list-status list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>Status</div>
                    <div className="table-list list-price list-description" style={!this.props.isSidebarFull ? {display: 'none'} : {display: 'inline-flex'}}>Price</div>
                </div>
            </div>
            : ''
        );

        if(!this.state.getNewItem) {
            productsLength = 0;
            modelsLength = 0;
        } else {
            productsLength = this.state.getNewItem.products.map((key) => key).length;
            modelsLength = this.state.getNewItem.models3D.map((key) => key).length;
        }
        if(this.state.buttonStatus[923] === true) {
            sendToEmail = (
                <Row className='send-elements-to-email'>
                    <Col s={12}>
                        <Input s={9} type='email' placeholder='Email address' className='left' value={this.state.email} onChange={(event) => this.setState({email: event.target.value})}/>
                        <Button waves='light' className='right elements-send' onClick={() => {
                            sendCaseElements(this.state.email, this.state.selectedItems)
                                .then(() => {
                                    this.setState({
                                        email: '',
                                        selectedItems: [],
                                        buttonStatus: [],
                                    })
                                })
                        }}>Send</Button>
                        <Button waves='light' className='right red elements-send' onClick={() => {
                            this.handleClick(923);
                            this.setState({
                                email: '',
                                selectedItems: [],
                                buttonStatus: [],
                            })
                        }}>Cancel</Button>
                    </Col>
                </Row>
            )
        } else {
            sendToEmail = false;
        }

        if (this.state.mode === 'product' && this.state.getNewItem) {
            return (
                <Row>
                    <div className={this.state.mode === 'model' ? 'quantity-loader models left' : 'quantity-loader products left'}>
                        <a onClick={this.toggleMode}>
                            <IconFont name="exchange" size="2x" />
                        </a>
                        <p>{this.state.mode === 'model' ? 'Model' : 'Product'}&nbsp;
                            <span className="item-numbers">{productsLength}</span>
                        </p>
                    </div>
                    {sendToEmail}
                    <div className={this.state.mode === 'model' ? 'quantity-loader models right' : 'quantity-loader products right'} style={this.state.buttonStatus[923] ? {display: 'none'} : {display: 'flex'}}>
                        {!this.props.isList || !this.state.selectedItems.length ?
                            ''
                            :
                            <a>
                                <IconFont name="share-alt" size="2x" click={() => this.handleClick(923)} />
                            </a>
                        }
                        <a onClick={() => this.props.toggleList(!this.props.isList)}>
                            {this.props.isList ? <IconFont name="th" size="2x" /> : <IconFont name="list-alt" size="2x" />}
                        </a>
                    </div>
                    {list}
                    <div className={this.props.isList ? 'card-content products' : 'card-content products is-list-container'} style={{'display': this.state.mode === 'product' ? 'inline-flex' : 'none'}}>
                        <div className={this.props.isList ? 'active-case' : 'active-case is-list-content'} style={this.props.isFunnel ? !this.props.isList ? {height: '65.5vh'} : {height: '59.5vh'}: !this.props.isList ? {height: '80.5vh'} : {height: '75.5vh'}}>
                            {this.state.getNewItem.products.map((product, index) => {
                                return (
                                    <Products product={product.item} item={product} permission={this.props.activeCase} key={index} number={index} filters={this.props.filters} activeFilters={this.props.activeFilters}  items={this.state.selectedItems} check={this.targetSortElement} list={!this.props.isList} sidebar={!this.props.isSidebarFull} rebaseData={this.props.rebaseData} funnelEvent={this.props.funnelEvent}  />
                                )
                            })}
                        </div>
                    </div>
                </Row>
            )
        } else if (this.state.mode === 'model' && this.state.getNewItem) {
            return (
                <Row>
                    <div className={this.state.mode === 'model' ? 'quantity-loader models left' : 'quantity-loader products left'}>
                        <a onClick={this.toggleMode}>
                            <IconFont name="exchange" size="2x" />
                        </a>
                        <p>{this.state.mode === 'model' ? 'Model' : 'Product'}&nbsp;
                            <span className="item-numbers">{modelsLength}</span>
                        </p>
                    </div>
                    {sendToEmail}
                    <div className={this.state.mode === 'model' ? 'quantity-loader models right' : 'quantity-loader products right'} style={this.state.buttonStatus[923] ? {display: 'none'} : {display: 'flex'}}>
                        {!this.props.isList || !this.state.selectedItems.length ?
                            ''
                            :
                            <a>
                                <IconFont name="share-alt" size="2x" click={() => this.handleClick(923)} />
                            </a>
                        }
                        <a onClick={() => this.props.toggleList(!this.props.isList)}>
                            <IconFont name={this.props.isList ? "th" : "list-alt" } size="2x" />
                        </a>
                     </div>
                    {list}
                    <div className={this.props.isList ? 'card-content models' : 'card-content models is-list-container'}  style={{'display': this.state.mode === 'model' ? 'inline-flex' : 'none'}}>
                        <div className={this.props.isList ? 'active-case' : 'active-case is-list-content'} style={this.props.isFunnel ? !this.props.isList ? {height: '65.5vh'} : {height: '59.5vh'}: !this.props.isList ? {height: '80.5vh'} : {height: '75.5vh'}}>
                            {this.state.getNewItem.models3D.map((model, index) => {
                                return (
                                    <Models model={model.item} item={model} permission={this.props.activeCase} key={index} number={index} filters={this.props.filters} activeFilters={this.props.activeFilters}  items={this.state.selectedItems} check={this.targetSortElement} list={!this.props.isList} sidebar={!this.props.isSidebarFull} rebaseData={this.props.rebaseData} funnelEvent={this.props.funnelEvent}  />
                                )
                            })}
                        </div>
                    </div>
                </Row>
            )
        } else {
            return (
                <Row>
                    <div className={this.state.mode === 'model' ? 'quantity-loader models' : 'quantity-loader products'}>
                        <a onClick={this.toggleMode}>
                            <IconFont name="exchange" size="2x" />
                        </a>
                        <p>{this.state.mode === 'model' ? 'Model' : 'Product'}&nbsp;
                            <span className="item-numbers">0</span>
                        </p>
                    </div>
                    <div className={this.state.mode === 'model' ? 'quantity-loader models right' : 'quantity-loader products right'}>
                        {!this.props.isList || !this.state.selectedItems.length ?
                            ''
                            :
                            <a>
                                <IconFont name="share-alt" size="2x" click={() => this.handleClick(923)} />
                            </a>
                        }
                        <a onClick={() => this.props.toggleList(!this.props.isList)}>
                            <IconFont name={this.props.isList ? "th" : "list-alt" } size="2x" />
                        </a>
                    </div>
                    {list}
                </Row>
            )
        }
    }

}

export default SideBar;