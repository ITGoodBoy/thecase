import React, {Component} from "react";
import './Users.css';
import {Row, Col, Button, Icon} from 'react-materialize';
import InfiniteScroll from 'react-infinite-scroll-component';
import { usersAllList, userContacts, addToContact, removeFromContact } from '../../services/http';

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            usersList: this.props.commandUsersList,
            userCount: 1,
        };
        this.generateUsers = this.generateUsers.bind(this);
        this.searchUsers = this.searchUsers.bind(this);
        this.addToContact = this.addToContact.bind(this);
        this.removeFromContact = this.removeFromContact.bind(this);
    }
    generateUsers () {
        let moreUsers = [];
        let count = this.state.userCount;
        if(!this.props.items.items.length) {
            let infiniteScroll = document.querySelector('.infinite-scroll-component');
            let noMore = document.createElement('p');
            noMore.className = "no-more-elements";
            noMore.innerHTML = 'No more Users';
            infiniteScroll.appendChild(noMore);
            setTimeout(function(){
                noMore.remove();
            }, 5000);
        } else {
            usersAllList(count)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUsersList', data);
                    }
                })
                .then(() => {
                    this.setState({userCount: this.state.userCount + 1})
                });
        }
        for (let i = 0; i < 1; i++) {
            if(this.props.commandUsersList == undefined || this.state.usersList == undefined || this.state.usersList.items === undefined || this.state.usersList.items.length === 0) {
                return false;
            } else {
                moreUsers.push(
                    this.props.commandUsersList
                );
            }
        }
        this.setState({usersList: this.state.usersList.concat(moreUsers)});
    }

    searchUsers(event) {
        console.log(event.target.value);
        if(!event.target.value.length) {
            usersAllList()
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUsersList', data);
                    }
                });
        } else {
            usersAllList(0, event.target.value)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUsersList', data);
                    }
                });
        }
    }
    addToContact(e, id) {
        e.preventDefault();
        addToContact(id)
            .then(() => {
                userContacts(0)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('commandUserContacts', data);
                        }
                    });
                usersAllList(0)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('commandUsersList', data);
                        }
                    });
            })
    }
    removeFromContact(e, id) {
        e.preventDefault();
        removeFromContact(id)
            .then(() => {
                userContacts(0)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('commandUserContacts', data);
                        }
                    });
                usersAllList(0)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('commandUsersList', data);
                        }
                    });
            })
    }

    componentWillReceiveProps() {
        this.setState({
            usersList: this.props.commandUsersList
        })
    }
    render() {
        let h = window.innerHeight - (80 + 53);
        return (
            <Row className='users-container'>
                <Col s={12} className='user-search'>
                    <input type="search" placeholder="Search Users by Name or Email" onChange={(event) => {
                        this.searchUsers(event);
                    }} />
                </Col>
                <Col s={12} className='user-list list-container'>
                    <Row>
                        <Col s={4}>User Name</Col>
                        <Col s={4}>Email</Col>
                        <Col s={4}>Control</Col>
                    </Row>
                </Col>
                <InfiniteScroll
                    next={this.generateUsers}
                    hasMore={true}
                    height={h}
                >
                    {this.props.commandUsersList.map((user, id) => {
                        let userControl;
                        if(user.friend == false) {
                            userControl = <Col s={4}><a className='btn-flat add-to-contact' onClick={(event) => {this.addToContact(event, user.user.id)}}><Icon>add</Icon> Add to contact</a></Col>
                        } else if(user.friend == true) {
                            userControl = <Col s={4}><a className='btn-flat remove-from-contact' onClick={(event) => {this.removeFromContact(event, user.user.id)}}><Icon>remove</Icon></a></Col>
                        } else {
                            userControl = false;
                        }
                        return (
                            <Col key={id} s={12} className='user-list'>
                                <Row>
                                    <Col s={4}>{user.user.username}</Col>
                                    <Col s={4}>{user.user.email}</Col>
                                    {userControl}
                                </Row>
                            </Col>
                        )
                    })}
                </InfiniteScroll>
            </Row>
        )
    }
}

export default User
