import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../actions/index';

import Users from './Users';

const mapStateToProps = ({app: {commandUsersList}}, ownProps) => {
    return {
        commandUsersList,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const UsersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Users);

export default UsersContainer;