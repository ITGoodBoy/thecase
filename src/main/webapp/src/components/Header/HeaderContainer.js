import {connect} from 'react-redux';

import {toggleProjectsFunc, toggleSideBarFullFunc, toggleSideBarFunc, rebaseDataFunc } from '../../actions/index';
import Header from './Header';

const mapStateToProps = ({app: {isSidebarFull, isProjects, isSidebar}}, ownProps) => {
    return {
        isSidebarFull,
        isProjects,
        isSidebar,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSideBarFull (property) {
            toggleSideBarFullFunc(property)(dispatch);
        },
        toggleProjects (property) {
            toggleProjectsFunc(property)(dispatch);
        },
        toggleSideBar (property) {
            toggleSideBarFunc(property)(dispatch);
        },
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const HeaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);

export default HeaderContainer;