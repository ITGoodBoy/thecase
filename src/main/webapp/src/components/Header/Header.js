import React, {Component} from "react";
import {Navbar, NavItem, SideNav, SideNavItem, Button, Icon, Modal, Row, Input} from 'react-materialize'

import Logo from "./Thecase_Logo.svg";
import './Header.css';
import LogIn from '../Log/LogIn'
import SignUp from '../Log/SignUp'
import ReplacePassword from '../Log/ReplacePassword'
import IconFont from '../IconFont/IconFont';
import { parseJwt, Logout, createCases, getCases } from '../../services/http';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            caseName: '',
        };
        this.createCase = this.createCase.bind(this);
    }
    createCase() {
        let name = this.state.caseName;
        createCases(name)
            .then(() => {
                getCases()
                    .then((data) => {
                        this.props.rebaseData('cases', data);
                    });
            })
    };
    render() {
        let didLogin = this.props.didLogin;
        let adminPanel, isSidebarFullOpened, homeLocation, homeCase;
        if(didLogin && parseJwt().admin === true) {
            adminPanel = (
                <div>
                    <SideNavItem href='/admin.tools' icon='perm_data_setting'>Admin Panel</SideNavItem>
                    <SideNavItem href='/admin.tools/items' icon='add'>Add Item</SideNavItem>
                    <SideNavItem divider />
                </div>
            )
        } else {
            adminPanel = false;
        }
        if(!this.props.isSidebarFull) {
            isSidebarFullOpened = !this.props.isSidebarFull;
        } else {
            isSidebarFullOpened = this.props.isSidebarFull;
        }
        if (window.location.pathname == '/') {
            homeLocation = '#';
            homeCase = <SideNavItem waves href='/main' icon="widgets">Landing</SideNavItem>
        } else if(window.location.pathname == '/main') {
            homeLocation = '/';
            homeCase = <SideNavItem waves href='/' icon="tab_unselected">The Case</SideNavItem>
        } else {
            homeLocation = '/';
            homeCase = <div><SideNavItem waves href='/' icon="tab_unselected">The Case</SideNavItem><SideNavItem waves href='/main' icon="widgets">Landing</SideNavItem></div>
        }
        return (
            <div className='head-component' >
                <Navbar className='nav-head' brand={<img src={Logo} alt='Logo' className="responsive-img" />} right>
                    {!didLogin ? (
                        <div>
                            <NavItem><LogIn rebaseCallback={this.props.rebaseCallback} /></NavItem>
                            <NavItem><SignUp rebaseCallback={this.props.rebaseCallback}/></NavItem>
                        </div>
                    ) : (
                        <div>
                            <NavItem className='open-projects valign-wrapper' onClick={() => {
                                this.props.toggleProjects(!this.props.isProjects);
                                this.props.toggleSideBarFull(isSidebarFullOpened);
                            }}><IconFont name={this.props.isProjects ? 'folder-open-o' : 'folder-o'} /></NavItem>
                            <NavItem className='create-project valign-wrapper'>
                                <Modal
                                    header='Create New Case'
                                    actions={[<Button waves='light' modal='close' flat>Close</Button>,
                                        <Button waves='light' className="btn blue darken-2" modal="close" onClick={this.createCase} disabled={!this.state.caseName}>
                                            Create
                                        </Button>]}
                                    trigger={
                                        <span><IconFont name='plus' /></span>
                                    }>
                                    <Row>
                                        <Input s={12} label="Case Name"
                                               type='text'
                                               onChange={event => this.setState({caseName: event.target.value})}
                                        />
                                    </Row>
                                </Modal>
                            </NavItem>
                            <NavItem href='#' className='user valign-wrapper'>HI, {parseJwt().username}</NavItem>
                            <SideNav
                                trigger={<Button className='btn-flat'><Icon right>sort</Icon></Button>}
                                options={{
                                    closeOnClick: true,
                                    edge: 'right',
                                    draggable: true,
                                }}
                            >
                                <SideNavItem userView
                                             user={{
                                                 background: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfWiLKw38MHb26vmUPn_scmOYMU0djbibmSzNouxISQiHq-lPnSg',
                                                 name: 'Hi, ' + parseJwt().username,
                                                 email: 'Role: ' + parseJwt().role,
                                             }}
                                />
                                {adminPanel}
                                {/*<SideNavItem waves icon="create_new_folder">*/}
                                    {/*<Modal*/}
                                        {/*header='Create New Case'*/}
                                        {/*actions={[<Button waves='light' modal='close' flat>Close</Button>,*/}
                                            {/*<Button waves='light' className="btn blue darken-2" modal="close" onClick={this.createCase} disabled={!this.state.caseName}>*/}
                                                {/*Create*/}
                                            {/*</Button>]}*/}
                                        {/*trigger={*/}
                                            {/*<span>Create case</span>*/}
                                        {/*}>*/}
                                        {/*<Row>*/}
                                            {/*<Input s={12} label="Case Name"*/}
                                                   {/*type='text'*/}
                                                   {/*onChange={event => this.setState({caseName: event.target.value})}*/}
                                            {/*/>*/}
                                        {/*</Row>*/}
                                    {/*</Modal>*/}
                                {/*</SideNavItem>*/}
                                {/*<SideNavItem waves href={homeLocation} icon="folder_open" onClick={() => {*/}
                                    {/*this.props.toggleProjects(!this.props.isProjects);*/}
                                    {/*this.props.toggleSideBarFull(isSidebarFullOpened);*/}

                                {/*}}>{this.props.isProjects ? 'Close Case Room' : 'Open Case Room'}</SideNavItem>*/}
                                {/*<SideNavItem divider />*/}
                                <SideNavItem waves href='/users' icon="person_add">Contacts Network</SideNavItem>
                                <SideNavItem waves href='/mycontacts' icon="person">My Contacts</SideNavItem>
                                <SideNavItem divider />
                                {homeCase}
                                <SideNavItem waves icon="mode_edit"><ReplacePassword /></SideNavItem>
                                <SideNavItem waves href='/main' onClick={() => Logout()} icon="exit_to_app">Log Out</SideNavItem>
                            </SideNav>
                        </div>
                    )}
                </Navbar>
            </div>
        );
    }
}

export default Header;