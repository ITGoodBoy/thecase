import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NavItemCustom extends Component {
    render () {
        const { divider, children, ...props } = this.props;
        if (divider) {
            return <li className='divider' />;
        } else {
            return (
                <li {...props}>
                    {children}
                </li>
            );
        }
    }
}

NavItemCustom.propTypes = {
    children: PropTypes.node,
    divider: PropTypes.bool,
    href: PropTypes.string
};

export default NavItemCustom;
