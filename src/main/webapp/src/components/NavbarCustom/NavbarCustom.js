
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Col from 'react-materialize/lib/Col';

class NavbarCustom extends Component {
  constructor(props) {
    super(props);
  }

  render () {
    const {
      className,
      fixed,
      left,
      right,
      href,
      ...other
    } = this.props;

    delete other.options;


    let classes = {
      right: right
    };

    let content = (
      <nav {...other} className={className}>
        <div className='nav-wrapper'>
          <Col s={12}>
            <ul className={cx(className, classes)}>
              {this.props.children}
            </ul>
          </Col>
        </div>
      </nav>
    );

    if (fixed) {
      content = <div className='navbar-fixed'>{content}</div>;
    }

    return content;
  }
}


NavbarCustom.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  left: PropTypes.bool,
  right: PropTypes.bool,
  href: PropTypes.string,
  /**
   * Makes the navbar fixed
   */
  fixed: PropTypes.bool,
  /**
   * Options hash for the sidenav.
   * More info: http://materializecss.com/side-nav.html#options
   */
  options: PropTypes.shape({
    menuWidth: PropTypes.number,
    edge: PropTypes.oneOf(['left', 'right']),
    closeOnClick: PropTypes.bool,
    draggable: PropTypes.bool
  })
};

NavbarCustom.defaultProps = {
  href: '/',
  options: {}
};

export default NavbarCustom;
