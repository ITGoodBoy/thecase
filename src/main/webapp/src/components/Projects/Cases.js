import React, {Component} from "react";
import {Row, Col, Modal, Button, Input} from 'react-materialize';
import DefaulItmage from "./defaulItmage.png";
import Company from "./company.png";
import InfiniteScroll from 'react-infinite-scroll-component';

import '../CardFlip/CardFlip.scss';
import IconFont from '../IconFont/IconFont';
import { getActivateCase, focusCases, editCases, getCases, removeCases, userContacts, followToCase, acceptFollow, cancelFollow, getUserFromTeamWork, sendFullCase } from '../../services/http';

class Cases extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonStatus: [],
            isFront: true,
            data: [],
            caseName: this.props.myCase.name,
            myContacts: this.props.users,
            User: '',
            Permission: 0,
            emailToSendPdf: '',
        };

        this.toggle = this.toggle.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.targetUser = this.targetUser.bind(this);
        this.editCase = this.editCase.bind(this);
        this.generateUsers = this.generateUsers.bind(this);
        this.targetPermission = this.targetPermission.bind(this);
    }


    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];
        this.setState({buttonStatus: newStatus});
    }

    toggle() {
        this.setState({
            isFront: !this.state.isFront
        });
    }
    targetUser(e) {
        const newSelection = e.target.value;
        if(!newSelection.length) {
            this.setState({ User: '' });
        } else {
            this.setState({ User: newSelection });
        }
    }
    targetPermission(e) {
        const newSelection = e.target.value;
        if(newSelection == 0) {
            this.setState({ Permission: '' });
        } else {
            this.setState({ Permission: newSelection });
        }
    }
    followUser(caseId) {
        let user = this.state.User;
        let permission = this.state.Permission;
        followToCase(caseId, user, permission)
    }
    editCase() {
        let name = this.state.caseName;
        let id = this.props.myCase.id;
        editCases(id, name)
            .then(() => {
                getCases()
                    .then((data) => {
                        this.props.rebaseData('cases', data)
                    });
                focusCases()
                    .then((data) => {
                        this.props.rebaseData('activeCase', data)
                    });
            })
    };
    generateUsers () {
        let moreItems = [];
        let count = this.state.myContacts.length - 19;
        if(count <= 0 || !this.state.myContacts.length) {
            return false
        } else {
            userContacts(count++)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUserContacts', data);
                    }
                });
        }
        for (let i = 0; i < 1; i++) {
            if(this.props.users == undefined || this.state.myContacts == undefined || this.state.myContacts.items === undefined || this.state.myContacts.items.length === 0) {
                return false
            } else {
                moreItems.push(
                    this.props.users
                );
            }
        }
        this.setState({myContacts: this.state.myContacts.concat(moreItems)});
    }

    deleteCase(id) {
        removeCases(id)
            .then(() => {
                getCases()
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('cases', data)
                        }
                    });
                focusCases()
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('activeCase', data)
                        }
                    })
            })
    }

    render() {
        let cases = this.props.myCase;
        let myProject = 'Project - ' + this.props.myCase.name;
        let myProjectSlice, confirmed, teamWork;
        if(cases.confirmed == false) {
            confirmed = (
                <div className="team-confirmed">
                    <a className="team-accept" onClick={() => {
                        acceptFollow(cases.id)
                            .then(() => {
                                getCases()
                                    .then((data) => {
                                        this.props.rebaseData('cases', data);
                                    });
                                focusCases()
                                    .then(data => {
                                        this.props.rebaseData('activeCase', data)
                                    });
                            })
                    }}>Accept</a>
                    <a className="team-cancel" onClick={() => {
                        cancelFollow(cases.id)
                            .then(() => {
                                getCases()
                                    .then((data) => {
                                        this.props.rebaseData('cases', data);
                                    });
                                focusCases()
                                    .then(data => {
                                        this.props.rebaseData('activeCase', data)
                                    });
                            })
                    }}>Cancel</a>
                </div>
            )
        } else {
            confirmed = false;
        }

        if(myProject.length < 40) {
            myProjectSlice = myProject;
        } else {
            myProjectSlice = myProject.slice(0, 40) + '...';
        }
        if(this.state.buttonStatus[cases.id] === true && this.props.getUserFromTeamWork.users.length && this.props.getUserFromTeamWork.caseId === cases.id) {
            teamWork = (
                <Row key={cases.id} className="all-team">
                    <Input s={12} type='select' label="Followed Users" defaultValue='2'>
                        {this.props.getUserFromTeamWork.users.map((user, id) => {
                            return (
                                <option key={id} value={user.id}>{user.username}</option>
                            )
                        })}
                    </Input>
                </Row>
            )
        } else {
            teamWork = false;
        }

        return (
            <Col s={3} className='grid-case' style={{'background': cases.active === true ? '#f9f9f9' : '#ffffff'}} >
                {confirmed}
                {/*{teamWork}*/}
                <div className="case-images">
                    <div onClick={() =>
                        getActivateCase(cases.id)
                            .then(() => {
                                focusCases()
                                    .then((data) => {
                                        if(data) {
                                            this.props.rebaseData('activeCase', data)
                                        }
                                    });
                            })
                            .then(() => {
                                this.props.toggleProjects();
                            })
                    }>
                        <div className="full-image">
                            <img src={cases.imageUrls[0] ? cases.imageUrls[0] : DefaulItmage} alt="item"/>
                        </div>
                        <div className="small-image">
                            <img src={cases.imageUrls[1] ? cases.imageUrls[1] : DefaulItmage} alt="item"/>
                        </div>
                        <div className="small-image">
                            <img src={cases.imageUrls[2] ? cases.imageUrls[2] : DefaulItmage} alt="item"/>
                        </div>
                    </div>
                    <div className="hover-icons">
                        <Modal
                            header='Case Options'
                            actions={[
                                <Button waves='light' modal='close' flat onClick={() => {this.setState({caseName: cases.name}) }}>
                                    Close
                                </Button>,
                                <Button waves='light' className="btn blue darken-2" modal="close" onClick={this.editCase} disabled={!this.state.caseName}>
                                    Edit
                                </Button>]}
                            trigger={
                                <Button flat>
                                    <IconFont id="pencil" name="pencil" size="2x" border={true} />
                                </Button>
                            }>
                            <Row>
                                <Input s={12} name='caseName' type='text' defaultValue={this.state.caseName} label="Case Name" onChange={(event) => this.setState({caseName: event.target.value})}
                                />
                            </Row>
                        </Modal>
                        <Modal
                            header='Follow User'
                            actions={[<Button waves='light' modal='close' flat onClick={() => {
                                this.setState({
                                    User: '',
                                    Permission: 0,
                                })
                            }}>Cancel</Button>,
                                <Button waves='light' className="btn blue darken-2" modal="close" onClick={() => {this.followUser(this.props.myCase.id)}} disabled={!this.state.caseName}>
                                    Follow
                                </Button>]}
                            trigger={
                                <Button flat>
                                    <IconFont id="user-plus" name="user-plus" size="2x" border={true} />
                                </Button>
                            }>
                            <Row>
                                <Col s={12} className='user-list'>
                                    <Row>
                                        <Col s={5}>
                                            <InfiniteScroll
                                                next={this.generateUsers}
                                                hasMore={true}
                                                height={450}
                                            >
                                                {this.state.myContacts.map((user, id) => {
                                                    return (
                                                        <Input key={id} s={12} name='users' type='radio' value={user.id} label={user.username} onChange={(event) => {
                                                            this.targetUser(event, id);
                                                        }} />
                                                    )
                                                })}
                                            </InfiniteScroll>
                                        </Col>
                                        <Col s={7}>
                                            <Input s={12} type='select' label="Permission Select" defaultValue={this.state.Permission} onChange={(event) => {this.targetPermission(event)}}>
                                                <option value='0'>Select</option>
                                                <option value='1'>Review</option>
                                                <option value='2'>Editing price and quantity</option>
                                                <option value='3'>Full access</option>
                                                <option value='4'>Mega access</option>
                                            </Input>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                        {/*<Button flat>*/}
                            {/*<IconFont id="user" name="user" size="2x" border={true} click={() => {*/}
                                {/*getUserFromTeamWork(cases.id)*/}
                                    {/*.then(data => {*/}
                                        {/*if(data) {*/}
                                            {/*this.props.rebaseData('getUserFromTeamWork', data);*/}
                                        {/*}*/}
                                    {/*})*/}
                                    {/*.then(() => {*/}
                                        {/*this.handleClick(cases.id)*/}
                                    {/*});*/}
                            {/*}} />*/}
                        {/*</Button>*/}
                        <Modal
                            header='Send full case to mail'
                            actions={[<Button waves='light' modal='close' flat>Close</Button>,
                                <Button waves='light' className="btn blue darken-2" modal="close" onClick={() => {
                                    sendFullCase(cases.id, this.state.emailToSendPdf)
                                        .then(() => {
                                            this.setState({emailToSendPdf: ''})
                                        })
                                }} disabled={!this.state.caseName}>
                                    Send
                                </Button>]}
                            trigger={
                                <Button flat>
                                    <IconFont id="paper-plane" name="paper-plane" size="2x" border={true} />
                                </Button>
                            }>
                            <Row>
                                <Input s={12} label="Email"
                                       type='email'
                                       value={this.state.emailToSendPdf}
                                       onChange={(event) => this.setState({emailToSendPdf: event.target.value})}
                                />
                            </Row>
                        </Modal>

                        <Modal
                            id="delete-case"
                            header='Delete case'
                            actions={[ <Button waves='light' modal='close' flat>Cancel</Button>,
                                <Button waves='light' className="btn btn-secondary"  modal='close' onClick={() => {this.deleteCase(cases.id)}}>Delete</Button>
                            ]}
                            trigger={
                                <Button flat><IconFont name="remove" size="2x" border={true} /></Button>
                            }
                        >
                        </Modal>

                    </div>
                </div>
                <div className="case-bottom-elements">
                    <div>
                        <span>{cases.productsCount} pro </span>
                        <span>{cases.models3dCount} mod</span>
                    </div>
                    <div>{myProjectSlice}</div>
                </div>
                <div className="case-bottom-elements case-description">
                    <div className="case-social case-partner">
                        <img className="partner-img parner-info" src={Company} alt="company"/>
                        <span className="partner-name parner-info">{cases.ownerName}</span>
                    </div>
                    <div className="case-social case-follow-buttons">
                        <IconFont id="fa-envelope" className="follow-button" name="envelope" size="2x" border={false} />
                        <IconFont id="fa-commenting" className="follow-button" name="commenting" size="2x" border={false} />
                        <IconFont id="fa-phone" className="follow-button" name="phone" size="2x" border={false} />
                    </div>
                </div>
            </Col>
        )
    }
}

export  default Cases;