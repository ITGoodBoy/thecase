import React, {Component} from "react";

import {Row, Modal, Button, Input, Col, CardPanel} from 'react-materialize'
import Cases from './Cases';

import './Projects.css';
import { getCases, createCases, focusCases, getAccess } from '../../services/http';

class Projects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mode: 'product',
            getNewCases: this.props.cases,
            caseName: '',
            access: this.props.userPermission
        };
        this.toggleMode = this.toggleMode.bind(this);
        this.createCase = this.createCase.bind(this);

        getAccess()
            .then(data => {
                if(data) {
                    this.props.rebaseData('userPermission', data)
                }
            })
            .then(() => {
                this.setState({
                    access: this.props.userPermission
                })
            })
    }

    toggleMode() {
        this.setState({
            mode: this.state.mode === 'product' ? 'model' : 'product'
        });
    }
    createCase() {
        let name = this.state.caseName;
        createCases(name)
            .then(() => {
                getCases()
                    .then((data) => {
                        this.props.rebaseData('cases', data);
                    })
                    .then(() => {
                            this.setState({
                                getNewCases: this.props.cases
                            });
                    });
                focusCases()
                    .then(data => {
                        this.props.rebaseData('activeCase', data)
                    })
            })
            .then(() => {
                getAccess()
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('userPermission', data)
                        }
                    })
                    .then(() => {
                        this.setState({
                            access: this.props.userPermission
                        })
                    })
            })
    };
    componentWillReceiveProps() {
        let self = this;
        setTimeout(function () {
            self.setState({
                getNewCases: self.props.cases
            });
        }, 400);
    }
    render() {
        let access = this.state.access.can_create_case;
        let cases = this.state.getNewCases;
        let funnelActive, projectActive;

        if(this.props.isProjects && !this.props.isFunnel) {
            projectActive = 'project-container active-project-container';
            funnelActive = '';
        }
        if (this.props.isProjects && this.props.isFunnel) {
            projectActive = 'project-container active-project-container';
            funnelActive = 'funnelIsActive ';
        }
        if (!this.props.isProjects && !this.props.isFunnel) {
            projectActive = 'project-container deactive-project-container';
            funnelActive = '';
        }
        if (!this.props.isProjects && this.props.isFunnel) {
            projectActive = 'project-container deactive-project-container';
            funnelActive = 'funnelIsActive ';
        }
        if (!!localStorage.getItem('X-Auth-Token') && this.props.cases.length) {
            return (
                <Row className={projectActive + ' ' + funnelActive}>
                    <div className='quantity-loader create-case'>
                        {!!access ?
                            (
                                <Modal
                                    header='Create New Case'
                                    actions={[<Button waves='light' modal='close' flat>Close</Button>,
                                        <Button waves='light' className="btn blue darken-2" modal="close" onClick={this.createCase} disabled={!this.state.caseName}>
                                            Create
                                        </Button>]}
                                    trigger={
                                        <Button floating className='yellow-text text-darken-3' waves='light' icon='add' style={{backgroundColor: '#f9a825 '}} />
                                    }>
                                    <Row>
                                        <Input s={12} label="Case Name"
                                               type='text'
                                               onChange={event => this.setState({caseName: event.target.value})}
                                        />
                                    </Row>
                                </Modal>
                            ) :  null
                        }
                        <p>Cases&nbsp;
                            <span className="item-numbers">{cases.length}</span>
                        </p>
                    </div>
                    <div className="cases" style={{'display': 'inline-flex'}}>
                        {cases.map((myCase, index) => {
                            return (
                                <Cases myCase={myCase} key={index} rebaseData={this.props.rebaseData} baseImageUrl={this.props.baseImageUrl} toggleProjects={this.props.toggleProjects} users={this.props.commandUserContacts} getUserFromTeamWork={this.props.getUserFromTeamWork} />
                            )
                        })}

                        {!access ?
                            (
                                <Col s={3} className='grid-case'>
                                    <CardPanel className='blue-grey darken-1 white-text maximum-height column'>
                                        <div className="margin-auto">More cases? Upgrade your current plan!</div>
                                        <p><Button waves='light' className="btn-secondary">Upgrade</Button></p>
                                    </CardPanel>
                                </Col>
                            ) : null
                        }
                    </div>
                    <div id="modal1" className="modal">
                        <div className="modal-content">
                            <h4>Modal Header</h4>
                            <p>A bunch of text</p>
                        </div>
                        <div className="modal-footer">
                            <a href="#!" className="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
                        </div>
                    </div>
                </Row>
            )
        } else {
            return (
                <Row className={projectActive + ' ' + funnelActive}>
                    <div className='quantity-loader create-case'>
                        <Modal
                            header='Create New Case'
                            actions={[<Button waves='light' modal='close' flat>Close</Button>,
                                <Button waves='light' className="btn blue darken-2" modal="close" onClick={this.createCase} disabled={!this.state.caseName}>
                                    Create
                                </Button>]}
                            trigger={
                                <Button floating className='yellow-text text-darken-3' waves='light' icon='add' style={{backgroundColor: '#f9a825 '}} />
                            }>
                            <Row>
                                <Input s={12} label="Case Name"
                                       type='text'
                                       onChange={event => this.setState({caseName: event.target.value})}
                                />
                            </Row>
                        </Modal>
                        <p>Cases&nbsp;
                            <span className="item-numbers">{cases.length}</span>
                        </p>
                        <div>
                            Demo Account
                        </div>
                    </div>
                </Row>
            )
        }
    }
}

export default Projects;