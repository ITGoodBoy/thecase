import {connect} from 'react-redux';

import { toggleSideBarFunc, rebaseDataFunc, addToFolderFunc, toggleSideBarFullFunc, toggleProjectsFunc } from '../../actions/index';
import Projects from './Projects';

const mapStateToProps = ({app: {isSidebar, isSidebarFull, isProjects, isFunnel, sidebars, cases, commandUserContacts, getUserFromTeamWork, baseImageUrl, userPermission}}, ownProps) => {
    return {
        isSidebar,
        isSidebarFull,
        isProjects,
        isFunnel,
        sidebars,
        cases,
        commandUserContacts,
        userPermission,
        getUserFromTeamWork,
        baseImageUrl,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSideBar (property) {
            toggleSideBarFunc(property)(dispatch);
        },
        toggleSideBarFull (property) {
            toggleSideBarFullFunc(property)(dispatch);
        },
        toggleProjects (property) {
            toggleProjectsFunc(property)(dispatch);
        },
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
        addToFolder(type, element) {
            addToFolderFunc(type, element)(dispatch);
        }
    }
};

const ProjectsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Projects);

export default ProjectsContainer;