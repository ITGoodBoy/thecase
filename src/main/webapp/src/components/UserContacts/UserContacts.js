import React, {Component} from "react";
import {Row, Col, Button, Icon, Modal, Collection, CollectionItem, Input} from 'react-materialize';
import InfiniteScroll from 'react-infinite-scroll-component';

import '../Users/Users.css';
import IconFont from '../IconFont/IconFont';
import { userContacts, usersAllList, removeFromContact, followToCase } from '../../services/http';

class UserContacts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonStatus: [],
            myContacts: this.props.commandUserContacts,
            userCount: 1,
            User: '',
            Permission: 0,
            caseId: this.props.activeCase.id
        };
        this.handleClick = this.handleClick.bind(this);
        this.generateUsers = this.generateUsers.bind(this);
        this.removeFromContacts = this.removeFromContacts.bind(this);
        this.followUser = this.followUser.bind(this);
        this.targetPermission = this.targetPermission.bind(this);
    }
    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];
        this.setState({
            buttonStatus: newStatus,
        });
    }
    generateUsers () {
        let moreUsers = [];
        let count = this.state.userCount;
        if(!this.props.items.items.length) {
            let infiniteScroll = document.querySelector('.infinite-scroll-component');
            let noMore = document.createElement('p');
            noMore.className = "no-more-elements";
            noMore.innerHTML = 'No more Users';
            infiniteScroll.appendChild(noMore);
            setTimeout(function(){
                noMore.remove();
            }, 5000);
        } else {
            userContacts(count)
                .then(data => {
                    if(data) {
                        this.props.rebaseData('commandUserContacts', data);
                    }
                })
                .then(() => {
                    this.setState({userCount: this.state.userCount + 1})
                })
        }
        for (let i = 0; i < 1; i++) {
            if(this.props.commandUserContacts == undefined || this.state.myContacts == undefined || this.state.myContacts.items === undefined || this.state.myContacts.items.length === 0) {
                return false
            } else {
                moreUsers.push(
                    this.props.commandUserContacts
                );
            }
        }
        this.setState({myContacts: this.state.myContacts.concat(moreUsers)});
    }
    removeFromContacts(event, id) {
        event.preventDefault();
        removeFromContact(id)
            .then(() => {
                userContacts(0)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('commandUserContacts', data);
                        }
                    })
                    .then(() => {
                        this.setState({
                            myContacts: this.props.commandUserContacts
                        })
                    });
                usersAllList(0)
                    .then(data => {
                        if(data) {
                            this.props.rebaseData('commandUsersList', data);
                        }
                    });
            });
    }

    followUser(userId) {
        let user = userId;
        let caseId = this.state.caseId;
        let permission = this.state.Permission;
        followToCase(caseId, user, permission)
    }

    targetPermission(e) {
        const newSelection = e.target.value;
        if(newSelection == 0) {
            this.setState({ Permission: '' });
        } else {
            this.setState({ Permission: newSelection });
        }
    }

    render() {
        let h = window.innerHeight - (80 + 53);
        let contactNone, listContainer;
        if(!this.state.myContacts.length) {
            listContainer = false;
            contactNone = <p className="center">You don't have any contacts yet, you can add contacts using this <a href="/users">link</a></p>
        } else {
            listContainer = (
                <Col s={12} className='user-list list-container'>
                    <Row>
                        <Col s={3}>User Name</Col>
                        <Col s={3}>Email</Col>
                        <Col s={3}>Control</Col>
                        <Col s={3}>Share case</Col>
                    </Row>
                </Col>
            );
            contactNone = false;
        }
        console.log(this.state.Permission)
        return (
            <Row className='users-container'>
                {listContainer}
                {contactNone}
                <InfiniteScroll
                    next={this.generateUsers}
                    hasMore={true}
                    height={h}
                >
                    {this.state.myContacts.map((user, id) => {
                        return (
                            <Col key={id} s={12} className='user-list'>
                                {this.state.buttonStatus[23] ?
                                    (
                                        <div className="to-project">
                                            <Row className="choose-projects">
                                                <Col s={12} className="projects-head">
                                                    <p className="heading">Follow {user.username}</p>
                                                    <a onClick={() => {
                                                        this.handleClick(23);
                                                        this.setState({
                                                            User: '',
                                                            Permission: 0,
                                                            caseId: this.props.activeCase.id
                                                        })
                                                    }}><IconFont name="times" /></a>
                                                </Col>
                                                <Col s={6}>
                                                    {this.props.cases.map((key, ind) => (
                                                        <Col s={12} className={key.id.indexOf(this.state.caseId) ? 'modal-projects' : 'modal-projects projects-is-active'} key={ind} onClick={(event) => {
                                                            event.preventDefault();
                                                            this.setState({caseId: key.id});
                                                        }}>
                                                            <IconFont name={key.id.indexOf(this.state.caseId) ? 'folder' : 'folder-open'} />
                                                            <p>{key.name}</p>
                                                        </Col>
                                                    ))}
                                                </Col>
                                                <Col s={6}>
                                                    <Input s={12} type='select' label="Permission Select" defaultValue={this.state.Permission} onChange={(event) => {this.targetPermission(event)}}>
                                                        <option value='0'>Select</option>
                                                        <option value='1'>Review</option>
                                                        <option value='2'>Editing price and quantity</option>
                                                        <option value='3'>Full access</option>
                                                    </Input>
                                                </Col>
                                                <Col s={12} className="projects-buttons">
                                                    <button onClick={() => {
                                                        this.handleClick(23);
                                                        this.setState({
                                                            User: '',
                                                            Permission: 0,
                                                            caseId: this.props.activeCase.id
                                                        })
                                                    }} className="project-cancel">Cancel</button>
                                                    <button className="project-save" onClick={() => {
                                                        this.followUser(user.id)
                                                    }}>Follow</button>
                                                </Col>
                                            </Row>
                                        </div>
                                    )
                                    :
                                    []
                                }
                                <Row>
                                    <Col s={3}>{user.username}</Col>
                                    <Col s={3}>{user.email}</Col>
                                    <Col s={3}><a className='btn-flat remove-from-contact' onClick={(event) => this.removeFromContacts(event, user.id) }><Icon>remove</Icon></a></Col>
                                    <Col s={3} className='follow-user-to-case'>
                                        <IconFont name="share-alt" size="2x" click={() => {
                                            this.handleClick(23);
                                        }}/>
                                    </Col>
                                </Row>
                            </Col>
                        )
                    })}
                </InfiniteScroll>
            </Row>
        )
    }
}

export default UserContacts
