import {connect} from 'react-redux';

import { rebaseDataFunc } from '../../actions/index';

import UserContacts from './UserContacts';

const mapStateToProps = ({app: {commandUserContacts, cases, activeCase}}, ownProps) => {
    return {
        commandUserContacts,
        cases,
        activeCase,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
    }
};

const UserContactsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserContacts);

export default UserContactsContainer;