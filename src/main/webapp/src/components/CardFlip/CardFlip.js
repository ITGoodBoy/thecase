import React, {Component} from "react";
import {Row, Col} from 'react-materialize'
import InfiniteScroll from 'react-infinite-scroll-component';
import { getItems, getModels, getCategories, getBrands, getTypes, getSortedTags } from '../../services/http';

import './CardFlip.css';
import '../QuantityLoader/QuantityLoader.css'
import IconFont from '../IconFont/IconFont';
import Item from '../Item/Item';
import Model from '../Model/Model';

class CardFlip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonStatus: [],
            mode: 'product',
            items: [],
            models: [],
            itemCount: 1,
            modelCount: 1,
            likedState: [],
        };

        this.handleClick = this.handleClick.bind(this);
        this.likeHandle = this.likeHandle.bind(this);
        this.toggleMode = this.toggleMode.bind(this);
        this.generateItems = this.generateItems.bind(this);
        this.generateModels = this.generateModels.bind(this);
        this.updateItem = this.updateItem.bind(this);

        getItems(0)
            .then(data => {
                if(data) {
                    this.props.rebaseData('items', data);
                    this.setState({items: data.items})
                }
            });
        getModels(0)
            .then(data => {
                if(data) {
                    this.props.rebaseData('models', data);
                    this.setState({models: data.items})
                }
            });
        getCategories()
            .then(data => {
                if(data) {
                    this.props.rebaseData('categories', data);
                }
            });

        getBrands()
            .then(data => {
                if(data) {
                    this.props.rebaseData('getBrands', data);
                }
            });
        getTypes()
            .then(data => {
                if(data) {
                    this.props.rebaseData('types', data);
                }
            });
        getSortedTags()
            .then(data => {
                if(data) {
                    this.props.rebaseData('sortedTags', data);
                }
            });
    }

    handleClick(i) {
        const newStatus = this.state.buttonStatus.slice();
        newStatus[i] = !this.state.buttonStatus[i];
        this.setState({buttonStatus: newStatus});
    }

    likeHandle(i) {
        const newStatus = this.state.likedState.slice();
        newStatus[i] = !this.state.likedState[i];
        this.setState({likedState: newStatus});
    }

    toggleMode() {
        this.setState({
            mode: this.state.mode === 'product' ? 'model' : 'product'
        });
        if(this.props.funnelActive == false) {
            this.props.funnelEvent(!this.props.funnelActive);
        } else {
            return false;
        }
    }
    updateItem() {
        if(this.state.mode === 'Products') {
            getItems()
                .then((data) => {
                    this.props.rebaseData('items', data);
                })
        } else if(this.state.mode === 'Models') {
            getModels()
                .then((data) => {
                    this.props.rebaseData('models', data);
                })
        } else {
            return false;
        }
    }

    generateItems () {
        let moreItems = [];
        let self = this;
        let count = this.state.itemCount;
        getItems(count)
            .then(data => {
                for (let i = 0; i < 1; i++) {
                    if(data.items == undefined || self.state.items == undefined  || self.state.items.length === 0) {
                        return false
                    } else {
                        moreItems = (
                            data.items
                        );
                    }
                }
                if(self.props.funnelActive == false) {
                    self.setState({items: self.state.items.concat(moreItems)});
                } else {
                    self.props.funnelEvent(!self.props.funnelActive);
                    self.setState({items: self.state.items.concat(moreItems)});
                }
                if(data) {
                    this.props.rebaseData('items', data);
                }
            })
            .then(() => {
                if(!self.props.items.items.length) {
                    let infiniteScroll = document.querySelector('.card-content.products .infinite-scroll-component');
                    let noMore = document.createElement('div');
                    noMore.className = "no-more-elements";
                    noMore.innerHTML = 'Yay! You have seen it all';
                    infiniteScroll.appendChild(noMore);
                    setTimeout(function(){
                        noMore.remove();
                    }, 5000);
                } else {
                    self.setState({itemCount: self.state.itemCount + 1})
                }
            })
    }

    generateModels () {
        let moreModels = [];
        let self = this;
        let count = this.state.modelCount;
        getModels(count)
            .then(data => {
                for (let i = 0; i < 1; i++) {
                    if(data.items == undefined || self.state.models == undefined  || self.state.models.length === 0) {
                        console.log('false')
                        return false
                    } else {
                        moreModels = (
                            data.items
                        );
                    }
                }
                if(self.props.funnelActive == false) {
                    self.setState({models: self.state.models.concat(moreModels)});
                } else {
                    self.props.funnelEvent(!self.props.funnelActive);
                    self.setState({models: self.state.models.concat(moreModels)});
                }
                if(data) {
                    this.props.rebaseData('models', data);
                }
            })
            .then(() => {
                if(!self.props.models.items.length) {
                    let infiniteScroll = document.querySelector('.card-content.models .infinite-scroll-component');
                    let noMore = document.createElement('div');
                    noMore.className = "no-more-elements";
                    noMore.innerHTML = 'Yay! You have seen it all';
                    infiniteScroll.appendChild(noMore);
                    setTimeout(function(){
                        noMore.remove();
                    }, 5000);
                } else {
                    self.setState({modelCount: self.state.modelCount + 1})
                }
            })
    }

    componentWillReceiveProps(newProps) {
        if(this.props.funnelActive == false) {
            return false;
        } else {
            this.setState({
                items: newProps.items.items,
                models: newProps.models.items,
                itemCount: 1,
                modelCount: 1,
                likedState: [],
            });
        }
    }

    render() {
        if (!!localStorage.getItem('X-Auth-Token') && this.state.mode === 'product' && this.state.items) {
            return (
                <Row>
                    <div
                        className={this.state.mode === 'model' ? 'quantity-loader models' : 'quantity-loader products'}>
                        <a onClick={() => {
                            this.toggleMode();
                            getItems(0)
                                .then((data) => {
                                    if(data) {
                                        this.props.rebaseData('items', data)
                                    }
                                })
                        }}>
                            <IconFont name="exchange" size="2x"/>
                        </a>
                        <p>{this.state.mode === 'model' ? 'Model' : 'Product'}&nbsp;
                            <span className="item-numbers">
                            {this.state.mode === 'model' ? this.props.models.count : this.props.items.count}
                        </span>
                        </p>
                    </div>
                    <div className="card-content products">
                            <InfiniteScroll
                                next={this.generateItems}
                                hasMore={true}
                                scrollThreshold={0.5}
                                height={783}
                                style={this.props.isFunnel ? {height: '65.5vh'} : {height: '80.5vh'}}
                            >
                                {this.state.items.map((item, index) =>
                                    (<Item item={item}
                                           cases={this.props.cases}
                                           activeCase={this.props.activeCase}
                                           likeHandle={this.likeHandle}
                                           updateItem={this.updateItem}
                                           filters={this.props.filters}
                                           activeFilters={this.props.activeFilters}
                                           getTags={this.props.getTags}
                                           getBrands={this.props.getBrands}
                                           types={this.props.types}
                                           categories={this.props.categories}
                                           sortedTags={this.props.sortedTags}
                                           key={index}
                                           rebaseData={this.props.rebaseData}
                                           funnelEvent={this.props.funnelEvent}
                                           toggleProjects={this.props.toggleProjects}
                                           toggleSideBarFull={this.props.toggleSideBarFull}
                                           toggleModal={this.props.toggleModal}
                                    />)
                                )}
                            </InfiniteScroll>
                    </div>
                </Row>
            );
        } else if (!!localStorage.getItem('X-Auth-Token') && this.state.mode === 'model' && this.state.models) {
            return (
                <Row>
                    <div
                        className={this.state.mode === 'model' ? 'quantity-loader models' : 'quantity-loader products'}>
                        <a onClick={() => {
                            this.toggleMode();
                            getModels(0)
                                .then((data) => {
                                    if(data) {
                                        this.props.rebaseData('models', data)
                                    }
                                })
                        }}>
                            <IconFont name="exchange" size="2x"/>
                        </a>
                        <p>{this.state.mode === 'model' ? 'Model' : 'Product'}&nbsp;
                            <span className="item-numbers">
                            {this.state.mode === 'model' ? this.props.models.count : this.props.items.count}
                        </span>
                        </p>
                    </div>

                    <div className="card-content models">
                        <InfiniteScroll
                            next={this.generateModels}
                            hasMore={true}
                            scrollThreshold={0.8}
                            height={783}
                            style={this.props.isFunnel ? {height: '65.5vh'} : {height: '80.5vh'}}
                        >
                            {this.state.models.map((model, index) =>
                                (<Model item={model}
                                        cases={this.props.cases}
                                        activeCase={this.props.activeCase}
                                        likeHandle={this.likeHandle}
                                        updateItem={this.updateItem}
                                        filters={this.props.filters}
                                        activeFilters={this.props.activeFilters}
                                        getTags={this.props.getTags}
                                        getBrands={this.props.getBrands}
                                        types={this.props.types}
                                        categories={this.props.categories}
                                        sortedTags={this.props.sortedTags}
                                        key={index}
                                        rebaseData={this.props.rebaseData}
                                        funnelEvent={this.props.funnelEvent}
                                        toggleProjects={this.props.toggleProjects}
                                        toggleSideBarFull={this.props.toggleSideBarFull}
                                        toggleModal={this.props.toggleModal}
                                />)
                            )}
                        </InfiniteScroll>
                    </div>
                </Row>
            )
        } else {
            return (
                <Row>
                    <div className={this.state.mode === 'model' ? 'quantity-loader models' : 'quantity-loader products'}>
                        <a onClick={this.toggleMode}>
                            <IconFont name="exchange" size="2x" />
                        </a>
                        <p>{this.state.mode === 'model' ? 'Model' : 'Product'}&nbsp;
                            <span className="item-numbers">0</span>
                        </p>
                    </div>
                </Row>
            )
        }
    }
}

export default CardFlip;