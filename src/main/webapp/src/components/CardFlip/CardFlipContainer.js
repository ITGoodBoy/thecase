import {connect} from 'react-redux';

import { addToFolderFunc, rebaseDataFunc, funnelEventFunc, toggleSideBarFullFunc, toggleProjectsFunc, toggleModalFunc } from '../../actions/index';

import CardFlip from './CardFlip';

const mapStateToProps = ({app: {items, itemsLoad, models, modelsLoad, postSidebar, activeCase, isFunnel, funnelActive, filters, activeFilters, types, categories, getBrands, sortedTags, getTags, isSidebarFull, isProjects, cases, modalWindow}}, ownProps) => {
    return {
        activeCase,
        items,
        itemsLoad,
        models,
        modelsLoad,
        postSidebar,
        isFunnel,
        funnelActive,
        filters,
        activeFilters,
        types,
        categories,
        getBrands,
        sortedTags,
        getTags,
        cases,
        isSidebarFull,
        isProjects,
        modalWindow,
    ...ownProps,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addToFolder(type, value) {
            addToFolderFunc(type, value)(dispatch);
        },
        rebaseData (key, value) {
            rebaseDataFunc(key, value)(dispatch);
        },
        funnelEvent (property) {
            funnelEventFunc(property)(dispatch);
        },
        toggleSideBarFull (property) {
            toggleSideBarFullFunc(property)(dispatch);
        },
        toggleProjects (property) {
            toggleProjectsFunc(property)(dispatch);
        },
        toggleModal (property) {
            toggleModalFunc(property)(dispatch);
        },
    }
};

const CardFlipContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CardFlip);

export default CardFlipContainer;