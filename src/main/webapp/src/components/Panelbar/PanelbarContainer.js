import {connect} from 'react-redux';

import {rebaseDataFunc, addToFolderFunc, toggleSideBarFunc, toggleSideBarFullFunc, toggleProjectsFunc, toggleFunnelFunc, funnelEventFunc } from '../../actions/index';
import Panelbar from './Panelbar';

const mapStateToProps = ({app: {isSidebar, isSidebarFull, isProjects, isFunnel, funnelActive, funnelData, activeCase, funnels, items, itemsLoad, filters, activeFilters}}, ownProps) => {
  return {
      isSidebar,
      isSidebarFull,
      isProjects,
      isFunnel,
      funnelActive,
      funnelData,
      activeCase,
      funnels,
      items,
      itemsLoad,
      filters,
      activeFilters,
      ...ownProps
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
      rebaseData (key, value) {
          rebaseDataFunc(key, value)(dispatch);
      },
      addToFolder(type, value) {
          addToFolderFunc(type, value)(dispatch);
      },
      toggleSideBar (property) {
          toggleSideBarFunc(property)(dispatch);
      },
      toggleSideBarFull (property) {
          toggleSideBarFullFunc(property)(dispatch);
      },
      toggleProjects (property) {
          toggleProjectsFunc(property)(dispatch);
      },
      toggleFunnel (property) {
          toggleFunnelFunc(property)(dispatch);
      },
      funnelEvent (property) {
          funnelEventFunc(property)(dispatch);
      },
  }
};

const PanelbarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Panelbar);

export default PanelbarContainer;