import React, { Component } from 'react';
import { Row, Col, NavItem, Dropdown, Button, Input } from 'react-materialize';
import CircularProgressbar from 'react-circular-progressbar';

import NavbarCustom from '../NavbarCustom/NavbarCustom';
import IconFont from '../IconFont/IconFont';
import './Panelbar.css'
import { focusCases, getFunnels, funnelsSearch, getItems, getModels, deactivateFunnels, getFilters, getActiveFilters, filterEvent, deactivateFilter, deactivateFilters } from '../../services/http';

class Panelbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            getNewCases: this.props.activeCase,
            funnel: this.props.funnels,
            funnelStatus: [],
            funnelColor: [],
            funnelData: this.props.funnelData,
        };
        this.funnelRebase = this.funnelRebase.bind(this);
        this.clearSearchResult = this.clearSearchResult.bind(this);
        this.filter = this.filter.bind(this);
        this.deactivateFilter = this.deactivateFilter.bind(this);
        getFilters()
            .then((data) => {
                if(data) {
                    this.props.rebaseData('filters', data);
                }
            });
        getActiveFilters()
            .then((data) => {
                if(data) {
                    this.props.rebaseData('activeFilters', data);
                }
            });
    }

    funnelRebase(i) {
        let self = this;
        const funnelStatus = this.state.funnelStatus;
        funnelStatus[i] = !this.state.funnelStatus[i];
        const funnelColor = this.state.funnelColor;
        funnelColor[i] = !this.state.funnelColor[i];
        funnelColor[i] = 'grey-text text-lighten-1';
        this.setState({
            funnelStatus: funnelStatus,
            funnelMaterialColor: funnelColor,
        });
        getItems()
            .then(data => {
                if(data) {
                    setTimeout(function () {
                        funnelColor[i] = 'yellow-text text-darken-3';
                        funnelStatus[i] = 100;
                        self.props.rebaseData('items', data);
                    }, 300)
                }
            });
        getModels()
            .then(data => {
                if(data) {
                    setTimeout(function () {
                        funnelColor[i] = 'yellow-text text-darken-3';
                        funnelStatus[i] = 100;
                        self.props.rebaseData('models', data);
                    }, 300)
                }
            })
            .then(() => {
                let scrollElement = document.querySelector('.infinite-scroll-component');
                scrollElement.scrollTop = 0;
            })
    }
    clearSearchResult() {
        let self = this;
        let defaultFunnelStatus = [];
        let defaultFunnelColor = [];
        for(let i = 0; i < this.props.funnels.length; i++){
            defaultFunnelStatus[i] = 0;
            defaultFunnelColor[i] = 'grey-text text-lighten-1';
        }
        this.setState({
            funnelStatus: defaultFunnelStatus,
            funnelColor: defaultFunnelColor,
        });
        deactivateFunnels()
            .then(() => {
                this.props.rebaseData('funnelData', [])
            })
            .then(() => {
                getItems()
                    .then(data => {
                        if(data) {
                            setTimeout(function () {
                                self.props.rebaseData('items', data);
                            }, 300)
                        }
                    });
                getModels()
                    .then(data => {
                        if(data) {
                            setTimeout(function () {
                                self.props.rebaseData('models', data);
                            }, 300)
                        }
                    });
            })
            .then(() => {
                let scrollElement = document.querySelector('.infinite-scroll-component');
                scrollElement.scrollTop = 0;
            })
    }
    filter(filter, name) {
        let f = filter;
        let n = name.toString();
        let m;
        if(Object.keys(this.props.activeFilters).map((key) => this.props.activeFilters[key].map((el) => el)).toString().indexOf(n) > -1) {
            m = 'delete';
        } else {
            m = 'post';
        }
        filterEvent(f, n, m)
            .then(() => {
                getItems()
                    .then((data) => {
                        this.props.rebaseData('items', data)
                    });
                getModels()
                    .then((data) => {
                        this.props.rebaseData('models', data)
                    })
            })
            .then(() => {
                let scrollElement = document.querySelector('.infinite-scroll-component');
                scrollElement.scrollTop = 0;
            })
            .then(() => {
                getFilters()
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('filters', data);
                        }
                    });
                getActiveFilters()
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('activeFilters', data);
                        }
                    });
            })
            .then(() => {
                if(this.props.funnelActive == false) {
                    this.props.funnelEvent(!this.props.funnelActive);
                } else {
                    return false;
                }
            })
    }
    deactivateFilter(filter) {
        deactivateFilter(filter)
            .then(() => {
                getItems()
                    .then((data) => {
                        this.props.rebaseData('items', data)
                    });
                getModels()
                    .then((data) => {
                        this.props.rebaseData('models', data)
                    })
            })
            .then(() => {
                let scrollElement = document.querySelector('.infinite-scroll-component');
                scrollElement.scrollTop = 0;
            })
            .then(() => {
                getFilters()
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('filters', data);
                        }
                    });
                getActiveFilters()
                    .then((data) => {
                        if(data) {
                            this.props.rebaseData('activeFilters', data);
                        }
                    });
            })
            .then(() => {
                if(this.props.funnelActive == false) {
                    this.props.funnelEvent(!this.props.funnelActive);
                } else {
                    return false;
                }
            })
    }
    componentWillMount() {
        getFunnels()
            .then(funnel => {
                let defaultFunnelStatus = [];
                let defaultFunnelColor = [];
                for(let i = 0; i < this.props.funnels.length; i++){
                    defaultFunnelStatus[i] = 0;
                    defaultFunnelColor[i] = 'grey-text text-lighten-1';
                }
                this.setState({
                    funnel,
                    funnelStatus: defaultFunnelStatus,
                    funnelColor: defaultFunnelColor,
                });
            })
    }
    componentWillReceiveProps() {
        this.setState({
            funnelData: this.props.funnelData
        })
    }

    render() {
        let iconCase, iconCard, myProjectSlice, funnelData, funnelInfo;
        const options = {
            constrainWidth: false,
            belowOrigin: true
        };
        let myProject = this.props.isProjects ? '' : 'My project - ' + this.props.activeCase.name;

        if(!this.props.isSidebar && !this.props.isSidebarFull) {
            iconCase = 'case-icon caseicon-closed';
            iconCard = 'cardflip-icon cardflipicon-closed';
        }
        if (!!this.props.isSidebar && !this.props.isSidebarFull) {
            iconCase = 'case-icon caseicon-small-opened';
            iconCard = 'cardflip-icon cardflipicon-small-opened';
        }
        if (!!this.props.isSidebar && !!this.props.isSidebarFull) {
            iconCase = 'case-icon caseicon-small-opened hide';
            iconCard = 'cardflip-icon cardflipicon-full-opened';
        }
        if (!this.props.isSidebar && !!this.props.isSidebarFull) {
            iconCase = 'case-icon caseicon-closed hide';
            iconCard = 'cardflip-icon cardflipicon-full-opened';
        }
        if (!!this.props.isProjects && !!this.props.isSidebarFull) {
            iconCard = 'cardflip-icon cardflipicon-full-opened hide';
        }

        if(myProject.length < 60) {
            myProjectSlice = myProject;
        } else {
            myProjectSlice = myProject.slice(0, 60) + '...';
        }
        if(this.state.funnelData == null || this.state.funnelData.length == 0) {
            funnelInfo = (
                <div className="funnel-info" style={{'display': 'block'}}>
                </div>
            );
        } else {
            funnelInfo = (
                this.state.funnelData.map((funnel, index) => {
                    return (
                        <div key={index} className="funnel-info" style={{'display': 'block'}}>
                            <span>{funnel.name}:</span>
                            {funnel.currentCombination.map((value, ind) => {
                                return (
                                    <span key={ind}>{value}</span>
                                )
                            })}
                        </div>
                    )
                })
            )
        }

        if(this.state.funnel == null || this.state.funnel.length == 0) {
            funnelData = (
                <Row className={this.props.isFunnel ? 'funnels-container' : 'hide'} style={{padding: '50px 0'}}>
                    <IconFont size="2x" name='times' className='deactivate-funnel' click={() => {
                        this.clearSearchResult();
                        if(this.props.funnelActive == false) {
                            this.props.funnelEvent(!this.props.funnelActive);
                        } else {
                            return false;
                        }
                    }} />
                </Row>
            );
        } else {
            funnelData = (
                <Row className={this.props.isFunnel ? 'funnels-container' : 'hide'}>
                    <Col className="funnels" s={this.state.funnel.length}>
                        {this.state.funnel.map((key, index) => {
                            return (
                                <div key={key.id} className='funnel-element' onClick={() => {
                                    funnelsSearch(key.id)
                                        .then((data) => {
                                            this.funnelRebase(index);
                                            this.props.rebaseData('funnelData', data);
                                        });
                                    if(this.props.funnelActive == false) {
                                        this.props.funnelEvent(!this.props.funnelActive);
                                    } else {
                                        return false;
                                    }
                                }}>
                                    <CircularProgressbar
                                        percentage={this.state.funnelStatus[index]}
                                        strokeWidth={3}
                                        textForPercentage={(pct) => ``}
                                    />
                                    <IconFont size="2x" name={key.icon} className={'funnel-icon ' + this.state.funnelColor[index]}/>
                                    <span className="funnel-name">{key.name}</span>
                                </div>
                            )
                        })}
                    </Col>
                    <Col s={12 - this.state.funnel.length} className='search-result'>
                        {funnelInfo}
                    </Col>
                    <IconFont size="2x" name='times' className='deactivate-funnel' click={() => {
                        this.clearSearchResult();
                        if(this.props.funnelActive == false) {
                            this.props.funnelEvent(!this.props.funnelActive);
                        } else {
                            return false;
                        }
                    }} />
                </Row>
            )
        }

        return(
            <div>
                <NavbarCustom className="grey lighten-4 menu-bar">
                    <NavItem className={!this.props.isSidebarFull ? !this.props.isSidebar ? 'hide' : 'yellow-text text-darken-3' : 'yellow-text text-darken-3'}>
                        <IconFont size="2x" name={this.props.isProjects ? 'level-down' : 'level-up'} className={!this.props.isSidebarFull ? 'hide' : this.props.isProjects ? 'yellow-text text-darken-3' : 'grey-text text-lighten-1'} click={() => {
                            this.props.toggleProjects(!this.props.isProjects);
                            focusCases()
                                .then(getNewCases => {
                                    this.setState({getNewCases});
                                });
                        }} />
                        <span className={this.props.isProjects ? 'hide' : 'active-project myactive-project'}>{myProjectSlice}</span>
                    </NavItem>
                    <NavItem onClick={() => {this.props.toggleSideBar(!this.props.isSidebar);}} className={iconCase}>
                      <IconFont size="2x" name={!this.props.isSidebar ? 'angle-right' : 'angle-left'} className={!this.props.isSidebar ? 'grey-text text-lighten-1' : 'yellow-text text-darken-3'}/>
                    </NavItem>

                    <NavItem onClick={() => {this.props.toggleSideBarFull(!this.props.isSidebarFull);}} className={iconCard}>
                      <IconFont size="2x" name={!this.props.isSidebarFull ? 'angle-double-right' : 'angle-double-left'} className={!this.props.isSidebarFull ? 'grey-text text-lighten-1' : 'yellow-text text-darken-3'}/>
                    </NavItem>

                    <NavItem onClick={() => {this.props.toggleFunnel(!this.props.isFunnel);}} className={this.props.isSidebarFull ? 'hide' : 'right'} >
                        <IconFont size="2x" name="filter" className={this.props.isFunnel ? 'yellow-text text-darken-3' : 'grey-text text-lighten-1'}/>
                    </NavItem>

                    <NavItem onClick={() => {
                        deactivateFilters()
                            .then(() => {
                                getItems()
                                    .then((data) => {
                                        this.props.rebaseData('items', data)
                                    });
                                getModels()
                                    .then((data) => {
                                        this.props.rebaseData('models', data)
                                    })
                            })
                            .then(() => {
                                if(this.props.funnelActive == false) {
                                    this.props.funnelEvent(!this.props.funnelActive);
                                } else {
                                    return false;
                                }
                            })
                            .then(() => {
                                let scrollElement = document.querySelector('.infinite-scroll-component');
                                scrollElement.scrollTop = 0;
                            })
                            .then(() => {
                                getFilters()
                                    .then((data) => {
                                        if(data) {
                                            this.props.rebaseData('filters', data);
                                        }
                                    });
                                getActiveFilters()
                                    .then((data) => {
                                        if(data) {
                                            this.props.rebaseData('activeFilters', data);
                                        }
                                    });
                            })
                    }} className={this.props.isSidebarFull ? 'hide' : 'right'} >
                      <IconFont size="2x" name="times" className='grey-text text-lighten-1'/>
                    </NavItem>
                    {Object.keys(this.props.filters).map((key, id) => {
                            return (
                                <li className={!this.props.isSidebar ? this.props.isSidebarFull ? 'hide' : 'filters-el left' : this.props.isSidebarFull ? 'hide' : 'filters-el filters-pos left'} key={id}>
                                    <Dropdown trigger={
                                        <Button flat>{key}</Button>
                                    } options={options}>
                                        {Object.entries(this.props.filters[key].content).map((str, ind) => {
                                            return (
                                                <NavItem key={ind} style={str[1] ? {background: '#f2f2f2'} : {background: '#fff'}}>
                                                    <Input name={key} type='checkbox' label={str[0]} value={str[0]} checked={str[1] ? 'checked' : false} onClick={(event) => this.filter(key, event.target.value)}/>
                                                </NavItem>
                                            )
                                        })}
                                    </Dropdown>
                                    <IconFont className={this.props.filters[key].used ? 'green-text text-lighten-1' : ''} name="times" click={() => {
                                        this.deactivateFilter(key)
                                    }}/>
                                </li>
                            )
                    })}
                    <NavItem onClick={() => {
                        this.props.toggleProjects(!this.props.isProjects);
                        this.props.toggleSideBarFull(!this.props.isSidebarFull);
                    }} className={!this.props.isProjects ? 'hide' : 'right'}>
                        <IconFont size="2x" name='angle-double-up' className='grey-text text-lighten-1' />
                    </NavItem>
                </NavbarCustom>
                {funnelData}
            </div>
        )
    }
}

export default Panelbar;
