import update from 'immutability-helper';

import { toggleSideBar, toggleModal, toggleSideBarFull, toggleProjects, toggleFunnel, funnelEvent, toggleList, rebaseData, addToFolder } from '../config/actionEvents';

const defaultState = {
    funnelActive: true,
    isList: false,
    toggleFunnel: false,
    isSidebarFull: false,
    isProjects: false,
    isSidebar: false,
    modalWindow: false,
    baseImageUrl: 'http://185.86.77.92:8080',
    items: [],
    itemsLoad: [],
    models: [],
    modelsLoad: [],
    allElements: [],
    allElementsLoad: [],
    getTags: [],
    sortedTags: [],
    types: [],
    categories: [],
    getBrands: [],
    users: [],
    funnelData: [],
    activeCase: {
        products: [],
        models3D: []
    },
    cases: [],
    selectedItems: [],
    selectedModels: [],
    funnels: [],
    commandUsersList: [],
    commandUserContacts: [],
    getUserFromTeamWork: [],
    token: localStorage.getItem('X-Auth-Token'),
    userPermission: [],
    filters: [],
    activeFilters: [],
};


export default (state = defaultState, action) => {
  switch (action.type) {
    case toggleProjects:
        return update(state, {isProjects: {$set: action.state.isProjects}});
    case toggleSideBarFull:
        return update(state, {isSidebarFull: {$set: action.state.isSidebarFull}});
    case toggleFunnel:
        return update(state, {isFunnel: {$set: action.state.isFunnel}});
    case funnelEvent:
        return update(state, {funnelActive: {$set: action.state.funnelActive}});
    case toggleList:
        return update(state, {isList: {$set: action.state.isList}});
    case rebaseData:
        return update(state, {[action.state.key]: {$set: action.state.value}});
    case toggleSideBar:
        return update(state, {isSidebar: {$set: action.state.isSidebar}});
    case toggleModal:
        return update(state, {modalWindow: {$set: action.state.modalWindow}});
    case addToFolder:
        if (action.state.type === 'item') {
            return update(state, {selectedItems: {$set: [action.state.element]}});
        } else if (action.state.type === 'model') {
            return update(state, {selectedModels: {$set: [action.state.element]}});
        }
      default:
          return state;
  }
}